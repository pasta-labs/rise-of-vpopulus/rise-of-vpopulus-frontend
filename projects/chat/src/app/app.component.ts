import { Component } from '@angular/core';
import { createClient } from 'graphql-ws';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ChatService } from './chat.service';

@Component({
  selector: 'app-chat-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.less'
})
export class ChatAppComponent {
  currentChannel = undefined;

  title = 'chat';

  subscription;

  constructor(
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.listenForNewMessages();
    this.chatService.refreshSubscription.subscribe(() => {
      this.subscription.unsubscribe();
      this.listenForNewMessages();
    })
  }



  listenForNewMessages = () => {
    console.log('new message');

    const client = createClient({
      url: environment.apiUrl.replace('https://', 'wss://') + '/graphql/chat'
    });

    function toObservable(operation) {
      return new Observable((observer) =>
        client.subscribe(operation, {
          next: (data) => observer.next(data),
          error: (err) => observer.error(err),
          complete: () => observer.complete(),
        }),
      );
    }

    const observable = toObservable({
      query: `subscription MessageAdded {
                newMessage(token: "${localStorage.getItem(`jwtToken`)}") {
                  id
                  chatGroupId
                  content
                  createdOn
                  createdBy { id name }
                }
              }`});

    this.subscription = observable.subscribe({
      next: (data: any) => {
        console.log('sub?', data, group);
        const item = data.data.newMessage;

        var group = this.chatService.publicGroups.find(x => x.id === item.chatGroupId);
        if (group) this.onNewMessage('public', group, item);

        if (!group) {
          group = this.chatService.privateGroups.find(x => x.id === item.chatGroupId);
          if (group) this.onNewMessage('private', group, item);
        }

        if (!group) {
          group = this.chatService.sharedGroups.find(x => x.id === item.chatGroupId);
          if (group) this.onNewMessage('shared', group, item);
        }
      },
    });
  }

  onNewMessage = (type, group, item) => {
    if (this.chatService.currentGroup.id !== group.id)
      this.chatService.updateGroup(type, group.id, { ...group, hasNew: true });
    else
      this.chatService.onMessagesUpdate.emit([item, ...this.chatService.messages]);
  }
}
