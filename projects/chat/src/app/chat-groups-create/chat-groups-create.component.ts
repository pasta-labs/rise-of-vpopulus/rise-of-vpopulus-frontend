import { Component } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ChatService } from '../chat.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { Subject, debounceTime, forkJoin } from 'rxjs';
import { forEach } from 'lodash-es';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-chat-groups-create',
  templateUrl: './chat-groups-create.component.html',
  styleUrl: './chat-groups-create.component.less'
})
export class ChatGroupsCreateComponent {
  isLoading = false;
  isLoadingUsers = false;

  searchChange$ = new Subject();
  optionsList = [];

  selectedUsers = [];

  form: FormGroup;

  constructor(
    private graphQlService: GraphQlService,
    private chatService: ChatService,
    private messageService: NzMessageService,
    private modalRef: NzModalRef,
    private httpService: GenericHttpService,
    private translateService: TranslateService,

  ) { 
    this.form = new FormGroup({
      targetEntityId: new FormControl(null, [Validators.required]),
      content: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit(): void {
    this.searchChange$.pipe(debounceTime(500)).subscribe((r: string) => {
      if (!r || r.length < 3) return;
      this.isLoadingUsers = true;
      forkJoin([
        this.httpService.get(`/api/entities/citizens/search?name=${r}`),
      ]).subscribe((searchResult) => {
        this.optionsList = searchResult[0].map((x) => {
          return { id: x.id, name: x.name };
        });
        this.isLoadingUsers = false;
      });
    });
  }

  onSearch(value: string): void {
    this.isLoadingUsers = true;
    this.searchChange$.next(value);
  }

  onSelect(data) {
    forEach(data, (item) => {
      if (
        this.optionsList.find((x) => x.id === item) &&
        !this.selectedUsers.find((x) => x.id === item)
      )
        this.selectedUsers.push(this.optionsList.find((x) => x.id === item));
    });
  }

  isSelected(id) {
    if(!this.form.get(`targetEntityId`).value) return false;
    return this.form.get(`targetEntityId`).value.includes(id);
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  createNewGroup = () => {
    console.log('form', this.form)
    if(this.form.status === "INVALID") return;

    this.isLoading = true;
    this.graphQlService.post(`chat`, `mutation mutate($data: ChatGroupCreatePrivateCommand) { chatGroupCreate(data: $data) }`, {
      data: this.form.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.success(this.translateService.instant('Social.Community.newMessageSent'));
        this.modalRef.close(r.data.chatGroupCreate);
      }
      this.isLoading = false;
    });
  }
}
