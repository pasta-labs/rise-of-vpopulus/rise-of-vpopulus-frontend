import { Component } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ChatService } from '../chat.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ChatGroupsCreateComponent } from '../chat-groups-create/chat-groups-create.component';
import { createClient } from 'graphql-ws';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-chat-groups',
  templateUrl: './chat-groups.component.html',
  styleUrl: './chat-groups.component.less'
})
export class ChatGroupsComponent {
  isLoading = false;

  publicGroups = undefined;
  privateGroups = undefined;
  sharedGroups = undefined;

  currentGroup;

  newGroupId;

  constructor(
    private graphQlService: GraphQlService,
    private chatService: ChatService,
    private modalService: NzModalService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.listenForNewGroup();
    this.load();

    this.chatService.onPublicGroupsUpdated.subscribe(r => this.publicGroups = r);
    this.chatService.onPrivateGroupsUpdated.subscribe(r => this.privateGroups = r);
    this.chatService.onSharedGroupsUpdated.subscribe(r => this.sharedGroups = r);
  }

  load = () => {
    this.graphQlService.get(`chat`, `{ getPublicGroups { id name hasNew }  }`)
      .subscribe((r: any) => {
        var groups = r.data.getPublicGroups;
        if (!this.currentGroup)
          this.chatService.onCurrentGroupChange.emit(groups[0]);

        this.chatService.onPublicGroupsUpdated.emit(groups);
      })

    this.graphQlService.get(`chat`, `{ getSharedGroups { id name hasNew }  }`)
      .subscribe((r: any) => {
        var groups = r.data.getSharedGroups;
        this.chatService.onSharedGroupsUpdated.emit(groups);
      })

    this.currentGroup = this.chatService.currentGroup;
    this.chatService.onCurrentGroupChange.emit(r => this.currentGroup = r);
    this.graphQlService.get(`chat`, `{ getPrivateGroups { id name hasNew } }`)
      .subscribe((r: any) => {
        var groups = r.data.getPrivateGroups;

        if (this.newGroupId) {
          this.chatService.onCurrentGroupChange.emit(groups.find(x => x.id === this.newGroupId));
          this.newGroupId = undefined;
        }
        this.chatService.onPrivateGroupsUpdated.emit(groups);
      })
  }

  selectGroup = (item) => {
    item.hasNew = false;
    this.chatService.onCurrentGroupChange.emit(item);
  }

  createNewGroup = () => {
    const modal = this.modalService.create({
      nzContent: ChatGroupsCreateComponent,
      nzTitle: this.translateService.instant('Social.Community.createNewGroup'),
      nzFooter: null,
      nzWidth: '70%'
    });
    modal.afterClose.subscribe(r => {
      if (r) {
        this.newGroupId = r;
        this.load();
      }
    })
  }

  listenForNewGroup = () => {
    const client = createClient({
      url: environment.apiUrl.replace('https://', 'wss://') + '/graphql/chat'
    });

    function toObservable(operation) {
      return new Observable((observer) =>
        client.subscribe(operation, {
          next: (data) => observer.next(data),
          error: (err) => observer.error(err),
          complete: () => observer.complete(),
        }),
      );
    }

    const observable = toObservable({
      query: `subscription MessageAdded {
            newGroup(token: "${localStorage.getItem(`jwtToken`)}") {
                  chatGroupId
                }
              }`});

    const subscription = observable.subscribe({
      next: () => {
        this.load();
        this.chatService.refreshSubscription.emit(new Date().getTime());
      },
    });
  }
}
