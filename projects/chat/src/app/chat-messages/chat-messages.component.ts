import { Component, ViewEncapsulation } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ChatService } from '../chat.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { minBy } from 'lodash-es';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-chat-messages',
  templateUrl: './chat-messages.component.html',
  styleUrl: './chat-messages.component.less',
  encapsulation: ViewEncapsulation.None
})
export class ChatMessagesComponent {
  isLoading = false;

  list = undefined;

  newMessageContent = undefined;

  currentGroup;

  hasMore = false;

  constructor(
    private graphQlService: GraphQlService,
    private chatService: ChatService,
    private messageService: NzMessageService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.chatService.onMessagesUpdate.subscribe(r => this.list = r);

    this.currentGroup = this.chatService.currentGroup;
    this.getMessages();

    this.chatService.onCurrentGroupChange.subscribe(r => {
      this.currentGroup = r;
      this.getMessages();
    });
  }

  getMessages = (loadMore = false) => {
    if (!this.currentGroup || !this.currentGroup.id) return;
    this.isLoading = true;

    var startPosition = null;
    if (loadMore) 
      startPosition = minBy(this.list, x => x.uniqueId)?.uniqueId

    this.graphQlService.get(`chat`, `{ chatMessages(groupId: "${this.currentGroup.id}", startPosition: ${startPosition}) { id content createdOn uniqueId createdBy { id name avatar } }  }`).subscribe((r: any) => {
      var list = r.data.chatMessages;
      this.chatService.onMessagesUpdate.emit(loadMore === false ? list : [...this.list, ...list]);
      this.hasMore = list.length == 10 ? true : false;
      this.isLoading = false;
    })
  }

  sendMessage = () => {
    this.isLoading = true;
    this.graphQlService.post(`chat`, `mutation mutate($data: ChatMessageCreateCommand) { chatMessageCreate(data: $data) }`, {
      data: {
        groupId: this.currentGroup.id,
        content: this.newMessageContent
      }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else this.messageService.success(this.translateService.instant('Social.Community.commentSent'));
      this.getMessages();
      this.isLoading = false;
      this.newMessageContent = undefined;
    });
  }

}
