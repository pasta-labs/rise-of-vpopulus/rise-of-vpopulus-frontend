import { Routes } from '@angular/router';
import { ChatAppComponent } from './app.component';

export const routes: Routes = [
    {
        path: '',
        component: ChatAppComponent
    },
];
