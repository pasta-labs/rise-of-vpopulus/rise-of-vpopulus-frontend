import { Component } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-chat-memebers',
  templateUrl: './chat-memebers.component.html',
  styleUrl: './chat-memebers.component.less'
})
export class ChatMemebersComponent {
  isLoading = false;

  list = [];
  statuses = { 'online': 0, 'away': 0, 'offline': 0 }

  constructor(
    private graphQlService: GraphQlService,
    private chatService: ChatService,
    private messageService: NzMessageService
  ) { }

  ngOnInit(): void {
    this.load();
    this.chatService.onCurrentGroupChange.subscribe(() => this.load());
  }

  load = () => {
    if (!this.chatService.currentGroup || !this.chatService.currentGroup.id) return;

    this.isLoading = true;
    this.graphQlService.get(`chat`, `{ chatMembers(groupId: "${this.chatService.currentGroup.id}") { entity { id name } status }  }`).subscribe((r: any) => {
      this.list = r.data.chatMembers;
      this.statuses.online = this.list.filter(x => x.status === 'online').length;
      this.statuses.away = this.list.filter(x => x.status === 'away').length;
      this.statuses.offline = this.list.filter(x => x.status === 'offline').length;
      this.isLoading = false;
    })
  }

}
