import { NgModule } from '@angular/core';
import { ChatMessagesComponent } from './chat-messages/chat-messages.component';
import { ChatGroupsComponent } from './chat-groups/chat-groups.component';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
import { ChatAppComponent } from './app.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { ChatMemebersComponent } from './chat-memebers/chat-memebers.component';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ChatGroupsCreateComponent } from './chat-groups-create/chat-groups-create.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { CustomI18NLoader } from 'src/app/shared/others/custom-i18n-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        ChatAppComponent,
        ChatMessagesComponent,
        ChatGroupsComponent,
        ChatMemebersComponent, 
        ChatGroupsCreateComponent
    ],
    imports: [
        RouterOutlet, BrowserModule, CommonModule, ReactiveFormsModule, FormsModule,
        NzGridModule, NzCardModule, NzDividerModule, NzListModule, NzIconModule, NzTagModule, NzCommentModule, NzAvatarModule,
        NzButtonModule, NzInputModule, NzMessageModule, NzModalModule, NzFormModule, NzSelectModule, NzSpinModule, NzBadgeModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useClass: CustomI18NLoader,
              deps: [HttpClient],
            },
          }),
    ]
})
export class ChatModule { }
