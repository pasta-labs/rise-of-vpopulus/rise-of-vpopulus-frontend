import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class ChatService {
    onCurrentGroupChange = new EventEmitter<any>();
    currentGroup: any;

    publicGroups = [];
    onPublicGroupsUpdated = new EventEmitter<any>();

    privateGroups = [];
    onPrivateGroupsUpdated = new EventEmitter<any>();

    sharedGroups = [];
    onSharedGroupsUpdated = new EventEmitter<any>();

    messages = [];
    onMessagesUpdate = new EventEmitter<any>();

    refreshSubscription = new EventEmitter<any>();

    constructor() {
        this.onCurrentGroupChange.subscribe(r => this.currentGroup = r);
        this.onPublicGroupsUpdated.subscribe(r => this.publicGroups = r);
        this.onPrivateGroupsUpdated.subscribe(r => this.privateGroups = r);
        this.onSharedGroupsUpdated.subscribe(r => this.sharedGroups = r);
        this.onMessagesUpdate.subscribe(r => this.messages = r);
    }

    updateGroup = (type, id, newValue) => {
        if (type === "public") {
            var index = this.publicGroups.findIndex(x => x.id === id);
            this.publicGroups[index] = newValue;
            this.onPublicGroupsUpdated.emit(this.publicGroups);
        } else if (type === "private") {
            var index = this.privateGroups.findIndex(x => x.id === id);
            this.privateGroups[index] = newValue;
            this.onPrivateGroupsUpdated.emit(this.privateGroups);
        } else if (type === "shared") {
            var index = this.sharedGroups.findIndex(x => x.id === id);
            this.sharedGroups[index] = newValue;
            this.onSharedGroupsUpdated.emit(this.sharedGroups);
        }
    }
}
