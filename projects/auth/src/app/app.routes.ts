import { Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { ResetPasswordBackgroundComponent } from './reset-password/reset-password-background/reset-password-background.component';
import { RecoverAccountBackgroundComponent } from './recover-account/recover-account-background/recover-account-background.component';
import { OAuthPastalabsComponent } from './oauth/pastalabs/pastalabs.component';

export const routes: Routes = [
  {
    path: '',
    component: LandingComponent
  },
  {
    path: 'login/:jwtToken',
    component: LandingComponent
  },
  {
    path: 'reset-password/:code',
    component: ResetPasswordBackgroundComponent
  },
  {
    path: 'recover-account',
    component: RecoverAccountBackgroundComponent
  },
  {
    path: 'oauth/pastalabs',
    component: OAuthPastalabsComponent
  },



];
