import { Component } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';
import { LoginService } from '../login.service';
import { Browser } from '@capacitor/browser';
import { TranslateService } from '@ngx-translate/core';
// import { Pi } from '@pinetwork-js/sdk';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  isLoading;

  form: UntypedFormGroup;

  loginPasswordAuthVisible = true;
  pastaLabsAuthVisible = true;
  piNetworkAuthVisible = false;

  constructor(
    private loginService: LoginService,
    private messageService: NzMessageService,
    private httpService: GenericHttpService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private graphQlService: GraphQlService

  ) {
    this.form = new UntypedFormGroup({
      email: new UntypedFormControl(null, [Validators.required]),
      password: new UntypedFormControl(null, [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((routeParams) => {
      var jwtToken = routeParams['jwtToken'];

      if (jwtToken) {
        localStorage.setItem('jwtToken', jwtToken);
        this.loginService.login();
      } else {
        var gameVersion = JSON.parse(localStorage.getItem(`gameConfig`)).type;

        if (gameVersion === 'Pi') {
          this.loginPasswordAuthVisible = false;
          this.pastaLabsAuthVisible = false;
          this.piNetworkAuthVisible = true;
        }
      }
    });
  }



  submitLogin() {
    this.isLoading = true;
    this.httpService.post(`/api/auth/login`, this.form.getRawValue()).subscribe(
      (r) => {
        localStorage.setItem('jwtToken', r.jwtToken);
        this.loginService.login();
      },
      (error) => {
        /*  this.messageService.error(JSON.stringify(error)); */

        if (error.status === 0) this.messageService.error(error.statusText);
        this.messageService.error(this.translateService.instant('Home.' + error.error.Message));
        this.isLoading = false;
      }
    );
  }

  loginWithFacebook() {
    window.open(`${environment.apiUrl}/api/auth/facebook/start`);
    window.focus();
  }

  loginWithPastaLabs = async () => {
    await Browser.open({ url: `${environment.apiUrl}/api/auth/pastalabs/start` });
  }

  loginWithPiNetwork = async () => {
    this.messageService.success(`Pi network auth initialized...`);
    // console.log('r1-a', JSON.stringify(window['Pi']))
    window['Pi'].authenticate(['username'], (payments) => { })
      .then((auth) => {
        // this.messageService.success(`auth r2? ok`);
        // console.log('auth r2? ok')

        this.httpService.get(`/api/auth/pinetwork/complete?accessToken=${auth.accessToken}`)
          .subscribe(r => {
            // console.log('jwtToken', r.token)
            localStorage.setItem('jwtToken', r.token);

            this.loginService.login();
          });
      })
      .catch((error) => {
        this.messageService.error(`error: ${JSON.stringify(error)}`)
        console.error(error);
      });
  }
}
