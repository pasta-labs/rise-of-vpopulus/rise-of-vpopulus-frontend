import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrl: './statistics.component.scss',
  encapsulation: ViewEncapsulation.None,
})
export class StatisticsComponent {
  isLoading = false;
  topCitizens = [];
  topCountries = [];

  constructor(private graphQlService: GraphQlService) { }

  ngOnInit(): void { 
    this.rankingCitizens();
    this.rankingCountries();
  }

  rankingCitizens() {
    this.isLoading = true;
    this.graphQlService.get('game', `{
      rankingCitizens {
        id
        name
        avatar
        experience
        militaryExperience
        militaryRank
        strength
      }
    }`).subscribe((r: any) => {
      if (r.errors) {
        console.error(r.errors);
        this.isLoading = false;
         console.log(r.errors);
      } else {
        this.topCitizens = r.data.rankingCitizens;
        this.isLoading = false;
        console.log(this.topCitizens);

      }
    });
  }

  rankingCountries() {
    this.isLoading = true;
    this.graphQlService.get('game', `{
      rankingCountries {
        id
        name
        flagLocation
        citizenCount
        winConditionPercentage
        regionsCount
      }
    }`).subscribe((r: any) => {
      if (r.errors) {
        console.error(r.errors);
        console.log(r.errors);

        this.isLoading = false;
      } else {
        this.topCountries = r.data.rankingCountries;
        this.isLoading = false;
      }
    });
  }
}