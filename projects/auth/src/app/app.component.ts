import { Component, HostListener, ViewEncapsulation } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-landing-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class AuthAppComponent {
  landindDrawerWidth = 0;
  @HostListener('window:resize', []) updateDrawerWidth() {
    // lg (for laptops and desktops - screens equal to or greater than 1200px wide)
    // md (for small laptops - screens equal to or greater than 992px wide)
    // sm (for tablets - screens equal to or greater than 768px wide)
    // xs (for phones - screens less than 768px wide)

    if (window.innerWidth >= 1200) {
      this.landindDrawerWidth = 1200; // lg
    } else if (window.innerWidth >= 992) {
      this.landindDrawerWidth = 900; //md
    } else if (window.innerWidth >= 768) {
      this.landindDrawerWidth = 700; //sm
    } else if (window.innerWidth < 768) {
      this.landindDrawerWidth = 300; //xs
    }
  }

  items = [
    // {
    //   row: 1,
    //   centerPositionClass: 'hex4 center-hex',
    //   title: 'About game',
    //   isSelected: true,
    //   component: LandingAboutGameComponent,
    // },
    // {
    //   row: 2,
    //   centerPositionClass: 'hex2 center-hex',
    //   title: 'News',
    //   isSelected: false,
    //   component: LandingNewsComponent,
    // },
    // {
    //   row: 3,
    //   centerPositionClass: 'hex6 center-hex',
    //   title: 'Statistics',
    //   isSelected: false,
    //   component: LandingStatisticsComponent,
    // },
    // {
    //   row: 4,
    //   centerPositionClass: 'hex7 center-hex',
    //   title: 'Map',
    //   isSelected: false,
    //   component: LandingMapComponent,
    // },
    // {
    //   row: 5,
    //   centerPositionClass: 'hex3 center-hex',
    //   title: 'Patch notes',
    //   isSelected: false,
    // },
    // {
    //   row: 6,
    //   centerPositionClass: 'hex5 center-hex',
    //   title: 'Start the game',
    //   isSelected: false,
    // },
  ];
  currentSelectedComponent;
  markerStyle;
  isDrawerVisible = false;

  isLoading = true;

  constructor(
    private router: Router,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.updateDrawerWidth();

    this.isLoading = true;
    this.graphQlService.get(`game`, `{ game { day type } }`).subscribe((gameResponse: any) => {

      localStorage.setItem(`gameConfig`, JSON.stringify(gameResponse.data.game));
      localStorage.setItem(`gameDay`, gameResponse.data.game.day);

      this.isLoading = false;
    })
  }
  onActivate(event) {
    //window.scroll(0,0);

    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
  selectItem(row, component) {
    if (row === 5) window.location.href = 'https://pastalabs.eu/blog';
    if (row === 6) this.router.navigateByUrl(`/login`);

    if (this.currentSelectedComponent == component) {
      this.currentSelectedComponent = false;
      this.markerStyle = {
        transition: 'grid-row 2s',
      };
    } else {
      this.markerStyle = {
        'grid-column': '2',
        'grid-row': `${row} / ${row + 1}`,
        'align-self': 'center',
        'place-self': 'center',
        height: 42,
        width: 42,
        transition: 'all .5s',
      };
      component
        ? (this.currentSelectedComponent = component)
        : (this.currentSelectedComponent = false);
    }
  }

  openDrawer(): void {
    this.isDrawerVisible = true;
  }

  closeDrawer(): void {
    this.isDrawerVisible = false;
  }
}
