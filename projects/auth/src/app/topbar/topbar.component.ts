import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrl: './topbar.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class TopbarComponent {
  showDrawer = false

  gameVersion;

  ngOnInit(): void {
    this.gameVersion = JSON.parse(localStorage.getItem(`gameConfig`)).type;
  }

  scroll(id: string) {
    let el = document.getElementById(id);
    console.log(id);
    el!.scrollIntoView();
  }

  onActivate(event) {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
