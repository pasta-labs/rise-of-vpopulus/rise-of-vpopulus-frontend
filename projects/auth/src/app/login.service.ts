import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  constructor(
    private authService: AuthService,
    private router: Router,
    private messageService: NzMessageService
  ) { }

  login() {
    this.authService.switchTo(false).then(async () => {
      var pathToRedirect = localStorage.getItem(`rov_redirectTo`);
      if (!pathToRedirect) pathToRedirect = `/`;

      this.authService.isLoggedIn.emit(true);

      localStorage.removeItem(`rov_redirectTo`);
      this.router.navigateByUrl(pathToRedirect);
    })
  }

}
