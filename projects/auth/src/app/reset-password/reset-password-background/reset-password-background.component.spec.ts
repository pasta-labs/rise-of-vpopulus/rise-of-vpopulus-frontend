import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPasswordBackgroundComponent } from './reset-password-background.component';

describe('ResetPasswordBackgroundComponent', () => {
  let component: ResetPasswordBackgroundComponent;
  let fixture: ComponentFixture<ResetPasswordBackgroundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ResetPasswordBackgroundComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ResetPasswordBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
