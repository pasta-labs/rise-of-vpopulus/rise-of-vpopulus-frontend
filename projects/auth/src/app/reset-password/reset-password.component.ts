import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrl: './reset-password.component.scss'
})
export class ResetPasswordComponent {
  isLoading;

  form: UntypedFormGroup;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = new UntypedFormGroup({
      code: new UntypedFormControl(null, [Validators.required]),
      password: new UntypedFormControl(null, [Validators.required]),
      password2: new UntypedFormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((r) => {
      this.form.get(`code`).setValue(r['code']);
      this.form.get(`code`).disable();
    });
  }

  submitReset() {
    this.isLoading = true;
    this.httpService
      .post(`/api/auth/resetpassword`, this.form.getRawValue())
      .subscribe(
        (r) => {
          this.messageService.success('Password reset email has been sent!');
          localStorage.setItem(`rov_redirectTo`, ``);
          this.router.navigateByUrl('/');
        },
        (error) => {
          if (error.status === 0) this.messageService.error(error.statusText);
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
