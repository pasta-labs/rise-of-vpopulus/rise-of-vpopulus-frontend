import { Component, ViewEncapsulation } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrl: './landing.component.scss',
  encapsulation: ViewEncapsulation.None,

})
export class LandingComponent {

  gameVersion;
  referral;

  constructor(
    private graphQlService: GraphQlService
  ) {}

  ngOnInit(): void {
    this.gameVersion = JSON.parse(localStorage.getItem(`gameConfig`)).type;

    var refId = localStorage.getItem(`refId`);
    if (refId !== null && refId !== undefined) {
      this.graphQlService.get(`game`, `{ citizenSimple(id: "${refId}") { id name } }`).subscribe((r) => {
        this.referral = r.data['citizenSimple'];
        console.log('referrer', this.referral);
      });
    }
  }


}
