import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoverAccountBackgroundComponent } from './recover-account-background.component';

describe('RecoverAccountBackgroundComponent', () => {
  let component: RecoverAccountBackgroundComponent;
  let fixture: ComponentFixture<RecoverAccountBackgroundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RecoverAccountBackgroundComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RecoverAccountBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
