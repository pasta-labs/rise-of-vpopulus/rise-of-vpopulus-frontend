import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
@Component({
  selector: 'app-recover-account-background',
  templateUrl: './recover-account-background.component.html',
  styleUrl: './recover-account-background.component.scss'
})
export class RecoverAccountBackgroundComponent {
  isLoading;

  form: UntypedFormGroup;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router
  ) {
    this.form = new UntypedFormGroup({
      email: new UntypedFormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {}

  submitRecover() {
    this.isLoading = true;
    this.httpService
      .get(`/api/auth/recover/${this.form.get(`email`).value}`)
      .subscribe(
        (r) => {
          this.messageService.success('Password reset email has been sent!');
          localStorage.setItem(`rov_redirectTo`, ``);
          this.router.navigateByUrl('/');
        },
        (error) => {
          if (error.status === 0) this.messageService.error(error.statusText);
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}