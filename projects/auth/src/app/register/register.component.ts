import { Component } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent {
  isLoading;

  form: UntypedFormGroup;

  referral;

  constructor(
    private messageService: NzMessageService,
    private router: Router,
    private httpService: GenericHttpService
  ) {
    this.form = new UntypedFormGroup({
      username: new UntypedFormControl(null, [Validators.required]),
      password: new UntypedFormControl(null, [Validators.required]),
      password2: new UntypedFormControl(null, [Validators.required]),
      email: new UntypedFormControl(null, [Validators.required]),
    });
  }

  ngOnInit() { }

  submitRegister() {
    var refId = localStorage.getItem(`refId`);
    var form = this.form.getRawValue();
    if (refId !== null && refId !== undefined) form['ReferralId'] = refId;

    this.isLoading = true;
    this.httpService.post(`/api/auth/register`, form).subscribe(
      (r) => {
        this.messageService.success(
          'Registered successfully! Please log or check your email to verify account!'
        );
        this.form.reset();
        localStorage.setItem(`rov_redirectTo`, ``);
      },
      (error) => {
        if (error.error.Message === 'ALREADY_REGISTERED') {
          this.messageService.error(`Account already exists! Please log in.`);
          this.router.navigateByUrl(`/login`);
        } else if (error.error.Message === 'USERNAME_IN_USE')
          this.messageService.error(
            `Username has already been taken! Please consider using different one!`
          );
        else if (error.error.Message === 'REGISTER_DISABLED_ATM')
          this.messageService.error(`Registration is disabled at this moment!`);
        else if (error.error.Message === 'PASTALABS_ACCOUNT_EXISTS') {
          this.messageService.error(
            `This email is already registered on PastaLabs! Please use option login with PastaLabs!`
          );
          this.router.navigateByUrl(`/login`);
        } else this.messageService.error(error.error.Message);

        this.isLoading = false;
      }
    );
  }

  registerWithFacebook() {
    var refId = localStorage.getItem(`refId`);
    if (refId === null || refId === undefined) refId = null;
    window.open(
      `${environment.apiUrl}/api/auth/facebook/start?refId=${refId}`
    );
    window.focus();
  }

  loginWithPastaLabs() {
    window.open(`${environment.apiUrl}/api/auth/pastalabs/start`);
    window.focus();
  }
}
