import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { LoginService } from '../../login.service';
import { environment } from 'src/environments/environment';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-oauth-pastalabs',
  templateUrl: './pastalabs.component.html',
  styleUrl: './pastalabs.component.scss'
})
export class OAuthPastalabsComponent {
  isLoading = false;

  error = undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private messageService: NzMessageService,
    private httpService: GenericHttpService,
    private loginService: LoginService,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(qp => {
      var code = qp['code'];
      var state = qp['state'];

      this.isLoading = true;
      this.httpService.get(`/api/auth/pastalabs/complete?code=${code}&state=${state}`).subscribe(
        (r) => {
          console.log('r', r)
          localStorage.setItem('jwtToken', r.jwtToken);
          this.isLoading = false;
          this.loginService.login();
        },
        (error) => {
          if (error.status === 0) {
            this.messageService.error(error.statusText);
            this.error = error.statusText;
          }
          this.messageService.error(error.error.Message);
          this.error = error.error.Message;
          this.isLoading = false;

          this.router.navigateByUrl(`/landing`);
        }
      );

    })
  }

  retry = async () => {
    await Browser.open({ url: `${environment.apiUrl}/api/auth/pastalabs/start` });
  }


}
