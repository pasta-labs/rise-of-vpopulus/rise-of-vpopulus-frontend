import { RouterModule, RouterOutlet } from "@angular/router";
import { AuthAppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzDividerModule } from "ng-zorro-antd/divider";
import { NzListModule } from "ng-zorro-antd/list";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzSpinModule } from "ng-zorro-antd/spin";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NgModule } from "@angular/core";
import { TopbarComponent } from "./topbar/topbar.component";
import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzDrawerModule } from "ng-zorro-antd/drawer";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NzTabsModule } from "ng-zorro-antd/tabs";
import { LandingComponent } from "./landing/landing.component";
import { NzMenuModule } from "ng-zorro-antd/menu";
import { LoginComponent } from "./login/login.component";
import { NzInputModule } from "ng-zorro-antd/input";
import { RegisterComponent } from "./register/register.component";
import { AboutGameComponent } from "./about-game/about-game.component";
import { NewsComponent } from "./news/news.component";
import { NzSkeletonModule } from "ng-zorro-antd/skeleton";
import { StatisticsComponent } from "./statistics/statistics.component";
import { NzTableModule } from "ng-zorro-antd/table";
import { ResetPasswordBackgroundComponent } from "./reset-password/reset-password-background/reset-password-background.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { RecoverAccountComponent } from "./recover-account/recover-account.component";
import { RecoverAccountBackgroundComponent } from "./recover-account/recover-account-background/recover-account-background.component";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CustomI18NLoader } from "src/app/shared/others/custom-i18n-loader";
import { OAuthPastalabsComponent } from "./oauth/pastalabs/pastalabs.component";
import { NzAlertModule } from "ng-zorro-antd/alert";

@NgModule({
    declarations: [
        AuthAppComponent,
        TopbarComponent,
        LandingComponent,
        LoginComponent,
        RegisterComponent,
        AboutGameComponent,
        StatisticsComponent,
        ResetPasswordBackgroundComponent,
        ResetPasswordComponent,
        NewsComponent,
        RecoverAccountComponent,
        RecoverAccountBackgroundComponent,
        OAuthPastalabsComponent
    ],
    imports: [
        RouterOutlet, BrowserModule, CommonModule, ReactiveFormsModule, FormsModule,
        NzGridModule, NzCardModule, NzDividerModule, NzListModule, NzIconModule,
        NzButtonModule, NzSpinModule, NzLayoutModule, RouterModule, NzDrawerModule, NzAlertModule,
        HttpClientModule, NzTabsModule, NzMenuModule, NzInputModule,
        NzSkeletonModule, NzTableModule,
        TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useClass: CustomI18NLoader,
              deps: [HttpClient],
            },
          }),

    ]
})
export class LandingModule { }
