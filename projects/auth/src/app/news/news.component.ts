import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrl: './news.component.scss'
})
export class NewsComponent {
  isLoading = false;

  list = [];
  events = [];

  constructor(private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ articlesNewest { id title content publishGameDay newspaper { name } } }`)
      .subscribe((r: any) => {
        this.list = r.data.articlesNewest.map(x => ({ ...x, content: x.content.substring(0, 150) + '...' }));
        this.isLoading = false;
      });

    this.graphQlService.get(`game`, `{ worldEvents { id country1 { id name } country2 { id name } type data } }`)
      .subscribe((r: any) => {
        this.events = r.data.worldEvents;
        this.isLoading = false;
        // Log each event type
        this.events.forEach(event => {
          console.log(event.type);
        });
      });

  }
  getText = (item: any): string => {
    if (!item || !item.type) {
      return 'Invalid event data';
    }

    let data;
    try {
      data = typeof item.data === 'string' ? JSON.parse(item.data) : item.data;
    } catch (error) {
      data = {};
    }

    // Remove known prefixes from the type string
    let type = item.type.replace('WorldEvent.Type.', '');
    type = type.replace('Politics.Laws.', '');

    const countryName = data.Country?.Name || 'Unknown Country';
    const regionName = data.Region?.Name || 'Unknown Region';

    switch (type) {
      case "NewBattle":
        return `A new battle has started between ${countryName} and ${regionName}!`;

      case "ChangeFlag":
      case "CitizenFee":
      case "MinimalWage":
      case "PrintCurrency":
      case "CloseBorder":
      case "CreateConstruction":
      case "CreateNationalOrganization":
      case "NewspaperCensorship":
      case "WelcomeMessage":
      case "ReopenBorder":
      case "TaxChange":
      case "TransferLeadership":
      case "WithdrawCurrency":
        switch (data.Status) {
          case "Pending":
            return `Member proposed ${this.translateService.instant(type)} law!`;
          case "Accepted":
            return `${this.translateService.instant('Politics.Laws.Law')} ${this.translateService.instant(type)} ${this.translateService.instant('Politics.Laws.hasAccepted')}`;
          case "Rejected":
            return `${this.translateService.instant('Politics.Laws.Law')} ${this.translateService.instant(type)} ${this.translateService.instant('Politics.Laws.hasRejected')}`;
          default:
            return `Unknown status for ${type}`;
        }

      case "DeclareWar":
        switch (data.Status) {
          case "Pending":
            return `Declare war against ${item.country1?.name || 'Unknown Country'} has been proposed`;
          case "Accepted":
            return `Declare war against ${item.country1?.name || 'Unknown Country'} has been accepted`;
          case "Rejected":
            return `Declare war against ${item.country1?.name || 'Unknown Country'} has been rejected`;
          default:
            return `Unknown status for DeclareWar`;
        }
      case "RegionLost":
        return `Region ${data.Region?.Name || 'Unknown Region'} has been lost!`;

      case "RegionCaptured":
        return `Region ${data.Region?.Name || 'Unknown Region'} has been captured!`;

      default:
        return `Unknown event type: ${type}`;
    }
  }

}