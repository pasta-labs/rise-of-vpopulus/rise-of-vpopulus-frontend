import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'eu.pastalabs.vpopuluslegacy',
  appName: 'vPopulus legacy',
  webDir: 'dist/Frontend'
};

export default config;
