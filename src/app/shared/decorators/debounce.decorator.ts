import { debounce } from 'lodash';

export function Debounce(milliseconds: number = 300, options = {}) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value;
    descriptor.value = debounce(originalMethod, milliseconds, options);
    return descriptor;
  };
}
