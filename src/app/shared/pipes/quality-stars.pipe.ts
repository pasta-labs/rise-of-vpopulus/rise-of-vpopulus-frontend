import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "q",
  pure: false,
})
export class QualityStarsPipe implements PipeTransform {
  transform(value: number): any {
    var result = "";
    for (let i = 0; i < value; i++) result += "&#9733;";
    return result;
  }
}
