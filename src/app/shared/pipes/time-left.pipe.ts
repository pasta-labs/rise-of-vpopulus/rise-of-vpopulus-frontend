import { Pipe, PipeTransform } from "@angular/core";
import { environment } from "src/environments/environment";

@Pipe({
  name: "timeLeft",
  pure: false,
})
export class TimeLeftPipe implements PipeTransform {
  transform(seconds: number): any {
    var minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    var hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return `${this.pad(hours)}:${this.pad(minutes)}:${this.pad(seconds)}`;
  }

  pad(num) {
    return ("0" + num).slice(-2);
  }
}
