import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'gameDay',
  pure: false,
})
export class GameDayPipe implements PipeTransform {
  transform(pDate: Date): any {
    if (!pDate) return '...';
    var d = localStorage.getItem(`gameStartDate`);
    if (!d) return '...';
    var gameStart = new Date(d).getTime();
    var difference = Date.parse(pDate.toString()) - gameStart;
    return `Day ${Math.floor(difference / (1000 * 3600 * 24))}`;
  }
}
