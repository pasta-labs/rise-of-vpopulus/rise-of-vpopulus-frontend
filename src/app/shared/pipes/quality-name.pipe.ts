import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'qualityName',
  pure: false
})
export class QualityNamePipe implements PipeTransform {

  transform(value: number): any {
      switch(value) {
          case 1: return "Low";
          case 2: return "Medium";
          case 3: return "High";
      }
  }

}
