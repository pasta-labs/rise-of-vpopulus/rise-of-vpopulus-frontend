import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';

import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';

import { CountdownConfig, CountdownModule } from 'ngx-countdown';

import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { GameDayPipe } from './pipes/game-day';
import { QualityNamePipe } from './pipes/quality-name.pipe';
import { QualityStarsPipe } from './pipes/quality-stars.pipe';
import { TimeLeftPipe } from './pipes/time-left.pipe';
import { RelativeTimePipe } from './pipes/relative-time';
import { ReportButtonComponent } from './components/report-button/report-button.component';
import { ChartComponent } from './components/chart/chart.component';
import { ItemComponent } from '../game/shared/item/item.component';
import { DraggableDirective } from './directives/draggable.directive';
import { ModalComponent } from './components/modal/modal.component';
import { ModalService } from './components/modal/modal.service';
import { MissionsComponent } from './components/layout/missions/missions.component';
import { ApolloModule } from 'apollo-angular';
import { GraphQLModule } from '../graphql.module';
import { NzFlexModule } from 'ng-zorro-antd/flex';

@NgModule({
  declarations: [
    ChartComponent,
    DraggableDirective,
    GameDayPipe,
    ItemComponent,
    QualityNamePipe,
    QualityStarsPipe,
    RelativeTimePipe,
    ReportButtonComponent,
    TimeLeftPipe,
    ModalComponent,
    MissionsComponent,
  ],
  imports: [
    // ChartsModule,
    CommonModule,
    FormsModule,
    NzAlertModule,
    NzAutocompleteModule,
    NzAvatarModule,
    NzBadgeModule,
    NzBreadCrumbModule,
    NzButtonModule,
    NzCardModule,
    NzCheckboxModule,
    NzCommentModule,
    NzDatePickerModule,
    NzDividerModule,
    NzDrawerModule,
    NzDropDownModule,
    NzEmptyModule,
    NzGridModule,
    NzInputModule,
    NzInputNumberModule,
    NzListModule,
    NzMenuModule,
    NzMessageModule,
    NzModalModule,
    NzNotificationModule,
    NzPaginationModule,
    NzPopconfirmModule,
    NzPopoverModule,
    NzProgressModule,
    NzRadioModule,
    NzSelectModule,
    NzSkeletonModule,
    NzSliderModule,
    NzSpinModule,
    NzStatisticModule,
    NzStepsModule,
    NzTableModule,
    NzTreeSelectModule,
    NzTabsModule,
    NzTabsModule,
    NzTagModule,
    NzTimelineModule,
    NzToolTipModule,
    NzUploadModule,
    NzSwitchModule,
    NzCollapseModule,
    NzIconModule,
    NzImageModule,
    NzSpaceModule,
    NzFormModule,
    NzLayoutModule,
    ReactiveFormsModule,
    TranslateModule,
    ApolloModule,
    CountdownModule,
    GraphQLModule,
    NzDescriptionsModule,
    NzCarouselModule
  ],
  exports: [
    AppRoutingModule,
    ChartComponent,
    DraggableDirective,
    FormsModule,
    GameDayPipe,
    ItemComponent,
    NzAlertModule,
    NzAutocompleteModule,
    NzAvatarModule,
    NzBadgeModule,
    NzBreadCrumbModule,
    NzButtonModule,
    NzCardModule,
    NzCheckboxModule,
    NzCommentModule,
    NzDatePickerModule,
    NzDividerModule,
    NzDrawerModule,
    NzDropDownModule,
    NzEmptyModule,
    NzGridModule,
    NzInputModule,
    NzInputNumberModule,
    NzListModule,
    NzMenuModule,
    NzMessageModule,
    NzModalModule,
    NzNotificationModule,
    NzPaginationModule,
    NzPopconfirmModule,
    NzPopoverModule,
    NzProgressModule,
    NzRadioModule,
    NzSelectModule,
    NzSkeletonModule,
    NzSliderModule,
    NzSpinModule,
    NzStatisticModule,
    NzStepsModule,
    NzTableModule,
    NzTabsModule,
    NzTagModule,
    NzTreeSelectModule,
    NzTimelineModule,
    NzSkeletonModule,
    NzPageHeaderModule,
    NzSwitchModule,
    NzCollapseModule,
    NzUploadModule,
    NzToolTipModule,
    NzIconModule,
    NzImageModule,
    NzSpaceModule,
    NzFormModule,
    TranslateModule,
    NzFlexModule,
    GameDayPipe,
    QualityNamePipe,
    QualityStarsPipe,
    ReactiveFormsModule,
    RelativeTimePipe,
    ReportButtonComponent,
    TimeLeftPipe,
    TranslateModule,
    MissionsComponent,
    NzLayoutModule,
    CountdownModule,
    NzCarouselModule,
    NzDescriptionsModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    ModalService
  ]
})
export class SharedModule { }
