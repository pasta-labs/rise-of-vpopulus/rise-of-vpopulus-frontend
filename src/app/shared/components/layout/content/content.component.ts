import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CountdownComponent } from 'ngx-countdown';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';
import { padStart, cloneDeep, orderBy } from 'lodash-es'
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-layout-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentComponent implements OnInit {
  mobileNavbarOpen = false;
  mobileTopBarOpen = false;

  apiUrl = environment.apiUrl;

  isLoading = true;
  regionId;

  deviceDisplayType;

  countries = [];

  currentDate: Date = null;
  displayDate = "";

  searchValue = '';

  gameConfig = undefined;

  countdownConfig = {
    leftTime: 999,
    format: "hh:mm:ss"
  };
  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;

  languages = [];
  selectedLanguage;

  constructor(
    private graphQlService: GraphQlService,
    private router: Router,
    private translateService: TranslateService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.loadLanguages();

    this.load();
    this.authService.isLoggedIn.subscribe(() => this.load());
  }

  load = () => {
    if (!localStorage.getItem(`jwtToken`)) {
        this.regionId = '123';
        this.isLoading = false;
        return;
      }

      this.isLoading = true;
    this.deviceDisplayType = localStorage.getItem(`deviceDisplayType`);

      const gameConfig = JSON.parse(localStorage.getItem('gameConfig'));
      if (gameConfig && (gameConfig.type === 'Legacy' || gameConfig.type === 'Pi')) {
      this.graphQlService.get(`game`, `{ topbar { region { id } }}`).subscribe((t: any) => {
            if (t.data.topbar.region === null) {
          this.graphQlService.get(`game`, `{ countries { id name flagLocation(size: "xl") } }`).subscribe((r: any) => {
                  this.countries = r.data.countries;
                  this.isLoading = false;
          })
            } else {
              this.regionId = t.data.topbar.region.id;
              this.isLoading = false;
            }
      });
      } else {
        this.regionId = '123';
        this.isLoading = false;
      }
  }

  ngAfterViewInit() {
    this.loadConfig();
  }

  countdownEvent = (e) => {
    if (e.action === 'done') this.loadConfig();
  }


  loadConfig = () =>
    this.graphQlService.get(`game`, `{ game { day nextDaySeconds } }`).subscribe((r: any) => {
      this.gameConfig = r.data.game;
      localStorage.setItem(`gameDay`, this.gameConfig.day);
      this.countdownConfig.leftTime = this.gameConfig.nextDaySeconds;

      if (this.countdown) this.countdown.restart();
      if (this.countdownConfig.leftTime <= 0) setTimeout(() => this.loadConfig(), 1000);
    });

  updateDayTime = () => {
    setTimeout(() => {
      this.currentDate = new Date(
        this.currentDate.setSeconds(this.currentDate.getSeconds() + 1)
      );
      this.displayDate = `${this.currentDate.getFullYear()}/${padStart(this.currentDate.getMonth() + 1, 2, '0')}/${padStart(this.currentDate.getDate(), 2, '0')} ${padStart(this.currentDate.getHours(), 2, '0')}:${padStart(this.currentDate.getMinutes(), 2, '0')}:${padStart(this.currentDate.getSeconds(), 2, '0')}`;
      this.updateDayTime();
    }, 1000);
  };

  search() {
    this.router.navigateByUrl(`/search/${this.searchValue}`);
  }

  selectCountry = async (id) => {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: SelectCountryCommand) { selectCountry(data: $data) }`, { data: { countryId: id } }).subscribe((r) => {
      window.location.reload();
    })
  }

  loadLanguages = () => {
    this.graphQlService.get(`game`, `{ i18nlanguages{ key name flag } }`).subscribe((r: any) => {
      this.languages = r.data.i18nlanguages;
      if (!this.selectedLanguage) this.switchLanguage(localStorage.getItem(`language`) ? localStorage.getItem(`language`) : 'en');
    })
  }

  switchLanguage = (key) => {
    localStorage.setItem(`language`, key);
    this.translateService.use(key);
    this.selectedLanguage = this.selectedLanguage = this.languages.find(x => x.key === key);
  }

}
