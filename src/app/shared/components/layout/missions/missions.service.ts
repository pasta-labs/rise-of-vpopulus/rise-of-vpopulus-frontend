import { EventEmitter, Injectable } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Injectable({
  providedIn: 'root'
})
export class MissionsService {

  dailyMissions: any = {};
  dailyMissionsUpdated = new EventEmitter();

  updateDailyMissionsRequest = new EventEmitter();
  updateMissionsRequest = new EventEmitter();

  constructor(
    private graphQlService: GraphQlService
  ) {
    this.loadDailyMissions();

    this.updateMissionsRequest.subscribe(() => {
      this.updateDailyMissionsRequest.emit();

      // reload non daily missions missions
    })

    this.updateDailyMissionsRequest.subscribe(() => {
      if (this.dailyMissions.hasTrained === false || this.dailyMissions.hasWorked === false || this.dailyMissions.hasFighted === false)
        this.loadDailyMissions();
    })
  }

  loadDailyMissions = () => {
    this.graphQlService.get(`game`, `{ dailyMissions { hasWorked hasTrained hasFighted } }`).subscribe((r: any) => {
      this.dailyMissions = r.data.dailyMissions;
      this.dailyMissionsUpdated.emit();
    });
  }
}
