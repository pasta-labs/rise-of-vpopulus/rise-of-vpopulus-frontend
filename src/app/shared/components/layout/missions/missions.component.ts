import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MissionsService } from './missions.service';

@Component({
  selector: 'app-layout-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.scss']
})
export class MissionsComponent implements OnInit {
  apiUrl = environment.apiUrl;

  entityType;

  dailyMissions = {};

  constructor(
    private missionsService: MissionsService
  ) { }

  ngOnInit(): void {
    // this.checkEntityType();

    // if (this.entityType === "Citizen") {
    //   this.dailyMissions = this.missionsService.dailyMissions;
    //   this.missionsService.dailyMissionsUpdated.subscribe(() => {
    //     this.checkEntityType();
    //     this.dailyMissions = this.missionsService.dailyMissions;
    //   });
    // }
  }

  checkEntityType = () => {
    // var profile = JSON.parse(localStorage.getItem(`profile`));
    // this.entityType = profile.entityType;
  }

}
