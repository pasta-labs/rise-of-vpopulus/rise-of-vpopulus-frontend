import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TravelComponent } from 'src/app/game/entity/travel/travel.component';
import { AuthService } from 'src/app/shared/services/auth.service';
import { padStart, cloneDeep, orderBy } from 'lodash-es'
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { CountdownComponent } from 'ngx-countdown';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent implements OnInit {
  isLoading = true;
  entityType;
  profile;
  currencies = undefined;

  constructor(
    private router: Router,
    private authService: AuthService,
    private modalService: NzModalService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.entityType = localStorage.getItem(`entityType`);

    this.loadData();
    this.authService.updateTopBar.subscribe((r: String) => this.loadData(r));

    var theme = localStorage.getItem(`theme`);
    if (theme != null && theme == 'dark') this.darkTheme();
  }

  loadData = async (elements = undefined) => {
    this.isLoading = true;
    if (!localStorage.getItem(`jwtToken`)) return;

    if (elements) elements = elements.split(',');

    if (!elements || elements.includes('profile'))
      await this.loadProfile();
    if (!elements || elements.includes('currencies'))
      this.loadCurrencies();
    this.isLoading = false;
  }

  loadProfile = async () => {
    const r = await firstValueFrom(this.graphQlService.get(`game`, `{ topbar { id entityType name avatar health experienceLevel experience nextLevelExperience region { id name country { id name currencyId }  } } }`));
    this.profile = r.data['topbar']

    this.profile.entityType = this.profile.entityType.toLowerCase();
    if (this.profile.entityType == 'citizen') {
      this.profile.experiencePercentage = Math.floor(
        (this.profile.experience / this.profile.nextLevelExperience) * 100
      );
    }
  }

  loadCurrencies = async () => {
    const r = await firstValueFrom(this.graphQlService.get(`game`, `{ entityCurrencies { amount currency { symbol isMainCurrency flagLocation(size: "s") } } }`));
    this.currencies = orderBy(r.data['entityCurrencies'], x => x.currency.isMainCurrency, 'desc');
  }

  loadRegion = () => {

  }


  travel() {
    this.modalService.create({
      nzTitle: "Travel",
      nzContent: TravelComponent,
      nzFooter: null,
      nzMaskClosable: false,
      nzStyle: {
        width: '90%',
      },
    });
  }

  switchTheme() {
    var theme = localStorage.getItem(`theme`);
    if (theme == null || theme != 'dark') this.darkTheme();
    else this.lightTheme();
  }

  darkTheme() {
    // = = = Main theme
    const style = document.createElement('link');
    style.type = 'text/css';
    style.rel = 'stylesheet';
    style.id = 'dark-theme';
    style.href = 'assets/themes/ng-zorro-antd.dark.min.css';

    const head = document.getElementsByTagName('head')[0];
    head.appendChild(style);

    // = = = Mobile navbar
    var style2 = document.createElement('style');
    style2.type = 'text/css';
    style2.innerHTML = '.am-tabs-default-bar-tab { background-color: black; }';
    style2.id = 'dark-theme-navbar';
    head.appendChild(style2);

    localStorage.setItem(`theme`, `dark`);
  }

  lightTheme() {
    const head = document.getElementsByTagName('head')[0];
    head.removeChild(document.getElementById('dark-theme'));
    head.removeChild(document.getElementById('dark-theme-navbar'));
    localStorage.setItem(`theme`, `light`);
  }
}
