import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep, orderBy } from 'lodash-es';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {
  entityType;
  isLoading = true;
  profile;

  @Input() menuOrientation = 'horizontal';

  constructor(
    private router: Router,
    private authService: AuthService,
    private httpService: GenericHttpService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.entityType = +localStorage.getItem(`entityType`);
    this.loadData();
    this.authService.updateNavBar.subscribe(() => {
      this.loadData();
    });
  }

  loadData() {
    this.isLoading = true;
    console.log('load data?')
    this.graphQlService.get(`game`, "{ topbar { id entityType name avatar health experienceLevel experience nextLevelExperience region { id name country { id name currencyId }  } } }").subscribe((r: any) => {
      this.profile = cloneDeep(r.data.topbar);
      console.log('load data? 2', this.profile)
      localStorage.setItem('profile', JSON.stringify(this.profile));
      this.profile.currencies = orderBy(this.profile.currencies, x => x.isMainCurrency, 'asc');
      this.profile.entityType = this.profile.entityType.toLowerCase();
      if (this.profile.entityType == 'citizen') {
        this.profile.experiencePercentage = Math.floor(
          (this.profile.experience / this.profile.nextLevelExperience) * 100
        );
      }
      this.authService.updatedProfile.emit(true);
      this.isLoading = false;
    });
  }

  switchBack() {
    this.httpService.get(`/api/entities/switch/back`).subscribe((r) => {
      localStorage.setItem('jwtToken', r.jwtToken);

      this.authService.switchTo().then(() => {
        window.location.reload();
      });
    });
  }

  logout() {
    localStorage.clear();
    this.authService.isLoggedIn.emit(false);
    this.router.navigateByUrl(`/landing`);
  }
}
