import { Component, Input, OnInit } from '@angular/core';
import Chart from 'chart.js/auto';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
})
export class ChartComponent implements OnInit {
  isLoading = true;

  @Input() labels?;
  @Input() datasets?;
  @Input() type?;
  @Input() options?;

  @Input() height = "auto";

  chartElementId;

  ngOnInit() {
    this.chartElementId = "chartCanvas-" + Math.floor(Math.random() * 999);
  }

  ngAfterViewInit() {
    let config: any = {
      type: this.type,
      data: {
        labels: this.labels,
        datasets: this.datasets
      },
      options: this.options
    };

    config.data.datasets.forEach((dataset, index) => {
      if (!dataset.pointRadius) dataset.pointRadius = 0.5;
      if (!dataset.borderWidth) dataset.borderWidth = 2;

      if (!dataset.borderColor) dataset.borderColor = `rgba(${this.colorArray[index][0]}, ${this.colorArray[index][1]}, ${this.colorArray[index][2]}, 1)`;
      if (!dataset.backgroundColor) dataset.backgroundColor = `rgba(${this.colorArray[index][0]}, ${this.colorArray[index][1]}, ${this.colorArray[index][2]}, 0.2)`;
    })

    setTimeout(() => {
      const ctx = (<HTMLCanvasElement>document.getElementById(this.chartElementId)).getContext('2d');
      const myChart = new Chart(ctx, config);
      this.isLoading = false;
    }, 1000)
  }

  @Input() colorArray = [
    [255, 99, 132],
    [54, 162, 235],
    [255, 206, 86],
    [75, 192, 192],
    [153, 102, 255],
    [255, 159, 64]
  ];


}
