import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from '../../services/generic-http-service';
import { GraphQlService } from '../../services/graphql.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-report-button',
  templateUrl: './report-button.component.html',
  styleUrls: ['./report-button.component.scss'],
})
export class ReportButtonComponent implements OnInit {
  isLoading = false;
  isVisible = false;

  reason;

  @Input() entityType;
  @Input() entityId;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void { }

  visibilityChange(value) {
    this.isVisible = value;
  }

  submit = async () => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: CreateReportCommand) { reportCreate(data: $data) }`, {
      data: {
        entityId: this.entityId,
        entityType: this.entityType,
        reason: this.reason,
      }
    })
      .subscribe(() => {
        this.messageService.success(
          `The report has been submitted! We will revise this soon!`
        );
        this.isVisible = false;
        this.reason = "";
        this.isLoading = false;
      });
  }
}
