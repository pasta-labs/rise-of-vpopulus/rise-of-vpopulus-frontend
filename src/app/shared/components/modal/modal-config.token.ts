export class ModalConfig<D = any> {
  context?: D;
  position?: {top: number, left: number};
}

export const MODAL_DEFAULT_CONFIG: ModalConfig = {
  position: {
    top: 0,
    left: 0,
  }
};
