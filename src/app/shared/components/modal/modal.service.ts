import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef, Inject,
  Injectable, Injector, Renderer2, RendererFactory2, Type
} from '@angular/core';
import { ModalComponent } from '../modal/modal.component';
import { DOCUMENT } from '@angular/common';
import { ModalConfig } from './modal-config.token';
import { ModalInjector } from './modal.injector';
import { ModalRef } from './modal-ref.class';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  private renderer: Renderer2;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private router: Router,
              private injector: Injector,
              rendererFactory: RendererFactory2,
              private applicationRef: ApplicationRef,
              @Inject(DOCUMENT) private document: Document) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  public openModal(component: Type<any>, config?: ModalConfig): Observable<any> {
    const {modalRef, componentRef} = this.appendModal(config, component);

    const routeSubscribe = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.closeModal(componentRef);
        routeSubscribe.unsubscribe();
      }
    });

    return modalRef.afterClosed.pipe(tap(() => {
      routeSubscribe.unsubscribe();
      this.closeModal(componentRef);
    }));
  }

  appendModal(context: ModalConfig, component: Type<any>): { modalRef: ModalRef; componentRef: ComponentRef<ModalComponent> } {
    const map = new WeakMap();
    map.set(ModalConfig, context);

    const modalRef = new ModalRef();
    map.set(ModalRef, modalRef);

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ModalComponent);
    const componentRef = componentFactory.create(new ModalInjector(this.injector, map));
    componentRef.instance.component = component;

    this.applicationRef.attachView(componentRef.hostView);
    const nativeElement = componentRef.location.nativeElement;
    this.renderer.appendChild(this.document.body, nativeElement);

    return {modalRef, componentRef};
  }

  closeModal(componentRef): void {
    componentRef.hostView.detectChanges();
    componentRef.destroy();
  }

}
