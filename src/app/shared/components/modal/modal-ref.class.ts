import { Observable, Subject } from 'rxjs';

export class ModalRef {
  constructor() {}

  private readonly _afterClosed = new Subject<any>();
  afterClosed: Observable<any> = this._afterClosed.asObservable();

  close(result?: any): void {
    this._afterClosed.next(result);
  }
}
