import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ComponentRef, Inject,
  Input,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { ModalConfig, MODAL_DEFAULT_CONFIG } from './modal-config.token';
import { ModalRef } from './modal-ref.class';
import { cloneDeep } from 'lodash-es';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],

})
export class ModalComponent implements AfterViewInit {
  @ViewChild('dynamicComponentPlaceholder', {read: ViewContainerRef}) placeholder: ViewContainerRef;
  @Input() component: any;
  dragEventTarget: MouseEvent | TouchEvent;
  inViewport = true;
  componentRef: ComponentRef<any>;

  constructor(private changeDetectorRef: ChangeDetectorRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              public config: ModalConfig,
              public modalRef: ModalRef) {
    this.config = { ...cloneDeep(MODAL_DEFAULT_CONFIG), ...this.config};
  }

  ngAfterViewInit(): void {
    if (this.component) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component as any);

      this.componentRef = this.placeholder.createComponent(componentFactory);
      this.componentRef.instance.context = this.config.context;
      this.changeDetectorRef.detectChanges();
    }
  }

  initDrag(event: MouseEvent | TouchEvent): void {
    this.dragEventTarget = event;
    this.changeDetectorRef.detectChanges();
  }

  close(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.modalRef.close();
  }
}
