import { Injectable } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CustomI18NLoader implements TranslateLoader {
  contentHeader = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  });

  constructor(private http: HttpClient) {}
  getTranslation(lang: string): Observable<any> {
    var apiAddress = `https://i18n.div.pt/api/projects/72c4f73e-d60f-4cc1-8e32-bb1592afef3e/languages/${lang.toUpperCase()}/export`;
    return Observable.create((observer) => {
      // { headers: this.contentHeader }
      this.http.get(apiAddress).subscribe(
        (res: Response) => {
          observer.next(res);
          observer.complete();
        },
        (error) => {
          //  failed to retrieve from api, switch to local
          // this.http
          //   .get(`/assets/i18n/${lang}.json`)
          //   .subscribe((res: Response) => {
          //     observer.next(res);
          //     observer.complete();
          //   });
        }
      );
    });
  }
}
