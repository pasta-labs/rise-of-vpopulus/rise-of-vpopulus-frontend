export enum EntityType {
  Citizen = 1001,
  Organisation = 1002,
  Party = 1003,
  Army = 1004,
  Country = 1005,
  NationalOrganization = 1008,
}
