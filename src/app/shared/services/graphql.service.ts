import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Apollo, gql } from 'apollo-angular';
import { NzMessageService } from 'ng-zorro-antd/message';

@Injectable({
  providedIn: 'root',
})
export class GraphQlService {
  constructor(
    private apollo: Apollo,
    private messageSerivce: NzMessageService,
    private translateService: TranslateService
  ) { }

  get = (area, query) => {
    return this.apollo.use(area).query({
      errorPolicy: 'all',
      fetchPolicy: 'no-cache',
      query: gql`${query}`,
    });
  };

  post = (area, query, variables) => {
    return this.apollo.use(area).mutate({
      errorPolicy: 'all',
      fetchPolicy: 'no-cache',
      mutation: gql`${query}`,
      variables: variables,
    });
  }

  displayErrors = (errors, prefix = ``) => {
    var message = undefined;
    if (errors && errors[0] && errors[0].extensions) {
      if (errors[0].extensions.code) message = errors[0].extensions.code;
      if (errors[0].extensions.data && errors[0].extensions.data.CODE) message = errors[0].extensions.data.CODE;
    }
    if (message)
      this.messageSerivce.error(this.translateService.instant(prefix + message));
  }
}
