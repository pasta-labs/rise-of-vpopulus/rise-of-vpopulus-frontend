import { EventEmitter, Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { EntityType } from '../models/EntityType';
import { GenericHttpService } from './generic-http-service';
import { GraphQlService } from './graphql.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  isLoggedIn: EventEmitter<boolean> = new EventEmitter(false);
  updateTopBar: EventEmitter<any> = new EventEmitter();
  updateNavBar: EventEmitter<boolean> = new EventEmitter(false);
  updatedProfile: EventEmitter<boolean> = new EventEmitter(false);

  constructor(
    private httpService: GenericHttpService,
    private router: Router,
    private graphQlService: GraphQlService
  ) { }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('jwtToken');
    return !!token;
  }

  canActivate(): boolean {
    if (!this.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
  switchTo(redirectToHome = true) {
    return new Promise((resolve, reject) => {
      return this.graphQlService
        .get(`game`, "{ currentUser { citizenId entityId entityType } }")
        .subscribe(async (user: any) => {
          console.log('user', user.data.currentUser);
          localStorage.setItem(`citizenId`, user.data.currentUser.citizenId);
          localStorage.setItem(`entityId`, user.data.currentUser.entityId);
          localStorage.setItem(`entityType`, user.data.currentUser.entityType);
          console.log('switchTo', localStorage);

          if (redirectToHome === true){
            setTimeout(() => {
              window.location.href = "/";
              window.location.reload();
            }, 1000);
          }
          resolve(true);
        });
    });
  }

 switchToEntity(entityType: EntityType, id) {
    this.httpService
      .post(`/api/entities/switch/to`, {
        entityType: entityType.toString().toLowerCase(),
        entityId: id,
      })
      .subscribe((r) => {
        localStorage.setItem('jwtToken', r.jwtToken);
        this.switchTo(false).then(() => {
          this.router.navigateByUrl(`/`);
          setTimeout(() => {
            window.location.reload();
          }, 100);
        });
      });
  }
}
