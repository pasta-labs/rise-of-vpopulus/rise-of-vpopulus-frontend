import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuardMod  {
  constructor() {}

  canActivate() {
    return localStorage.getItem(`modPermissions`) !== null;
  }
}
