import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private router: Router) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let currentUserToken = localStorage.getItem('jwtToken');
    if (currentUserToken) {
      request = request.clone({
        setHeaders: {
          authorization: `Bearer ${currentUserToken}`
        },
      });
    }
    return next.handle(request).pipe(
      catchError((error: any) => {
        console.log('error', error);

        if (
          error.status === 401 &&
          !request.url.includes('login') &&
          !request.url.includes('register')
        ) {
          localStorage.clear();
          localStorage.setItem(`rov_redirectTo`, window.location.pathname);
          window.location.href = 'login';
          return;
        }

        // if (error.status == 500 && error.error.Message == 'WRONG_ENTITY') {
        //   this.router.navigateByUrl(`/`);
        // }
        return throwError(error);
      })
    );
  }
}
