import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard  {
  constructor(private router: Router) {}

  canActivate() {
    var currentUser = localStorage.getItem('jwtToken');
    if (!currentUser) {
      localStorage.setItem(`rov_redirectTo`, window.location.pathname);

      this.router.navigateByUrl('/landing');
      return false;
    }
    return true;
  }
}
