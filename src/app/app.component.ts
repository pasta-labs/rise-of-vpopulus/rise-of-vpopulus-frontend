import { Component, NgZone, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { DeviceDetectorService } from 'ngx-device-detector';

import { AuthService } from './shared/services/auth.service';
import { GraphQlService } from './shared/services/graphql.service';
import { ActivatedRoute, Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { OneSignal as NgxOneSignal } from 'onesignal-ngx';
import OneSignal from 'onesignal-cordova-plugin';
import { SplashScreen } from '@capacitor/splash-screen';
import { environment } from 'src/environments/environment';
import { App, URLOpenListenerEvent } from '@capacitor/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  isLoading = true;
  isLoggedIn = false;

  deviceDisplayType;

  layoutType = "landing";

  @ViewChild('cookieTemplate') private cookieTemplateTpl: TemplateRef<any>;
  cookiesNotificationId;

  constructor(
    private authService: AuthService,
    private translateService: TranslateService,
    private notificationService: NzNotificationService,
    private deviceDetector: DeviceDetectorService,
    private graphQlService: GraphQlService,
    private activatedRoute: ActivatedRoute,
    private oneSignal: NgxOneSignal,
    private zone: NgZone,
    private router: Router,
  ) { }

  ngOnInit(): void {
    localStorage.setItem(`platform`, environment.platform);

    if (environment.platform === "mobile") {
      App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
        this.zone.run(() => {
          const slug = event.url.split(".net").pop();
          if (slug)
            this.router.navigateByUrl(slug);
        });
      });
    }

    this.asyncInit();
  }

  asyncInit = async () => {
    // await SplashScreen.hide();

    this.deviceDisplayType = (this.deviceDetector.isDesktop() || this.deviceDetector.isTablet()) ? 'full' : 'mobile';
    localStorage.setItem(`deviceDisplayType`, this.deviceDisplayType);

    this.translateService.use(`en`);

    const opts = {
      appId: "5475d7d3-41f2-4efa-980e-102b5d7256d8",
      safari_web_id: "web.onesignal.auto.5694d1e9-fcaa-415d-b1f1-1ef52daca700",
      notifyButton: {
        enable: true,
      },
      allowLocalhostAsSecureOrigin: true,
      serviceWorkerPath: "assets/OneSignalSDKWorker.js",
      serviceWorkerParam: {
        scope: "/assets/"
      }
    };

    // if (environment.platform === "web")
    //   this.oneSignal.init(opts).then(() => { }).catch(e => console.log('onesignal', e));
    // else if (environment.platform === "mobile") {
    //   OneSignal.Debug.setLogLevel(6)
    //   OneSignal.initialize(opts.appId)
    // }
    this.checkAuthStatus();
    this.authService.isLoggedIn.subscribe((r) => this.checkAuthStatus());

    await SplashScreen.show({
      showDuration: 1000,
      autoHide: true,
    });
  }

  checkAuthStatus = async () => {
    this.isLoading = true;
    this.isLoggedIn = localStorage.getItem(`jwtToken`) ? true : false;

    if (this.isLoggedIn) {
      if (localStorage.getItem(`outside-oauth-pending`)) {
        setTimeout(() => {
          this.router.navigateByUrl(`/oauth-outside`)
        }, 1000);
      }

      this.layoutType = "game";
      const gameResponse: any = await firstValueFrom(this.graphQlService.get(`game`, `{ game { day type } }`));
      localStorage.setItem(`gameConfig`, JSON.stringify(gameResponse.data.game));
      localStorage.setItem(`gameDay`, gameResponse.data.game.day);

      const topbarResponse: any = await firstValueFrom(this.graphQlService.get(`game`, "{ topbar { region { id name country { id } } } }"));
      localStorage.setItem(`countryId`, topbarResponse.data.topbar.region?.country?.id);

      // if (environment.platform === "web") {
      //   await this.oneSignal.login(localStorage.getItem(`citizenId`));
      // } else if (environment.platform === "mobile") {
      //   OneSignal.Notifications.requestPermission(true).then((success: Boolean) => {
      //     console.log("Notification permission granted " + success);
      //     OneSignal.login(localStorage.getItem(`citizenId`));
      //   })
      // }


      this.isLoading = false;
    } else {
      if (window.location.pathname.includes('/article/')) this.layoutType = "article";
      // else if (window.location.pathname.includes('/newspaper/')) this.layoutType = "newspaper";
      else this.layoutType = "auth";
      this.isLoading = false;
    }
  }


  ngAfterViewInit() {
    if (!localStorage.getItem(`cookiesAccepted`))
      this.cookiesNotificationId = this.notificationService.template(
        this.cookieTemplateTpl,
        {
          nzDuration: 0,
          nzPlacement: 'bottomLeft',
          nzCloseIcon: null,
        }
      ).messageId;
  }

  acceptCookies() {
    localStorage.setItem(`cookiesAccepted`, 'true');
    this.notificationService.remove(this.cookiesNotificationId);
  }

  rejectCookies() {
    window.location.href = 'https://google.com';
  }
}
