import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-oauth-outside',
  templateUrl: './oauth-outside.component.html',
  styleUrl: './oauth-outside.component.scss'
})
export class OauthOutsideComponent {

  isCitizen = true;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    if (!localStorage.getItem(`jwtToken`)) {
      localStorage.setItem(`outside-oauth-pending`, "true");
      this.router.navigateByUrl(`/landing`);
    } else {
      if (localStorage.getItem(`entityType`) === '1001') {
        localStorage.removeItem(`outside-oauth-pending`);
        window.location.href = `https://wiki.vpopulus.net/oauth/login/${localStorage.getItem(`jwtToken`)}`;
      } else {
        localStorage.removeItem(`outside-oauth-pending`);
        this.isCitizen = false;
      }
    }
  }
}
