import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { CitizenFriendsComponent } from './friends/friends.component';
import { CitizenProfileComponent } from './profile/profile.component';
import { CitizenReferralsComponent } from './referrals/referrals.component';
import { CitizenTrainingComponent } from './training/training.component';

export const routes: Routes = [
  {
    path: 'citizen/:id',
    component: CitizenProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'citizen/:id/friendlist',
    component: CitizenFriendsComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'training',
    component: CitizenTrainingComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'referrals',
    component: CitizenReferralsComponent,
    canActivate: [AuthGuard],
  },
  { path: 'referral/:token', component: CitizenReferralsComponent },
];
