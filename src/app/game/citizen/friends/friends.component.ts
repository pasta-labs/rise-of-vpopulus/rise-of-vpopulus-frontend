import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-citizen-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss'],
})
export class CitizenFriendsComponent implements OnInit {
  isLoading = false;

  citizenId;
  citizen;

  friends = [];
  friendsSent = [];
  friendsReceived = [];

  canManage = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messageService: NzMessageService,
    private graphQlService: GraphQlService,    
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.citizenId = params['id'];

      this.graphQlService.get(`game`, `{ citizen(id: "${this.citizenId}"){ name } }`).subscribe((r: any) => this.citizen = r.data.citizen);

      if (
        localStorage.getItem('entityType') == (1001).toString() &&
        localStorage.getItem('entityId') == this.citizenId
      )
        this.canManage = true;

      this.loadData();
    });
  }

  loadData() {
    this.isLoading = true;
    forkJoin([
      this.httpService.get(`/api/citizens/friends/active/${this.citizenId}`),
      this.httpService.get('/api/citizens/friends/requests/sent'),
      this.httpService.get(`/api/citizens/friends/requests/recieved`),
    ]).subscribe(([active, sent, received]) => {
      this.friends = active;
      this.friendsSent = sent;
      this.friendsReceived = received;
      this.isLoading = false;
    });
  }

  remove(id) {
    this.isLoading = true;
    this.httpService.delete(`/api/citizens/friends/${id}`).subscribe(
      () => {
        this.messageService.success(this.translateService.instant('Citizen.friendshipRemoved'));
        this.loadData();
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }

  accept(id) {
    this.isLoading = true;
    this.httpService
      .put(`/api/citizens/friends/approve`, { requestId: id })
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant('Citizen.friendshipApproved'));
          this.loadData();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  decline(id) {
    this.isLoading = true;
    this.httpService
      .put(`/api/citizens/friends/reject`, { requestId: id })
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant('Citizen.friendshipRejected'));
          this.loadData();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
