import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { uniq } from 'lodash-es';
import { MissionsService } from 'src/app/shared/components/layout/missions/missions.service';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-citizen-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
})
export class CitizenTrainingComponent implements OnInit {
  isLoading = [false, false];
  todaysData: any;
    chartData;
    trainingModal?: NzModalRef;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private authService: AuthService,
    private missionsService: MissionsService,
    private modal: NzModalService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.loadData();
  }

  train() {
    this.isLoading[0] = true;
    this.httpService.post(`/api/entities/citizens/training`, {}).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant('Citizen.Training.youTrained'));
        this.authService.updateTopBar.emit(`profile`);
        this.missionsService.updateDailyMissionsRequest.emit();
        this.loadData();
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading[0] = false;
      }
    );
  }

  loadData() {
    this.isLoading[1] = true;
    this.httpService
      .get(`/api/entities/citizens/training/average`)
      .subscribe((average) => {
        this.chartData = {
          labels: uniq(average.map((x) => x.gameDay)),
          datasets: [
            {
              data: average
                .filter((x) => x.entity.toLowerCase() === 'you')
                .map((x) => x.payload.strengthIncrease),
              label: 'You',
            },
            {
              data: average
                .filter((x) => x.entity.toLowerCase() === 'average')
                .map((x) => x.payload.strengthIncrease),
              label: 'Average',
            },
          ],
          options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              y: {
                ticks: {
                  min: 0,
                },
              },
            },
          },
        };
        this.isLoading[1] = false;
      });

    this.isLoading[0] = true;
    this.httpService
      .get(`/api/entities/citizens/training/today`)
      .subscribe((today) => {
        if (today != null) this.todaysData = today;
        this.isLoading[0] = false;
      });
  }
}
