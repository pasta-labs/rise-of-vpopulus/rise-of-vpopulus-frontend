import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CitizenTrainingComponent } from './training/training.component';
import { CitizenProfileComponent } from './profile/profile.component';
import { CitizenReferralsComponent } from './referrals/referrals.component';
import { CitizenFriendsComponent } from './friends/friends.component';
import { CitizenProfileFriendsComponent } from './profile/components/friends/friends.component';
import { CitizenProfileMedalsComponent } from './profile/components/medals/medals.component';
import { CitizenProfilePasswordsAlikeComponent } from './profile/components/passwords-alike/passwords-alike.component';
import { EntityModule } from '../entity/entity.module';
import { CitizenProfileSkillsComponent } from './profile/components/skills/skills.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';

@NgModule({
  declarations: [
    CitizenTrainingComponent,
    CitizenProfileComponent,
    CitizenReferralsComponent,
    CitizenFriendsComponent,
    CitizenProfileFriendsComponent,
    CitizenProfileMedalsComponent,
    CitizenProfilePasswordsAlikeComponent,
    CitizenProfileSkillsComponent
  ],
  imports: [CommonModule, SharedModule, EntityModule, NzTabsModule],
  exports: [],
})
export class CitizenModule {}
