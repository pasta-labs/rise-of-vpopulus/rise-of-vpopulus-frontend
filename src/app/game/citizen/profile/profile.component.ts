import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { EntityModPanelComponent } from '../../entity/components/entity-mod-panel/entity-mod-panel.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-citizen',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class CitizenProfileComponent implements OnInit {
  isLoading = false;

  citizenId;

  citizen;

  showEditBio = false;
  inputValue: string = '';

  isEntity = false;

  isVisible = false;

  newspapersEditor = [];
  company;
  army;
  party;

  canFriend = true;

  isMod = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messageService: NzMessageService,
    private modalService: NzModalService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.isLoading = true;

      this.citizenId = params['id'];
      this.isMod = localStorage.getItem(`modPermissions`) !== null;

      this.loadData();
    });
  }

  loadData = () => {
    this.isEntity = false;
    if (
      localStorage.getItem('entityType') == (1001).toString() &&
      localStorage.getItem('entityId') == this.citizenId
    )
      this.isEntity = true;

    if (localStorage.getItem('entityType') !== (1001).toString())
      this.canFriend = false;

    if (this.canFriend === true && this.isEntity === false) this.checkFriendship();

    this.loadCitizen();
    this.loadNewspapers();
    this.loadArmy();
    this.loadParty();
    this.loadCompany();
  }

  checkFriendship = () => {
    this.httpService
      .get(`/api/citizens/friends/exists/${this.citizenId}/${localStorage.getItem('entityId')}`)
      .subscribe((r) => this.canFriend = !r);
  }

  loadCitizen() {
    this.isLoading = true;
    this.citizen = undefined;
    this.httpService
      .get(`/api/entities/citizens/${this.citizenId}`)
      .subscribe((r) => {
        this.citizen = r;
        this.isLoading = false;
      });
  }

  addFriend() {
    this.httpService
      .post(`/api/citizens/friends/request`, { entityId: this.citizenId })
      .subscribe((r) => {
        this.messageService.success(this.translateService.instant('Citizen.requestBeenSent'));
        this.checkFriendship();
      });
  }

  loadNewspapers() {
    this.newspapersEditor = [];
    this.httpService
      .get(`/api/citizens/${this.citizenId}/newspapers`)
      .subscribe((r) => {
        this.newspapersEditor = r;
      });
  }

  loadArmy() {
    this.army = undefined;
    this.httpService
      .get(`/api/citizens/${this.citizenId}/armies`)
      .subscribe((r) => {
        this.army = r;
      });
  }

  loadParty = () => {
    this.party = undefined;
    this.httpService
      .get(`/api/citizens/${this.citizenId}/parties`)
      .subscribe((r) => {
        this.party = r;
      });
  }

  loadCompany() {
    this.company = undefined;
    this.httpService
      .get(`/api/companies/employees/${this.citizenId}/current`)
      .subscribe((r) => {
        this.company = r.company;
      });
  }

  showModPanel = () => {
    this.modalService.create({
      nzContent: EntityModPanelComponent,
      nzTitle: this.translateService.instant('Citizen.modAccess'),
      nzFooter: null,
      nzWidth: '90%',
      nzData: {
        entity: this.citizen,
        entityType: 'Citizen'
      }
    });
  }
}
