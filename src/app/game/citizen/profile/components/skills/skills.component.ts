import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-citizen-profile-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss'],
})
export class CitizenProfileSkillsComponent implements OnInit {
  isLoading = false;

  @Input() citizen;

  wellnessFormatter = (percent: number) => `${percent}`;
  experienceFormatter = (percent: number) => `${percent}%`;
  militaryExperienceFormatter = (percent: number) => `${percent}%`;

  constructor() {}

  ngOnInit(): void {
    this.citizen['experiencePercent'] = Math.floor(
      (this.citizen.experience / this.citizen.experienceRequiredNextLevel) * 100
    );
    this.citizen['militaryRankPercent'] = Math.floor(
      (this.citizen.militaryExperience.experience /
        this.citizen.militaryExperience.nextLevelExperience) *
        100
    );
  }
}
