import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-citizen-profile-passwords-alike',
  templateUrl: './passwords-alike.component.html',
  styleUrls: ['./passwords-alike.component.scss'],
})
export class CitizenProfilePasswordsAlikeComponent implements OnInit {
  @Input() citizenId;

  isLoading = true;

  list = [];

  isMod = false;

  constructor(private httpService: GenericHttpService) {}

  ngOnInit() {
    if (localStorage.getItem(`entityType`) !== (1001).toString()) return;
    var moderatorPermissions = JSON.parse(
      localStorage.getItem(`moderatorPermissions`)
    );
    this.isMod = moderatorPermissions?.Citizens?.SamePasswordList != undefined;
    if (this.isMod) this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService
      .get(`/api/mp/passwords/${this.citizenId}`)
      .subscribe((r) => {
        this.list = r;
        this.isLoading = false;
      });
  }
}
