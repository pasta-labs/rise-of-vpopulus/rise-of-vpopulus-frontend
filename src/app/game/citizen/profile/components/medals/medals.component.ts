import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-citizen-profile-medals',
  templateUrl: './medals.component.html',
  styleUrls: ['./medals.component.scss'],
})
export class CitizenProfileMedalsComponent implements OnInit {
  medals = [];

  @Input() citizen;

  isLoading = false;

  constructor(private httpService: GenericHttpService) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.httpService.get('/api/common/medals').subscribe((allMedals) => {
      this.medals = allMedals;
      console.log('medals', this.medals)
      this.httpService
        .get(`/api/common/medals/citizen/${this.citizen.id}`)
        .subscribe((r) => {
          r.forEach((item) => {
            this.medals.find((x) => x.id === item.id).amount = item.amount;
          });
          this.isLoading = false;
        });
    });
  }
}
