import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-citizen-profile-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss'],
})
export class CitizenProfileFriendsComponent implements OnInit {
  isLoading = false;

  @Input() citizenId;

  friends = [];

  constructor(private httpService: GenericHttpService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.httpService
      .get(`/api/citizens/friends/active/${this.citizenId}`)
      .subscribe((r) => {
        this.friends = r.slice(0, 10);
        this.isLoading = false;
      });
  }
}
