import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-citizen-referrals',
  templateUrl: './referrals.component.html',
  styleUrls: ['./referrals.component.scss'],
})
export class CitizenReferralsComponent implements OnInit {
  isLoading = true;

  list = [];

  refUrl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.refUrl = `${window.location.protocol}//${window.location.host
      }/referral/${localStorage.getItem(`entityId`)}`;

    this.route.params.subscribe((params) => {
      var token = params['token'];
      if (token !== null && token !== undefined) {
        localStorage.setItem('refId', token);
        this.router.navigateByUrl('/');
      } else {
        this.graphQlService.get( `game`, `{ citizenReferrals { citizen { id name avatar } goldEarned } }`).subscribe((r) => {
          this.list = r.data['citizenReferrals'];
          this.isLoading = false;
        });
      }
    });
  }
}
