import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { OrganizationCreateComponent } from './create/create.component';
import { OrganizationListComponent } from './list/list.component';
import { OrganizationProfileComponent } from './profile/profile.component';

export const routes: Routes = [
  {
    path: 'organisations',
    component: OrganizationListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'organisations/create',
    component: OrganizationCreateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'organisation/:id',
    component: OrganizationProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'nationalorganization/:id',
    component: OrganizationProfileComponent,
    canActivate: [AuthGuard],
  },
];
