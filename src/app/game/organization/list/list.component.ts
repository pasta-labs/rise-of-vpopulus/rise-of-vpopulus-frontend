import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { EntityType } from 'src/app/shared/models/EntityType';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { OrganizationCreateComponent } from '../create/create.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-organization-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class OrganizationListComponent implements OnInit {
  isLoading = true;

  list = [];

  constructor(
    private authService: AuthService,
    private modalService: NzModalService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ organisations { id name avatar nationalOfCountry region { id name country { id name } } companies { id } } }`).subscribe((r: any) => {
      this.list = r.data.organisations;
      this.isLoading = false;
    })
  }

  create() {
    this.modalService.create({
      nzWidth: '400px',
      nzContent: OrganizationCreateComponent,
      nzTitle: this.translateService.instant('Politics.createOrganisation'),
      nzFooter: null,
    });
  }

  switchTo = (id) =>
    this.authService.switchToEntity(
      this.list.find((x) => x.id === id).nationalOfCountry ? EntityType.NationalOrganization : EntityType.Organisation,
      id);
}
