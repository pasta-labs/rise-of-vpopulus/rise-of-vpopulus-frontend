import { Component, OnInit } from '@angular/core';
import { round } from 'lodash';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-organization-enable-public-offering',
  templateUrl: './enable-public-offering.component.html',
  styleUrls: ['./enable-public-offering.component.scss'],
})
export class OrganizationEnablePublicOfferingComponent implements OnInit {
  isLoading = false;

  sharesNumber = 50;
  currentShareValue;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private modalRef: NzModalRef
  ) {}

  ngOnInit(): void {
    this.calculateShareValue();
  }

  calculateShareValue() {
    this.currentShareValue = round(20 / this.sharesNumber, 2);
  }

  enablePublicOffering() {
    this.isLoading = true;
    this.httpService
      .post(`/api/organizations/public-offering`, {
        sharesNumber: this.sharesNumber,
      })
      .subscribe(
        (r) => {
          this.modalRef.close(`ok`);
          this.isLoading = false;
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
