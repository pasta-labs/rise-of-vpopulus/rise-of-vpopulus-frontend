import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { EntityType } from 'src/app/shared/models/EntityType';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { OrganizationEnablePublicOfferingComponent } from './components/enable-public-offering/enable-public-offering.component';
import { EntityModPanelComponent } from '../../entity/components/entity-mod-panel/entity-mod-panel.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-organization',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class OrganizationProfileComponent implements OnInit {
  isLoading = false;
  entityType;

  organisation;
  id;

  companiesList = [];

  showEditBio = false;
  isEntity = false;

  targetEntityType = 'Organisation';

  isMod = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private modalService: NzModalService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isMod = localStorage.getItem(`modPermissions`) !== null;

    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.entityType = EntityType[localStorage.getItem('entityType')];

      if (
        (this.entityType == EntityType[EntityType.Organisation] ||
          this.entityType == EntityType[EntityType.NationalOrganization]) &&
        localStorage.getItem('entityId') == this.id
      )
        this.isEntity = true;

      this.loadData();
    });
  }

  loadData() {
    this.httpService.get(`/api/organisations/${this.id}`).subscribe((r) => {
      this.organisation = r;
      console.log('org', this.organisation);
      this.targetEntityType = this.organisation.nationalOfCountry ? 'NationalOrganization' : 'Organisation';
    });
    this.httpService
      .get(`/api/companies/${this.entityType}/${this.id}`)
      .subscribe((r) => {
        this.companiesList = r;
      });
  }

  switchTo(id) {
    this.httpService
      .post(`/api/entities/switch/to`, {
        entityType: this.targetEntityType,
        entityId: id,
      })
      .subscribe((r) => {
        localStorage.setItem('jwtToken', r.jwtToken);
        this.authService.switchTo().then(() => {
          window.location.reload();
        });
      });
  }

  enablePublicOffering() {
    this.modalService.create({
      nzContent: OrganizationEnablePublicOfferingComponent,
      nzTitle: this.translateService.instant('Economy.enablingPublicOfferModal'),
      nzWidth: '60%',
      nzMask: true,
      nzFooter: null,
      nzMaskClosable: false,
    });
  }

  showModPanel = () => {
    this.modalService.create({
      nzContent: EntityModPanelComponent,
      nzTitle: this.translateService.instant('Citizen.modAccess'),
      nzFooter: null,
      nzWidth: '90%',
      nzData: {
        entity: this.organisation,
        entityType: this.targetEntityType
      }
    });
  }
}
