import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-organization-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class OrganizationCreateComponent implements OnInit {
  isLoading = false;
  name = '';

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private modalRef: NzModalRef,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void { }

  create() {
    this.isLoading = true;
    this.httpService.post(`/api/organisations`, { name: this.name }).subscribe(
      (r) => {
        this.router.navigateByUrl(`/organisation/${r}`);
        this.modalRef.close(`ok`);
      },
      (error) => {
        this.messageService.error(this.translateService.instant(`Common.Errors.` + error.error.Code))
        this.isLoading = false;
      }
    );
  }
}
