import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationProfileComponent } from './profile/profile.component';
import { OrganizationCreateComponent } from './create/create.component';
import { OrganizationListComponent } from './list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntityModule } from '../entity/entity.module';
import { OrganizationEnablePublicOfferingComponent } from './profile/components/enable-public-offering/enable-public-offering.component';

@NgModule({
  declarations: [
    OrganizationProfileComponent,
    OrganizationCreateComponent,
    OrganizationListComponent,
    OrganizationEnablePublicOfferingComponent,
  ],
  imports: [CommonModule, SharedModule, EntityModule],
})
export class OrganizationModule {}
