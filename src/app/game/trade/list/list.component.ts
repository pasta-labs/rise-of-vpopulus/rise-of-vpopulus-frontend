import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { clone } from 'lodash';

@Component({
  selector: 'app-trade-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class TradeListComponent implements OnInit {
  isLoading = false;

  list = [];
  totalCount = 0;

  constructor(
    private graphQlService: GraphQlService,
    private httpService: GenericHttpService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.graphQlService
      .get(
        `game`,
        `{ trades { id createdOn toAccepted fromAccepted isInstant from { id name type } to { id name type } } }`
      )
      .subscribe((r: any) => {
        this.list = r.data.trades.map((x) => {
          var entity = clone(
            localStorage.getItem(`entityId`) === x.from.id ? x.to : x.from
          );
          if (entity.type === 'NationalOrganization')
            entity.type = 'Organisation';
          return {
            ...x,
            entity,
          };
        });
        this.isLoading = false;
      });
  }
}
