import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TradeListComponent } from './list/list.component';
import { TradeComponent } from './trade/trade.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TradeItemsComponent } from './trade/components/items/items.component';
import { TradeCurrenciesComponent } from './trade/components/currencies/currencies.component';

@NgModule({
  declarations: [TradeComponent, TradeListComponent, TradeItemsComponent, TradeCurrenciesComponent],
  imports: [CommonModule, SharedModule],
})
export class TradeModule { }
