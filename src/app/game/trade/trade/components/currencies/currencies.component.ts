import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { orderBy } from 'lodash-es';

@Component({
  selector: 'app-trade-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.scss']
})
export class TradeCurrenciesComponent implements OnInit {
  isLoading = false;

  currencies = [];

  @Input() trade;
  @Output() reload = new EventEmitter<boolean>();

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.load();
  }

  load = () => {
    this.isLoading = true;
    this.httpService.get(`/api/currency`).subscribe((r) => {
      this.currencies = orderBy(r, x => x.name, 'asc');
      this.isLoading = false;
    });
  }

  addCurrency(item) {
    this.isLoading = true;
    this.httpService
      .post(`/api/trades/items`, {
        tradeId: this.trade.id,
        currency: { currencyId: item.id, amount: item.providedAmount },
      })
      .subscribe((r) => {
        this.messageService.success(
          `${item.providedAmount} ${this.translateService.instant(item.name)} ${this.translateService.instant('Economy.Trade.beenAddedTransaction')}`
        );
        item.providedAmount = undefined;
        this.isLoading = false;
        this.reload.emit(true);
      });
  }

}
