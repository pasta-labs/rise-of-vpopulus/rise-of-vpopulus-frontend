import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { orderBy } from 'lodash-es';

@Component({
  selector: 'app-trade-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class TradeItemsComponent implements OnInit {
  isLoading = false;

  inventory = [];

  @Input() trade;
  @Output() reload = new EventEmitter<boolean>();

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.load();
  }

  load = () => {
    this.isLoading = true;
    this.httpService.get(`/api/inventory`).subscribe((r) => {
      this.inventory = orderBy(r, ['name', 'quality'], ['asc', 'asc']);
      this.isLoading = false;
    });
  }

  addItem(item) {
    this.isLoading = true;
    this.httpService
      .post(`/api/trades/items`, {
        tradeId: this.trade.id,
        item: {
          itemId: item.id,
          quality: item.quality,
          amount: item.providedAmount,
        },
      })
      .subscribe((r) => {
        this.messageService.success(
          `${item.providedAmount} ${this.translateService.instant(item.name)} ${this.translateService.instant('Economy.Trade.beenAddedTransaction')}`
        );
        item.providedAmount = undefined;
        this.isLoading = false;
        this.reload.emit(true);
      });
  }

}
