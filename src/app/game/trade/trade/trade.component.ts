import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Apollo, gql } from 'apollo-angular';
import { NzMessageService } from 'ng-zorro-antd/message';
import { firstValueFrom } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TradeComponent implements OnInit {
  isLoading = false;

  type;
  trade;

  goodsListAvailable = [];

  listenerStarted = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private apollo: Apollo,
    private graphQlService: GraphQlService,
    private translateService: TranslateService
  ) { }


  ngOnInit(): void {
    this.route.params.subscribe((r) => {
      this.type = this.router.url.split('/')[1];
      if (this.type === "trades") this.type = "trade";
      if (r['id']) this.loadTradeById(r['id']);
      else if (r['entityType'] && r['entityId']) this.loadTradeByEntity(r['entityType'], r['entityId']);
    });
  }

  loadTradeByEntity = async (type, id) => {
    this.isLoading = true;
    const tradeResponse: any = await firstValueFrom(this.graphQlService
      .get(`game`, `{ tradeByEntity(type: "${this.type}", entityType: "${type}", entityId: "${id}") { id createdOn toAccepted fromAccepted isInstant from { id name type } to { id name type } } }`));
    this.trade = tradeResponse.data.tradeByEntity;
    console.log('trade', this.trade)
    this.initTrade();
  }

  loadTradeById = async (id) => {
    this.isLoading = true;
    const tradeResponse: any = await firstValueFrom(this.graphQlService
      .get(`game`, `{ tradeById(id: "${id}") { id createdOn toAccepted fromAccepted isInstant from { id name type } to { id name type } } }`));
    this.trade = tradeResponse.data.tradeById;
    console.log('trade', this.trade)
    this.initTrade();
  }

  initTrade = () => {
    this.trade.items = [];

    if ((this.trade.isInstant === false && this.trade.toAccepted && this.trade.fromAccepted) || (this.trade.isInstant === true && this.trade.fromAccepted))
      this.trade.isFinished = true;
    else this.trade.isFinished = false;

    if (this.trade.to.type === 1005) this.goodsListAvailable = ['currencies'];
    else this.goodsListAvailable = ['items', 'currencies'];

    if (!this.listenerStarted) this.listenForUpdates();

    this.loadTradeItems();
  }

  listenForUpdates = () => {
    var tradeRef = this.apollo.use(`game`).watchQuery({
      fetchPolicy: 'no-cache',
      query: gql`{ __typename }`,
    });

    tradeRef.subscribeToMore({
      document: gql`subscription refreshTrade { gamepartyrefresh(id: "${this.trade.id}") }`,
      updateQuery: () => {
        this.listenerStarted = true;
        this.loadTradeById(this.trade.id);
      },
    });
  }

  isGoodTypeAvailable = (symbol) => this.goodsListAvailable.find(x => x === symbol) != null

  loadTradeItems = () => {
    this.isLoading = true;
    this.httpService
      .get(`/api/trades/${this.trade.id}/items`)
      .subscribe((items) => {
        if (this.trade.isInstant === true) this.trade.items[0] = {
          showAccept: this.trade.from.id === localStorage.getItem(`entityId`) && this.trade.fromAccepted === false,
          isAccepted: this.trade.fromAccepted,
          title: this.trade.from.id === localStorage.getItem(`entityId`) ? `To ` + this.trade.to.name : "From " + this.trade.to.name, items: items
        };
        else {
          this.trade.items[0] = {
            showAccept: this.trade.fromId === localStorage.getItem(`entityId`) && this.trade.fromAccepted === false,
            isAccepted: this.trade.fromAccepted,
            title: this.trade.from.id === localStorage.getItem(`entityId`) ? JSON.parse(localStorage.getItem(`profile`))['name'] : this.trade.to.name,
            items: items.filter(x => x.entityId === this.trade.from.id)
          };
          this.trade.items[1] = {
            showAccept: this.trade.to.id === localStorage.getItem(`entityId`) && this.trade.toAccepted === false,
            isAccepted: this.trade.toAccepted,
            title: this.trade.to.id === localStorage.getItem(`entityId`) ? JSON.parse(localStorage.getItem(`profile`))['name'] : this.trade.to.name,
            items: items.filter(x => x.entityId === this.trade.to.id)
          };
        }
        this.isLoading = false;
      });
  }

  accept() {
    this.isLoading = true;
    this.httpService
      .put(`/api/trades/accept`, { tradeId: this.trade.id })
      .subscribe(() => {
        this.messageService.success(this.translateService.instant('Economy.Trade.youAcceptedTrade'));
        this.loadTradeById(this.trade.id);
      });
  }

  cancel = () => {
    this.isLoading = true;
    this.httpService.delete(`/api/trades/${this.trade.id}`).subscribe(() => {
      this.messageService.success(this.translateService.instant('Economy.Trade.youCancelledTrade'));
      this.router.navigateByUrl(`/trades`);
    });
  }

  removeItem = (id) => {
    this.isLoading = true;
    this.httpService.delete(`/api/trades/items/${id}`).subscribe(() => {
      this.messageService.success(this.translateService.instant('Economy.Trade.itemHasRemoved'));
      this.loadTradeItems();
    });
  }

}
