import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { TradeListComponent } from './list/list.component';
import { TradeComponent } from './trade/trade.component';

export const routes: Routes = [
  { path: 'trades', component: TradeListComponent, canActivate: [AuthGuard] },
  {
    path: 'trades/:id',
    component: TradeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'trade/:entityType/:entityId',
    component: TradeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'donate/:entityType/:entityId',
    component: TradeComponent,
    canActivate: [AuthGuard],
  },
];
