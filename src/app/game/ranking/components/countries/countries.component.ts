import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-ranking-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss'],
})
export class RankingCountriesComponent implements OnInit {
  isLoading = true;

  list = [];

  page = 1;
  pageSize = 25;

  constructor(private httpService: GenericHttpService) {}

  ngOnInit() {
    this.getRankingCountries();
  }

  sortCitizensCount = (a, b) => a.citizenCount - b.citizenCount;
  sortRegionsCount = (a, b) => a.regionsCount - b.regionsCount;
  sortWinProgress = (a, b) =>
    a.winConditionPercentage - b.winConditionPercentage;

  getRankingCountries() {
    this.isLoading = true;
    this.httpService
      .get(
        `/api/ranking/countries?&page=${this.page}&pageSize=${this.pageSize}`
      )
      .subscribe((r) => {
        this.list = r.list.map((x) => {
          return {
            ...x,
            id: r.list.indexOf(x) + 1,
            winConditionPercentage: Math.round(x.winConditionPercentage),
          };
        });
        console.log('list', this.list)
        this.isLoading = false;
      });
  }
}
