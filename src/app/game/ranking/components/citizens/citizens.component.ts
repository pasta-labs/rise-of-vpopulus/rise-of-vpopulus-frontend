import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-ranking-citizens',
  templateUrl: './citizens.component.html',
  styleUrls: ['./citizens.component.scss'],
})
export class RankingCitizensComponent implements OnInit {
  isLoading = true;

  list = [];
  @Input() selectedCountry: EventEmitter<String>;
  currentSelectedCountry;

  page = 1;
  pageSize = 25;
  totalEntries = 0;

  orderBy = 'experience|descend';

  constructor(private httpService: GenericHttpService) {}

  ngOnInit() {
    this.getRankingCitizens(undefined);
    this.selectedCountry.subscribe((r) => {
      this.getRankingCitizens(r);
    });
  }

  getRankingCitizens(selectedCountry) {
    this.isLoading = true;
    this.currentSelectedCountry = selectedCountry;
    this.httpService
      .get(
        `/api/ranking/citizens?${
          selectedCountry === undefined ? '' : `countryId=${selectedCountry}`
        }&page=${this.page}&pageSize=${this.pageSize}&orderBy=${this.orderBy}`
      )
      .subscribe((r) => {
        this.list = r.list;
        console.log('list', r);
        this.totalEntries = r.totalEntries;
        this.isLoading = false;
      });
  }

  sortByChange(key, direction) {
    if (direction == null) this.orderBy = 'experience|descend';
    else this.orderBy = `${key}|${direction}`;
    this.getRankingCitizens(this.currentSelectedCountry);
  }

  pageIndexChange(index) {
    this.page = index;
    this.getRankingCitizens(this.currentSelectedCountry);
  }
}
