import { Routes } from '@angular/router';
import { RankingComponent } from './ranking/ranking.component';

export const routes: Routes = [
  { path: 'ranking', component: RankingComponent },
];
