import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RankingComponent } from './ranking/ranking.component';
import { RankingCountriesComponent } from './components/countries/countries.component';
import { RankingCitizensComponent } from './components/citizens/citizens.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    RankingComponent,
    RankingCitizensComponent,
    RankingCountriesComponent,
  ],
  imports: [CommonModule, SharedModule],
})
export class RankingModule {}
