import { Component, EventEmitter, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
})
export class RankingComponent implements OnInit {
  isLoading = false;

  countryList;

  selectedCountry;
  selectedType = 'citizens';
  selectedCountryAction = new EventEmitter();

  constructor(private httpService: GenericHttpService) {}

  ngOnInit(): void {
    this.getCountriesList();
  }

  changeSelectedCountry = (value) => {
    if (value === null) value = undefined;
    this.selectedCountryAction.emit(value);
  };

  getCountriesList() {
    this.httpService.get(`/api/countries`).subscribe((r) => {
      this.countryList = r;
    });
  }
}
