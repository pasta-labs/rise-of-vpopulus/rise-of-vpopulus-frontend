import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-mini-game-memo-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class MiniGameMemoViewComponent implements OnInit {
  isLoading = false;

  game;

  constructor(
    private graphQlService: GraphQlService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(r => {
      this.load(r.id);
    })
  }

  load = (id) => {
    this.isLoading = true;
    this.graphQlService.get(`minigames`, `{ memo(id: "${id}") { id isFinished size grid { id isSelected isHidden item { value } } creator { name } participant { name } } }`).subscribe((r: any) => {
      console.log('r', r.data.memo);
      this.game = r.data.memo;
      this.isLoading = false;
    });
  }

  select = ( index) => {
    this.isLoading = true;
    this.graphQlService.post(`minigames`, `mutation mutate($gameId: Guid, $selectedIndex: Int) { memoSelect(gameId: $gameId, selectedIndex: $selectedIndex) }`, { gameId: this.game.id, selectedIndex: index }).subscribe((r: any) => {
      this.load(this.game.id);
    });
  }

}
