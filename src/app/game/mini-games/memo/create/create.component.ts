import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-mini-game-memo-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class MiniGameMemoCreateComponent implements OnInit {
  isLoading = false;

  size = 3;

  constructor(
    private graphQlService: GraphQlService,
    private modalRef: NzModalRef,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  submit = () => {
    this.isLoading = true;
    this.graphQlService.post(`minigames`, `mutation mutate($size: Int) { memoCreate(size: $size) }`, { size: this.size }).subscribe((r: any) => {
      console.log('create', r);
      this.isLoading = false;
      this.modalRef.close(`ok`);
      this.router.navigateByUrl(`/mini-game/memo/${r.data.memoCreate}`);
    })
  }

}
