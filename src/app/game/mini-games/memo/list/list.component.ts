import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { MiniGameMemoCreateComponent } from '../create/create.component';

@Component({
  selector: 'app-mini-game-memo-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class MiniGameMemoListComponent implements OnInit {
  isLoading = false;

  list = [];

  constructor(
    private graphQlService: GraphQlService,
    private modalService: NzModalService
  ) { }

  ngOnInit(): void {
    this.load();
  }

  load = () => {
    this.isLoading = true;
    this.graphQlService.get(`minigames`, `{ memos { id isFinished size creator { name } participant { name } } }`).subscribe((r: any) => {
      console.log('r', r.data.memos);
      this.list = r.data.memos;
      this.isLoading = false;
    });
  }

  create = () => {
    this.modalService.create({
      nzContent: MiniGameMemoCreateComponent,
      nzTitle: "New game",
      nzFooter: null, nzWidth: '80%',
      nzMaskClosable: false
    });
  }

  join = (game) => {
    this.isLoading = true;
    this.graphQlService.post(`minigames`, `mutation mutate($gameId: Guid ) { memoJoin(gameId: $gameId) }`, { gameId: game.id }).subscribe((r: any) => {
      this.load();
    });
  }
}
