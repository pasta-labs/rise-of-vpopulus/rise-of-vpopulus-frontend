import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as signalR from '@aspnet/signalr';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';
import { clone } from 'lodash';

@Component({
  selector: 'app-mini-game-checkers',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class MiniGameCheckersComponent implements OnInit {
  gameId;
  game;

  grid = [];
  selector = 'x';

  opponent = {
    id: 'x',
    name: 'X',
  };

  selecting;

  currentEntityId;
  dragInProgress;

  hubConnection: signalR.HubConnection;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messageService: NzMessageService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.gameId = params.id;

      this.currentEntityId = localStorage.getItem(`entityId`);
      this.loadData();
      this.initSignalR();
    });
  }

  loadData() {
    this.httpService
      .get(`/api/games/checkers/${this.gameId}`)
      .subscribe((r) => {
        this.game = r;
        this.game.payload = JSON.parse(this.game.payload);

        if (this.currentEntityId === this.game.createdBy.id) {
          this.opponent = this.game.user;
        } else {
          this.opponent = this.game.createdBy;
        }

        if (this.game.payload['CurrentUser'] === this.game.user.id)
          this.selecting = this.game.user;
        else this.selecting = this.game.createdBy;

        console.log('game', this.game);
      });
  }

  draggable = {
    effectAllowed: 'all',
    disable: false,
    handle: false,
  };

  onDragStart(row, col) {
    this.dragInProgress = [row, col];
  }

  onDragCanceled(event: DragEvent) {
    this.dragInProgress = undefined;
  }

  onDrop(row, col) {
    var source = clone(this.dragInProgress);
    this.dragInProgress = undefined;
    this.httpService
      .get(
        `/api/games/checkers/move?gameId=${this.gameId}&fromRow=${source[0]}&fromCol=${source[1]}&toRow=${row}&toCol=${col}`
      )
      .subscribe(
        (r) => {
          this.hubConnection.invoke('UpdateGame', this.gameId);
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }

  initSignalR() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/tttHub`)
      .build();

    this.hubConnection
      .start()
      .then(() => {
        this.hubConnection.invoke(
          'ClientConnected',
          localStorage.getItem(`jwtToken`),
          this.gameId
        );

        this.hubConnection.on('UpdateGame', (watchers) => {
          this.loadData();
        });
      })
      .catch((err) =>
        console.log(`Error while starting SignalR connection: ${err}`)
      );
  }
}
