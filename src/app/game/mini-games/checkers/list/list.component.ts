import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-mini-game-checkers-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class MiniGameCheckersListComponent implements OnInit {
  active = [];
  queue = [];
  history = [];

  currentEntityId;

  constructor(
    private httpService: GenericHttpService,
    private router: Router,
    private messageService: NzMessageService
  ) {}

  ngOnInit(): void {
    this.currentEntityId = localStorage.getItem(`entityId`);

    this.loadData();
  }

  loadData() {
    this.httpService.get(`/api/games/checkers/active`).subscribe((list) => {
      this.active = list;
    });

    this.httpService.get(`/api/games/checkers/queue`).subscribe((list) => {
      this.queue = list;
    });

    this.httpService.get(`/api/games/checkers/history`).subscribe((list) => {
      this.history = list;
    });
  }

  create() {
    this.httpService.get(`/api/games/checkers/create`).subscribe(
      () => {
        this.loadData();
        this.messageService.success(`Game has been created!`);
      },
      (error) => {
        this.messageService.error(error.error.Message);
      }
    );
  }

  join(gameId) {
    this.httpService.get(`/api/games/checkers/join?gameId=${gameId}`).subscribe(
      (r) => {
        this.router.navigateByUrl(`/mini-game/checkers/${gameId}`);
      },
      (error) => {
        this.messageService.error(error.error.Message);
      }
    );
  }
}
