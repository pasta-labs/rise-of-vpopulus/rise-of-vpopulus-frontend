import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as signalR from '@aspnet/signalr';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-mini-games-ttt',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class TttViewComponent implements OnInit {
  gameId;
  game;

  grid = [];
  selector = 'x';

  opponent = {
    id: 'x',
    name: 'X',
  };

  selecting = 'x';

  currentEntityId;
  watchers = [];

  hubConnection: signalR.HubConnection;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private messageService: NzMessageService
  ) {}

  ngOnInit(): void {
    let index = 1;
    for (let row = 1; row <= 3; row++) {
      var entry = { row: row, cols: [] };
      for (let col = 1; col <= 3; col++) {
        entry.cols.push({
          index: index,
          col: col,
          value: '',
        });
        index += 1;
      }
      this.grid.push(entry);
    }
    this.route.params.subscribe((params) => {
      this.gameId = params.id;

      this.currentEntityId = localStorage.getItem(`entityId`);
      this.loadData();
      this.initSignalR();
    });
  }

  loadData() {
    this.httpService
      .get(`/api/games/tic-tac-toe/${this.gameId}`)
      .subscribe((r) => {
        this.game = r;
        this.game.payload = JSON.parse(r.payload);
        if (this.currentEntityId === this.game.createdBy.id) {
          this.opponent = this.game.user;
        } else {
          this.opponent = this.game.createdBy;
        }

        if (this.game.payload['CurrentUser'] === this.game.user.id)
          this.selecting = this.game.user.name;
        else this.selecting = this.game.createdBy.name;

        var index = 1;
        for (let row = 1; row <= 3; row++) {
          for (let col = 1; col <= 3; col++) {
            this.grid[row - 1].cols[col - 1].value = this.game.payload[
              'Grid'
            ].find((x) => x.Id === index).Value;
            index += 1;
          }
        }
      });
  }

  selectPlace(item) {
    this.httpService
      .get(
        `/api/games/tic-tac-toe/place?gameId=${this.gameId}&position=${item.index}`
      )
      .subscribe(
        (r) => {
          this.hubConnection.invoke('UpdateGame', this.gameId);
        },
        (error) => {
          if (error.error.Message === 'GAME_ENDED')
            this.messageService.error(`Game has ended!`);
          else this.messageService.error(error.error.Message);
        }
      );
  }

  initSignalR() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/tttHub`)
      .build();

    this.hubConnection
      .start()
      .then(() => {
        this.hubConnection.invoke(
          'ClientConnected',
          localStorage.getItem(`jwtToken`),
          this.gameId
        );

        this.hubConnection.on('UpdateGame', (watchers) => {
          this.watchers = watchers;
          this.loadData();
        });
      })
      .catch((err) =>
        console.log(`Error while starting SignalR connection: ${err}`)
      );
  }
}
