import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-mini-games-ttt-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class TttListComponent implements OnInit {
  active = [];
  queue = [];
  history = [];

  currentEntityId;

  constructor(
    private httpService: GenericHttpService,
    private router: Router,
    private messageService: NzMessageService
  ) {}

  ngOnInit(): void {
    this.currentEntityId = localStorage.getItem(`entityId`);

    this.loadData();
  }

  loadData() {
    this.httpService.get(`/api/games/tic-tac-toe/active`).subscribe((list) => {
      this.active = list;
    });

    this.httpService.get(`/api/games/tic-tac-toe/queue`).subscribe((list) => {
      this.queue = list;
    });

    this.httpService.get(`/api/games/tic-tac-toe/history`).subscribe((list) => {
      this.history = list;
    });
  }

  create() {
    this.httpService.get(`/api/games/tic-tac-toe/create`).subscribe(
      () => {
        this.loadData();
        this.messageService.success(`Game has been created!`);
      },
      (error) => {
        this.messageService.error(error.error.Message);
      }
    );
  }

  join(gameId) {
    this.httpService
      .get(`/api/games/tic-tac-toe/join?gameId=${gameId}`)
      .subscribe(
        (r) => {
          this.router.navigateByUrl(`/mini-game/tic-tac-toe/${gameId}`);
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }
}
