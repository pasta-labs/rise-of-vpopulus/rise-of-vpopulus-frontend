import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntityModule } from '../entity/entity.module';

import { TttListComponent } from './ttt/list/list.component';
import { TttViewComponent } from './ttt/view/view.component';

import { MiniGameBattleshipsViewComponent } from './battleships/view/view.component';

import { MiniGameCheckersComponent } from './checkers/view/view.component';
import { MiniGameCheckersListComponent } from './checkers/list/list.component';

import { DndModule } from 'ngx-drag-drop';
import { MiniGameMemoListComponent } from './memo/list/list.component';
import { MiniGameMemoViewComponent } from './memo/view/view.component';
import { MiniGameMemoCreateComponent } from './memo/create/create.component';

@NgModule({
  declarations: [
    TttListComponent,
    TttViewComponent,
    MiniGameBattleshipsViewComponent,
    MiniGameCheckersComponent,
    MiniGameCheckersListComponent,
    MiniGameMemoListComponent,
    MiniGameMemoViewComponent,
    MiniGameMemoCreateComponent,
  ],
  imports: [CommonModule, SharedModule, EntityModule, DndModule],
})
export class MiniGamesModule {}
