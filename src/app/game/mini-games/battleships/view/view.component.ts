import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mini-game-battleships',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class MiniGameBattleshipsViewComponent implements OnInit {
  gameId;
  game;

  grid = [];
  selector = 'x';

  opponent = {
    id: 'x',
    name: 'X',
  };

  selecting = 'x';

  currentEntityId;

  constructor() {}

  ngOnInit(): void {}
}
