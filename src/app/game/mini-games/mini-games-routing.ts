import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';

import { TttListComponent } from './ttt/list/list.component';
import { TttViewComponent } from './ttt/view/view.component';

import { MiniGameBattleshipsViewComponent } from './battleships/view/view.component';

import { MiniGameCheckersComponent } from './checkers/view/view.component';
import { MiniGameCheckersListComponent } from './checkers/list/list.component';
import { MiniGameMemoListComponent } from './memo/list/list.component';
import { MiniGameMemoViewComponent } from './memo/view/view.component';

export const routes: Routes = [
  {
    path: 'mini-game/tic-tac-toe',
    component: TttListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mini-game/tic-tac-toe/:id',
    component: TttViewComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'mini-game/battleships/:id',
    component: MiniGameBattleshipsViewComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mini-game/checkers',
    component: MiniGameCheckersListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mini-game/checkers/:id',
    component: MiniGameCheckersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mini-game/memo',
    component: MiniGameMemoListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mini-game/memo/:id',
    component: MiniGameMemoViewComponent,
    canActivate: [AuthGuard],
  },
];
