import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { WarListComponent } from './list/list.component';
import { WarComponent } from './war/war.component';

export const routes: Routes = [
  { path: 'war/:id', component: WarComponent, canActivate: [AuthGuard] },
  { path: 'wars', component: WarListComponent, canActivate: [AuthGuard] },
];
