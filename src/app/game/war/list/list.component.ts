import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-war-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class WarListComponent implements OnInit {
  isLoading = true;
  apiUrl = environment.apiUrl;
  list = [];
  battles = [];

  stats = {
    attackerWon: 0,
    inProgress: 0,
    defenderWon: 0,
  };

  constructor(
    private graphQlService: GraphQlService,
    private httpService: GenericHttpService,
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ wars { id startDateTime endDateTime attacker { id name flagLocation(size: "xl") } defender { id name flagLocation(size: "xl") } } }`)
      .subscribe((r: any) => {
        this.list = r.data.wars;
        this.loadAllBattles();
        this.isLoading = false;
      });

    }

    loadAllBattles() {
      const battleRequests = this.list.map(war =>
        this.httpService.get(`/api/military/war/${war.id}/battles`).toPromise()
      );
  
      Promise.all(battleRequests).then(results => {
        results.forEach((battles, index) => {
          const war = this.list[index];
          war.attackerWon = battles.filter(
            (x) => x.winnerId && x.winnerId === war.attacker.id
          ).length;
          war.defenderWon = battles.filter(
            (x) => x.winnerId && x.winnerId === war.defender.id
          ).length;
          war.inProgress = battles.filter((x) => !x.winnerId).length;
        });
  
        this.isLoading = false;
      }).catch(error => {
        console.error('Error loading battles', error);
        this.isLoading = false;
      });
    }
  }