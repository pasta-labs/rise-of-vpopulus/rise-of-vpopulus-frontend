import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WarComponent } from './war/war.component';
import { WarListComponent } from './list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TimeLeftPipe } from 'src/app/shared/pipes/time-left.pipe';

@NgModule({
  declarations: [WarComponent, WarListComponent],
  imports: [CommonModule, SharedModule],
  providers: [],
})
export class WarModule {}
