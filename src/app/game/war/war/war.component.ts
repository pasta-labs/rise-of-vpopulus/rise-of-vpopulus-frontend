import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';
import { format, addDays, parseISO } from 'date-fns';

@Component({
  selector: 'app-war',
  templateUrl: './war.component.html',
  styleUrls: ['./war.component.scss'],
})
export class WarComponent implements OnInit {
  isLoading = false;
  apiUrl = environment.apiUrl;

  war;
  battles = [];

  stats = {
    attackerWon: 0,
    inProgress: 0,
    defenderWon: 0,
  };

  constructor(
    private graphQlService: GraphQlService,
    private httpService: GenericHttpService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      var warId = params['id'];

      this.loadWar(warId);
    });
  }

  loadWar(id) {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ war(id: "${id}") { id attacker { id name flagLocation(size: "xl") } defender { id name flagLocation(size: "xl") } } }`).subscribe((war: any) => {
      this.war = war.data.war;
      console.log(this.war)

      this.loadBattles();
    });
  }

  loadBattles() {
    this.isLoading = true;
    this.httpService
      .get(`/api/military/war/${this.war.id}/battles`)
      .subscribe((battles) => {
        this.battles = battles.sort((a, b) => {
          const dateA = new Date(a.endDateTime);
          const dateB = new Date(b.endDateTime);
  
          return dateB.getTime() - dateA.getTime();
        });
    
        this.stats.attackerWon = this.battles.filter(
          (x) => x.winnerId && x.winnerId === x.countryAttacking.id
        ).length;
        this.stats.inProgress = this.battles.filter((x) => !x.winnerId).length;
        this.stats.defenderWon = this.battles.filter(
          (x) => x.winnerId && x.winnerId === x.countryDefending.id
        ).length;
  
        this.battles.forEach((item) => {
          item.endDateTimeSeconds = item.endDateTimeSeconds * 1000;
        });
        console.log(this.battles)
        this.isLoading = false;
      });
  }
  formatDate(dateTimeString): string {
    if (!dateTimeString) {
      return '';
    }
    const date = new Date(dateTimeString);
    if (isNaN(date.getTime())) {
      return '';
    }
    return format(date, 'dd/MM/yyyy');
  }
}
