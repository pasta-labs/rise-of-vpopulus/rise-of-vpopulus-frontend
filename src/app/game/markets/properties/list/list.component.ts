import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-markets-properties-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class MarketPropertiesListComponent implements OnInit {
  isLoading = false;

  offers = [];

  types = [];

  countryId;
  country;

  selectedConstruction;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private graphQlService: GraphQlService,
    private router: Router,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    var profile = JSON.parse(localStorage.getItem(`profile`));
    if (profile && profile.region && profile.region.country) this.countryId = profile.region.country.id;
    if (!this.countryId) this.router.navigateByUrl(`/`);

    this.graphQlService.get(`game`, `{ country(id: "${this.countryId}"){ id name } }`).subscribe((r: any) => this.country = r.data.country);

    this.getConstructions();
  }

  getConstructions() {
    this.httpService.get(`/api/dictionaries/constructions`).subscribe((r) => {
      this.types = r.filter(x => x.symbol === 'Construction.House' || x.symbol === 'Construction.Hotel');
      this.selectedConstruction = this.types.find(x => x.symbol === 'Construction.House').id;
    });
  }
  getCurrentSelectedConstruction = () => this.types.find(x => x.id === this.selectedConstruction);

  loadProperties() {
    this.isLoading = true;
    this.httpService
      .get(`/api/markets/estate/${this.countryId}/${this.selectedConstruction}`)
      .subscribe((r) => {
        this.offers = r;
        this.isLoading = false;
      });
  }

  acceptOffer(id) {
    this.httpService
      .post(`/api/markets/estate/accept`, { offerId: id })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Economy.Construction.propertyBeenBought'));
          this.loadProperties();
        },
        (error) => {
          this.messageService.error(error.message);
        }
      );
  }

  deleteOffer(id) {
    this.httpService.delete(`/api/markets/estate/${id}`).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant('Economy.Construction.offerBeenDeleted'));
        this.loadProperties();
      },
      (error) => {
        this.messageService.error(error.message);
      }
    );
  }
}
