import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { MarketExchangeCreateComponent } from '../create/create.component';
import _ from 'lodash';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-markets-exchange-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MarketExchangeListComponent implements OnInit {
  isLoading = false;

  offers = undefined;
  showsOnlyMyOffers = false;

  currencies = [];
  currency;

  buyCurrencies = [];
  sellCurrencies = [];

  selectedBuyCurrencyId;
  selectedSellCurrencyId;

  chartData;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private graphQlService: GraphQlService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.graphQlService.get(`game`, `{ currencies { id symbol flagLocation(size: "s") isMainCurrency } }`).subscribe((r: any) => {
        console.log('r', r)
        this.currencies = r.data.currencies;
        this.buyCurrencies = this.currencies;
        this.sellCurrencies = this.currencies;

        if (params.b && params.s) {
          this.selectedBuyCurrencyId = params.b;
          this.selectedSellCurrencyId = params.s;
        } else {
          this.selectedSellCurrencyId = this.currencies.find(x => x.isMainCurrency === true).id;
          var profile = JSON.parse(localStorage.getItem(`profile`));
          if (profile && profile.region && profile.region.country && profile.region.country.currencyId) this.selectedBuyCurrencyId = profile.region.country.currencyId;
        }
        this.selectedCurrency();
      })
    })
  }

  selectedCurrency() {
    if (this.selectedBuyCurrencyId) {
      this.sellCurrencies = this.currencies.filter(
        (x) => x.id !== this.selectedBuyCurrencyId
      );
    }

    if (this.selectedSellCurrencyId) {
      this.buyCurrencies = this.currencies.filter(
        (x) => x.id !== this.selectedSellCurrencyId
      );
    }

    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: { b: this.selectedBuyCurrencyId, s: this.selectedSellCurrencyId }, queryParamsHandling: 'merge' });
    this.loadOffers();
  }

  switch() {
    var buyId = this.selectedBuyCurrencyId;
    this.selectedBuyCurrencyId = this.selectedSellCurrencyId;
    this.selectedSellCurrencyId = buyId;
    this.selectedCurrency();
  }

  loadOffers() {
    if (!this.selectedSellCurrencyId || !this.selectedBuyCurrencyId) return;

    this.isLoading = true;
    this.graphQlService.get(`game`, `{ exchangeOffers(fromCurrencyId: "${this.selectedBuyCurrencyId}", toCurrencyId: "${this.selectedSellCurrencyId}") { id amount rate owner { id name type } sellCurrency { symbol value flagLocation } buyCurrency { symbol value flagLocation } } }`).subscribe((r: any) => {
      this.offers = r.data.exchangeOffers.map((x) => {
        return {
          ...x,
          isOwner: x.owner.id === localStorage.getItem(`entityId`),
        };
      });
      this.showsOnlyMyOffers = false;
      this.isLoading = false;
    });

    this.chartData = undefined;
    this.graphQlService.get(`game`, `{ exchangeOffersValueHistory(fromCurrencyId: "${this.selectedBuyCurrencyId}", toCurrencyId: "${this.selectedSellCurrencyId}") }`).subscribe((r: any) => {
      var data = JSON.parse(r.data.exchangeOffersValueHistory);

      this.chartData = {
        labels: data.map(x => x.day),
        options: {
          responsive: true,
          maintainAspectRatio: false,
          interaction: {
            intersect: false,
            mode: 'index',
          },
          scales: { x: { title: { display: true, text: 'Game day' } } }
        },
        datasets: [
          {
            label: 'Maximum',
            data: data.map(x => x.max),
            fill: false
          },
          {
            label: 'Average',
            data: data.map(x => x.avg),
            fill: false
          },
          {
            label: 'Minimum',
            data: data.map(x => x.min),
            fill: false
          }
        ]
      };
    });
  }

  showMyOffers() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ myExchangeOffers { id amount rate sellCurrency { symbol value flagLocation } buyCurrency { symbol value flagLocation } } }`).subscribe((r: any) => {
      this.offers = r.data.myExchangeOffers;
      this.showsOnlyMyOffers = true;
      this.isLoading = false;
    });
  }

  addNewOffer() {
    var modal = this.modalService.create({
      nzContent: MarketExchangeCreateComponent,
      nzTitle: this.translateService.instant('Economy.Market.AddNewOffer'),
      nzWidth: '60%',
      nzFooter: null,
      nzData: {
        sellCurrency: this.currencies.filter(
          (x) => x.id === this.selectedSellCurrencyId
        )[0],
        buyCurrency: this.currencies.filter(
          (x) => x.id === this.selectedBuyCurrencyId
        )[0],
      },
    });
    modal.afterClose.subscribe((r) => {
      if (r === 'ok') this.loadOffers();
    });
  }

  acceptOffer(offer) {
    this.isLoading = true;
    console.log('offer', offer);
    this.httpService
      .post(`/api/markets/exchange/accept`, {
        offerId: offer.id,
        amount: offer.selectedAmount,
      })
      .subscribe(
        (r) => {
          this.loadOffers();
          var pricePerUnit = 1 / offer.rate;
          this.messageService.success(
            this.translateService.instant('Economy.Market.youExchanged') `${pricePerUnit * +offer.selectedAmount} ${offer.sellCurrency.name
            } for ${offer.selectedAmount} ${offer.buyCurrency.name}!`
          );
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  removeOffer(id) {
    this.isLoading = true;
    this.httpService.delete(`/api/markets/exchange/${id}`).subscribe(
      (r) => {
        this.loadOffers();
        this.messageService.success(this.translateService.instant('Economy.Market.OfferRemoved'));
        this.isLoading = false;
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }

  getCurrencyData = (id) => this.currencies.find(x => x.id == id);

}
