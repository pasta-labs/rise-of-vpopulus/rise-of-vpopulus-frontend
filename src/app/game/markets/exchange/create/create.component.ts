import { Component, Input, OnInit, inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-markets-exchange-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class MarketExchangeCreateComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  @Input() buyCurrency;
  @Input() sellCurrency;

  form: UntypedFormGroup;

  constructor(
    private messageService: NzMessageService,
    private httpService: GenericHttpService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,

  ) {}

  ngOnInit() {
    this.buyCurrency = this.nzModalData.buyCurrency;
    this.sellCurrency = this.nzModalData.sellCurrency;

    this.form = new UntypedFormGroup({
      sellCurrencyId: new UntypedFormControl(this.sellCurrency.id, [
        Validators.required,
      ]),
      buyCurrencyId: new UntypedFormControl(this.buyCurrency.id, [
        Validators.required,
      ]),
      amount: new UntypedFormControl(null, [
        Validators.required,
        Validators.min(0.0001),
      ]),
      rate: new UntypedFormControl(null, [
        Validators.required,
        Validators.min(0.0001),
      ]),
    });
    console.log('test',this.buyCurrency)
  }

  create() {
    this.isLoading = true;
    this.httpService
      .post(`/api/markets/exchange`, this.form.getRawValue())
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Economy.Market.offerAdded'));
          this.isLoading = false;
          this.modalRef.close('ok');
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
