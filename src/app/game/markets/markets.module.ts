import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';

import { MarketItemsListComponent } from './items/list/list.component';

import { MarketExchangeCreateComponent } from './exchange/create/create.component';
import { MarketExchangeListComponent } from './exchange/list/list.component';
import { MarketPropertiesListComponent } from './properties/list/list.component';
import { MarketJobsListComponent } from './jobs/list/list.component';
import { MarketRawsListComponent } from './raws/list/list.component';

@NgModule({
  declarations: [
    // = = = = Items
    MarketItemsListComponent,

    // = = = = Raws
    MarketRawsListComponent,

    // = = = = Jobs
    MarketJobsListComponent,

    // = = = = Properties
    MarketPropertiesListComponent,

    // = = = = Exchange
    MarketExchangeListComponent,
    MarketExchangeCreateComponent,
  ],
  imports: [CommonModule, SharedModule],
  providers: [],
})
export class MarketsModule {}
