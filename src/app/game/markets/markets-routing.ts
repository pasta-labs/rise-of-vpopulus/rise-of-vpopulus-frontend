import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { MarketExchangeListComponent } from './exchange/list/list.component';
import { MarketItemsListComponent } from './items/list/list.component';
import { MarketJobsListComponent } from './jobs/list/list.component';
import { MarketPropertiesListComponent } from './properties/list/list.component';
import { MarketRawsListComponent } from './raws/list/list.component';

export const marketsRoutes: Routes = [
  {
    path: 'market/items',
    component: MarketItemsListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'market/raws',
    component: MarketRawsListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'market/jobs',
    component: MarketJobsListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'market/properties',
    component: MarketPropertiesListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'market/exchange',
    component: MarketExchangeListComponent,
    canActivate: [AuthGuard],
  }
];
