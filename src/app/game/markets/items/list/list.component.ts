import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

import { NzMessageService } from 'ng-zorro-antd/message';
import { head } from "lodash";

import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { NzMarks } from 'ng-zorro-antd/slider';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-markets-items-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MarketItemsListComponent implements OnInit {
  isLoading = false;

  offers;

  countryId;
  country;

  selectedItem;
  selectedQuality;

  industryTypes = [];
  qualities = [
    { key: 1, quality: 'Economy.Quality.Low' },
    { key: 2, quality: 'Economy.Quality.Medium' },
    { key: 3, quality: 'Economy.Quality.High' },
  ];

  chartData;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService,
    private graphQlService: GraphQlService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.isLoading = true;

    var profile = JSON.parse(localStorage.getItem(`profile`));
    if (profile && profile.region && profile.region.country) this.countryId = profile.region.country.id;
    if (!this.countryId) this.router.navigateByUrl(`/`);

    this.graphQlService.get(`game`, `{ country(id: "${this.countryId}"){ id name } }`).subscribe((r: any) => this.country = r.data.country);

    this.activatedRoute.queryParams.subscribe(params => {
      this.httpService.get(`/api/dictionaries/items`).subscribe((r) => {
        this.industryTypes = r;

        if (params.i && params.q) {
          this.selectedItem = params.i;
          this.selectedQuality = +params.q;
        } else {
          this.selectedItem = head(r)['id'];
          this.selectedQuality = 0;
        }
        this.loadMarketItems();
      })
    });
  }

  loadMarketItems() {
    this.isLoading = true;
    var path = `/api/economic/markets/items/${this.countryId}?${this.selectedItem != undefined ? `industryType=${this.selectedItem}&` : ''
      }${this.selectedQuality != undefined ? `quality=${this.selectedQuality}` : ''
      }`;

    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: { i: this.selectedItem, q: this.selectedQuality }, queryParamsHandling: 'merge' });

    this.httpService.get(path).subscribe((r) => {

      this.offers = r.map(_ => {
        var marks: NzMarks = { 0: '0' };
        marks[_.amount] = `${_.amount}`
        return { ..._, marks: marks }
      });
      this.isLoading = false;
    });

    this.chartData = undefined;
    if (this.selectedQuality < 1) return;
    this.graphQlService.get(`game`, `{ itemsMarketOffersValueHistory(countryId: "${this.countryId}", productId: "${this.selectedItem}", quality: ${this.selectedQuality}) }`).subscribe((r: any) => {
      var data = JSON.parse(r.data.itemsMarketOffersValueHistory);
      this.chartData = {
        labels: data.map(x => x.day),
        options: {
          responsive: true,
          maintainAspectRatio: false,
          interaction: {
            intersect: false,
            mode: 'index',
          },
          scales: { x: { title: { display: true, text: 'Game day' } } }
        },
        datasets: [
          {
            label: 'Maximum',
            data: data.map(x => x.max),
            fill: false
          },
          {
            label: 'Average',
            data: data.map(x => x.avg),
            fill: false
          },
          {
            label: 'Minimum',
            data: data.map(x => x.min),
            fill: false
          }
        ]
      };
    });
  }

  acceptOffer(offer) {
    this.isLoading = true;
    this.httpService
      .post(`/api/economic/markets/items/accept`, {
        offerId: offer.id,
        amount: offer.amountSelected,
      })
      .subscribe(
        (r) => {
          this.messageService.success(
            this.translateService.instant(
              `Economy.Market.Items.PurchaseSuccessful`,
              {
                amount: offer.amountSelected,
                itemName: this.translateService.instant(
                  `Economy.` + offer.industry.name
                ),
                priceAmount: offer.price * offer.amountSelected,
                priceCurrencyName: offer.priceCurrencyName,
              }
            )
          );
          this.loadMarketItems();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  selectedItemData = () => this.industryTypes.find(x => x.id === this.selectedItem);

}
