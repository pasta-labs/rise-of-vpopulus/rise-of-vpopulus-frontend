import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-markets-jobs-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class MarketJobsListComponent implements OnInit {
  isLoading = false;

  countryId = undefined;
  country;

  offers = [];

  selectedSkill = 0;
  selectedQuality = 0;

  qualities = [
    { key: 1, quality: 'Quality.Low' },
    { key: 2, quality: 'Quality.Medium' },
    { key: 3, quality: 'Quality.High' },
  ];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isLoading = true;

    var profile = JSON.parse(localStorage.getItem(`profile`));
    if (profile && profile.region && profile.region.country) this.countryId = profile.region.country.id;
    if (!this.countryId) window.location.href = '/';

    this.graphQlService.get(`game`, `{ country(id: "${this.countryId}"){ id name } }`).subscribe((r: any) => this.country = r.data.country);

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.s && params.q) {
        this.selectedSkill = +params.s;
        this.selectedQuality = +params.q;
      }
      this.loadJobs();
    });
  }


  loadJobs() {
    this.isLoading = true;
    this.router.navigate([], { relativeTo: this.activatedRoute, queryParams: { s: this.selectedSkill, q: this.selectedQuality }, queryParamsHandling: 'merge' });

    this.graphQlService.get(`game`, `{
      jobOffers(countryId:"${this.countryId}", skillId: ${this.selectedSkill}, quality: ${this.selectedQuality}) {
        id amount quality skillRequired salaryCurrency { value symbol id } salary 
        canAccept owner { id name avatar } industry { id value, symbol }
      }
    }`).subscribe((r: any) => {
      this.offers = r.data.jobOffers;
      this.isLoading = false;
    });
  }

  acceptOffer(item) {
    this.isLoading = true;
    this.httpService
      .post(`/api/economic/markets/jobs/accept`, { offerId: item.id })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Economy.Market.jobOfferAccepted'));
          this.isLoading = false;
          this.router.navigateByUrl(`/company/${item.owner.id}`)
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
    this.loadJobs();
  }
}
