import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzMarks } from 'ng-zorro-antd/slider';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-markets-raws-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class MarketRawsListComponent implements OnInit {
  isLoading = false;

  companies;
  selectedCompany;

  industryTypes = [];
  selectedItem;

  country;

  offers = [];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private graphQlService: GraphQlService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      var profile = JSON.parse(localStorage.getItem(`profile`));
      if (profile && profile.region && profile.region.country) this.country = profile.region.country;
      if (!this.country) this.router.navigateByUrl(`/`);

      this.graphQlService.get(`game`, `{ country(id: "${this.country.id}"){ id name } }`).subscribe((r: any) => this.country = r.data.country);

      this.loadRaws(params.r);
      this.loadCompanies(params.c);

      this.loadOffers();
    });
  }

  loadRaws = (defaultId) => {
    this.httpService.get(`/api/dictionaries/raws`).subscribe((r) => {
      this.industryTypes = r;

      if (defaultId) this.selectedItem = this.industryTypes.find(x => x.id === defaultId);
    });
  }

  loadCompanies = (defaultId) => {
    if (+localStorage.getItem(`entityType`) !== 1001) {
      this.httpService
        .get(
          `/api/companies/${localStorage.getItem(
            `entityType`
          )}/${localStorage.getItem(`entityId`)}`
        )
        .subscribe((r) => {
          this.companies = r.filter((x) => !x.stock.type.isRaw);
          if (defaultId) this.selectedCompany = this.companies.find(x => x.id === defaultId);
          this.loadOffers();
        });
    }
  }

  loadOffers = () => {
    if (!this.selectedItem || !this.country) return;

    this.isLoading = true;
    this.httpService
      .get(`/api/markets/raws/${this.country.id}/${this.selectedItem.id}`)
      .subscribe((r) => {
        this.offers = r.map(_ => {
          var marks: NzMarks = { 0: '0' };
          marks[_.amount] = `${_.amount}`
          return { ..._, marks: marks }
        });
        console.log('offers', this.offers)
        this.isLoading = false;
      });

  }

  selectCompany() {
    this.selectedItem = this.industryTypes.find(x => x.id == this.selectedCompany.rawStock.type.id)
    this.loadOffers();
  }

  acceptOffer(offer) {
    if(!this.selectCompany)
    {
      this.messageService.error(this.translateService.instant('Economy.companyNotSelected'))
      return;
    }

    this.isLoading = true;
    this.httpService
      .post(`/api/markets/raws/accept`, {
        companyId: this.selectedCompany.id,
        offerId: offer.id,
        amount: offer.amountSelected,
      })
      .subscribe(
        (r) => {
          this.messageService.success(
            this.translateService.instant('Economy.purchaseSuccess', {
              amount: offer.amountSelected,
              company: this.selectedCompany.name
            })
          );
          this.loadOffers();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
