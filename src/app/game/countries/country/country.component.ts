import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep, sum } from 'lodash-es';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'],
})
export class CountryComponent implements OnInit {
  apiUrl;

  isLoading = [true];
  mapData = undefined;

  id;
  country;

  totalPopulation = 0;

  tabIndex = 0;
  tabsDictionary = [
    { i: 0, l: 'society' },
    { i: 1, l: 'regions' },
    { i: 2, l: 'economy' },
    { i: 3, l: 'politics' },
    { i: 4, l: 'military' },
    { i: 5, l: 'administration' },
    { i: 6, l: 'citizenship' },
  ];

  requestCitizenshipModalVisible = false;
  requestCitizenshipReason;

  currentRoles;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService,
    private location: Location,
    private graphQlService: GraphQlService,
    private modalRef: NzModalService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.apiUrl = environment.apiUrl;
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      if (params.tab) this.tabIndex = this.getTabIndex(params.tab);

      if (!this.id) this.router.navigateByUrl(`/`);

      // this.httpService.get(`/api/maps/by/country/${this.id}`).subscribe((r) => {
      //   this.mapData = r;
      // });

      this.loadMain();
    });
  }

  hasCitizenship() {
    this.httpService
      .get(`/api/countries/${this.id}/members/current`)
      .subscribe((r) => this.currentRoles = r);
  }

  loadMain() {
    this.isLoading[0] = true;
    this.graphQlService.get(`game`, `{ country(id: "${this.id}"){ id name population flagLocation(size: "xl") governmentType borderClosedUntilGameDay protectionEnabledUntilGameDay capital { id name } } }`).subscribe((r: any) => {
      this.country = cloneDeep(r.data.country);
      this.hasCitizenship();
      this.isLoading[0] = false;
    })
  }

  joinCountry() {
    this.isLoading[2] = true;
    this.httpService
      .post(`/api/countries/members/requests`, { countryId: this.id, reason: this.requestCitizenshipReason })
      .subscribe(
        (r) => {
          this.message.success(this.translateService.instant('Citizen.citizenshipRequestSent'));
          this.requestCitizenshipReason = undefined;
          this.isLoading[2] = false;
          this.hasCitizenship();
          this.modalRef.closeAll();
        },
        (error) => {
          this.message.error(error.message);
          this.isLoading[2] = false;
        }
      );
  }

  getTabIndex(tabName) {
    var tab = this.tabsDictionary.find((x) => x.l === tabName.toLowerCase());
    if (tab) return tab.i;
    return 0;
  }

  onTabIndexChanged(tab) {
    this.location.replaceState(
      `/country/${this.id}/${this.tabsDictionary.find((x) => x.i === tab.index)?.l || 0
      }`
    );
  }
}
