import { Component, Input } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-regions',
  templateUrl: './country-regions.component.html',
  styleUrl: './country-regions.component.scss'
})
export class CountryRegionsComponent {
  isLoading = false;
  resources = [
    { symbol: 'wood', value: '../../../../../../assets/img/icons/wood.png' },
    { symbol: 'symbol2', quality: 'low', value: 'symbol2.png' },
  ];

  imageMap = {
    symbol1: '../../../../../../assets/img/icons/wood.png',
    symbol2: 'symbol2.png',
  };

  regions = [];

  @Input() countryId;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.loadRegions();
  }

  loadRegions() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ regions(countryId: "${this.countryId}") { id x y name population companies houses hotels hospital defenseSystem  resources { symbol value quality } } }`).subscribe((r: any) => {
      this.regions = r.data.regions.map(x => ({ ...x, name: x.name ? x.name : `${x.x}, ${x.y}` }));
      // this.country.population = sum(this.regions.map(x => x.population));
      this.isLoading = false;
    })
  }

}
