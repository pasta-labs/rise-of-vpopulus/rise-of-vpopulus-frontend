import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { keys } from 'lodash';
import { Chart } from '@antv/g2';
import { NzModalService } from 'ng-zorro-antd/modal';
import { CountryManageGovComponent } from '../manage-gov/manage-gov.component';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { firstValueFrom } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-country-politics',
  templateUrl: './politics.component.html',
  styleUrls: ['./politics.component.scss'],
})
export class CountryPoliticsComponent implements OnInit {
  @Input() country;

  elections;
  powerByWings;

  governmentMembersByRole = [];
  government = [];

  @Input() currentRoles = [];

  canManage = false;

  canStartCivilWar: any = {
    status: false,
    reason: ''
  }

  constructor(
    private httpService: GenericHttpService,
    private router: Router,
    private modalService: NzModalService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit() {
    this.load();
  }

  load = async () => {
    const civilWarButtonStatusRepsonse = await firstValueFrom(this.graphQlService.get(`game`, `{ countryCanStartCivilWar(countryId: "${this.country.id}") }`));
    if (civilWarButtonStatusRepsonse.errors) {
      this.canStartCivilWar = { status: false };
    } else {
      this.canStartCivilWar = { status: true };
    }

    this.httpService
      .get(`/api/countries/${this.country.id}/politics/separation-of-powers`)
      .subscribe((r) => {
        this.generateChartPowerByWings(
          `chart1`,
          keys(r.politicalSeparationByWing).map((x) => {
            return { type: x, value: r.politicalSeparationByWing[x] };
          })
        );
        this.generateChartPowerByWings(
          `chart2`,
          keys(r.politicalSeparationByParty).map((x) => {
            return { type: x, value: r.politicalSeparationByParty[x] };
          })
        );
      });
    this.loadGovernment();
  }

  ngAfterViewInit() {
    if (this.currentRoles.includes('CountryPresident')) this.canManage = true;
  }

  loadGovernment() {
    this.httpService
      .get(`/api/countries/${this.country.id}/members/government`)
      .subscribe((r) => {
        this.governmentMembersByRole = r;
        console.log(this.governmentMembersByRole)
        if (this.country.governmentType === 1) {
          this.government.push({ type: "Presidents", members: r.filter(x => x.role === "CountryPresident"), colSpan: 8 });
        } else if (this.country.governmentType === 2) {
          this.government.push({ type: "Presidents", members: r.filter(x => x.role === "CountryPresident" || x.role === "ViceCountryPresident"), colSpan: 8 });
          this.government.push({ type: "Ministers", members: r.filter(x => x.role.includes("Minister")), colSpan: 6 });
          this.government.push({ type: "Congress", members: r.filter(x => x.role === "CongressMember" && x.citizen), colSpan: 4 });
        }        
      });
  }

  generateChartPowerByWings(id, data) {
    const chart = new Chart({
      id: id,
      container: id,
      autoFit: true,
      height: 300,
      padding: [20, 40],
    });
  
    const translatedData = data.map(item => ({
      type: this.translateService.instant(`Politics.Laws.${item.type}`),
      value: item.value
    }));
  
    chart.coordinate('theta', {
      startAngle: Math.PI,
      endAngle: Math.PI * 2,
      innerRadius: 0.5,
    });
    
    chart.data(translatedData); 
    chart.tooltip({
      showTitle: false,
      showMarkers: false,
    });
    
    chart
      .interval()
      .adjust('stack')
      .position('value')
      .color('type')
      .label('value', {
        offset: 15,
      })
      .style({
        lineWidth: 2,
        stroke: '#fff',
      });
    
    chart.interaction('element-active');
    chart.render();
  }
  

  startCivilWar() {
    this.httpService
      .post(`/api/military/battles/civil-war`, { countryId: this.country.id })
      .subscribe((r) => {
        this.router.navigateByUrl(`/battle/${r}`);
      });
  }

  editGov = () => {
    this.modalService.create({
      nzWidth: '60%',
      nzTitle: this.translateService.instant('Politics.manageGovernment'),
      nzContent: CountryManageGovComponent,
      nzData: {
        country: this.country,
        governmentMembersByRole: this.governmentMembersByRole
      },
      nzFooter: null
    })
  }

}
