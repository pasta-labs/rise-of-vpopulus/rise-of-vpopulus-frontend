import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { CountryLawProposeComponent } from '../../../laws/propose/propose.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-country-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss'],
})
export class CountryAdministrationComponent implements OnInit {
  isLoading = false;

  @Input() countryId;

  laws = [];

  @Input() currentRoles = [];

  constructor(
    private httpService: GenericHttpService,
    private modalService: NzModalService,
    private cd: ChangeDetectorRef,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.loadLaws();
  }

  loadLaws() {
    this.isLoading = true;
    this.httpService
      .get(`/api/countries/${this.countryId}/laws`)
      .subscribe((r) => {
        this.laws = r;

        this.isLoading = false;
        this.cd.detectChanges();
      });
  }

  showProposeLawModal() {
    var modal = this.modalService.create({
      nzTitle: this.translateService.instant('Politics.Laws.proposeNewLaw'),
      nzContent: CountryLawProposeComponent,
      nzData: {
        countryId: this.countryId
      },
      nzFooter: null,
      nzMaskClosable: false,
      nzWidth: '90%'
    });
    modal.afterClose.subscribe(() => {
      this.loadLaws();
    });
  }

  canPropose = () => {
    if (this.currentRoles.includes('CountryPresident') || this.currentRoles.includes('ViceCountryPresident') || this.currentRoles.includes('CongressMember') || this.currentRoles.includes('MinisterOfDefense')) return true;
    return false;
  }
}
