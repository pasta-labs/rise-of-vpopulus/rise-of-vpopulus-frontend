import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-country-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss'],
})
export class CountryRequestsComponent implements OnInit {
  @Input() countryId;

  requestsData;
  isLoading = false;

  canManage = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService,

  ) { }

  ngOnInit() {
    this.getCandidates();
    this.httpService.get(`/api/countries/${this.countryId}/members/current`).subscribe(r => {
      if (r.includes("CountryPresident") || r.includes("CongressMember")) this.canManage = true;
    })
  }

  getCandidates() {
    this.isLoading = true;
    this.httpService
      .get(`/api/countries/members/requests?countryId=${this.countryId}`)
      .subscribe(
        (r) => {
          this.requestsData = r;
          console.log('data', r)
          this.isLoading = false;
        },
        (error) => {
          this.isLoading = false;
        }
      );
  }

  changeCandidate(id, isAccepted) {
    const url = `/api/countries/members/requests/${isAccepted ? 'accept' : 'reject'
      }`;
    const payload = isAccepted
      ? { requesterCitizenId: id }
      : { requestCitizenId: id };
    this.httpService.post(url, payload).subscribe(
      (r) => {
        const mess = isAccepted
          ? this.translateService.instant('Citizen.candidateAccepted')
          : this.translateService.instant('Citizen.candidateRejected');
        this.messageService.success(mess);
        this.getCandidates();
      },
      (error) => {
        this.messageService.error(error.message);
      }
    );
  }
}
