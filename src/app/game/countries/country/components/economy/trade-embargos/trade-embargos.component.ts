import { Component, Input } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-economy-trade-embargos',
  templateUrl: './trade-embargos.component.html',
  styleUrl: './trade-embargos.component.scss'
})
export class CountryEconomyTradeEmbargosComponent {
  isLoading = false;

  @Input() countryId;

  list = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ countryLawsTradeEmbargo(countryId: "${this.countryId}") { id name flagLocation(size: "xl") } }`).subscribe((r: any) => {
      this.list = r.data.countryLawsTradeEmbargo.filter(y => y.id !== this.countryId);
      this.isLoading = false;
    });
  }
}
