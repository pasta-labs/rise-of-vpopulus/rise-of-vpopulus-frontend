import { Component, Input, OnInit } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-economy-trade-agreements',
  templateUrl: './trade-agreements.component.html',
  styleUrls: ['./trade-agreements.component.scss']
})
export class CountryEconomyTradeAgreementsComponent implements OnInit {
  isLoading = false;

  @Input() countryId;

  list = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ countryTradeAgreements(countryId: "${this.countryId}") { id name flagLocation(size: "xl") } }`).subscribe((r: any) => {
      this.list = r.data.countryTradeAgreements.filter(y => y.id !== this.countryId);
      this.isLoading = false;
    });
  }

}
