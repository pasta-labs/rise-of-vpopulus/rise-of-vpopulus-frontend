import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-country-economy',
  templateUrl: './economy.component.html',
  styleUrls: ['./economy.component.scss'],
})
export class CountryEconomyComponent implements OnInit {
  isLoading = false;

  @Input() countryId;

  taxes = [];
  filteredTaxes = [];

  constructor(private httpService: GenericHttpService) {}

  ngOnInit() {
    this.loadTaxes();
  }

  loadTaxes() {
    this.isLoading = true;
    this.httpService
      .get(`/api/countries/${this.countryId}/taxes`)
      .subscribe((taxes) => {
        this.taxes = taxes;
        this.showTaxes('Item');
        this.isLoading = false;
      });
  }

  showTaxes(type) {
    this.filteredTaxes = this.taxes.filter(
      (x) => x.product.name.split('.')[0] === type
    );
  }
}
