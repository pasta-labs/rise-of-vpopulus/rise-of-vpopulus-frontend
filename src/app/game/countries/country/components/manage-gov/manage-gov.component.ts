import { Component, Input, OnInit, inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { Subject, debounceTime } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-manage-gov',
  templateUrl: './manage-gov.component.html',
  styleUrls: ['./manage-gov.component.scss']
})
export class CountryManageGovComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  @Input() governmentMembersByRole;

  roles = [
    { key: "ViceCountryPresident", citizenId: undefined, citizen: undefined },
    { key: "MinisterOfDefense", citizenId: undefined, citizen: undefined },
    { key: "MinisterOfForeignAffairs", citizenId: undefined, citizen: undefined },
    { key: "MinisterOfEconomy", citizenId: undefined, citizen: undefined }
  ];
  citizens = [];

  searchChange$ = new Subject();

  constructor(
    private graphQlService: GraphQlService,
    private httpService: GenericHttpService,
    private modalRef: NzModalRef,
    private messageService: NzMessageService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.searchChange$.pipe(debounceTime(500)).subscribe((r) => {
      if (r.toString().length < 3) return;
      this.isLoading = true;
      this.graphQlService.get(`game`, `{ citizensByCountry(countryId: "${this.nzModalData.country.id}", searchValue: "${r}"){ id name avatar } }`)
        .subscribe((r) => {
          this.isLoading = false;

          if (r.errors) {
            this.graphQlService.displayErrors(r.errors);
            return;
          }

          this.citizens = r.data['citizensByCountry'];
        })
    });
  }

  searchCitizen(value) {
    this.searchChange$.next(value);
  }

  citizenForPositionSelected = (role, e) => {
    var item = this.roles.find(x => x.key === role.key);
    item.citizenId = e;
    item.citizen = this.citizens.find(x => x.id === e);
  }

  save = () => {
    this.isLoading = true;
    this.httpService.post(`/api/countries/government/set`, { positions: this.roles.map(x => ({ role: x.key, citizenId: x.citizenId })) }).subscribe(() => {
      this.messageService.success(this.translateService.instant('Politics.Countries.governmentBeenSet'));
      this.isLoading = false;
      this.modalRef.close(`ok`);
    }, error => {
      this.messageService.error(error.error.Message);
      this.isLoading = false;
    })
  }


}
