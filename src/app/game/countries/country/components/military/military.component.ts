import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-military',
  templateUrl: './military.component.html',
  styleUrls: ['./military.component.scss'],
})
export class CountryMilitaryComponent implements OnInit {
  isLoading = true;

  list;

  @Input() country;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService
      .get(`game`, `{ wars(countryId: "${this.country.id}") { id startDateTime endDateTime attacker { id name flagLocation(size: "s") } defender { id name flagLocation(size: "s") } } }`)
      .subscribe((r: any) => {
        this.list = r.data.wars;
        this.list.forEach(item => {
          item.startDateTime = item.startDateTime.replace('T', ' ').split('.')[0];
          item.endDateTime = item.endDateTime ? item.endDateTime.replace('T', ' ').split('.')[0] : undefined;
        })
        this.isLoading = false;
      });
  }
}
