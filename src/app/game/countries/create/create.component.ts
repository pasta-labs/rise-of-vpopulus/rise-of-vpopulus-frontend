import {
  HttpClient,
  HttpRequest,
  HttpHeaders,
  HttpEvent,
  HttpEventType,
  HttpResponse,
} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';
import { of } from 'zen-observable';

@Component({
  selector: 'app-country-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CountryCreateComponent implements OnInit {
  apiUrl = environment.apiUrl;
  profile;

  isLoading = false;

  form: UntypedFormGroup;
  flagLocation;
  flagData;

  loading = false;
  avatarUrl: string;

  color;
  borderSelect = 'Open';
  formatterDollar = (value: number) => `${value} $`;
  parserDollar = (value: string) => value.replace('$ ', '');

  constructor(
    private httpService: GenericHttpService,
    private router: Router,
    private messageService: NzMessageService,
    private translateService: TranslateService,
    private http: HttpClient
  ) {
    this.form = new UntypedFormGroup({
      name: new UntypedFormControl(null, [Validators.required]),
      flagLocation: new UntypedFormControl(null, [Validators.required]),
      currencyName: new UntypedFormControl(null, [Validators.required]),
      welcomeMessage: new UntypedFormControl('Hello everyone!', [Validators.required]),
      citizenFee: new UntypedFormControl(1, [Validators.required]),
      minimalWage: new UntypedFormControl(1, [Validators.required]),
      capitalRegionId: new UntypedFormControl(null, [Validators.required]),
      color: new UntypedFormControl(null, [Validators.required]),
      governmentType: new UntypedFormControl('Democracy', [Validators.required]),
      borderClosedUntilGameDay: new UntypedFormControl('Open', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.profile = JSON.parse(localStorage.getItem("profile"));
    this.form.controls['capitalRegionId'].setValue(this.profile.region.id);
    this.form.controls['flagLocation'].setValue(`${environment.apiUrl}/defaults/country.PNG`);
    this.flagLocation = `${environment.apiUrl}/defaults/country.PNG`;
  }

  register() {
    this.form.controls['color'].setValue(this.color);
    if (!this.form.valid) {
      this.messageService.error(
        this.translateService.instant('Politics.Countries.pleaseFillRegion')
      );
      return;
    }
    this.isLoading = true;
    this.form.controls['borderClosedUntilGameDay'].setValue(
      this.form.controls['borderClosedUntilGameDay'].value === 'Open' ? null : '99999'
    );

    this.httpService.post(`/api/countries`, this.form.getRawValue()).subscribe(
      (r) => {

        if (!this.flagData) this.router.navigateByUrl(`/country/${r}`);
        else {
          const formData = new FormData();
          formData.append('file', this.flagData.file as any);
          formData.append('type', 'country-icon');
          formData.append('managedEntityId', r);
          const req = new HttpRequest('POST', this.flagData.action!, formData, {
            headers: new HttpHeaders({ authorization: `Bearer ${localStorage.getItem(`jwtToken`)}` }),
            reportProgress: true,
            withCredentials: false,
          });

          this.http.request(req).subscribe(
            (event: HttpEvent<{}>) => {
              if (event instanceof HttpResponse) {
                this.router.navigateByUrl(`/country/${r}`);
              }
            },
            (err) => {
              this.messageService.error('Network error');
              this.isLoading = false;
            }
          );
        }
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }

  uploadDocument = (item) => {
    this.flagData = item;

    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(item.file as any);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => this.messageService.error(JSON.stringify(error));
    }).then(base64 => {
      this.flagLocation = base64;
    })
  };
}
