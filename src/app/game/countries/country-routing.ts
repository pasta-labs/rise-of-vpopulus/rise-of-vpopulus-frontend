import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { CountryComponent } from './country/country.component';
import { CountryCreateComponent } from './create/create.component';
import { CountryLawComponent } from './laws/law/law.component';

export const routes: Routes = [
  {
    path: 'country',
    component: CountryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'countries/create',
    component: CountryCreateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'country/:id',
    component: CountryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'country/:id/:tab',
    component: CountryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'country/:countryId/law/:lawId',
    component: CountryLawComponent,
    canActivate: [AuthGuard],
  },
];
