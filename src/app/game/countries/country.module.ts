import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryComponent } from './country/country.component';
import { CountryCreateComponent } from './create/create.component';
import { CountryLawProposeComponent } from './laws/propose/propose.component';
import { CountryLawComponent } from './laws/law/law.component';

import { CountryLawProposeBorderComponent } from './laws/propose/types/propose-border/propose-border.component';
import { CountryLawProposeCitizenFeeComponent } from './laws/propose/types/propose-citizen-fee/propose-citizen-fee.component';
import { CountryLawProposeConstructionComponent } from './laws/propose/types/propose-construction/propose-construction.component';
import { CountryLawProposeDeclareWarComponent } from './laws/propose/types/propose-declare-war/propose-declare-war.component';
import { CountryLawProposeMinimalWageComponent } from './laws/propose/types/propose-minimal-wage/propose-minimal-wage.component';
import { CountryLawProposeNationalOrganizationComponent } from './laws/propose/types/propose-national-organization/propose-national-organization.component';
import { CountryLawProposeNewWelcomeMessageComponent } from './laws/propose/types/propose-new-welcome-message/propose-new-welcome-message.component';
import { CountryLawProposePeaceComponent } from './laws/propose/types/propose-peace/propose-peace.component';
import { CountryLawProposePrintCurrencyComponent } from './laws/propose/types/propose-print-currency/propose-print-currency.component';
import { CountryLawProposeTaxesComponent } from './laws/propose/types/propose-taxes/propose-taxes.component';
import { CountryLawProposeTransferLeadershipComponent } from './laws/propose/types/propose-transfer-leadership/propose-transfer-leadership.component';
import { CountryLawProposeWithdrawCurrencyComponent } from './laws/propose/types/propose-withdraw-currency/propose-withdraw-currency.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CountryAdministrationComponent } from './country/components/administration/administration.component';
import { CountryEconomyComponent } from './country/components/economy/economy.component';
import { CountryMilitaryComponent } from './country/components/military/military.component';
import { CountryPoliticsComponent } from './country/components/politics/politics.component';
import { CountryRequestsComponent } from './country/components/requests/requests.component';
import { CountrySocialComponent } from './country/components/social/social.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { EntityModule } from '../entity/entity.module';
import { MapModule } from '../shared/maps/map.module';
import { PartyModule } from '../party/party.module';
import { ArmyModule } from '../army/army.module';
import { NewspaperCensorshipComponent } from './laws/propose/types/newspaper-censorship/newspaper-censorship.component';
import { CountriesLawsProposeChangeFlagComponent } from './laws/propose/types/change-flag/change-flag.component';
import { CountryManageGovComponent } from './country/components/manage-gov/manage-gov.component';
import { CountryEconomyTradeAgreementsComponent } from './country/components/economy/trade-agreements/trade-agreements.component';
import { CountryLawProposeTradeAgreementCancelComponent } from './laws/propose/types/propose-trade-agreement-cancel/propose-trade-agreement-cancel.component';
import { CountryLawProposeTradeAgreementComponent } from './laws/propose/types/propose-trade-agreement/propose-trade-agreement.component';
import { CountryLawProposeRegionNameChangeComponent } from './laws/propose/types/propose-region-name-change/propose-region-name-change.component';
import { NewspaperConsorshipLawProposeComponent } from './laws/propose/types/newspaper-consorship-law-propose/newspaper-consorship-law-propose.component';
import { CountryRegionsComponent } from './country/components/country-regions/country-regions.component';
import { CountryLawProposeTradeEmbargoComponent } from './laws/propose/types/propose-trade-embargo/propose-trade-embargo.component';
import { CountryEconomyTradeEmbargosComponent } from './country/components/economy/trade-embargos/trade-embargos.component';
import { ProposeTradeEmbargoCancelComponent } from './laws/propose/types/propose-trade-embargo-cancel/propose-trade-embargo-cancel.component';
import { RevokeCitizenshipLawComponent } from './laws/propose/types/revoke-citizenship-law/revoke-citizenship-law.component';

@NgModule({
  declarations: [
    CountryComponent,
    CountryCreateComponent,
    CountryAdministrationComponent,
    CountryEconomyComponent,
    CountryMilitaryComponent,
    CountryPoliticsComponent,
    CountryRequestsComponent,
    CountrySocialComponent,

    CountryLawProposeComponent,
    CountryLawComponent,

    CountryLawProposeBorderComponent,
    CountryLawProposeCitizenFeeComponent,
    CountryLawProposeConstructionComponent,
    CountryLawProposeDeclareWarComponent,
    CountryLawProposeMinimalWageComponent,
    CountryLawProposeNationalOrganizationComponent,
    CountryLawProposeNewWelcomeMessageComponent,
    CountryLawProposePeaceComponent,
    CountryLawProposePrintCurrencyComponent,
    CountryLawProposeTaxesComponent,
    CountryLawProposeTransferLeadershipComponent,
    CountryLawProposeWithdrawCurrencyComponent,
    CountryLawProposeTradeEmbargoComponent,
    NewspaperCensorshipComponent,
    CountriesLawsProposeChangeFlagComponent,
    CountryManageGovComponent,
    CountryEconomyTradeAgreementsComponent,
    CountryLawProposeTradeAgreementCancelComponent,
    CountryLawProposeTradeAgreementComponent,
    CountryLawProposeRegionNameChangeComponent,
    CountryEconomyTradeEmbargosComponent,
    ProposeTradeEmbargoCancelComponent,
    NewspaperConsorshipLawProposeComponent,
    CountryRegionsComponent,
    RevokeCitizenshipLawComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ColorPickerModule,
    EntityModule,
    MapModule,
    PartyModule,
    ArmyModule,
  ],
})
export class CountryModule {}
