import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-declare-war',
  templateUrl: './propose-declare-war.component.html',
  styleUrls: ['./propose-declare-war.component.scss'],
})
export class CountryLawProposeDeclareWarComponent implements OnInit {
  isLoading = true;

  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  countries = [];

  constructor(private graphQlService: GraphQlService, private httpService: GenericHttpService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      countryId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
    this.graphQlService.get(`game`, `{ countries{ id name } }`).subscribe((r: any) => {
      var countries = r.data.countries;
      this.httpService
        .get(`/api/countries/laws/propose/declare-war/countries`)
        .subscribe((response) => {
          this.countries = countries.filter((country) => response.includes(country.id));
          this.isLoading = false;
        });
    });
  };
}
