import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-country-law-propose-construction',
  templateUrl: './propose-construction.component.html',
  styleUrls: ['./propose-construction.component.scss'],
})
export class CountryLawProposeConstructionComponent implements OnInit {
  @Input() currentCountry;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  hospital;
  defenseSystem;
  constructions = [];
  organizations = [];

  constructor(private httpService: GenericHttpService, private translateService: TranslateService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      constructionTypeId: new UntypedFormControl(null, [Validators.required]),
      quality: new UntypedFormControl(null, [Validators.required]),
      regionId: new UntypedFormControl(null, [Validators.required]),
      nationalOrganizationId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))

    this.httpService.get(`/api/dictionaries/constructions`).subscribe((r) => {
      this.hospital = r.find(
        (construction) => construction.symbol === 'Construction.Hospital'
      );
      this.defenseSystem = r.find(
        (construction) => construction.symbol === 'Construction.DefenseSystem'
      );
      this.constructions = [this.hospital, this.defenseSystem];
      this.form.get(`constructionTypeId`).setValue(this.constructions[0].id)
    });
      this.httpService
        .get(`/api/countries/${this.currentCountry.id}/organisations`)
        .subscribe((response) => {
          this.organizations = response;
        });
  }

  regionSelected = (region) => this.form.get(`regionId`).setValue(region.id);

  getRequirements = () => {
    const con = this.form.get('constructionTypeId').value;
    const object = this.constructions.find((constr) => constr.id === con);
    const requirements = `<b>Required resource:</b> ${this.translateService.instant("Economy." + object?.itemPayload.rawResourceSymbol)}<br/><b>Amount:</b> ${object?.itemPayload.rawsMaterialRequiredPerLevel[this.form.get(`quality`).value]}`;
    return requirements;
  }
}
