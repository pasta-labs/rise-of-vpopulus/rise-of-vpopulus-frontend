import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, debounceTime } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-revoke-citizenship-law',
  templateUrl: './revoke-citizenship-law.component.html',
  styleUrl: './revoke-citizenship-law.component.scss'
})
export class RevokeCitizenshipLawComponent {
  isLoading = true;

  @Input() form: FormGroup;
  @Input() countryId;
  @Output() formChange = new EventEmitter<FormGroup>();

  citizens = [];
  selectedCitizen = undefined;
  citizenSearchValue = undefined;
  searchChange$ = new BehaviorSubject('');

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      targetCitizenId: new FormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form));
    this.form.get(`targetCitizenId`).valueChanges.subscribe((cId) => this.selectedCitizen = this.citizens.find(x => x.id === cId));
    this.searchChange$
      .asObservable()
      .pipe(debounceTime(1000))
      .subscribe(r => {
        this.graphQlService.get(`game`, `{ citizensByCountry(countryId: "${this.countryId}", searchValue: "${r}") { id name avatar } }`).subscribe(cResponse => {
          this.citizens = cResponse.data['citizensByCountry'];
          this.isLoading = false;
        })
      })
  };

  onSearch(value: string): void {
    this.isLoading = true;
    this.searchChange$.next(value);
  }
}
