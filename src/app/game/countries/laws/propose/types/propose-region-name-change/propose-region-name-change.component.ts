import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-region-name-change',
  templateUrl: './propose-region-name-change.component.html',
  styleUrls: ['./propose-region-name-change.component.scss']
})
export class CountryLawProposeRegionNameChangeComponent implements OnInit {
  @Input() countryId;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  regions = [];

  constructor(private graphQlService: GraphQlService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      regionId: new UntypedFormControl(null, [Validators.required]),
      name: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
    this.graphQlService.get(`game`, `{ country(id: "${this.countryId}"){ regions { id name x y } } }`).subscribe((r: any) => this.regions = r.data.country.regions);
  }
}
