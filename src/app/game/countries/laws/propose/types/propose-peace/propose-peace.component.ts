import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-peace',
  templateUrl: './propose-peace.component.html',
  styleUrls: ['./propose-peace.component.scss'],
})
export class CountryLawProposePeaceComponent implements OnInit {
  isLoading = true;

  @Input() countryId
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  countries = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      warId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form));

    this.graphQlService.get(`game`, `{ wars(countryId: "${this.countryId}") { id attacker { id name } defender { id name } } }`)
      .subscribe((r: any) => {
        this.countries = r.data.wars.map((war) =>
          war.attacker.id === this.countryId
            ? { ...war.defender, warId: war.id }
            : { ...war.attacker, warId: war.id }
        );
        this.isLoading = false;
      });
  }
}
