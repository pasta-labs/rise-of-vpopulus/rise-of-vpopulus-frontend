import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-country-law-propose-taxes',
  templateUrl: './propose-taxes.component.html',
  styleUrls: ['./propose-taxes.component.scss'],
})
export class CountryLawProposeTaxesComponent implements OnInit {
  isLoading = true;

  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  items = [];
  raws = [];
  constructions = [];

  constructor(private httpService: GenericHttpService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      productId: new UntypedFormControl(null, [Validators.required]),
      vat: new UntypedFormControl(null, [Validators.required]),
      incomeTax: new UntypedFormControl(null, [Validators.required]),
      exportTax: new UntypedFormControl(null, [Validators.required]),
      importTax: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
    forkJoin({
      items: this.httpService.get(`/api/dictionaries/items`),
      raws: this.httpService.get(`/api/dictionaries/raws`),
      constructions: this.httpService.get(`/api/dictionaries/constructions`),
    }).subscribe((r) => {
      this.items = r.items;
      this.raws = r.raws;
      this.constructions = r.constructions;
      this.isLoading = false;
    });
  }
}
