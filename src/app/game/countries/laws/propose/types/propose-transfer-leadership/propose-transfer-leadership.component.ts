import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-country-law-propose-transfer-leadership',
  templateUrl: './propose-transfer-leadership.component.html',
  styleUrls: ['./propose-transfer-leadership.component.scss'],
})
export class CountryLawProposeTransferLeadershipComponent implements OnInit {
  isLoading = false;

  @Input() countryId;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  citizens = [];
  searchedCitizen;

  constructor(private httpService: GenericHttpService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      newLeaderId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }

  searchCitizen() {
    if (typeof this.searchedCitizen === 'string') {
      this.form.get(`newLeaderId`).setValue(null);
      if (this.searchedCitizen.length >= 3) {
        this.isLoading = true;
        this.httpService
          .get(`/api/entities/citizens/search?name=${this.searchedCitizen}`)
          .subscribe((r) => {
            console.log(r);
            this.citizens = r.filter(x => x.region && x.region.country && x.region.country.id === this.countryId);
            this.isLoading = false;
          });
      } else {
        this.citizens = [];
      }
    } else
      this.form.get(`newLeaderId`).setValue(this.searchedCitizen.id);
  }
}
