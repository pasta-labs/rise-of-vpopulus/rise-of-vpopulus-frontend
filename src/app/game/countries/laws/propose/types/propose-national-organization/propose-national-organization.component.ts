import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-national-organization',
  templateUrl: './propose-national-organization.component.html',
  styleUrls: ['./propose-national-organization.component.scss'],
})
export class CountryLawProposeNationalOrganizationComponent implements OnInit {
  @Input() currentCountry;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  // regions;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      regionId: new UntypedFormControl(null, [Validators.required]),
      name: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))

    // this.graphQlService.get(`game`, `{ country(id: "${this.currentCountry.id}"){ id regions { id x y name } } }`).subscribe((r: any) => {
    //   this.regions = r.data.country.regions;
    // })
  }

  regionSelected = (region) => this.form.get(`regionId`).setValue(region.id);
}
