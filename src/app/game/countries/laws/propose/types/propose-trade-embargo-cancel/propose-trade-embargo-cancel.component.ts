import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-trade-embargo-cancel',
  templateUrl: './propose-trade-embargo-cancel.component.html',
  styleUrl: './propose-trade-embargo-cancel.component.scss'
})
export class ProposeTradeEmbargoCancelComponent {
  isLoading = true;

  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  @Input() countryId;

  countries = [];

  constructor(
    private graphQlService: GraphQlService,
  ) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      countryId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
    this.loadData();
  };

  loadData = async () => {
    const tradeEmbargoCountries = await firstValueFrom(this.graphQlService.get(`game`, `{ countryLawsTradeEmbargo(countryId: "${this.countryId}"){ id name } }`))

    this.countries = tradeEmbargoCountries.data['countryLawsTradeEmbargo'];
    this.isLoading = false;
  }
}
