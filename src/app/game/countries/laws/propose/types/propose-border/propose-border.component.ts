import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-country-law-propose-border',
  templateUrl: './propose-border.component.html',
  styleUrls: ['./propose-border.component.scss'],
})
export class CountryLawProposeBorderComponent implements OnInit {
  @Input() type: String;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  regions = [];

  constructor() { }

  ngOnInit() {
    if (this.type === 'reopen-border') {
      this.form = new UntypedFormGroup({});
      this.formChange.emit(this.form);
    } else {
      this.form = new UntypedFormGroup({
        days: new UntypedFormControl(true, [Validators.required]),
      });
    }
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }
}
