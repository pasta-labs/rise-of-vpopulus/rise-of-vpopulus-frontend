import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-print-currency',
  templateUrl: './propose-print-currency.component.html',
  styleUrls: ['./propose-print-currency.component.scss'],
})
export class CountryLawProposePrintCurrencyComponent implements OnInit {
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  constructor() { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      amount: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }
}
