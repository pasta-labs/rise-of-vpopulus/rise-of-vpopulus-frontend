import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-countries-laws-propose-newspaper-censorship',
  templateUrl: './newspaper-censorship.component.html',
  styleUrls: ['./newspaper-censorship.component.scss']
})
export class NewspaperCensorshipComponent implements OnInit {
  @Input() parentForm: UntypedFormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
