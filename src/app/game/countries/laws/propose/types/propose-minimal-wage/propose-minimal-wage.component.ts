import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-country-law-propose-minimal-wage',
  templateUrl: './propose-minimal-wage.component.html',
  styleUrls: ['./propose-minimal-wage.component.scss'],
})
export class CountryLawProposeMinimalWageComponent implements OnInit {
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  constructor() { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      amount: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }
}
