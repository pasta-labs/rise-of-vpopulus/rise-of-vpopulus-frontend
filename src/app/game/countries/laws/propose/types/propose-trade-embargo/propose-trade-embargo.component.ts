import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { firstValueFrom } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-trade-embargo',
  templateUrl: './propose-trade-embargo.component.html',
  styleUrl: './propose-trade-embargo.component.scss'
})
export class CountryLawProposeTradeEmbargoComponent {
  isLoading = true;

  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  @Input() countryId;

  countries = [];

  constructor(
    private graphQlService: GraphQlService,
  ) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      countryId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
    this.graphQlService.get(`game`, `{ countries{ id name } }`)
      .subscribe(async (r: any) => {
        var countries = r.data.countries.filter(x => x.id !== this.countryId);
        const tradeEmbargoCountries = await firstValueFrom(this.graphQlService.get(`game`, `{ countryLawsTradeEmbargo(countryId: "${this.countryId}"){ id } }`))
        console.log('embarg', tradeEmbargoCountries)
        this.countries = countries.filter(x => !tradeEmbargoCountries['data']['countryLawsTradeEmbargo'].map(te => te.id).includes(x.id));

        this.isLoading = false;
      })
  };
}
