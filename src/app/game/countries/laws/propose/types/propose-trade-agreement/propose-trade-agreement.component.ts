import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-trade-agreement',
  templateUrl: './propose-trade-agreement.component.html',
  styleUrls: ['./propose-trade-agreement.component.scss']
})
export class CountryLawProposeTradeAgreementComponent implements OnInit {
  @Input() countryId;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  countries = [];

  constructor(private graphQlService: GraphQlService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      countryId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))

    forkJoin({ countries: this.graphQlService.get(`game`, `{ countries { id name } }`), agreements: this.graphQlService.get(`game`, `{ countryTradeAgreements(countryId: "${this.countryId}") { id } }`) })
      .subscribe((r: any) => {
        console.log('r', r)
        var agreementCountryIds = r.agreements.data.countryTradeAgreements.map(x => x.countries.filter(y => y.id !== this.countryId)[0]).map(x => x.id);
        this.countries = r.countries.data.countries.filter(x => !agreementCountryIds.includes(x.id));
      });
  }

}
