import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-trade-agreement-cancel',
  templateUrl: './propose-trade-agreement-cancel.component.html',
  styleUrls: ['./propose-trade-agreement-cancel.component.scss']
})
export class CountryLawProposeTradeAgreementCancelComponent implements OnInit {
  @Input() countryId;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  countries = [];

  constructor(private graphQlService: GraphQlService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      countryId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form));
    this.graphQlService.get(`game`, `{ countryTradeAgreements(countryId: "${this.countryId}") { countries { id name } } }`).subscribe((r: any) => this.countries = r.data.countryTradeAgreements.map(x => x.countries.filter(y => y.id !== this.countryId)[0]));
  }
}
