import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-newspaper-consorship-law-propose',
  templateUrl: './newspaper-consorship-law-propose.component.html',
  styleUrls: ['./newspaper-consorship-law-propose.component.scss']
})
export class NewspaperConsorshipLawProposeComponent implements OnInit {
  @Input() countryId;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  isLoading = true;

  newspapers = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      newspaperId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form));
    this.loadNewspapers();
  }

  loadNewspapers = () => {
    this.graphQlService.get(`game`, `{ newspapersForCensorshipLaw(countryId: "${this.countryId}") { id name avatar } }`).subscribe((r: any) => {
      console.log('r', this.newspapers)
      this.newspapers = r.data.newspapersForCensorshipLaw;
      this.isLoading = false;
    })
  }

  getNewspaper = (id) => this.newspapers.find(x => x.id === id);
}
