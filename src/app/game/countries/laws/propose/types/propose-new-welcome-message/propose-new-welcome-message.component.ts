import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-country-law-propose-new-welcome-message',
  templateUrl: './propose-new-welcome-message.component.html',
  styleUrls: ['./propose-new-welcome-message.component.scss'],
})
export class CountryLawProposeNewWelcomeMessageComponent implements OnInit {
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  constructor() { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      content: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }
}
