import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
7;

@Component({
  selector: 'app-country-law-propose-citizen-fee',
  templateUrl: './propose-citizen-fee.component.html',
  styleUrls: ['./propose-citizen-fee.component.scss'],
})
export class CountryLawProposeCitizenFeeComponent implements OnInit {
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  constructor() { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      amount: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }
}
