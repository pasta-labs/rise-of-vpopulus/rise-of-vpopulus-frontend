import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law-propose-withdraw-currency',
  templateUrl: './propose-withdraw-currency.component.html',
  styleUrls: ['./propose-withdraw-currency.component.scss'],
})
export class CountryLawProposeWithdrawCurrencyComponent implements OnInit {
  @Input() countryId;
  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  organizations;
  currencies;

  constructor(private httpService: GenericHttpService) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      currencyId: new UntypedFormControl(null, [Validators.required]),
      amount: new UntypedFormControl(null, [Validators.required]),
      nationalOrganizationId: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))

    this.httpService
      .get(`/api/countries/${this.countryId}/organisations`)
      .subscribe((response) => {
        console.log(response);
        this.organizations = response;
      });
    this.httpService
      .get(`/api/currency/country/${this.countryId}`)
      .subscribe((response) => {
        console.log(response);
        this.currencies = response;
      });
  }
}
