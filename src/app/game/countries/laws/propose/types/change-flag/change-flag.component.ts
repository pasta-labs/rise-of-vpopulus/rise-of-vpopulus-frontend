import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-countries-laws-propose-change-flag',
  templateUrl: './change-flag.component.html',
  styleUrls: ['./change-flag.component.scss']
})
export class CountriesLawsProposeChangeFlagComponent implements OnInit {
  apiUrl = environment.apiUrl;

  @Input() form: UntypedFormGroup;
  @Output() formChange = new EventEmitter<UntypedFormGroup>();

  flagLocation;

  constructor(
    private messageService: NzMessageService,
  ) { }

  ngOnInit() {
    this.form = new UntypedFormGroup({
      flagLocation: new UntypedFormControl(null, [Validators.required]),
      name: new UntypedFormControl(null, [Validators.required]),
      contentType: new UntypedFormControl(null, [Validators.required]),
    });
    this.form.valueChanges.subscribe(() => this.formChange.emit(this.form))
  }

  uploadDocument = (item) => {
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(item.file as any);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => this.messageService.error(JSON.stringify(error));
    }).then(base64 => {
      this.form.get(`flagLocation`).setValue(base64);
      this.form.get(`name`).setValue(item.file.name);
      this.form.get(`contentType`).setValue(item.file.type);
      this.flagLocation = base64;
    })
  };

}
