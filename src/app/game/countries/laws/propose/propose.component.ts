import { Component, Input, OnInit, inject } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { orderBy } from 'lodash-es';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-country-law-propose',
  templateUrl: './propose.component.html',
  styleUrls: ['./propose.component.scss'],
})
export class CountryLawProposeComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  countryId;
  form: UntypedFormGroup;

  lawTypes = [
    { key: 'declare-war', label: 'DeclareWar' },
    { key: "region-name-change", label: "RegionNameChange" },
    { key: 'citizen-fee', label: 'CitizenFee' },
    { key: 'peace', label: 'ProposePeace' },
    { key: 'welcome-message', label: 'WelcomeMessage' },
    { key: 'minimal-wage', label: 'MinimalWage' },
    { key: 'tax-change', label: 'TaxesChange' },
    { key: 'trade-agreement', label: 'TradeAgreement' },
    { key: 'trade-agreement-cancel', label: 'TradeAgreementCancel' },
    { key: 'national-organization', label: 'CreateNationalOrganization' },
    { key: 'construction', label: 'CreateConstruction' },
    { key: 'transfer-leadership', label: 'TransferLeadership' },
    { key: 'print-currency', label: 'PrintCurrency' },
    { key: 'withdraw-currency', label: 'WithdrawCurrency' },
    { key: 'change-flag', label: 'ChangeFlag' },
    { key: 'newspaper-censorship', label: 'NewspaperCensorship' },
    { key: 'trade-embargo', label: 'TradeEmbargo' },
    { key: 'trade-embargo-cancel', label: 'TradeEmbargoCancel' },
    { key: 'revoke-citizenship', label: 'RevokeCitizenship' }
  ];

  selectedLawType;

  countries = [];

  currentCountry;

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private modalRef: NzModalRef,
    private graphQlService: GraphQlService,
  ) {
    this.resetForm();
  }

  ngOnInit() {
    this.countryId = this.nzModalData.countryId;

    this.lawTypes = orderBy(this.lawTypes, x => x.key, 'asc');

    this.graphQlService.get(`game`, `{ country(id: "${this.countryId}"){ id borderClosedUntilGameDay capital { id x y } } }`).subscribe((response: any) => {
      this.currentCountry = response.data.country;
      if ('borderClosedUntilGameDay' in this.currentCountry) this.lawTypes.push({ key: 'reopen-border', label: 'ReopenBorders' });
      else this.lawTypes.push({ key: 'close-border', label: 'CloseBorders' });

      this.lawTypes = orderBy(this.lawTypes, x => x.key, 'asc');
      this.isLoading = false;
    });
  }

  resetForm = () => {
    this.form = new UntypedFormGroup({
      init: new UntypedFormControl(null, Validators.required)
    });
  }

  handleOk() {
    this.isLoading = true;
    this.httpService
      .post(`/api/countries/laws/propose/${this.selectedLawType.key}`, this.form.getRawValue())
      .subscribe(
        (r) => {
          this.message.success('Law has been proposed!');
          this.modalRef.close(`ok`);
          this.isLoading = false;
        },
        (error) => {
          console.log('error', error)
          if (error.error.code === "NAME_TOO_SHORT") this.message.error(`Name is too short!`);
          else if (error.error.code === "SIMILAR_LAW_ACTIVE") this.message.error(`Similar law type is already ongoing!`);
          else if (error.error.code === "NAME_TOO_LONG") this.message.error(`Name is too long!`);
          else this.message.error(error.error.Code);
          this.isLoading = false;
        }
      );
  }
}
