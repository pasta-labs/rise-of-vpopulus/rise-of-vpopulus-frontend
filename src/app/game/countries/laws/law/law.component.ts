import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-country-law',
  templateUrl: './law.component.html',
  styleUrls: ['./law.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CountryLawComponent implements OnInit {
  lawId;

  countryId;
  country;

  isLoading = false;
  law;
  lawDetails;
  forVoters = [];
  againstVoters = [];
  abstentVoters = [];
  timer;
  timerDisplay = "--:--:--";
  voteTime;
  isAllowedToVote = false;

  chartData;

  products = [];
  viewedProduct;
  targetedCountry;
  currency;
  nationalOrganization;

  countdownInterval;
  processingRefreshInterval;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private cd: ChangeDetectorRef,
    private graphQlService: GraphQlService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.lawId = params['lawId'];
      this.countryId = params['countryId'];

      if (!this.lawId || !this.countryId) this.router.navigateByUrl(`/`);
      this.loadLaw();
      this.graphQlService
        .get(`game`, `{ country(id: "${this.countryId}") { name } }`)
        .subscribe((r: any) => (this.country = r.data.country));
    });
  }

  loadLaw() {
    clearInterval(this.processingRefreshInterval);
    this.isLoading = true;
    this.httpService.get(`/api/countries/laws/${this.lawId}`).subscribe((law) => {
      this.law = law;
      this.lawDetails = JSON.parse(law.payload);
      console.log('law', this.law, this.lawDetails)

      // = = = Get data for laws
      var query = ``;
      switch (this.law?.title) {
        case 'PrintCurrency':
        case 'MinimalWage':
        case 'CitizenFee':
          query = 'country { currency { symbol } }';
          break;
        case 'RevokeCitizenship':
          query = 'targetCitizen { id name }';
          break;
        case 'CreateConstruction':
          query =
            'region { id name x y } nationalOrganization { id name } constructionType { symbol }';
          break;
        case 'CreateNationalOrganization':
        case 'RegionNameChange':
          query = 'region { id name x y }';
          break;
        case 'WithdrawCurrency':
          query = 'currency { symbol } nationalOrganization { id name }';
          break;
        case 'TransferLeadership':
          query = 'newLeader { id name }';
          break;
        case 'TradeEmbargo':
        case 'TradeEmbargoCancel':
        case 'DeclareWar':
        case 'ProposePeace':
        case 'TradeAgreement':
        case 'CancelTradeAgreement':
          query = 'targetCountry { id name }';
          break;
        case 'TaxChange':
          query = 'product { symbol }';
          break;
        case 'NewspaperCensorship':
          query = `newspaper(id: "${this.lawDetails.NewspaperId}") { id name }`;
          break;
      }
      console.log('query', query)
      this.graphQlService
        .get(
          `game`,
          `{ countryLaw(id: "${this.lawId}") { timeLeftInSeconds creator { id name } ${query} } }`
        )
        .subscribe((r: any) => {
          this.law['data'] = r.data.countryLaw;
          this.timer = this.law['data'].timeLeftInSeconds;

          if (this.law.outcome === 'Ongoing') {
            if (this.timer > 0) this.countdownInterval = setInterval(() => this.refreshTimer(1), 1000);
            else if (this.timer < 0) this.processingRefreshInterval = setInterval(() => this.loadLaw(), 5000);
          }
        });

      // Check if can vote
      if (law.outcome === 'Ongoing') {
        this.graphQlService.get(`game`, `{ countryLawCanVote(lawId: "${this.lawId}") }`)
          .subscribe((r: any) => {
            this.isAllowedToVote = r.data.countryLawCanVote;
            this.isLoading = false;
          })
      } else {
        this.setPieChartData();
        this.isLoading = false;
      }
    });
  }

  refreshTimer = (seconds) => {
    if (this.law?.outcome !== 'Ongoing') return;

    if (this.timer > 0) {
      this.timer -= seconds;
      this.timerDisplay = this.convertHMS(this.timer);
    }

    if (this.timer <= 0) {
      clearInterval(this.countdownInterval);
      this.loadLaw();
    }
  }

  convertHMS(value) {
    const sec = parseInt(value, 10); // convert value to number if it's string
    var hours = Math.floor(sec / 3600).toString(); // get hours
    let minutes = Math.floor((sec - (+hours * 3600)) / 60).toString(); // get minutes
    let seconds = (sec - (+hours * 3600) - (+minutes * 60)).toString(); //  get seconds
    // add 0 if value < 10; Example: 2 => 02
    if (+hours < 10) { hours = "0" + hours; }
    if (+minutes < 10) { minutes = "0" + minutes; }
    if (+seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds; // Return is HH : MM : SS
  }

  setPieChartData() {
    this.law?.votes?.map((vote) => {
      switch (vote.vote) {
        case 'Y':
          this.forVoters.push(vote);
          break;
        case 'N':
          this.againstVoters.push(vote);
          break;
        case 'A':
          this.abstentVoters.push(vote);
          break;
      }
    });
    this.chartData = {
      labels: ['For', 'Against', 'Abstent'],
      datasets: [
        {
          data: [this.law.votes.filter(x => x.vote === "Y").length, this.law.votes.filter(x => x.vote === "N").length, this.law.votes.filter(x => x.vote === "A").length],
          label: 'Votes',
          backgroundColor: [
            'rgba(0,255,0,0.3)',
            'rgba(255,0,0,0.3)',
            'rgba(0,0,255,0.3)',
          ],
          borderColor: 'rgba(255, 255, 255, 1)'
        },

      ],
      colors: [
        [0, 255, 0],
        [255, 0, 0],
        [0, 0, 255]
      ],
      options: {
        responsive: true
      },
    };
    this.cd.detectChanges();
  }

  vote(option) {
    this.isLoading = true;
    this.httpService
      .post(`/api/countries/laws/vote`, { lawId: this.lawId, vote: option })
      .subscribe(
        (r) => {
          this.message.success('Voted!');
          this.isAllowedToVote = false;
          clearInterval(this.countdownInterval);
          this.loadLaw();
        },
        (error) => {
          this.message.error(error.message);
          this.isLoading = false;
        }
      );
  }

  loadTaxes() {
    this.isLoading = true;
    this.httpService.get(`/api/dictionaries/items`).subscribe((r) => {
      this.isLoading = false;
      this.viewedProduct = r.find(
        (product) => product.id === this.lawDetails.ProductId
      );
    });
  }

  loadCountries() {
    this.isLoading = true;
    this.httpService.get(`/api/countries/`).subscribe((r) => {
      this.isLoading = false;
      (this.law.title === 'DeclareWar' || this.law.title === 'Embargo') &&
        (this.targetedCountry = r.find(
          (country) => country.id === this.lawDetails.CountryId
        ));
      this.law.title === 'MakePeace' &&
        (this.targetedCountry = r.find(
          (country) => country.id === this.lawDetails.ProposingCountryId
        ));
    });
  }

  loadCitizen() {
    this.isLoading = true;
    this.httpService
      .get(`/api/entities/citizens/${this.lawDetails.NewLeaderId}`)
      .subscribe((r) => {
        this.isLoading = false;
        this.lawDetails.NewLeaderName = r.name;
      });
  }

  loadCurrencies() {
    this.isLoading = true;
    this.httpService
      .get(`/api/currency/country/${this.lawDetails.CountryId}`)
      .subscribe((r) => {
        this.isLoading = false;
        this.lawDetails.CurrencyName = r.find(
          (currency) => this.lawDetails.CurrencyId === currency.id
        ).name;
      });
  }

  loadOrganizations() {
    this.httpService
      .get(`/api/countries/${this.lawDetails.CountryId}/organisations`)
      .subscribe((r) => {
        this.isLoading = false;
        this.lawDetails.OrganizationName = r.find(
          (organization) =>
            this.lawDetails.NationalOrganizationId === organization.id
        ).name;
      });
  }

  getTranslation = (key) =>
    this.translateService.instant(key);

}
