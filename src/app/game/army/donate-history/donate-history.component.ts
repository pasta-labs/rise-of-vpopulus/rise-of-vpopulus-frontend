import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-army-donate-history',
  templateUrl: './donate-history.component.html',
  styleUrls: ['./donate-history.component.scss'],
})
export class ArmyDonateHistoryComponent implements OnInit {
  isLoading = false;

  army;
  squadId;
  user;
  isEntity;
  donationHistory;

  expandSet = new Set<number>();

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.army = { id: params.id };

      if (this.army.id == undefined) this.router.navigateByUrl(`/armies`);

      this.graphQlService.get(`game`, `{ army(id: "${this.army.id}") { id name } }`)
        .subscribe((r: any) => this.army = r.data.army);

      this.loadArmyHistory();
      this.graphQlService.get(`game`, "{ currentUser { citizenId entityId entityType } }").subscribe((r: any) => this.user = r.data.currentUser);
    });
  }

  loadArmyHistory() {
    this.isLoading = true;
    this.httpService.get(`/api/armies/${this.army.id}/donate/history`).subscribe(
      (r) => {
        this.donationHistory = r;
        console.log('history army', this.donationHistory)
        this.isLoading = false;
      },
      (error) => {
        this.message.error(error.message);
      }
    );
  }
}
