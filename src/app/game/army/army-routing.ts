import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { ArmyBattleHistoryComponent } from './battle-history/battle-history.component';
import { ArmyDonateHistoryComponent } from './donate-history/donate-history.component';
import { ArmyDonateComponent } from './donate/donate.component';
import { ArmyListComponent } from './list/list.component';
import { ArmyMassTravelComponent } from './mass-travel/mass-travel.component';
import { ArmyMembersComponent } from './members/members.component';
import { ArmyProfileComponent } from './profile/profile.component';
import { ArmySquadsComponent } from './squads/squads.component';

export const routes: Routes = [
  { path: 'country/:countryId/armies', component: ArmyListComponent, canActivate: [AuthGuard] },
  { path: 'army', component: ArmyProfileComponent, canActivate: [AuthGuard] },
  {
    path: 'army/:id',
    component: ArmyProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'army/:id/squads',
    component: ArmySquadsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'army/:id/members',
    component: ArmyMembersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'army/:id/donate',
    component: ArmyDonateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'army/:id/donate/history',
    component: ArmyDonateHistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'army/:id/battle-history',
    component: ArmyBattleHistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'army/:id/travel',
    component: ArmyMassTravelComponent,
    canActivate: [AuthGuard],
  },
];
