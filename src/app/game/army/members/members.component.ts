import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-army-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class ArmyMembersComponent implements OnInit {
  isLoading = false;

  army;

  roles = [];

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.army = { id: params['id'] };
      console.log('params', this.army)
      if (!this.army.id) this.router.navigateByUrl(`/armies`);
      this.load();
    });
  }

  load = () => {
    this.roles = [];
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ army(id: "${this.army.id}"){ id name members { id citizen { id name avatar } permissions { isOwner isRecruit isNewspaperEditor isCompanyManager } } } }`).subscribe((r: any) => {
      this.army = r.data.army;
      this.roles.push({
        key: "owner", label: "Owner", members: this.army.members.filter(x => x.permissions.isOwner === true)
      });
      this.roles.push({
        key: "member", label: "Members", members: this.army.members.filter(x => x.permissions.isOwner === false && (!x.permissions.isRecruit))
      });
      this.roles.push({
        key: "recruit", label: "Recruits", members: this.army.members.filter(x => x.permissions.isRecruit === true)
      });
      console.log('army', this.army, this.roles);

      this.isLoading = false;
    })
  }


  deleteMember(member) {
    this.isLoading = true;
    this.httpService
      .delete(`/api/armies/members/${member.citizen.id}/remove`)
      .subscribe(
        () => {
          this.message.success(this.translateService.instant('Military.Army.removedFromArmy'));
          this.load();
        },
        (error) => {
          this.message.error(error.message);
          this.isLoading = false;
        }
      );

  }

  acceptCandidate(member) {
    this.isLoading = true;
    this.httpService.put(`/api/armies/members/accept`, { memberId: member.citizen.id }).subscribe(
      () => {
        this.message.success(this.translateService.instant('Military.Army.acceptedCandidate'));
        this.load();
      },
      (error) => {
        this.message.error(error.message);
        this.isLoading = false;
      }
    );
  }

  rejectCandidate(member) {
    this.isLoading = true;
    this.httpService.put(`/api/armies/members/reject`, { memberId: member.citizen.id }).subscribe(
      () => {
        this.message.success(this.translateService.instant('Military.Army.rejectedCandidate'));
        this.load();
      },
      (error) => {
        this.message.error(error.message);
        this.isLoading = false;
      }
    );
  }
}
