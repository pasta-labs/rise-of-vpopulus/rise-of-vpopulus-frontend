import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { cloneDeep } from 'lodash';
import { environment } from 'src/environments/environment';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-army-squads',
  templateUrl: './squads.component.html',
  styleUrls: ['./squads.component.scss'],
})
export class ArmySquadsComponent implements OnInit {
  isLoading = false;

  apiUrl = environment.apiUrl;

  army;

  isArmyLeader = false;

  isCreateFormVisible = false;
  name = '';

  manageSquad;
  unassignedMembers = [];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService,
    private modal: NzModalService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.army = { id: params.id };
      if (this.army.id == undefined) this.router.navigateByUrl(`/armies`);
      this.load();
    });
  }

  load = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ army(id: "${this.army.id}"){ id name members { citizen { id name avatar } permissions { isOwner isRecruit } } squads { id name leaderId leader { id name } order members { citizen { id name avatar } } } } }`).subscribe((r: any) => {
      this.army = cloneDeep(r.data.army);
      this.army.members = this.army.members.filter(x => !x.permissions.isRecruit);
      this.army.squads.forEach(squad => {
        if (squad.leader && squad.leader.id === localStorage.getItem(`citizenId`)) squad.isLeader = true;
      })
      var leader = this.army.members.find(x => x.permissions.isOwner === true);
      if (leader && leader.citizen.id === localStorage.getItem(`citizenId`)) this.isArmyLeader = true;
      console.log('members', this.army)
      this.getUnassignedMembers();
      this.isLoading = false;
    })
  }

  createSquad = () => {
    this.isLoading = true;
    this.httpService.post(`/api/armies/squads`, { name: this.name }).subscribe(
      () => {
        this.message.success(this.translateService.instant('Military.Army.squadCreated'));
        this.isCreateFormVisible = false;
        this.load();
      },
      (error) => {
        this.message.error(error.message);
        this.isLoading = false;
      }
    );
  }

  deleteSquad = (id) => {
    this.isLoading = true;
    this.httpService.delete(`/api/armies/squads/${id}`).subscribe(
      () => {
        var squad = this.army.squads.find(x => x.id === id);
        this.message.success(`${squad.name}` + this.translateService.instant('Military.Army.hasRemoved'));
        this.load();
      },
      (error) => {
        this.message.error(error.message);
        this.isLoading = false;
      }
    );
  }

  setOrder = (squad) => {
    squad.isLoading = true;
    this.httpService.post(`/api/armies/squads/orders`, { squadId: squad.id, content: squad.newOrder }).subscribe(
      (r) => {
        squad.order = squad.newOrder;
        squad.editingOrder = false;
        this.message.success(this.translateService.instant('Military.Army.newOrderSet'));
        squad.isLoading = false;
      },
      (error) => {
        this.message.error(error.message);
        squad.isLoading = false;
      }
    );
  }

  cancelOrder(squad) {
    squad.isLoading = true;
    this.httpService.delete(`/api/armies/squads/orders`).subscribe(
      (r) => {
        this.message.success(this.translateService.instant('Military.Army.orderBeenCanceled'));
        squad.order = undefined;
        squad.isLoading = false;
      },
      (error) => {
        this.message.error(error.message);
      }
    );
  }

  setLeader = (squad) => {
    squad.isLoading = true;
    this.httpService
      .post(`/api/armies/squads/members/assign/leader`, {
        squadId: squad.id,
        leaderId: squad.leaderId,
      })
      .subscribe(
        () => {
          this.message.success(this.translateService.instant('Military.Army.leaderChanged'));
          var member = this.army.members.find(x => x.citizen.id === squad.leaderId);
          squad.leader = member.citizen;
          this.getUnassignedMembers();
          squad.isLoading = false;
        },
        (error) => {
          this.message.error(error.message);
          squad.isLoading = false;
        }
      );
  }

  getUnassignedMembers = () => {
    var members = cloneDeep(this.army.members);
    this.army.squads.forEach(squad => {
      if (squad.leaderId) members = members.filter(x => x.citizen.id != squad.leaderId);
      if (!squad.members) return;
      squad.members.forEach(member => {
        members = members.filter(x => x.citizen.id != member.citizen.id);
      });
    });
    this.unassignedMembers = members;
  }

  setMember = (squad) => {
    squad.isLoading = true;
    this.httpService
      .post(`/api/armies/squads/members/assign/member`, {
        squadId: squad.id,
        memberId: squad.newMemberId,
      })
      .subscribe(
        () => {
          this.message.success(this.translateService.instant('Military.Army.memberBeenAdded'));
          var member = this.army.members.find(x => x.citizen.id === squad.newMemberId);
          if (!squad.members) squad.members = [];
          squad.members.push(member);
          this.getUnassignedMembers();
          squad.isLoading = false;
          squad.newMemberId = undefined;
        },
        (error) => {
          this.message.error(error.message);
          squad.isLoading = false;
        }
      );
  }

  removeMember = (squad, citizenId) => {
    squad.isLoading = true;
    this.httpService
      .delete(`/api/armies/squads/members/${citizenId}`)
      .subscribe(
        () => {
          this.message.success(this.translateService.instant('Military.Army.memberBeenRemoved'));
          squad.members = squad.members.filter(x => x.citizen.id !== citizenId);
          this.getUnassignedMembers();
          squad.isLoading = false;
        },
        (error) => {
          this.message.error(error.message);
          squad.isLoading = false;
        }
      );
  }

}
