import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep } from 'lodash-es';

@Component({
  selector: 'app-army-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss'],
})
export class ArmyDonateComponent implements OnInit {
  isLoading = false;

  army;

  currencies;
  inventory;

  itemsToDonate = [];
  currenciesToDonate = [];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.army = { id: params['id'] };
      if (!this.army.id) this.router.navigateByUrl(`/armies`);
      this.load();
    });
  }

  load = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ army(id: "${this.army.id}"){ id name members { id citizen { id name avatar } } } }`).subscribe((r: any) => {
      this.army = cloneDeep(r.data.army);
      if (!this.army.membersSelected) this.army.membersSelected = 0;
      if (!this.army.formValid) this.army.formValid = false;

      forkJoin({ currencies: this.httpService.get(`/api/currency`), inventory: this.httpService.get(`/api/inventory`) })
        .subscribe(
          (r) => {
            this.currencies = r.currencies;
            this.currencies.forEach(i => i.totalAmountRequired = 0);
            this.inventory = r.inventory;
            this.inventory.forEach(i => i.totalAmountRequired = 0);
            this.isLoading = false;
          },
          (error) => {
            this.message.error(error.message);
            this.isLoading = false;
          }
        );
    })
  }

  donate(): void {
    this.isLoading = true;

    this.httpService.post(`/api/armies/donate`, {
      memberIds: this.army.members.filter(x => x.isChecked === true).map(x => x.citizen.id),
      currencies: this.currenciesToDonate.map(x => ({ currencyId: x.id, amount: x.amountRequired })),
      items: this.itemsToDonate.map(x => ({ itemId: x.id, quality: x.quality, amount: x.amountRequired }))
    }).subscribe(
      () => {
        this.message.success('Packages has been sent to selected members.');
        this.isLoading = false;
        window.location.reload();
      },
      (error) => {
        this.message.error(error.message);
        this.isLoading = false;
      }
    );
  }

  selectMember = (id) => {
    var item = this.army.members.find(x => x.id === id);
    item.isChecked = !item.isChecked;

    this.army.membersSelected = this.army.members.filter(x => x.isChecked === true).length;
    this.calculateTotalAmountRequired();
  }

  addCurrency(item) {
    var toDonate = this.currenciesToDonate.find((el) => el.id === item.id);
    if (!toDonate) this.currenciesToDonate.push(item);

    toDonate = this.currenciesToDonate.find((el) => el.id === item.id);
    if (!toDonate.amountRequired) toDonate.amountRequired = 0;
    toDonate.amountRequired += (+item.providedAmount);
    item.providedAmount = null;
    this.calculateTotalAmountRequired();
  }

  addItem(item) {
    var toDonate = this.itemsToDonate.find((el) => el.id === item.id && el.quality == item.quality);
    if (!toDonate) this.itemsToDonate.push(item);

    toDonate = this.itemsToDonate.find((el) => el.id === item.id && el.quality == item.quality);
    if (!toDonate.amountRequired) toDonate.amountRequired = 0;
    toDonate.amountRequired += (+item.providedAmount);
    item.providedAmount = null;
    this.calculateTotalAmountRequired();
  }

  deleteFromSelected(index, item) {
    if (item.hasOwnProperty('currencyId')) {
      var item = this.currencies.find((cur) => cur.id === item.id);
      item.amountRequired -= item.amount;
      item.totalAmountRequired = 0;
      this.currenciesToDonate.splice(index, 1);
    } else {
      var item = this.inventory.find((el) => el.id === item.id && el.quality == item.quality);
      item.amountRequired -= item.amount;
      item.totalAmountRequired = 0;
      this.itemsToDonate.splice(index, 1);
    }
    this.calculateTotalAmountRequired();
  }

  calculateTotalAmountRequired = () => {
    this.army.formValid = true;

    this.itemsToDonate.forEach(i =>
      this.inventory.find(x => x.id === i.id && x.quality == i.quality).totalAmountRequired = i.amountRequired * this.army.membersSelected
    );
    if (this.inventory.filter(x => x.amount < x.totalAmountRequired).length > 0) this.army.formValid = false;

    this.currenciesToDonate.forEach(i =>
      this.currencies.find(x => x.id === i.id).totalAmountRequired = i.amountRequired * this.army.membersSelected
    );
    if (this.currencies.filter(x => x.amount < x.totalAmountRequired).length > 0) this.army.formValid = false;
  }
}
