import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArmyProfileComponent } from './profile/profile.component';
import { ArmyListComponent } from './list/list.component';
import { ArmyCreateComponent } from './create/create.component';
import { ArmySquadsComponent } from './squads/squads.component';
import { ArmyBattleHistoryComponent } from './battle-history/battle-history.component';
import { ArmyDonateHistoryComponent } from './donate-history/donate-history.component';
import { ArmyDonateComponent } from './donate/donate.component';
import { ArmyMembersComponent } from './members/members.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntityModule } from '../entity/entity.module';
import { ArmyMassTravelComponent } from './mass-travel/mass-travel.component';

@NgModule({
  declarations: [
    ArmyProfileComponent,
    ArmyDonateComponent,
    ArmyDonateHistoryComponent,
    ArmyMembersComponent,
    ArmyListComponent,
    ArmyCreateComponent,
    ArmySquadsComponent,
    ArmyBattleHistoryComponent,
    ArmyMassTravelComponent,
  ],
  imports: [CommonModule, SharedModule, EntityModule],
  exports: [ArmyListComponent],
})
export class ArmyModule {}
