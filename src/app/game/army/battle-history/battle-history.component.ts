import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-army-battle-history',
  templateUrl: './battle-history.component.html',
  styleUrls: ['./battle-history.component.scss'],
})
export class ArmyBattleHistoryComponent implements OnInit {
  armyId;
  selectedBattle;
  battles;
  isLoading = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.armyId = params.id;
      if (this.armyId == undefined) this.router.navigateByUrl(`/armies`);
    });
    this.getBattles();
  }

  getBattles() {
    this.isLoading = true;
    this.httpService.get(`/api/military/battles/active`).subscribe((r) => {
      this.isLoading = false;
      this.battles = r;
      this.selectedBattle = r[0];
      this.selectedBattleChange();
    });
  }

  selectedBattleChange() {
    this.isLoading = true;
    this.httpService
      .get(
        `/api/armies/${this.armyId}/squads/stats/battles/${this.selectedBattle.id}`
      )
      .subscribe((r) => {
        this.selectedBattle.data = [
          { data: r.map((element) => element.damage), label: 'Damage' },
        ];
        this.selectedBattle.labels = r.map((element) =>
          element.squad.name === 'NoSquad' ? 'Unasigned' : element.squad.name
        );
        this.selectedBattle.options = {
          responsive: true,
          scales: { xAxes: [{}], yAxes: [{}] },
          plugins: {
            datalabels: {
              anchor: 'end',
              align: 'end',
            },
          },
        };
        this.isLoading = false;
      });
  }
}
