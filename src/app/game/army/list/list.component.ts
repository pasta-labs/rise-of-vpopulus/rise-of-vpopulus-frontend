import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ArmyCreateComponent } from '../create/create.component';
import { cloneDeep } from 'lodash';
import { EntityType } from 'src/app/shared/models/EntityType';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-army-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ArmyListComponent implements OnInit {
  isLoading = true;

  @Input() countryId;
  @Input() showActions = true;
  list = undefined;

  canCreate = false;
  country;

  constructor(
    private httpService: GenericHttpService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private notificationService: NzNotificationService,
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      if (!this.countryId) {
        if (!params.countryId) this.router.navigateByUrl(`/`);
        this.countryId = params.countryId;
      }
      this.loadData();
    });
  }

  loadData() {
    this.isLoading = true;
    forkJoin({
      citizen: this.graphQlService.get(
        `game`,
        `{ citizen { armyMember { fromId canSwitch } region { country { id } } }}`
      ),
      country: this.graphQlService.get(
        `game`,
        `{ country(id:"${this.countryId}") { id name armies { id name avatar members { id } squads { id } region { id x y } } }}`
      ),
    }).subscribe((data: any) => {
      this.country = cloneDeep(data.country.data.country);
      if (
        !data.citizen.data.citizen.armyMember &&
        data.citizen.data.citizen?.region?.country?.id === this.country.id
      ) {
        this.canCreate = true;
        this.country.armies = this.country.armies.map((x) => ({
          ...x,
          canJoin: true,
        }));
      }

      if (
        data.citizen.data.citizen.armyMember &&
        data.citizen.data.citizen.armyMember.canSwitch === true
      ) {
        var army = this.country.armies.find(
          (x) => x.id === data.citizen.data.citizen.armyMember.fromId
        );
        if (army) army.canSwitch = true;
      }

      this.isLoading = false;
    });
  }

  join(item) {
    this.isLoading = true;
    this.httpService
      .post(`/api/armies/members/join`, { armyId: item.id })
      .subscribe(() => {
        this.notificationService.success(
          `Applied successfully!`,
          `You have applied successfully to join ${item.name}! Please wait for decision from army leader!`
        );
        this.isLoading = false;
        this.router.navigateByUrl(`/army/${item.id}`);
      });
  }

  switchTo(id) {
    this.authService.switchToEntity(EntityType.Army, id);
  }

  create() {
    this.modalService.create({
      nzWidth: '90%',
      nzContent: ArmyCreateComponent,
      nzTitle: 'Create army',
      nzFooter: null,
    });
  }
}
