import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { EntityType } from 'src/app/shared/models/EntityType';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep } from 'lodash-es';
import { NzModalService } from 'ng-zorro-antd/modal';
import { EntityModPanelComponent } from '../../entity/components/entity-mod-panel/entity-mod-panel.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-army',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ArmyProfileComponent implements OnInit {
  isLoading = true;
  id;

  showEditBio = false;

  army;
  isEntity = false;

  orders;
  ordersLoading = false;
  newOrder;
  orderIsEdited = false;

  canJoin = false;
  canSwitch = false;
  isMember = false;

  isMod = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private messageService: NzMessageService,
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isMod = localStorage.getItem(`modPermissions`) !== null;

    this.route.params.subscribe((params) => {
      this.id = params['id'];

      if (this.id == undefined) {
        this.httpService
          .get(`/api/citizens/${localStorage.getItem('entityId')}/armies`)
          .subscribe(
            (army) => {
              this.router.navigateByUrl(`/army/${army.id}`);
            },
            () => {
              var profile = JSON.parse(localStorage.getItem(`profile`));
              if (profile && profile.region.country && profile.region.country.id) this.router.navigateByUrl(`/country/${profile.region.country.id}/armies`);
              else this.router.navigateByUrl(`/`)
            }
          );
      } else {
        this.loadData();
        this.loadOrders();
      }
    });
  }

  loadData() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ army(id: "${this.id}"){ id name avatar isBanned bio region { id name country { id name } } squads { id name membersCount } } }`).subscribe((r: any) => {
      this.army = cloneDeep(r.data.army);
      if (localStorage.getItem('entityId') == this.id) this.isEntity = true;

      this.graphQlService.get(`game`, `{ citizen { armyMember { fromId canSwitch }}}`).subscribe((r: any) => {
        this.canJoin = !r.data.citizen.armyMember;
        if (r.data.citizen.armyMember && r.data.citizen.armyMember.fromId === this.army.id) {
          this.isMember = true;
          this.canSwitch = r.data.citizen.armyMember.canSwitch;
        }
      });
      this.isLoading = false;
    });
  }

  loadOrders() {
    this.ordersLoading = true;
    this.httpService.get(`/api/armies/${this.id}/orders`).subscribe(
      (r) => {
        this.orders = r;
        this.ordersLoading = false;
      },
      (error) => {
        this.messageService.error(error.message);
        this.ordersLoading = false;
      }
    );
  }

  saveNewOrder() {
    if (this.orderIsEdited && this.newOrder) {
      const payload = {
        content: this.newOrder,
      };
      this.httpService.post(`/api/armies/orders`, payload).subscribe(
        (r) => {
          this.orders = this.newOrder;
          this.messageService.success(this.translateService.instant('Military.Army.newOrderCreated'));
          this.newOrder = null;
          this.orderIsEdited = false;
          this.loadOrders();
        },
        (error) => {
          this.messageService.error(error.message);
        }
      );
    }
  }

  cancelOrder() {
    this.httpService.delete(`/api/armies/orders`).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant('Military.Army.orderCanceled'));
        this.loadOrders();
      },
      (error) => {
        this.messageService.error(error.message);
      }
    );
  }

  changeEditOrder() {
    this.orderIsEdited = !this.orderIsEdited;
    this.newOrder = null;
  }

  switchTo(id) {
    this.authService.switchToEntity(EntityType.Army, id);
  }

  join() {
    this.isLoading = true;
    this.httpService.post(`/api/armies/members/join`, { armyId: this.army.id }).subscribe(
      () => {
        this.messageService.success(this.translateService.instant('Military.Army.joinRequestSent'));
        this.loadData();
      },
      (error) => {
        this.messageService.error(error.message);
        this.isLoading = false;
      }
    );
  }

  resign() {
    this.isLoading = true;
    this.httpService.delete(`/api/armies/members/resign`).subscribe(
      () => {
        this.messageService.success(this.translateService.instant('Military.Army.YouLeftArmy'));
        this.loadData();
      },
      (error) => {
        this.messageService.error(error.message);
        this.isLoading = false;
      }
    );
  }

  showModPanel = () => {
    this.modalService.create({
      nzContent: EntityModPanelComponent,
      nzTitle: this.translateService.instant('Military.Army.modAccess'),
      nzFooter: null,
      nzWidth: '90%',
      nzData: {
        entity: this.army,
        entityType: 'Army'
      }
    });
  }
}
