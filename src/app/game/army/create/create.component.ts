import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-army-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class ArmyCreateComponent implements OnInit {
  isLoading = false;
  name = '';

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private modalRef: NzModalRef
  ) {}

  ngOnInit(): void {}

  create() {
    this.isLoading = true;
    this.httpService.post(`/api/armies`, { name: this.name }).subscribe(
      (r) => {
        this.modalRef.close(`ok`);
        this.router.navigateByUrl(`/army/${r}`);
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }
}
