import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { cloneDeep } from 'lodash-es';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-army-mass-travel',
  templateUrl: './mass-travel.component.html',
  styleUrls: ['./mass-travel.component.scss']
})
export class ArmyMassTravelComponent implements OnInit {
  isLoading = false;

  army;

  travelOptionsCost = [];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService,
    private modal: NzModalService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.army = { id: params['id'] };
      if (!this.army.id) this.router.navigateByUrl(`/armies`);
      this.load();
    });
  }

  load = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ army(id: "${this.army.id}"){ id name members { id citizen { id name avatar } } } }`).subscribe((r: any) => {
      this.army = cloneDeep(r.data.army);

      forkJoin({ currenciesDef: this.graphQlService.get(`game`, `{ currencies { id symbol value isMainCurrency } }`), currencies: this.httpService.get(`/api/currency`), inventory: this.httpService.get(`/api/inventory`) })
        .subscribe(
          (r: any) => {
            var goldOption = cloneDeep(r.currenciesDef.data.currencies.find(x => x.isMainCurrency === true));
            goldOption.amount = r.currencies.find(x => x.id === goldOption.id).amount;
            this.travelOptionsCost.push(goldOption)

            r.inventory.filter(x => x.name === "Item.Ticket").forEach(item => {
              this.travelOptionsCost.push({ id: item.id, symbol: item.name, value: item.icon, quality: item.quality, amount: item.amount })
            })
            this.isLoading = false;
          },
          (error) => {
            this.message.error(error.message);
            this.isLoading = false;
          }
        );
    })
  }

}
