import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { ArticleComponent } from './article/article.component';
import { ArticleListComponent } from './list/list.component';
import { ArticleManageComponent } from './manage/manage.component';

export const routes: Routes = [
  { path: 'article/:id', component: ArticleComponent },
  { path: 'articles', component: ArticleListComponent },
  {
    path: 'article/:articleId/edit',
    component: ArticleManageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'newspaper/:newspaperId/article/create',
    component: ArticleManageComponent,
    canActivate: [AuthGuard],
  },
];
