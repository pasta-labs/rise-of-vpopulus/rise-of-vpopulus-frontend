import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
declare var editormd: any;

@Component({
  selector: 'app-article-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss'],
})
export class ArticleManageComponent implements OnInit {
  isLoading = false;

  newspaper;
  article = undefined;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params['newspaperId']) this.loadNewspaper(params['newspaperId']);
      if (params['articleId']) this.loadArticle(params['articleId']);
    });
  }

  edit: any;
  ngAfterViewInit(): void {
    fetch(`assets/editormd/languages/en.json`)
      .then((x) => x.json())
      .then((en) => {
        this.edit = editormd('editorMdDiv', {
          path: 'assets/editormd/lib/',
          pluginPath: 'assets/editormd/plugins/',
          width: '100%',
          saveHTMLToTextarea: true,
          height: 400,
          toolbar: true,
          watch: true,
          lang: en,
          onchange: () => {
            this.article.content = this.edit.getValue();
            this.article.htmlContent = this.edit.getHTML();

            console.log('content', this.article.content)
            console.log('content', this.article.htmlContent)
            console.log('content', this.edit.getPreviewedHTML())
            console.log('content', this.edit.getMarkdown())
          },
          onpreviewed: () => this.edit.unwatch()
        });
        if (this.article) this.loadArticle(this.article.id);
      });
  }

  loadNewspaper = (id) => {
    this.graphQlService.get(`game`, `{ newspaper(id: "${id}") { id name  } }`).subscribe((r: any) => {
      this.newspaper = r.data.newspaper;
      this.article = {
        targetAudience: 'public'
      };
    });
  }

  loadArticle = (id) => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ article(id: "${id}") { id title content htmlContent isPublished targetAudience newspaper { id name }  } }`).subscribe((r: any) => {
      console.log('load article', r)
      this.article = r.data.article;
      this.newspaper = this.article.newspaper;

      this.edit.setValue(this.article.content);
      this.edit.unwatch();
      this.isLoading = false;
    });
  }

  preview = () => {
    this.edit.watch();
    this.edit.previewing();
  }

  save() {
    if (this.article.id) this.update();
    else this.create();
  }

  create = () => {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: CreateArticleCommand) { articleCreate(data: $data) }`, {
      data: {
        newspaperId: this.newspaper.id, title: this.article.title,
        content: this.article.content, htmlContent: this.article.htmlContent,
        targetAudience: this.article.targetAudience
      }
    }).subscribe((r: any) => {
      if (r.errors) {
        this.graphQlService.displayErrors(r.errors);
        this.isLoading = false;
      }
      else {
        this.messageService.success(this.translateService.instant('Social.Newspaper.ArticleCreated'));
        this.router.navigateByUrl(`/article/${r.data.articleCreate}/edit`);
      }
    });
  }

  update = () => {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: UpdateArticleCommand) { articleUpdate(data: $data) }`, {
      data: {
        articleId: this.article.id, title: this.article.title,
        content: this.article.content, htmlContent: this.article.htmlContent,
        targetAudience: this.article.targetAudience
      }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else this.messageService.success(this.translateService.instant('Social.Newspaper.ArticleUpdated'));
      this.isLoading = false;
    });
  }

  publish() {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: PublishArticleCommand) { articlePublish(data: $data) }`, {
      data: {
        articleId: this.article.id
      }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else this.messageService.success(this.translateService.instant('Social.Newspaper.ArticlePublished'));
      this.isLoading = false;
    });
  }
}
