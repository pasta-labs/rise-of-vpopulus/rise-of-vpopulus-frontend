import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from './article/article.component';
import { ArticleListComponent } from './list/list.component';
import { ArticleManageComponent } from './manage/manage.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ArticleComponent,
    ArticleListComponent,
    ArticleManageComponent,
  ],
  imports: [CommonModule, SharedModule],
  exports: [ArticleListComponent]
})
export class ArticleModule { }
