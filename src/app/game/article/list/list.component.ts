import { ViewEncapsulation } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ArticleListComponent implements OnInit {
  articles = undefined;
  unreadSubscribedArticlesCount = 0;

  countryId;
  pageIndex = 1;
  @Input() pageSize = 20;
  totalCount = 0;

  showCountryArticles = true;
  currentTab = undefined;

  @Input() showHeader = true;

  constructor(
    private graphQlService: GraphQlService,
    private httpService: GenericHttpService
  ) { }

  ngOnInit(): void {
    this.httpService.get(`/api/countries/current`).subscribe((countryId) => {
      this.countryId = countryId;
      if (!this.countryId) this.showCountryArticles = false;

      this.loadArticles();
    });
    if (+localStorage.getItem(`entityType`) === 1001) this.unreadSubscribedArticles();
  }

  loadArticles = () => {
    switch (this.currentTab) {
      default: return this.getNewestArticles();
      case "top": return this.getTopArticles();
      case "subscribed": return this.getSubscribedArticles();
    }
  }

  getNewestArticles = () => {
    this.currentTab = "newest";
    this.articles = undefined;
    this.graphQlService.get(`game`, `{ articlesNewest(pageIndex: ${this.pageIndex}, pageSize: ${this.pageSize}, countryId: ${(this.countryId && this.showCountryArticles === true) ? `"${this.countryId}"` : null}) { id title publishGameDay votesCount commentsCount newspaper { id name avatar } } }`).subscribe((r: any) => {
      this.articles = r.data.articlesNewest;
      this.totalCount = r.data.extensions.totalCount;
    });
  }

  getTopArticles = () => {
    this.currentTab = "top";
    this.articles = undefined;
    this.graphQlService.get(`game`, `{ articlesTop(pageIndex: ${this.pageIndex}, pageSize: ${this.pageSize}, countryId: ${(this.countryId && this.showCountryArticles === true) ? `"${this.countryId}"` : null}) { id title publishGameDay votesCount commentsCount newspaper { id name avatar } } }`).subscribe((r: any) => {
      this.articles = r.data.articlesTop;
      this.totalCount = r.data.extensions.totalCount;
      console.log('d', r)
    });
  }

  getSubscribedArticles = () => {
    this.currentTab = "subscribed";
    this.articles = undefined;
    this.graphQlService.get(`game`, `{ articleSubscriber(pageIndex: ${this.pageIndex}, pageSize: ${this.pageSize}) { id title publishGameDay votesCount commentsCount unread newspaper { id name avatar } } }`).subscribe((r: any) => {
      this.articles = r.data.articleSubscriber;
      this.totalCount = r.data.extensions.totalCount;
    });
  }

  unreadSubscribedArticles = () => {
    this.graphQlService.get(`game`, `{ articleSubscriberUnreadCount }`).subscribe((r: any) => {
      this.unreadSubscribedArticlesCount = r.data.articleSubscriberUnreadCount;
    });
  }

}
