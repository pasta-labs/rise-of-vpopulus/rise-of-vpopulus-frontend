import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, uniq, orderBy } from 'lodash-es';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent implements OnInit {
  isLoading = {
    article: true,
    comments: true,
  };

  article = undefined;

  profile;
  canVote = false;
  comment;

  canHideArticle = false;
  canHideComments = false;

  isCitizen = false;

  chartData = undefined;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messageService: NzMessageService,
    private router: Router,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.getArticle(params['id']);

      if (localStorage.getItem(`profile`)) {
        this.profile = JSON.parse(localStorage.getItem(`profile`));
        this.isCitizen = this.profile.entityType === "Citizen";
        var moderatorPermissions = JSON.parse(localStorage.getItem(`moderatorPermissions`));
        this.canHideArticle = moderatorPermissions?.Articles?.CanHideArticle;
        this.canHideComments = moderatorPermissions?.Articles?.CanHideArticleComment;
      }
    });
  }

  getArticle = (id) => {
    this.isLoading.article = true;
    this.graphQlService.get(`game`, `{ article(id: "${id}"){ id title htmlContent publishGameDay uniqueViews editors { id name } newspaper { id name owner { id } } } }`).subscribe((r: any) => {
      var article = cloneDeep(r.data.article);
      console.log('article', article)
      article.editors = article.editors
        .map((x) => `<a href="/citizen/${x.id}">${x.name}</a>`)
        .join(', ');

      this.article = article;
      if (this.article.newspaper.owner.id === localStorage.getItem(`entityId`))
        this.loadStats();

      this.getArticleComments();
      this.getVotes();
    });
  }

  loadStats = () => {
    this.graphQlService.get(`game`, `{ articleUniqueViewsStats(articleId: "${this.article.id}") }`).subscribe((r: any) => {
      console.log('stats', r)
      var data = orderBy(JSON.parse(r.data.articleUniqueViewsStats), x => x.Day, 'asc');

      this.chartData = {
        labels: uniq(data.map((x) => x.Day)),
        datasets: [
          {
            data: data.map((x) => x.Sub),
            label: 'Subscribe',
          },
          {
            data: data.map((x) => x.UnSub),
            label: 'Non-subscribers',
          },
        ],
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            yAxes: [
              {
                ticks: {
                  min: 0
                },
              },
            ],
          },
        },
      };
    })
  }

  getArticleComments() {
    this.isLoading.comments = true;
    this.httpService
      .get(`/api/social/articles/${this.article.id}/comments`)
      .subscribe((r) => {
        this.article['comments'] = r;
        this.article['comments'].forEach(item => {
          item.canDelete = item.author.id === this.profile?.id;
          item.createdOn = item.createdOn.split('.')[0].replace('T', ' ');
        })
        this.isLoading.comments = false;
      });
  }

  addComment() {
    this.isLoading.comments = true;
    this.httpService
      .post(`/api/social/articles/comments`, {
        articleId: this.article.id,
        comment: { description: this.comment },
      })
      .subscribe((r) => {
        this.messageService.success(this.translateService.instant('Social.Newspaper.commentBeenAdded'));
        this.comment = '';
        this.getArticleComments();
      });
  }

  getVotes() {
    this.httpService
      .get(`/api/social/articles/${this.article.id}/votes/count`)
      .subscribe((r) => {
        this.article['votes'] = r;
        this.isLoading.article = false;
      });
    if (localStorage.getItem(`jwtToken`))
      this.httpService
        .get(`/api/social/articles/${this.article.id}/canVote`)
        .subscribe((r) => {
          this.canVote = r;
        });
  }

  vote() {
    this.httpService
      .post(`/api/social/articles/${this.article.id}/votes`, {
        articleId: this.article.id,
      })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Social.Newspaper.Voted'));
          this.getVotes();
        },
        (error) => {
          this.messageService.error(this.translateService.instant('Social.Newspaper.alreadyVoted'));
        }
      );
    this.getVotes();
  }

  // = = = = = Moderator = = = = =
  deleteReason = '';
  deleteArticle() {
    this.httpService
      .delete(`/api/mp/articles/${this.article.id}?reason=${this.deleteReason}`)
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Social.Newspaper.articleBeenDeleted'));
          this.router.navigateByUrl(`/`);
          this.deleteReason = '';
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }

  deleteComment(commentId) {
    this.httpService
      .delete(
        `/api/mp/articles/comments/${commentId}?reason=${this.deleteReason}`
      )
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Social.Newspaper.articleCommentDeleted'));
          this.getArticleComments();
          this.deleteReason = '';
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }
}
