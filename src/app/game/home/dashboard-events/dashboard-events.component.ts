import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-dashboard-events',
  templateUrl: './dashboard-events.component.html',
  styleUrl: './dashboard-events.component.scss'
})
export class DashboardEventsComponent {
  isLoading = false;
  list: any[] = [];

  showCountry = false;

  constructor(private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.load();
  }

  load = async (showCountry = false) => {
    this.showCountry = showCountry;
    this.isLoading = true;

    try {
      let list: any[];

      if (showCountry) {
        const response = await firstValueFrom(this.graphQlService.get('game', `{ countryEvents(countryId: "${localStorage.getItem('countryId')}") { id country { id name }type data } }`));
        list = response.data['countryEvents'];
      } else {
        const response = await firstValueFrom(this.graphQlService.get('game', `{ worldEvents { id country1 { id name } country2 { id name } type data } }`));
        list = response.data['worldEvents'];
      }
      this.list = list.map(i => ({
        ...i,
        rawType: i.type.split('.')[2],
        data: JSON.parse(i.data || "{}"),
      }));
      this.list.forEach(i => {
        i.text = this.getText(i);
      })

      // console.log('list', this.list);
    } catch (error) {
      console.error('Error loading events', error);
    } finally {
      this.isLoading = false;
    }
  }

  getText = (item: any): string => {
    let data;
    try {
      data = typeof item.data === 'string' ? JSON.parse(item.data) : item.data;
    } catch (error) {
      // console.error('Error parsing JSON data:', error);
      data = {};
    }

    console.log('get text', item, data)

    if (this.showCountry === true) {
      switch (item.rawType) {
        case "NewBattle":
          return this.translateService.instant('Politics.Laws.newBattleStarted', {
            regionId: data.Region?.Id,
            regionName: data.Region?.Name
          });
        case "RegionCaptured":
          return this.translateService.instant('Politics.Laws.regionCapturedCountry', {
            regionId: data.Region?.Id,
            regionName: data.Region?.Name
          });
          case "NewPresident":
            return this.translateService.instant('Politics.Laws.newPresidentElected', {
              countryName: data.Country?.Name,
              presidentName: data.President?.Name
            });
          case "NewCongress":
            return this.translateService.instant('Politics.Laws.newCongressFormed', {
              countryName: data.Country?.Name
            });
        case "TradeEmbargo":
          switch (data.Status) {
            case "Pending":
              return `${this.translateService.instant('Politics.Laws.memberProposed')} ${this.translateService.instant(item.type)} ${this.translateService.instant('Politics.Laws.Law')}!`;
            case "Accepted":
              return this.translateService.instant('Politics.Laws.embargoAgreed', {
                countryId: data.Country?.Id,
                countryName: data.Country?.Name
              }); 
              case "Rejected":
              return `${this.translateService.instant('Politics.Laws.Law')} ${this.translateService.instant(item.type)} ${this.translateService.instant('Politics.Laws.hasRejected')}`;
            default:
              return `Unknown status for ${item.rawType}`;
          }
      }
    } else {
      switch (item.rawType) {
        case "NewBattle":
          return this.translateService.instant('Politics.Laws.battleStartedBetween', {
            country1Id: item.country1?.id,      
            country1Name: item.country1?.name,  
            country2Id: item.country2?.id,    
            country2Name: item.country2?.name,  
            regionId: data.Region?.Id,       
            regionName: data.Region?.Name   
          });
        
        case "RegionCaptured":
          return this.translateService.instant('Politics.Laws.battlesRegionCaptured', {
            regionId: data.Region?.Id,           
            regionName: data.Region?.Name,        
            capturingCountryId: item.country1?.id, 
            capturingCountryName: item.country1?.name 
          });
        
        case "TradeEmbargo":
          return this.translateService.instant('Politics.Laws.battlesTradeEmbargo', {
            country1Id: item.country1?.id,   
            country1Name: item.country1?.name,
            country2Id: item.country2?.id,   
            country2Name: item.country2?.name
          });
      }
      

    }

    switch (item.rawType) {
      case "ChangeFlag":
      case "CitizenFee":
      case "MinimalWage":
      case "PrintCurrency":
      case "CloseBorder":
      case "CreateConstruction":
      case "CreateNationalOrganization":
      case "NewspaperCensorship":
      case "WelcomeMessage":
      case "ReopenBorder":
      case "TaxChange":
      case "TransferLeadership":
      case "RevokeCitizenship":
      case "ReopenBorders":
      case "TradeEmbargo":
      case "ProposePeace":
      case "TradeAgreement":
      case "WithdrawCurrency":
        switch (data.Status) {
          case "Pending":
            return `${this.translateService.instant('Politics.Laws.memberProposed')} ${this.translateService.instant(item.type)} ${this.translateService.instant('Politics.Laws.Law')}!`;
          case "Accepted":
            return `${this.translateService.instant('Politics.Laws.Law')} ${this.translateService.instant(item.type)} ${this.translateService.instant('Politics.Laws.hasAccepted')}`;
          case "Rejected":
            return `${this.translateService.instant('Politics.Laws.Law')} ${this.translateService.instant(item.type)} ${this.translateService.instant('Politics.Laws.hasRejected')}`;
          default:
            return `Unknown status for ${item.rawType}`;
        }

      case "DeclareWar":
        switch (data.Status) {
          case "Pending":
            return `${this.translateService.instant('Politics.Laws.declareWarAgainst')} ${item.country1?.name || 'Unknown Country'} ${this.translateService.instant('Politics.Laws.hasBeenProposed')}`;
          case "Accepted":
            return `${this.translateService.instant('Politics.Laws.declareWarAgainst')} ${item.country1?.name || 'Unknown Country'} ${this.translateService.instant('Politics.Laws.hasAccepted')}`;
          case "Rejected":
            return `${this.translateService.instant('Politics.Laws.declareWarAgainst')} ${item.country1?.name || 'Unknown Country'} ${this.translateService.instant('Politics.Laws.hasRejected')}`;
          default:
            return `Unknown status for DeclareWar`;
        }

      case "RegionLost":
        return `${this.translateService.instant('Common.Region')} ${data.Region?.Name || 'Unknown Region'} ${this.translateService.instant('Politics.Laws.hasBeenLost')}`;




      default:
        return `Unknown event type: ${item.type}`;
    }
  }

  getLink = (item: any): string => {
    const countryId = item.country1?.id || localStorage.getItem('countryId') || 'unknown';

    switch (item.rawType) {
      case "ChangeFlag":
      case "CitizenFee":
      case "MinimalWage":
      case "PrintCurrency":
      case "CloseBorder":
      case "CreateConstruction":
      case "CreateNationalOrganization":
      case "NewspaperCensorship":
      case "WelcomeMessage":
      case "ReopenBorder":
      case "TaxChange":
      case "TransferLeadership":
      case "WithdrawCurrency":
      case "DeclareWar":
      case "ProposePeace":
      case "TradeAgreement":
      case "RevokeCitizenship":
      case "ReopenBorders":
      case "TradeEmbargo":
      case "TradeAgreement":
      case "CancelTradeAgreement":
        return `country/${countryId}/law/${item.data?.Id || 'unknown'}`;

      case "NewBattle":
        return '';

      default:
        return '';
    }
  }
}
