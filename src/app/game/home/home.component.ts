import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { padStart, orderBy } from 'lodash';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  isLoading = false;
  activeBattles;

  countryId;
  orders;

  electionVoteDay;

  currentRegion = undefined;

  constructor(
    private httpService: GenericHttpService,
    private graphQlService: GraphQlService,
    private authService: AuthService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.graphQlService.get(`game`, `{ entity { region { id x y }}}`).subscribe((r: any) => {
      this.currentRegion = r.data.entity.region;
    });


    this.getActiveBattles();
    this.getCountryId();
    this.upcomingElections();
    this.getOrders();

    this.authService.updateTopBar.subscribe(() => {
      this.currentRegion = undefined;
      this.graphQlService.get(`game`, `{ entity { region { id x y }}}`).subscribe((r: any) => {
        this.currentRegion = r.data.entity.region;
      });
      this.getCountryId();
      this.getActiveBattles();
    });

  }
  getElectionTypeTranslation(): string {
    return this.translate.instant(`Politics.ElectionsTypes.${this.electionVoteDay.type}`);
  }
  upcomingElections = () => {
    this.graphQlService.get(`game`, `{ electionsGetUpcoming { id type voteGameDay } }`)
      .subscribe((r: any) => {
        var day = localStorage.getItem(`gameDay`);
        this.electionVoteDay = r.data.electionsGetUpcoming.find(x => x.voteGameDay === +day);
      });
  }

  getActiveBattles() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ battlesActive { id startingWallSize secondsLeft wallSize winnerId isResistance isCivilWar region { id name } attackerCountry { id name flagLocation(size: "xl") } defenderCountry { id name flagLocation(size: "xl") } } }`)
      .subscribe((r: any) => {
        this.activeBattles = r.data.battlesActive;
        this.activeBattles.forEach((item) => {
          item.endDateTime = new Date(Date.now())
          item.endDateTime.setSeconds(item.secondsLeft);
        });
        this.activeBattles = orderBy(
          this.activeBattles,
          (x) => x.secondsLeft
        );
        console.log('r', this.activeBattles)
        this.isLoading = false;
      })
  }

  getCountryId() {
    this.httpService.get(`/api/countries/current`).subscribe((r) => {
      this.countryId = r;
    });
  }

  getOrders() {
    this.httpService.get(`/api/armies/orders`).subscribe((r) => {
      this.orders = r;
    });
  }
}
