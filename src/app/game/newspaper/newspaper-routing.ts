import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { NewspaperCreateComponent } from './create/create.component';
import { NewspaperProfileComponent } from './profile/profile.component';

export const routes: Routes = [
  {
    path: 'newspaper', component: NewspaperProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'newspaper/:id', component: NewspaperProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'newspapers/create',
    component: NewspaperCreateComponent,
    canActivate: [AuthGuard],
  },
];
