import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewspaperCreateComponent } from './create/create.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntityModule } from '../entity/entity.module';
import { NewspaperProfileComponent } from './profile/profile.component';
import { NewspaperEditorsComponent } from './profile/components/editors/editors.component';
import { NewspaperArticlesComponent } from './profile/components/articles/articles.component';
import { NewspaperSubscribersComponent } from './profile/components/subscribers/subscribers.component';
import { NewspaperMedalsComponent } from './profile/components/medals/medals.component';

@NgModule({
  declarations: [
    NewspaperCreateComponent,
    NewspaperProfileComponent,
    NewspaperEditorsComponent,
    NewspaperArticlesComponent,
    NewspaperSubscribersComponent,
    NewspaperMedalsComponent,
  ],
  imports: [CommonModule, SharedModule, EntityModule],
})
export class NewspaperModule {}
