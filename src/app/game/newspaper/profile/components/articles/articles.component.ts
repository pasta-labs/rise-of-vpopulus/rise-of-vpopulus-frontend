import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { take, drop } from 'lodash-es';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-newspaper-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class NewspaperArticlesComponent implements OnInit {
  isLoading = true;

  list = [];
  filteredList = [];

  currentPage = 1;

  @Input() newspaperId;
  @Input() canManage = false;
  @Input() isEditor = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService,

  ) {}

  ngOnInit(): void {
    this.loadArticles();
  }

  loadArticles() {
    this.isLoading = true;
    this.httpService
      .get(`/api/newspapers/${this.newspaperId}/articles`)
      .subscribe((r) => {
        this.list = r;
        this.filteredList = take(this.list, 5);
        this.isLoading = false;
      });
  }

  delete(id) {
    this.isLoading = true;
    this.httpService.delete(`/api/articles/${id}`).subscribe(() => {
      this.messageService.success(this.translateService.instant('Social.Newspaper.articleBeenDeleted'));
      this.loadArticles();
    });
  }

  filterDetailedList = () => 
    this.filteredList = take(drop(this.list, (this.currentPage - 10) * 10), 1);
}

