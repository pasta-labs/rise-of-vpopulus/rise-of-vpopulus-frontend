import { Component, Input, OnInit } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { uniq, orderBy, take, drop } from 'lodash-es';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-newspaper-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.scss'],
})
export class NewspaperSubscribersComponent implements OnInit {
  isLoading = true;

  list = [];
  filteredList = [];

  currentPage = 1;

  chartData = undefined;

  showAllSubsModalVisible = false;

  @Input() newspaperId;

  constructor(
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.loadChart();
  }

  loadChart = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ newspaperSubscribersStats(newspaperId: "${this.newspaperId}") }`).subscribe((r: any) => {
      var data = orderBy(JSON.parse(r.data.newspaperSubscribersStats), x => x.Day, 'asc');

      this.chartData = {
        labels: uniq(data.map((x) => x.Day)),
        datasets: [
          {
            data: data.map((x) => x.Sub),
            label: this.translateService.instant('Common.Buttons.Subscribe'),
          },
          {
            data: data.map((x) => x.UnSub),
            label: this.translateService.instant('Common.Buttons.Unsubscribe'),
          },
        ],
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            yAxes: [
              {
                ticks: {
                  min: 0
                },
              },
            ],
          },
        },
      };
      this.isLoading = false;
    })
  }

  loadDetailed = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ newspaperSubscribers(newspaperId: "${this.newspaperId}") { startGameDay endGameDay isActive citizen { id name avatar } } }`).subscribe((r: any) => {
      this.list = r.data.newspaperSubscribers;
      this.filteredList = take(this.list, 10);
      this.isLoading = false;
    })
  }

  filterDetailedList = () => 
    this.filteredList = take(drop(this.list, (this.currentPage - 10) * 10), 1);
  
}
