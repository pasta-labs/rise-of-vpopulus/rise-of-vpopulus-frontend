import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-newspaper-editors',
  templateUrl: './editors.component.html',
  styleUrls: ['./editors.component.scss'],
})
export class NewspaperEditorsComponent implements OnInit {
  isLoading = true;

  list = [];

  @Input() newspaperId;

  constructor(private httpService: GenericHttpService) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.httpService
      .get(`/api/social/newspapers/${this.newspaperId}/editors`)
      .subscribe((r) => {
        this.list = r;
        this.isLoading = false;
      });
  }
}
