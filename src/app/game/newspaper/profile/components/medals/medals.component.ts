import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-newspaper-medals',
  templateUrl: './medals.component.html',
  styleUrls: ['./medals.component.scss'],
})
export class NewspaperMedalsComponent implements OnInit {
  isLoading = true;

  @Input() newspaperId;

  list = [
    {
      title: this.translateService.instant('Medals.Likes'),
      description: this.translateService.instant('Medals.get1kLikes'),
      icon: `assets/img/icons/newspaper/likes/like-`,
      level: '01',
      isAchieved: false
    },
    {
      title: this.translateService.instant('Social.Newspaper.Subscribers'),
      description: this.translateService.instant('Medals.get500Subscribers'),
      icon: `assets/img/icons/newspaper/subscribers/subscriber-`,
      level: '02',
      isAchieved: true
    },
    {
      title: this.translateService.instant('Medals.Views'),
      description: this.translateService.instant('Medals.get1kArticle'),
      icon: `assets/img/icons/newspaper/views/views-`,
      level: '03',
      isAchieved: true
    }
  ];

  constructor(
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.list.forEach(item => item.icon = this.getIcon(item));

    this.isLoading = false;
  }

  getIcon = (item) => {
    return `${item.icon}${item.level}.png`
  }
}
