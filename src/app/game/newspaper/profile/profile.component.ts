import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { EntityType } from 'src/app/shared/models/EntityType';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-newspaper',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class NewspaperProfileComponent implements OnInit {
  apiUrl = environment.apiUrl;

  isLoading = false;

  id;
  newspaper;
  entityType;

  isSubscriber = undefined;

  newSubscriptionPrice = undefined;
  setNewSubscriptionPriceModalVisible = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: NzMessageService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.entityType = EntityType[localStorage.getItem(`entityType`)];

      if (this.id === undefined) {
        this.httpService.get(`/api/newspapers/current`).subscribe((r) => {
          if (r == null) this.router.navigateByUrl(`/newspapers/create`);
          else this.router.navigateByUrl(`/newspaper/${r}`);
        });
      } else {
        this.loadData();
      }
    });
  }

  loadData() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ newspaper(id: "${this.id}") { id name avatar subscriptionPrice activeSubscribersCount isEntityManager isEditor owner { id name type } } }`).subscribe((r: any) => {
      this.newspaper = r.data.newspaper;
      this.newSubscriptionPrice = this.newspaper.subscriptionPrice;

      this.graphQlService.get(`game`, `{ newspaperIsSubscriber(newspaperId: "${this.id}") }`).subscribe((isSub: any) => {
        if (this.entityType == EntityType[EntityType.Citizen]) this.isSubscriber = isSub.data.newspaperIsSubscriber;
      })

      console.log('newspaper', r, this.isSubscriber)
      this.isLoading = false;
    });
  }

  subscribe() {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: SubscribeNewspaperCommand) { newspaperSubscribe(data: $data) }`, { data: { newspaperId: this.newspaper.id } }).subscribe((r: any) => {
      if (r.errors) {
        this.graphQlService.displayErrors(r.errors);
        this.isLoading = false;
      }
      else { 
        this.messageService.success(this.translateService.instant('Social.Newspaper.Subscribed') `!`);
        this.loadData();
      }
    });
  }

  unsubscribe() {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: UnsubscribeNewspaperCommand) { newspaperUnsubscribe(data: $data) }`, { data: { newspaperId: this.newspaper.id } }).subscribe((r: any) => {
      if (r.errors) {
        this.graphQlService.displayErrors(r.errors);
        this.isLoading = false;
      }
      else {
        this.messageService.success(this.translateService.instant('Social.Newspaper.Unsubscribed') `!`);
        this.loadData();
      }
    });
  }

  setSubscriptionPrice = () => {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: SetNewspaperSubscriptionCostCommand) { newspaperSetSubscriptionPrice(data: $data) }`, { data: { newspaperId: this.newspaper.id, price: this.newSubscriptionPrice } }).subscribe((r: any) => {
      if (r.errors) {
        this.graphQlService.displayErrors(r.errors);
        this.isLoading = false;
      }
      else {
        this.setNewSubscriptionPriceModalVisible = false;
        this.loadData();
      }
    });
  }
}
