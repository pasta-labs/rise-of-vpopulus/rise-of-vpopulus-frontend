import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-newspaper-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class NewspaperCreateComponent implements OnInit {
  isLoading = false;
  name = '';

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  create() {
    this.isLoading = true;
    this.httpService.post(`/api/newspapers`, { name: this.name }).subscribe(
      (r) => {
        this.router.navigateByUrl(`/newspaper/${r}`);
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }
}
