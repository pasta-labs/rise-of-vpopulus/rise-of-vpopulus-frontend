import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { firstValueFrom } from 'rxjs';
import { EntityType } from 'src/app/shared/models/EntityType';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.scss'],
})
export class CurrenciesComponent implements OnInit {
  isLoading = true;

  historyDataLoading = false;
  historyModalVisible = false;

  currencies = [];
  allCurrencies = undefined;

  modifyCurrencyForm: FormGroup = undefined;

  @Input() entityType: EntityType = EntityType.Citizen;
  @Input() entityId;
  @Input() showHistoryButton = false;
  @Input() canEdit = false;

  constructor(
    private graphQlService: GraphQlService,
    private messageService: NzMessageService,
    private translateService: TranslateService,

  ) { }

  ngOnInit() {
    this.loadFinances();
  }

  loadFinances() {
    this.modifyCurrencyForm = undefined;
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ entityCurrencies${this.entityType && this.entityId ? `(entityType: "${this.entityType}", entityId: "${this.entityId}")` : ""} { id amount currency { id symbol value flagLocation } } }`)
      .subscribe(r => {
        this.currencies = r.data['entityCurrencies'];
        this.isLoading = false;
      });
  }

  // = = = = = History = = = = = //
  historyDonations = {
    isLoading: true,
    totalEntries: 20,
    pageSize: 10,
    pageIndex: 1,
    list: [
      {
        from: 'Admin',
        fromItems: '2 Weapons (Q3), 3 Tickets (Q1)',
        date: new Date(),
      },
    ],
  };
  historyTrades = {
    isLoading: true,
    totalEntries: 20,
    pageSize: 10,
    pageIndex: 1,
    list: [
      {
        from: 'Admin',
        to: 'You',
        fromItems: '5 Weapons (Q1), 10 Food (Q2)',
        toItems: '3 Gold',
        date: new Date(),
      },
    ],
  };
  goldPurchases = {
    isLoading: true,
    totalEntries: 20,
    pageSize: 10,
    pageIndex: 1,
    list: [{ amount: 1, date: new Date() }],
  };

  loadHistory() {
    this.historyModalVisible = true;

    setTimeout(() => {
      this.historyDonations.isLoading = false;
      this.historyTrades.isLoading = false;
      this.goldPurchases.isLoading = false;
    }, 3000);
  }

  showModifyForm = async (currencyId = undefined) => {
    if (!currencyId) {
      this.allCurrencies = (await firstValueFrom(this.graphQlService.get(`game`, `{ currencies { id symbol value flagLocation(size: "s") } }`)))
        .data['currencies'].filter(x => !this.currencies.map(c => c.currency.id).includes(x.id));
    }

    this.modifyCurrencyForm = new FormGroup({
      entityType: new FormControl(this.entityType, [Validators.required]),
      entityId: new FormControl(this.entityId, [Validators.required]),
      currencyId: new FormControl(currencyId, [Validators.required]),
      action: new FormControl('+', [Validators.required]),
      amount: new FormControl(null, [Validators.required]),
      reason: new FormControl(null, [Validators.required]),
    })
  }

  submitModifyForm = () => {
    if (this.modifyCurrencyForm.status == "INVALID")
      return;

    this.modifyCurrencyForm.get(`amount`).setValue(+this.modifyCurrencyForm.get(`amount`).value);

    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: EntityModifyCurrencyCommand) { modifyCurrency(data: $data) }`, {
      data: this.modifyCurrencyForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.loadFinances();
        this.messageService.success(this.translateService.instant('Entity.Updated'));
        this.modifyCurrencyForm = undefined;
      }
      this.isLoading = false;
    });


  }

}
