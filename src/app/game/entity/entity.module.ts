import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccommodationComponent } from './accommodation/accommodation.component';
import { CurrenciesComponent } from './currencies/currencies.component';
import { InventoryComponent } from './inventory/inventory.component';
import { TravelComponent } from './travel/travel.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntityAvatarComponent } from './components/avatar/avatar.component';
import { EntityBanManageComponent } from './components/ban-manage/ban-manage.component';
import { EntityBioComponent } from './components/bio/bio.component';
import { EntityCompanyListComponent } from './components/company-list/company-list.component';
import { EntityMembersComponent } from './components/members/members.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { EntityModeratorNotesListComponent } from './notes/list/list.component';
import { EntityNameComponent } from './components/name/name.component';
import { MapModule } from '../shared/maps/map.module';
import { HotelsTabComponent } from './accommodation/hotels-tab/hotels-tab.component';
import { HousesTabComponent } from './accommodation/houses-tab/houses-tab.component';
import { CreateConstructionComponent } from './accommodation/create-construction/create-construction.component';
import { ManageConstructionComponent } from './accommodation/manage-construction/manage-construction.component';
import { EntitySettingsComponent } from './components/settings/settings.component';
import { ItemsListComponent } from './items-list/items-list.component';
import { EntityModPanelComponent } from './components/entity-mod-panel/entity-mod-panel.component';
import { EntitySessionActionsComponent } from 'src/app/mod-panel/entity-session-actions/entity-session-actions.component';
import { ModPanelEntitySessionsComponent } from 'src/app/mod-panel/entity-sessions/entity-sessions.component';
import { ModPanelEntityNotesComponent } from 'src/app/mod-panel/mod-panel-entity-notes/mod-panel-entity-notes.component';
import { ModPanelEntityTradesComponent } from 'src/app/mod-panel/mod-panel-entity-trades/mod-panel-entity-trades.component';

@NgModule({
  declarations: [
    AccommodationComponent,
    CurrenciesComponent,
    InventoryComponent,
    NotificationsComponent,
    TravelComponent,
    EntityAvatarComponent,
    EntityBanManageComponent,
    EntityBioComponent,
    EntityCompanyListComponent,
    EntityMembersComponent,
    EntityModeratorNotesListComponent,
    EntityNameComponent,
    HotelsTabComponent,
    HousesTabComponent,
    CreateConstructionComponent,
    ManageConstructionComponent,
    EntitySettingsComponent,
    ItemsListComponent,
    EntityModPanelComponent
  ],
  exports: [
    CurrenciesComponent,
    EntityBioComponent,
    EntityMembersComponent,
    EntityCompanyListComponent,
    EntityBanManageComponent,
    EntityAvatarComponent,
    EntityModeratorNotesListComponent,
    EntityNameComponent,
    EntitySettingsComponent,
    InventoryComponent,
    ItemsListComponent
  ],
  imports: [CommonModule, SharedModule, MapModule, EntitySessionActionsComponent, ModPanelEntitySessionsComponent,
    ModPanelEntityNotesComponent,
    ModPanelEntityTradesComponent
  ]
})
export class EntityModule { }
