import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import _ from 'lodash';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Apollo, gql } from 'apollo-angular';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss'],
})
export class InventoryComponent implements OnInit {
  profile;

  ngOnInit(): void {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    console.log('profile', this.profile)
  }


}
