import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { firstValueFrom } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.scss'],
})
export class TravelComponent implements OnInit {
  isLoading = false;

  region;

  selectedTravelOption;
  selectedRegion;

  travelOptions = [
    { name: 'Gold', type: 0, icon: 'assets/img/icons/currencies/gold.png', Required: 0, Available: 0, CanUse: false },
    { name: 'Ticket Q1', type: 1, icon: 'assets/img/icons/Item.Ticket.png', Required: 0, Available: 0, CanUse: false },
    { name: 'Ticket Q2', type: 2, icon: 'assets/img/icons/Item.Ticket.png', Required: 0, Available: 0, CanUse: false },
    { name: 'Ticket Q3', type: 3, icon: 'assets/img/icons/Item.Ticket.png', Required: 0, Available: 0, CanUse: false },
  ];

  distance = undefined;

  constructor(
    private messageService: NzMessageService,
    private authService: AuthService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.graphQlService.get(`game`, `{ entity { region { id x y name } } }`).subscribe((r: any) => {
      this.region = r.data.entity.region;
    });
  }

  regionSelected = async (selectedRegion) => {
    this.selectedRegion = selectedRegion;

    const distanceResponse = await firstValueFrom(this.graphQlService.get(`game`, `{ travelGetDistance(fromRegionId: "${this.region.id}", toRegionId: "${this.selectedRegion.id}") }`));
    this.distance = distanceResponse.data['travelGetDistance'];

    const optionsResponse = await firstValueFrom(this.graphQlService.get(`game`, `{ travelGetOptions(fromRegionId: "${this.region.id}", toRegionId: "${this.selectedRegion.id}") }`));

    var options = JSON.parse(optionsResponse.data['travelGetOptions']);
    this.travelOptions.find(x => x.type === 0).Available = options.TravelOptionGold.Available;
    this.travelOptions.find(x => x.type === 0).Required = options.TravelOptionGold.Required;
    this.travelOptions.find(x => x.type === 0).CanUse = options.TravelOptionGold.CanUse;

    options.TravelOptionTickets.forEach(o => {
      this.travelOptions.find(x => x.type === o.Quality).Available = o.Available;
      this.travelOptions.find(x => x.type === o.Quality).Required = o.Required;
      this.travelOptions.find(x => x.type === o.Quality).CanUse = o.CanUse;
    })
  }

  get isDisabled() {
    if (!this.selectedRegion || !this.selectedTravelOption) return true;
    return !this.selectedTravelOption.CanUse;
  }

  travel() {
    this.isLoading = true;

    this.graphQlService.post(`game`, `mutation mutate($data: TravelCommand) { travel(data: $data) }`, {
      data: {
        regionId: this.selectedRegion.id,
        ticketQuality: +this.selectedTravelOption.type
      }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.region = undefined;
        this.messageService.success(
          `${this.translateService.instant('Entity.Inventory.youMovedRegion')} ${this.selectedRegion.name}`
        );
        this.graphQlService.get(`game`, "{ topbar { id entityType name avatar region { id x y name country { id name currencyId } } } }")
          .subscribe((r: any) => {
            localStorage.setItem('profile', JSON.stringify(r.data.topbar));
            this.region = r.data.topbar.region;

            this.authService.updateTopBar.emit();
            this.authService.updateNavBar.emit();

            this.selectedRegion = undefined;
            this.selectedTravelOption = undefined;
          });
      }
      this.isLoading = false;
    });
  }
}
