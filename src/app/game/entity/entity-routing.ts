import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { AccommodationComponent } from './accommodation/accommodation.component';
import { InventoryComponent } from './inventory/inventory.component';
import { NotificationsComponent } from './notifications/notifications.component';

export const routes: Routes = [
  {
    path: 'accommodation',
    component: AccommodationComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'inventory',
    component: InventoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'notifications',
    component: NotificationsComponent,
    canActivate: [AuthGuard],
  },
];
