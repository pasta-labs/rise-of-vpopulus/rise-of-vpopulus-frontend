import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { NzMessageService } from 'ng-zorro-antd/message';
import { firstValueFrom } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { sum } from 'lodash-es';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrl: './items-list.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class ItemsListComponent {
  isLoading = false;

  profile;

  items = [];
  itemsCount = 0;

  isSettingsModalVisible = false;

  @Input() canEdit = false;
  @Input() entityType;
  @Input() entityId;

  selectedItem = undefined;
  modifyForm: FormGroup = undefined;

  foodQuality = '1';
  foodAmount = 1;

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    console.log('profile', this.profile)
    this.loadData();
  }

  loadData = async () => {
    const items = await firstValueFrom(this.httpService.get(`/api/dictionaries/items`));
    const itemsResponse = (await firstValueFrom(this.graphQlService.get(`game`, `{ inventory(entityType: "${this.entityType}", entityId: "${this.entityId}"){ id item { id symbol value } quality amount } }`))).data['inventory'];

    const inventory = items.map((itemType) => {
      let array = [];
      for (let index = 0; index < 3; index++) {
        array.push({
          amount:
            itemsResponse.find(
              (item) => item.item.id === itemType.id && item.quality == index + 1
            )?.amount || 0,
          icon: itemType.value,
          id: itemType.id,
          name: itemType.symbol,
          quality: index + 1,
        });
      }
      return array;
    });
    this.itemsCount = sum(inventory.map(c => sum(c.map(v => v.amount))));

    this.items = inventory.sort((a, b) =>
      a[0].name > b[0].name ? 1 : b[0].name > a[0].name ? -1 : 0
    );
    // if (this.profile?.entityType.toLowerCase() === 'citizen')
    //   this.graphQlService
    //     .get(
    //       `game`,
    //       `{ citizen(id: "${this.profile?.id}") { preferredFoodAmount preferredFoodQuality }}`
    //     )
    //     .subscribe((r: any) => {
    //       this.foodQuality = r.data.citizen.preferredFoodQuality;
    //       this.foodAmount = r.data.citizen.preferredFoodAmount;
    //     });
  }

  saveSettings() {
    this.httpService
      .post(`/api/entities/citizens/settings/daychange/food`, {
        quality: this.foodQuality,
        amount: this.foodAmount,
      })
      .subscribe(
        () => {
          this.message.success(this.translateService.instant('Citizen.Inventory.savedSettings'));
        },
        (error) => {
          this.message.error(error.Message);
        }
      );
  }

  showModifyForm = async (item = undefined) => {
    console.log('form?', item)
    this.selectedItem = item;
    this.modifyForm = new FormGroup({
      entityType: new FormControl(this.entityType, [Validators.required]),
      entityId: new FormControl(this.entityId, [Validators.required]),
      itemId: new FormControl(item.id, [Validators.required]),
      quality: new FormControl(item.quality, [Validators.required]),
      action: new FormControl('+', [Validators.required]),
      amount: new FormControl(null, [Validators.required]),
      reason: new FormControl(null, [Validators.required]),
    })
  }

  submitModifyForm = () => {
    if (this.modifyForm.status == "INVALID")
      return;

    this.modifyForm.get(`amount`).setValue(+this.modifyForm.get(`amount`).value);

    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: EntityModifyInventoryCommand) { modifyInventory(data: $data) }`, {
      data: this.modifyForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.loadData();
        this.message.success(this.translateService.instant('Entity.Updated'));
        this.modifyForm = undefined;
      }
      this.isLoading = false;
    });
  }

}
