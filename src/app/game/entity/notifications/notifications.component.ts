import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationsComponent implements OnInit {
  isLoading = true;

  categories = [];

  notifications = [];
  countryId
  pageIndex = 1;
  total = 0;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.loadCategories();
    this.countryId = localStorage.getItem(`countryId`);

  }

  loadCategories() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ notificationCategories { id name totalCount unreadCount } }`)
      .subscribe((r: any) => {
        this.categories = r.data['notificationCategories'];
        this.loadData(this.categories[0].id);
      });
  }

  loadData(categoryId) {
    this.isLoading = true;
    this.countryId = localStorage.getItem('countryId');

    this.graphQlService.get(`game`, `{ notifications(categoryId: "${categoryId}", pageIndex: ${this.pageIndex}) { id type { id name categoryId } payload createdOn } }`)
      .subscribe((r: any) => {
        console.log('r', r)
        this.notifications = r.data['notifications'].map(x => {
          let parsedPayload = JSON.parse(x.payload);
          parsedPayload.countryId = this.countryId;
          return {
            ...x,
            createdOn: x.createdOn.replace('T', ' ').split('.')[0],
            payload: parsedPayload
          };
        });      this.total = r.data.extensions.total;

        console.log('list', this.notifications);
        this.isLoading = false;
      });
  }
}
