import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-create-construction',
  templateUrl: './create-construction.component.html',
  styleUrls: ['./create-construction.component.scss'],
})
export class CreateConstructionComponent implements OnInit {
  isLoading = false;

  constructionTypes = [];
  constructionType = undefined;

  form: UntypedFormGroup;

  constructor(
    private httpService: GenericHttpService,
    private modalRef: NzModalRef,
    private message: NzMessageService,
    private translateService: TranslateService
  ) {
    this.form = new UntypedFormGroup({
      constructionType: new UntypedFormControl(null, [Validators.required]),
      name: new UntypedFormControl('Name'),
      quality: new UntypedFormControl(1, [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.getConstructions();
  }

  getConstructions() {
    this.httpService.get(`/api/dictionaries/constructions`).subscribe((r) => {
      this.constructionTypes = r.filter(
        (x) =>
          x.symbol.toLowerCase().includes('house') ||
          x.symbol.toLowerCase().includes('hotel')
      );
      console.log('types', r);
    });
  }

  constructionTypeSelected(id) {
    this.constructionType = this.constructionTypes.find((x) => x.id === id);
    this.form
      .get(`name`)
      .setValue(
        this.translateService.instant(`Economy.` + this.constructionType.symbol)
      );
  }

  submit() {
    this.isLoading = true;
    this.httpService
      .post(`/api/constructions/craft`, this.form.getRawValue())
      .subscribe(
        (r) => {
          this.message.success(this.translateService.instant('Economy.Construction.constructionCreated'));
          this.modalRef.close(`ok`);
        },
        (error) => {
          this.message.error(error.error.Message);
        }
      );
  }

  close() {
    this.modalRef.close(``);
  }
}
