import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { orderBy } from 'lodash';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ManageConstructionComponent } from '../manage-construction/manage-construction.component';
import { AuthService } from 'src/app/shared/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-hotels-tab',
  templateUrl: './hotels-tab.component.html',
  styleUrls: ['./hotels-tab.component.scss'],
})
export class HotelsTabComponent implements OnInit {
  isLoading = false;
  hotelsList = [];

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private authService: AuthService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.getHotels();
  }

  getHotels() {
    this.isLoading = true;
    this.graphQlService
      .get(
        `game`,
        `{ hotels { id name quality price owner { id } currency { symbol } region { id x, y name country { currency { symbol } } } } }`
      )
      .subscribe((r: any) => {
        this.hotelsList = r.data.hotels;
        this.isLoading = false;
        this.httpService
          .get(`/api/constructions/hotels/canUse`)
          .subscribe((r) => {
            var profile = JSON.parse(localStorage.getItem(`profile`));
            this.hotelsList = orderBy(
              this.hotelsList.map((x) => ({
                ...x,
                canManage: x.owner.id === profile.id,
                canUse:
                  r === true && profile.region.id === x.region.id
                    ? true
                    : false,
              })),
              ['canUse', 'price'],
              ['desc', 'asc']
            );
          });
      });
  }

  use(construction) {
    this.isLoading = true;
    this.httpService
      .post(`/api/constructions/hotels/use`, {
        hotelId: construction.id,
      })
      .subscribe(
        () => {
          this.message.success(
            this.translateService.instant('Economy.Construction..usedHotelHealth', { health: construction.quality * 10 })
          );
          this.authService.updateTopBar.emit();
          this.getHotels();
        },
        (error) => {
          this.message.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  manage(item) {
    this.modalService
      .create({
        nzTitle: this.translateService.instant('Entity.Manage'),
        nzWidth: '90%',
        nzContent: ManageConstructionComponent,
        nzData: {
          type: 'hotel',
          construction: item,
        },
        nzFooter: null,
      })
      .afterClose.subscribe(() => this.getHotels());
  }
}
