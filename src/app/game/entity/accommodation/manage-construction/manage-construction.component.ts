import { Component, Input, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-manage-construction',
  templateUrl: './manage-construction.component.html',
  styleUrls: ['./manage-construction.component.scss'],
})
export class ManageConstructionComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  type;
  construction;

  rentPrice = 1;

  currentSellOffer = undefined;
  sellPrice = 1;

  constructor(
    private modalRef: NzModalRef,
    private httpService: GenericHttpService,
    private messageService: NzMessageService,    
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.type = this.nzModalData.type;
    this.construction = this.nzModalData.construction;

    console.log('construction', this.construction);
    this.rentPrice = this.construction.price ? this.construction.price : 0;
    this.loadSellOffer();
  }

  close = () => this.modalRef.close();

  updatePrice() {
    this.isLoading = true;
    this.httpService
      .put(`/api/constructions/hotels/price`, {
        hotelId: this.construction.id,
        price: this.rentPrice,
      })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Economy.Construction.rentPriceChanged'));
          this.isLoading = false;
        },
        (e) => {
          this.messageService.error(e.error.Message);
          this.isLoading = false;
        }
      );
  }

  loadSellOffer() {
    this.currentSellOffer = undefined;
    this.isLoading = true;
    this.httpService
      .get(`/api/markets/estate/constructions/${this.construction.id}`)
      .subscribe(
        (r) => {
          this.currentSellOffer = r;
          console.log('offer', this.currentSellOffer);
          this.isLoading = false;
        },
        (e) => (this.isLoading = false)
      );
  }

  sell() {
    this.isLoading = true;
    this.httpService
      .post(`/api/markets/estate`, {
        constructionId: this.construction.id,
        price: this.sellPrice,
      })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Economy.Construction.marketOfferAdded'));
          this.loadSellOffer();
        },
        (e) => {
          this.messageService.error(e.error.Message);
        }
      );
  }

  removeOffer() {
    this.isLoading = true;
    this.httpService
      .delete(`/api/markets/estate/${this.construction.id}`)
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Economy.Construction.marketOfferRemoved'));
          this.loadSellOffer();
        },
        (e) => {
          this.messageService.error(e.error.Message);
        }
      );
  }
}
