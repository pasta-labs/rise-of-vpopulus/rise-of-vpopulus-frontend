import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ManageConstructionComponent } from '../manage-construction/manage-construction.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-houses-tab',
  templateUrl: './houses-tab.component.html',
  styleUrls: ['./houses-tab.component.scss'],
})
export class HousesTabComponent implements OnInit {
  isLoading = false;
  housesList = [];

  deviceDisplayType = 'full';


  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private authService: AuthService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.deviceDisplayType = localStorage.getItem(`deviceDisplayType`);

    this.getHouses();
  }

  getHouses() {
    this.isLoading = true;
    this.graphQlService
      .get(
        `game`,
        `{ houses { id name quality region { id x, y name country { currency { symbol } } } } }`
      )
      .subscribe((r: any) => {
        this.housesList = r.data.houses;
        this.isLoading = false;
        this.httpService
          .get(`/api/constructions/houses/canUse`)
          .subscribe((r) => {
            var profile = JSON.parse(localStorage.getItem(`profile`));
            this.housesList = this.housesList.map((x) => ({
              ...x,
              canUse:
                r === true && profile.region.id === x.region.id ? true : false,
            }));
          });
      });
  }

  useHouse(house) {
    this.isLoading = true;
    this.httpService
      .post(`/api/constructions/houses/use`, {
        houseId: house.id,
      })
      .subscribe(
        () => {
          this.message.success(
            this.translateService.instant('Economy.Construction.RestoredHealth', { health: house.quality * 10 })
          );
          this.authService.updateTopBar.emit();
          this.getHouses();
        },
        (error) => {
          this.message.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  manage(house) {
    this.modalService
      .create({
        nzTitle: this.translateService.instant('Entity.Manage'),
        nzWidth: '90%',
        nzContent: ManageConstructionComponent,
        nzData: {
          type: 'house',
          construction: house,
        },
        nzFooter: null,
      })
      .afterClose.subscribe(() => this.getHouses());
  }
}
