import { Component, OnInit, ViewChild } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AuthService } from 'src/app/shared/services/auth.service';
import { environment } from 'src/environments/environment';
import { CreateConstructionComponent } from './create-construction/create-construction.component';
import { HotelsTabComponent } from './hotels-tab/hotels-tab.component';
import { HousesTabComponent } from './houses-tab/houses-tab.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-accommodation',
  templateUrl: './accommodation.component.html',
  styleUrls: ['./accommodation.component.scss'],
})
export class AccommodationComponent implements OnInit {
  @ViewChild(HousesTabComponent, { static: false }) houses;
  @ViewChild(HotelsTabComponent, { static: false }) hotels;

  apiUrl = environment.apiUrl;
  deviceDisplayType = 'full';

  constructions = [];

  constructor(
    private modalService: NzModalService,
    private authService: AuthService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.deviceDisplayType = localStorage.getItem(`deviceDisplayType`);
    this.authService.updatedProfile.subscribe(() => {
      if (this.houses) this.houses.getHouses();
      if (this.hotels) this.hotels.getHotels();
    });
  }

  create() {
    this.modalService
      .create({
        nzTitle: this.translateService.instant('Politics.Laws.CreateConstruction'),
        nzContent: CreateConstructionComponent,
        nzFooter: null,
        nzWidth: '90%',
      })
      .afterClose.subscribe((r) => {
        if (r === 'ok') {
          if (this.houses) this.houses.getHouses();
          if (this.hotels) this.hotels.getHotels();
        }
      });
  }
}
