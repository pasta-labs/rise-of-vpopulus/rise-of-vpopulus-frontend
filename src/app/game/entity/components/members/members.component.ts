import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep } from 'lodash-es';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-entity-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class EntityMembersComponent implements OnInit {
  isLoading = true;

  @Input() entityType = 'organisations';
  @Input() entityId;
  @Input() companies;

  editPermissionsModalVisible = false;
  allMembersModalVisible = false;
  roles = [];
  managers = [];
  selectedRole = null;
  citizens = [];
  newManagerId;

  currentUserPermissions;
  currentPageCitizens = 1;
  currentPageManagers = 1;
  pageSize = 10; 
  totalMembers = 0;
  totalManagers = 0;
  searchChange$ = new Subject();

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.loadManagers();
    if (!this.companies) this.loadCompanies();

    this.searchChange$.pipe(debounceTime(500)).subscribe((r) => {
      if (r.toString().length < 3) return;
      this.isLoading = true;
      this.httpService.get(`/api/entities/citizens/search?name=${r}`).subscribe(
        (r) => {
          this.citizens = r;
          this.totalMembers = r.length;
          this.isLoading = false;
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
    });
  }
  showAllMembers(role) {
    this.selectedRole = role;
    this.allMembersModalVisible = true; 
    this.currentPageCitizens = 1; 
    this.totalMembers = role.citizen.length; 
  }
  get paginatedCitizens() {
    if (!this.selectedRole) return []; 
    const startIndex = (this.currentPageCitizens - 1) * this.pageSize; 
    return this.selectedRole.citizen.slice(startIndex, startIndex + this.pageSize);
  }

  onPageChangeCitizens(page: number) { 
    this.currentPageCitizens = page; 
  }
  onPageChangeManagers(page: number) { 
    this.currentPageManagers = page; 
  }
  get paginatedManagers() {
    const startIndex = (this.currentPageManagers - 1) * this.pageSize;
    return this.managers.slice(startIndex, startIndex + this.pageSize);
}

  selectManager(citizen) {
    if (!citizen.permissions.companies) citizen.permissions.companies = [];
    this.currentUserPermissions = citizen;
    this.companies.forEach((company) => {
      if (
        citizen.permissions.companies.filter((x) => x.companyId == company.id)
          .length > 0
      )
        company.isManaging = true;
    });
  }

  searchCitizen(value) {
    this.searchChange$.next(value);
  }
  loadManagers() {
    this.isLoading = true;
    this.httpService
      .get(`/api/${this.entityType}/${this.entityId}/members`)
      .subscribe(
        (r) => {
          console.log('members', r);
          this.roles = r;
          this.managers = [];
          this.roles.forEach((role) => {
            if (role.citizen.length > 0) {
              this.managers.push(...role.citizen.map((x) => x));
            }
          });
          this.totalManagers = this.managers.length; 
          this.isLoading = false; 
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  loadCompanies = () => {
    this.graphQlService.get(`game`, `{ companies(ownerId: "${this.entityId}") { id name } }`).subscribe((r: any) => {
      this.companies = cloneDeep(r.data.companies);
      this.isLoading = false;
    })
  }

  addNewManager() {
    this.isLoading = true;
    this.httpService
      .post(`/api/${this.entityType}/members`, {
        newManagerId: this.newManagerId,
      })
      .subscribe(
        (r) => {
          this.currentUserPermissions = undefined;
          this.messageService.success(this.translateService.instant('Entity.Components.managerAdded'));
          this.loadManagers();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  updatePermissions(user) {
    console.log(
      'user',
      user,
      this.companies.filter((x) => x.isManaging == true).map((x) => x.id)
    );
    this.isLoading = true;
    this.httpService
      .put(`/api/${this.entityType}/members`, {
        managerId: user.id,
        companyIds: this.companies
          .filter((x) => x.isManaging == true)
          .map((x) => x.id),
        permissions: user.permissions,
      })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Entity.Components.managerUpdated'));
          this.isLoading = false;
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  removeManager(id) {
    this.isLoading = true;
    this.httpService.delete(`/api/${this.entityType}/members/${id}`).subscribe(
      (r) => {
        this.currentUserPermissions = undefined;
        this.messageService.success(this.translateService.instant('Entity.Components.managerRemoved'));
        this.loadManagers();
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }
}
