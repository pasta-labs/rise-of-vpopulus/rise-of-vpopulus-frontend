import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { toBoolean } from 'ng-zorro-antd/core/util';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-entity-ban-manage',
  templateUrl: './ban-manage.component.html',
  styleUrls: ['./ban-manage.component.scss'],
})
export class EntityBanManageComponent implements OnInit {
  isLoading = false;

  @Input() entityId;
  @Input() entityType;
  @Input() isBanned;

  permissions: any = {};

  banFormVisible = false;
  banForm: FormGroup;

  @Output() reloadEntity = new EventEmitter();

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) {
    this.banForm = new FormGroup({
      entityType: new FormControl(null, [Validators.required]),
      entityId: new FormControl(null, [Validators.required]),
      expireDateTime: new FormControl(null, [Validators.required]),
      reason: new FormControl(null)
    })
  }

  ngOnInit() {
    console.log('ban manage?', localStorage.getItem(`modPermissions`))

    if (localStorage.getItem(`modPermissions`) == null)
      return;

    this.permissions = JSON.parse(localStorage.getItem(`modPermissions`) ?? "{}");

    this.banForm.get(`entityType`).setValue(this.entityType);
    this.banForm.get(`entityId`).setValue(this.entityId);
  }

  ban() {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: EntityBanCommand) { ban(data: $data) }`, {
      data: this.banForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.warning(  
          this.translateService.instant('Entity.Components.BannedMessage', { entityType: this.entityType })
      );
        this.banForm.reset();
        this.banFormVisible = false;
        this.reloadEntity.emit();
      }
      this.isLoading = false;
    });
  }

  unban() {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: EntityUnbanCommand) { unban(data: $data) }`, {
      data: {
        entityType: this.entityType,
        entityId: this.entityId,
      }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.success(
          this.translateService.instant('Entity.Components.unbannedMessage', { entityType: this.entityType })
        );
        this.reloadEntity.emit();
      }
      this.isLoading = false;
    });
  }
}
