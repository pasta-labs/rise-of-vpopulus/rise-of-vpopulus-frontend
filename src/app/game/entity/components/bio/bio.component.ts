import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-entity-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.scss'],
})
export class EntityBioComponent implements OnInit {
  isLoading = false;

  showEditBio = false;
  @Input() bio = '';
  @Input() isEntity = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService,

  ) {}

  ngOnInit(): void {}

  updateBio() {
    this.isLoading = true;
    this.httpService
      .put(`/api/entities/bio`, { bio: this.bio })
      .subscribe((r) => {
        this.messageService.success(this.translateService.instant('Entity.Components.BioUpdated'));
        this.isLoading = false;
        this.showEditBio = false;
      });
  }
}
