import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-entity-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss'],
})
export class EntityCompanyListComponent implements OnInit {
  isLoading = true;

  list = [];

  @Input() entityType;
  @Input() entityId;

  constructor(private graphQlService: GraphQlService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ companies(ownerId: "${this.entityId}") { id name avatar quality industry { value symbol isRaw } region { id x y country { id name } } } }`).subscribe((r: any) => {
      this.list = r.data.companies;
      this.isLoading = false;
    })
  }
}
