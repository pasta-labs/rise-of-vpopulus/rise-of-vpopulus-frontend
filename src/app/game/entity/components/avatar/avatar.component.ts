
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-entity-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class EntityAvatarComponent implements OnInit {
  isLoading = false;

  @Input() entityType;
  @Input() entityId;

  @Input() avatar;

  constructor() { }

  ngOnInit(): void {
  }
}
