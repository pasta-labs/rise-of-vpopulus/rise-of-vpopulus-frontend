import { Component, inject } from '@angular/core';
import { NZ_MODAL_DATA } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-entity-mod-panel',
  templateUrl: './entity-mod-panel.component.html',
  styleUrl: './entity-mod-panel.component.scss'
})
export class EntityModPanelComponent {
  entity;
  entityType;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  constructor() { }

  ngOnInit(): void {
    this.entity = this.nzModalData.entity;
    this.entityType = this.nzModalData.entityType;

    console.log('entityType', this.entityType)
  }

  reloadEntity = () => { }
}
