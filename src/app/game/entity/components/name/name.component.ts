import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-entity-name',
  templateUrl: './name.component.html',
  styleUrls: ['./name.component.scss'],
})
export class EntityNameComponent implements OnInit {
  isLoading = false;

  @Input() entity;
  @Input() isEntity = false;

  isNameEdited = false;
  newName;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService
  ) {}

  ngOnInit(): void {}

  editName() {
    this.newName = this.entity.name;
    this.isNameEdited = !this.isNameEdited;
  }

  changeName() {
    if (this.newName) {
      this.isLoading = true;
      this.httpService
        .put(`/api/entities/name`, { name: this.newName })
        .subscribe(
          (r) => {
            this.entity.name = this.newName;
            this.editName();
            this.isLoading = false;
          },
          (error) => {
            console.log('error', error)
            this.isLoading = false;
            this.messageService.error(error.error.Message);
          }
        );
    }
  }
}
