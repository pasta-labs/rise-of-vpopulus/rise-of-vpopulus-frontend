import { HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-entity-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class EntitySettingsComponent implements OnInit {
  isLoading = false;
  apiUrl = environment.apiUrl;

  modalVisible = false;

  @Input() entity;
  @Input() entityType;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private http: HttpClient,
    private authService: AuthService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void { }

  changeName() {
    this.isLoading = true;
    var payload = { name: this.entity.name };
    if(this.entityType === "company") {
      payload['targetEntityType'] = this.entityType;
      payload['targetEntityId'] = this.entity.id;
    }
    this.httpService
      .put(`/api/entities/name`, payload)
      .subscribe(
        (r) => {
          this.isLoading = false;
          this.messageService.success(this.translateService.instant('Entity.Components.nameBbeenChanged'));
          this.authService.updateTopBar.emit();
        },
        (error) => {
          console.log('error', error)
          this.isLoading = false;
          this.messageService.error(error.error.Message);
        }
      );
  }

  uploadDocument = (item) => {
    this.isLoading = true;
    const formData = new FormData();
    formData.append('file', item.file as any);
    formData.append('type', `${this.entityType}-icon`);
    switch (this.entityType) {
      case 'company':
        formData.append('managedEntityId', this.entity.id);
        break;
      case 'newspaper':
        formData.append('managedEntityId', this.entity.id);
        break;
      case 'country':
        formData.append('managedEntityId', this.entity.id);
        break;
    }
    const req = new HttpRequest('POST', item.action!, formData, {
      headers: new HttpHeaders({ authorization: `Bearer ${localStorage.getItem(`jwtToken`)}` }),
      reportProgress: true,
      withCredentials: false,
    });

    return this.http.request(req).subscribe(
      (event: HttpEvent<{}>) => {
        if (event.type === HttpEventType.UploadProgress) {
          if (event.total! > 0) {
            (event as any).percent = (event.loaded / event.total!) * 100;
          }
          item.onProgress!(event, item.file!);
        } else if (event instanceof HttpResponse) {
          item.onSuccess!(event.body, item.file!, event);
          this.isLoading = false;
          this.entity.avatar = event.body['url'];
          this.authService.updateTopBar.emit();
          this.messageService.success(this.translateService.instant('Entity.Components.imageUploaded'));
        }
      },
      (err) => {
        item.onError!(err, item.file!);
        this.messageService.error('Network error');
        this.isLoading = false;
      }
    );
  };
}
