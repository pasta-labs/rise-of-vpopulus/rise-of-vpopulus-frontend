import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { take } from 'lodash';

@Component({
  selector: 'app-entity-moderator-notes-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class EntityModeratorNotesListComponent implements OnInit {
  isLoading = true;

  @Input() entityId;
  @Input() entityType;

  allNotesVisible = false;

  newNote;

  simpleList = [];
  list = [];

  moderatorPermissions;

  canView = false;
  canAdd = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService
  ) {}

  ngOnInit() {
    this.moderatorPermissions = JSON.parse(
      localStorage.getItem(`moderatorPermissions`)
    );
    this.canView = this.moderatorPermissions?.Entities?.ViewNotes != undefined;
    if (this.canView) this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService
      .get(`/api/mp/notes/${this.entityType}/${this.entityId}`)
      .subscribe((list) => {
        this.list = list;
        this.simpleList = take(this.list, 2);
        this.isLoading = false;
      });
  }

  addNewNote() {
    this.isLoading = true;
    this.httpService
      .post(`/api/mp/notes`, {
        content: this.newNote,
        entityId: this.entityId,
        entityType: this.entityType,
      })
      .subscribe(
        (list) => {
          this.loadData();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
