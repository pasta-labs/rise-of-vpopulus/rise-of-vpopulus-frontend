import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PartyProfileComponent } from './profile/profile.component';
import { PartyElectionsManagerComponent } from './elections-manager/elections-manager.component';
import { PartyListComponent } from './list/list.component';
import { PartyCreateComponent } from './create/create.component';
import { EntityModule } from '../entity/entity.module';
import { ElectionsModule } from '../elections/elections.module';
import { PartyElectionsManagerListComponent } from './elections-manager/components/list/list.component';
import { PartyElectionsManagerListGoalsComponent } from './elections-manager/components/goals/goals.component';

@NgModule({
  declarations: [
    PartyProfileComponent,
    PartyElectionsManagerComponent,
    PartyListComponent,
    PartyCreateComponent,
    PartyElectionsManagerListComponent,
    PartyElectionsManagerListGoalsComponent,
  ],
  exports: [PartyElectionsManagerComponent, PartyListComponent],
  imports: [CommonModule, SharedModule, EntityModule, ElectionsModule],
})
export class PartyModule { }
