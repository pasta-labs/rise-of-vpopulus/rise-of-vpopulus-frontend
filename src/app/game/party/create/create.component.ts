import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-party-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class PartyCreateComponent implements OnInit {
  isLoading = false;
  name = '';
  politicalSpectrum;

  spectrums = [];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router, 
    private modalRef: NzModalRef
  ) {}

  ngOnInit(): void {
    this.httpService
      .get(`/api/dictionaries/politics-spectrum`)
      .subscribe((r) => {
        this.spectrums = r.map((x) => x.value);
        this.politicalSpectrum = this.spectrums[0];
      });
  }

  create() {
    this.isLoading = true;
    this.httpService
      .post(`/api/parties`, {
        name: this.name,
        politicalSpectrum: this.politicalSpectrum,
      })
      .subscribe(
        (r) => {
          this.modalRef.close();
          this.router.navigateByUrl(`/party/${r}`);
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
