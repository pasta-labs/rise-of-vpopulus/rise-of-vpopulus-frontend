import { EventEmitter, Injectable } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Injectable({
  providedIn: 'root'
})
export class ElectionsManagerService {
  party;
  isEntity;

  selectedElectionTypeChanged = new EventEmitter();

  constructor(
    private graphQlService: GraphQlService
  ) {
  }

}
