import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { remove, orderBy } from 'lodash';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ElectionsManagerService } from './elections-manager.service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-party-elections-manager',
  templateUrl: './elections-manager.component.html',
  styleUrls: ['./elections-manager.component.scss'],
})
export class PartyElectionsManagerComponent implements OnInit {
  electionTypes = [
    {
      key: 'CountryPresident',
      name: 'Country president',
      day: 5,
      type: 0,
      id: '',
    },
    { key: 'Congress', name: 'Congress', day: 15, type: 1, id: '' },
    {
      key: 'PartyPresident',
      name: 'Party president',
      day: 25,
      type: 2,
      id: '',
    },
  ];
  selectedElectionType;
  party;

  canCandidate = false;

  constructor(
    private electionsManagerService: ElectionsManagerService,
    private httpService: GenericHttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private graphQlService: GraphQlService
  ) {
    this.electionsManagerService.selectedElectionTypeChanged.subscribe(r => this.selectedElectionType = r)
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      var id = params['id'];
      if (id == undefined) this.router.navigateByUrl(`/parties`);

      if (
        localStorage.getItem('entityType') == (1003).toString() &&
        localStorage.getItem('entityId') == id
      )
        this.electionsManagerService.isEntity = true;

      this.graphQlService.get(`game`, `{ party(id: "${id}") { id name } }`).subscribe((r: any) => {
        this.electionsManagerService.party = r.data.party;
        this.party = this.electionsManagerService.party;
        this.loadUpcomingElections()
      });
      this.graphQlService.get(`game`, `{ citizen { partyMember { fromId canSwitch } citizenshipId region { country { id flagLocation(size: "s") } } }}`
      ).subscribe((r: any) => this.canCandidate = r.data.citizen.partyMember?.fromId === id);
    });
  }

  loadUpcomingElections = () => {
    this.graphQlService.get(`game`, `{ electionsGetUpcoming { id type voteGameDay } }`)
      .subscribe((r: any) => {
        for (const election of r.data.electionsGetUpcoming) {
          this.electionTypes.find((item) => item.key === election.type).id = election.id;
        }

        if (!this.selectedElectionType) this.changeType(this.electionTypes[0]);
      })
  };

  changeType = (type) =>
    this.electionsManagerService.selectedElectionTypeChanged.emit(type);


}
