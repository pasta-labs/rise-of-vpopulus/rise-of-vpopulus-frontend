import { Component, OnInit, inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-party-elections-manager-list-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class PartyElectionsManagerListGoalsComponent implements OnInit {
  editMode = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  candidate;

  goals = {
    military: "",
    economic: "",
    social: "",
    others: ""
  }

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.candidate = this.nzModalData.candidate;

    if (this.candidate.goals) this.goals = this.candidate.goals;
    if (this.candidate.id === localStorage.getItem("entityId")) this.editMode = true;
  }

  save(): void {
    this.httpService
      .put(`/api/parties/elections/candidates/goals`, { ...this.goals })
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant('Politics.Elections.changesSaved'));
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }

  parseGoals = (text) => {
    return text.replaceAll("\n", "<br/>")
  }

}
