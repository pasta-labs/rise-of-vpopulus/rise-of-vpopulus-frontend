import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { ElectionsManagerService } from '../../elections-manager.service';
import { orderBy, cloneDeep, max } from 'lodash-es'
import { NzModalService } from 'ng-zorro-antd/modal';
import { PartyElectionsManagerListGoalsComponent } from '../goals/goals.component';
import { TranslateService } from '@ngx-translate/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-party-elections-manager-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class PartyElectionsManagerListComponent implements OnInit {
  isLoading = false;

  candidates;
  maxPosition = 0;
  isCandidate;
  regions;
  candidatesToRemove = [];
  selectedRegionId: string | null = null;
  selectedElectionType;

  canManage = true;
  @Input() countryId;

  @Input() canCandidate = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private electionsManagerService: ElectionsManagerService,
    private modalService: NzModalService,
    private translateService: TranslateService,
    private graphQlService: GraphQlService,
    private route: ActivatedRoute
  ) {
    this.electionsManagerService.selectedElectionTypeChanged.subscribe((r: any) => this.load(r));
  }

  ngOnInit(): void {
    this.countryId = this.route.snapshot.queryParamMap.get('countryId');

   }

  load = (type) => {
    this.canManage = this.electionsManagerService.isEntity;
    this.selectedElectionType = type;
    if (!this.selectedElectionType || !this.electionsManagerService.party) return;

    this.candidates = undefined;
    this.candidatesToRemove = [];
    this.isLoading = true;
    this.httpService
      .get(
        `/api/parties/${this.electionsManagerService.party.id}/elections/${this.selectedElectionType.id}/candidates`
      )
      .subscribe(
        (r) => {
          this.candidates = cloneDeep(orderBy(r, 'position', 'asc'));
          this.candidates = this.candidates.map(x => ({ ...x, position: (x.isOfficial === true ? x.position : undefined) }));
          this.isCandidate = this.candidates.filter(x => x.id === localStorage.getItem("entityId")).length > 0;
          this.candidates = orderBy(this.candidates, 'position', 'asc');
          console.log('candidates', this.candidates)
          this.loadRegions()
          this.isLoading = false;
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }

  loadRegions() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ regions(countryId: "${this.countryId}") { id x y name population companies houses hotels hospital defenseSystem  resources { symbol value quality } } }`).subscribe((r: any) => {
      this.regions = r.data.regions.map(x => ({ ...x, name: x.name ? x.name : `${x.x}, ${x.y}` }));
      console.log('regions', this.regions),
       this.isLoading = false;
    })
  }

  changeOfficiality = (candidateId) => {
    var candidate = this.candidates.find(x => x.id === candidateId)
    candidate.isOfficial = !candidate.isOfficial;

    if (candidate.isOfficial === false) candidate.position = undefined;
    else candidate.position = 9999;

    orderBy(this.candidates.filter(x => x.isOfficial === true), 'position', 'asc').forEach((candidate, index) => candidate.position = index)
    this.maxPosition = max(this.candidates.filter(x => x.position || x.position === 0).map(x => x.position))
    this.candidates = orderBy(this.candidates, 'position', 'asc');
  }

  changePosition = (candidateId, targetPosition) => {
    var target = this.candidates.find(x => x.position === targetPosition);
    var candidate = this.candidates.find(x => x.id === candidateId);
    target.position = candidate.position;
    candidate.position = targetPosition;
    this.candidates = orderBy(this.candidates, 'position', 'asc');
  }

  remove = (candidateId) => {
    this.candidatesToRemove.push(candidateId);
    this.candidates = this.candidates.filter(x => x.id != candidateId);
    this.candidates = orderBy(this.candidates, 'position', 'asc');
  }

  candidate() {
    const requestBody: any = { electionId: this.selectedElectionType.id };
  
    if (this.selectedElectionType.key === 'Congress' && this.selectedRegionId) {
      requestBody.regionId = this.selectedRegionId;
    }
  
    this.httpService.post(`/api/elections/candidates`, requestBody).subscribe(
      () => {
        this.messageService.success(this.translateService.instant('Politics.Elections.youCandidatingElections'));
        this.load(this.selectedElectionType);
      },
      (error) => {
        this.messageService.error(error.error.Message);
      }
    );
  }
  

  resign() {
    this.httpService
      .delete(
        `/api/elections/candidates/resign?electionId=${this.selectedElectionType.id}`
      )
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant('Politics.Elections.resignedCandidatingElections'));
          this.load(this.selectedElectionType);
        },
        (error) => {
          this.messageService.error(error.error.Message);
        }
      );
  }

  saveChanges = () => {
    this.isLoading = true;
    this.httpService
      .put(`/api/parties/elections/candidates`, { electionId: this.selectedElectionType.id, candidatesToRemove: this.candidatesToRemove, candidates: this.candidates.map(x => ({ citizenId: x.id, isOfficial: x.isOfficial, position: x.position })) })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant('Politics.Elections.changesSaved'));
          this.isLoading = false;
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  viewGoals = (candidate) => {
    this.modalService.create({
      nzTitle: this.translateService.instant('Politics.Elections.politicalGoals'),
      nzContent: PartyElectionsManagerListGoalsComponent,
      nzFooter: null,
      nzWidth: '60%',
      nzMaskClosable: false,
      nzData: {
        candidate: candidate
      }
    })
  }

}
