import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { EntityType } from 'src/app/shared/models/EntityType';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep } from 'lodash-es'
import { NzModalService } from 'ng-zorro-antd/modal';
import { EntityModPanelComponent } from '../../entity/components/entity-mod-panel/entity-mod-panel.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-party',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class PartyProfileComponent implements OnInit {
  isLoading = true;
  id;

  showEditBio = false;

  party;
  isEntity = false;

  isMember = false;

  isMod = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isMod = localStorage.getItem(`modPermissions`) !== null;

    this.route.params.subscribe((params) => {
      this.id = params['id'];
      if (this.id == undefined) {
        this.httpService
          .get(`/api/citizens/${localStorage.getItem('entityId')}/parties`)
          .subscribe(
            (item) => {
              this.router.navigateByUrl(`/party/${item.id}`);
            },
            () => {
              this.router.navigateByUrl(`/parties`);
            }
          );
      } else {
        this.loadData();
        this.checkIfIsMember();
      }
    });
  }

  loadData() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ party(id: "${this.id}"){ id name isBanned avatar bio region { id name country { id name } } } }`).subscribe((r: any) => {
      this.party = cloneDeep(r.data.party);
      if (localStorage.getItem('entityId') == this.id) this.isEntity = true;
      this.isLoading = false;
    });
  }

  checkIfIsMember = () => {
    if (localStorage.getItem('entityType') != (1001).toString()) return;
    this.httpService.get(`/api/parties/members/current`).subscribe(r => this.isMember = (this.id === r))
  }

  switchTo(id) {
    this.authService.switchToEntity(EntityType.Party, id);
  }

  resign() {
    this.httpService.delete(`/api/parties/members/resign`).subscribe(
      (r) => {
        this.message.success(this.translateService.instant('Politics.Elections.youResigned'));
        this.loadData();
        this.checkIfIsMember();
      },
      (error) => {
        this.message.error(error.message);
      }
    );
  }

  showModPanel = () => {
    this.modalService.create({
      nzContent: EntityModPanelComponent,
      nzTitle: this.translateService.instant('Politics.Elections.citizenModAccess'),
      nzFooter: null,
      nzWidth: '90%',
      nzData: {
        entity: this.party,
        entityType: 'Party'
      }
    });
  }

}
