import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { PartyCreateComponent } from './create/create.component';
import { PartyElectionsManagerComponent } from './elections-manager/elections-manager.component';
import { PartyListComponent } from './list/list.component';
import { PartyProfileComponent } from './profile/profile.component';

export const routes: Routes = [
  {
    path: 'parties/create',
    component: PartyCreateComponent,
    canActivate: [AuthGuard],
  },
  { path: 'parties', component: PartyListComponent, canActivate: [AuthGuard] },
  { path: 'party', component: PartyProfileComponent, canActivate: [AuthGuard] },
  {
    path: 'party/:id',
    component: PartyProfileComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'party/:id/election',
    component: PartyElectionsManagerComponent,
    canActivate: [AuthGuard],
  },
];
