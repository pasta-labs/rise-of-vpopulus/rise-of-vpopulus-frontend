import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { forkJoin } from 'rxjs';
import { EntityType } from 'src/app/shared/models/EntityType';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

import { cloneDeep } from 'lodash'
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { PartyCreateComponent } from '../create/create.component';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-party-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class PartyListComponent implements OnInit {
  isLoading = true;

  @Input() showActions = true;
  @Input() countryId;
  country;

  list = [];

  canCreate = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private notificationService: NzNotificationService,
    private authService: AuthService,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private router: Router,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    if (!this.countryId) {
      this.route.params.subscribe((params) => {
        this.countryId = params['id'];

        if (this.countryId === undefined) {
          this.httpService.get(`/api/countries/current`).subscribe((r) => {
            this.countryId = r;
            this.loadData();
          });
        } else this.loadData();
      });
    } else this.loadData();
  }

  loadData() {
    this.isLoading = true;
    forkJoin({
      citizen: this.graphQlService.get(
        `game`,
        `{ citizen { partyMember { fromId canSwitch } citizenshipId region { country { id flagLocation(size: "s") } } }}`
      ),
      country: this.graphQlService.get(
        `game`,
        `{ country(id:"${this.countryId}") { id name parties { id name avatar members { id } politicalSpectrum region { id x y } } }}`
      ),
    }).subscribe((data: any) => {
      this.country = cloneDeep(data.country.data.country);
      if (
        !data.citizen.data.citizen.partyMember &&
        data.citizen.data.citizen?.region?.country?.id === this.country.id && data.citizen.data.citizen?.citizenshipId === this.country.id
      ) {
        this.canCreate = true;
        this.country.parties = this.country.parties.map((x) => ({
          ...x,
          canJoin: true,
        }));
      }

      if (
        data.citizen.data.citizen.partyMember &&
        data.citizen.data.citizen.partyMember.canSwitch === true
      ) {
        var party = this.country.parties.find((x) => x.id === data.citizen.data.citizen.partyMember.fromId);
        if (party) party.canSwitch = true;
      }
      this.isLoading = false;
    });
  }

  join(item) {
    this.isLoading = true;
    this.httpService.post(`/api/parties/members/join`, { partyId: item.id }).subscribe(
      (r) => {
        this.notificationService.success(
          this.translateService.instant('Politics.Elections.allGood'),
          this.translateService.instant('Politics.Elections.joinedParty', { partyName: item.name })
        );        this.isLoading = false;
        this.router.navigateByUrl(`/party/${item.id}`);
      },
      (error) => {
        this.notificationService.success(this.translateService.instant('Politics.Elections.Error'), error.message);
        this.isLoading = false;
      }
    );
  }

  switchTo(id) {
    this.authService.switchToEntity(EntityType.Party, id);
  }

  create() {
    this.modalService.create({
      nzWidth: '90%',
      nzContent: PartyCreateComponent,
      nzTitle: this.translateService.instant('Politics.Elections.createParty'),
      nzFooter: null,
    });
  }
}
