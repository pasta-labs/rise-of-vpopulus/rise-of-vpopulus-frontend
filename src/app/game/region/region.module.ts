import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegionComponent } from './region/region.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapModule } from '../shared/maps/map.module';
import { EntityModule } from '../entity/entity.module';
import { RegionBordersComponent } from './region-borders/region-borders.component';
import { RegionBattlesComponent } from './region-battles/region-battles.component';
import { RegionModPanelComponent } from './region-mod-panel/region-mod-panel.component';

@NgModule({
  declarations: [RegionComponent, RegionBordersComponent, RegionBattlesComponent, RegionModPanelComponent],
  imports: [CommonModule, SharedModule, MapModule, EntityModule],
})
export class RegionModule { }
