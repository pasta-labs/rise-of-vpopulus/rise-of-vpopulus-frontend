import { Component, inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-region-mod-panel',
  templateUrl: './region-mod-panel.component.html',
  styleUrl: './region-mod-panel.component.scss'
})
export class RegionModPanelComponent {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  constructionQualityUpdateForm: FormGroup = undefined;
  currentQuality = undefined;

  constructor(
    private graphQlService: GraphQlService,
    private messageService: NzMessageService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,

  ) {
    this.constructionQualityUpdateForm = new FormGroup({
      regionId: new FormControl(null, [Validators.required]),
      constructionType: new FormControl(null, [Validators.required]),
      quality: new FormControl(null, [Validators.required]),
      reason: new FormControl(null, [Validators.required]),
    })
  }

  ngOnInit(): void {
    this.constructionQualityUpdateForm.get(`regionId`).setValue(this.nzModalData.region.id);
    this.constructionQualityUpdateForm.get(`constructionType`).valueChanges.subscribe((value) => {
      this.currentQuality = this.nzModalData.region[value];
    })
  }

  updateConstructionQualitySubmit = () => {
    if (this.constructionQualityUpdateForm.status === "INVALID")
      return;

    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: RegionConstructionModifyCommand) { modifyRegionConstruction(data: $data) }`, {
      data: this.constructionQualityUpdateForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.success(this.translateService.instant('Economy.Construction.qualityUpdated'));
        this.modalRef.close(`r`);
      }
      this.isLoading = false;
    });
  }


}
