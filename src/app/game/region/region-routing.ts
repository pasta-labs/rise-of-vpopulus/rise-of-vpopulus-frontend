import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { RegionComponent } from './region/region.component';

export const routes: Routes = [
  {
    path: 'region/:id',
    component: RegionComponent,
    canActivate: [AuthGuard],
  },
];
