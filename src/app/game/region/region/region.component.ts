import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { cloneDeep } from 'lodash-es'
import { environment } from 'src/environments/environment';
import { NzModalService } from 'ng-zorro-antd/modal';
import { RegionModPanelComponent } from '../region-mod-panel/region-mod-panel.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
})
export class RegionComponent implements OnInit {
  isLoading = false;

  apiUrl = environment.apiUrl;

  regionId;
  region;

  country;
  canResist = false;
  coreCountry;
  gameType;

  isMod = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private messageService: NzMessageService,
    private router: Router,
    private modalService: NzModalService,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.isMod = localStorage.getItem(`modPermissions`) !== null;

    this.route.params.subscribe((params) => {
      this.regionId = params['id'];
      this.getRegion();
      this.gameType = JSON.parse(localStorage.getItem(`gameConfig`))['type'];
    });
  }

  getRegion() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ region(id: "${this.regionId}") { country { id name flagLocation(size: "xl") } coreCountry { id name flagLocation } id x y name canStartResistanceBattle isWasteland canAttack  resources { symbol value quality } } }`).subscribe((r: any) => {
      this.region = cloneDeep(r.data.region);
      this.country = this.region.country;
      this.canResist = this.region.canStartResistanceBattle;
this.coreCountry = this.region.coreCountry
      console.log('region', this.coreCountry)
      this.getRegionBorders();
      this.isLoading = false;
    })
  }

  getRegionBorders() {
    this.graphQlService.get(`game`, `{ regionBorders(regionId: "${this.regionId}") { id x y name country { id name flagLocation(size: "xl") } resources { symbol value quality } population } }`).subscribe((r: any) => {
      this.region['borders'] = r.data.regionBorders;
    })
  }

  startResistanceWar() {
    this.httpService
      .post(`/api/military/battles/resistance`, { regionId: this.regionId })
      .subscribe(
        (r) => {
          this.message.success(this.translateService.instant('Military.Wars.resistanceWarBegins'));
          this.canResist = false;
        },
        (error) => {
          this.message.error(error.message);
        }
      );
  }

  attack = () => {
    this.isLoading = true;
    this.httpService
      .post(`/api/military/battles`, { regionId: this.region.id })
      .subscribe(
        (r) => {
          this.messageService.success(
            this.translateService.instant('Military.Wars.attackedRegion', { regionName: this.region.name })
          );
          this.isLoading = false;
          this.router.navigateByUrl(`/battle/${r}`);
        },
        (err) => {
          this.isLoading = false;
          if (err.error.Message === 'NO_DIRECT_BORDER') {
            this.messageService.error(this.translateService.instant('Military.Wars.noDirectBorderDetected'));
          } else if (err.error.Message === 'NO_WAR') {
            this.messageService.error(this.translateService.instant('Military.Wars.needDeclareWarFirst'));
          } else if (err.error.Message === 'COUNTRY_PROTECTION_IS_ENABLED') {
            this.messageService.error(this.translateService.instant('Military.Wars.countryProtectionEnabled'));
          } else {
            this.messageService.error(err.error.Message);
          }
        });
  }

  showModPanel = () => {
    const r = this.modalService.create({
      nzContent: RegionModPanelComponent,
      nzTitle: this.translateService.instant('Military.Wars.regionModAccess'),
      nzFooter: null,
      nzWidth: '90%',
      nzData: {
        region: this.region
      }
    });
    r.afterClose.subscribe(r => {
      if (r === "r") this.getRegion();
    });
  }
}
