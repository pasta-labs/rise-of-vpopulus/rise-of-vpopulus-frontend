import { Component, Input, input } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-region-battles',
  templateUrl: './region-battles.component.html',
  styleUrl: './region-battles.component.scss'
})
export class RegionBattlesComponent {
  isLoading = false;

  list = [];

  @Input() regionId;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ regionBattles(regionId: "${this.regionId}") { id startingWallSize wallSize winnerId isResistance isCivilWar attackerCountry { id name flagLocation(size: "xl") } defenderCountry { id name flagLocation(size: "xl") } } }`)
      .subscribe((r: any) => {
        this.list = r.data.regionBattles;
        console.log('r', this.list)
        this.isLoading = false;
      })
  }

}
