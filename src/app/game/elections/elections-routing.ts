import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { ElectionsResultsComponent } from './results/results.component';
import { ElectionsVoteComponent } from './vote/vote.component';

export const routes: Routes = [
  {
    path: 'elections/results',
    component: ElectionsResultsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'elections/vote',
    component: ElectionsVoteComponent,
    canActivate: [AuthGuard],
  },
];
