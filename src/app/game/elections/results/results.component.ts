import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { groupBy, keys } from 'lodash-es';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-elections-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
})
export class ElectionsResultsComponent implements OnInit {
  isLoading = false;

  countries = [];

  electionTypes = [
    { key: 'CountryPresident', name: 'Country president', type: 0, list: [] },
    { key: 'Congress', name: 'Congress', type: 1, list: [] },
    { key: 'PartyPresident', name: 'Party president', type: 2, list: [] },
  ];

  electionData;
  date;

  selectedCountry;
  selectedElectionType;
  selectedElectionDate;

  pieLabels = [];
  pieData = [];

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ electionsGetFinished { id type finishGameDay } }`)
      .subscribe((r: any) => {
        r.data.electionsGetFinished.forEach(election => {
          this.electionTypes.find((e) => e.key === election.type).list.push(election);
        })
        this.selectedElectionType = this.electionTypes[0];
      })
    this.loadCountries();
  }

  loadCountries() {
    this.graphQlService.get(`game`, `{ countries { id name flagLocation(size: "s") } }`)
      .subscribe((r: any) => {
        this.countries = r.data.countries;
        this.selectedCountry = r[0];
        this.isLoading = false;

      });
  }

  getElection() {
    if (this.selectedElectionDate && this.selectedCountry && this.selectedElectionType) {
      this.isLoading = true;
      this.pieData = undefined;
      const electionId = this.selectedElectionDate;
      this.graphQlService.get(`game`, `{ electionsGetResults(countryId: "${this.selectedCountry.id}", electionId: "${electionId}") { votes winType candidate { id name avatar goals { military economic social others } } party { id name avatar } } }`)
        .subscribe((r: any) => {
          if (r.errors) {

          } else {
            if (this.selectedElectionType.type === 0 || this.selectedElectionType.type === 1) this.electionData = r.data.electionsGetResults;
            else {
              var electionData = [];
              var partiesGrouped = groupBy(r.data.electionsGetResults, x => x.party?.id);
              keys(partiesGrouped).forEach(key => electionData.push({
                party: partiesGrouped[key][0].party,
                candidates: partiesGrouped[key].map(candidate => ({
                  ...candidate,
                  party: undefined
                }))
              }));
              this.electionData = electionData;
            }
            this.makeChart(r.data.electionsGetResults);
          }
          this.isLoading = false;
        });
    }
  }

  makeChart(data) {
    this.pieLabels = data.map(
      (position) => `${position.candidate.name} - ${position.party?.name}`
    );
    this.pieData = [
      {
        label: "Votes",
        data: data.map((position) => position.votes)
      }
    ];
  }
}
