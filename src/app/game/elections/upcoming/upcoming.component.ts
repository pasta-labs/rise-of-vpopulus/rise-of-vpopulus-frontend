import { Component, Input, OnInit } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-elections-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.scss'],
})
export class ElectionsUpcomingComponent implements OnInit {
  isLoading = true;

  @Input() partyId;
  @Input() countryId;
  list = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ electionsGetUpcoming { id type voteGameDay secondsUntilElections } }`)
      .subscribe((r: any) => {
        var day = localStorage.getItem(`gameDay`);

        this.list = r.data.electionsGetUpcoming.map(e => {
          return {
            ...e,
            type: `Politics.ElectionsTypes.${e.type}`,
            voteGameDay: e.voteGameDay,
            isVoteDay: e.voteGameDay === +day,
            days: Math.floor(e.secondsUntilElections / (3600 * 24))
          };
        });
        console.log('list', this.list)
        this.isLoading = false;
      })
  }
}
