import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElectionsResultsComponent } from './results/results.component';
import { ElectionsVoteComponent } from './vote/vote.component';
import { ElectionsUpcomingComponent } from './upcoming/upcoming.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PartyModule } from '../party/party.module';

@NgModule({
  declarations: [
    ElectionsResultsComponent,
    ElectionsVoteComponent,
    ElectionsUpcomingComponent,
  ],
  exports: [ElectionsUpcomingComponent],
  imports: [CommonModule, SharedModule],
})
export class ElectionsModule { }
