import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { padStart, uniqBy } from 'lodash-es';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-elections-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss'],
})
export class ElectionsVoteComponent implements OnInit {
  isLoading = false;

  electionTypes = [
    { key: 'CountryPresident', name: 'Country president', voteDate: 0, id: '', isVoteDay: false },
    { key: 'Congress', name: 'Congress', id: '', voteDate: 0, isVoteDay: false },
    { key: 'PartyPresident', name: 'Party president', id: 0, voteDate: '', isVoteDay: false },
  ];
  selectedElectionType;

  electionData = [];

  canVote = false;

  countries = [];
  countrySelected;

  parties = [];
  selectedParty;

  goals;

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.loadUpcomingElections();
    this.loadCountries();
  }

  loadUpcomingElections = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ electionsGetUpcoming { id type voteGameDay } }`)
      .subscribe((r: any) => {
        var day = localStorage.getItem(`gameDay`);

        for (const election of r.data.electionsGetUpcoming) {
          var el = this.electionTypes.find((item) => item.key === election.type);
          el.id = election.id;
          el.voteDate = +election.voteGameDay;
          el.isVoteDay = el.voteDate === +day;

        }

        if (this.electionTypes.find(x => x.isVoteDay === true)) this.changeType(this.electionTypes.find(x => x.isVoteDay === true));
        else this.changeType(this.electionTypes[0]);

        this.isLoading = false;
      })
  }

  changeType = (type) => {
    this.selectedElectionType = type;
    this.loadElectionData();
  }

  loadElectionData = () => {
    if (!this.selectedElectionType || !this.countrySelected) return;

    this.isLoading = true;
    this.httpService
      .get(
        `/api/elections/${this.selectedElectionType.id}/candidates/${this.countrySelected.id}`
      )
      .subscribe(
        (response) => {
          this.electionData = response;
          this.selectedParty = undefined;
          this.parties = uniqBy(this.electionData.map(x => x.party), 'id');
          this.isLoading = false;
        },
        (error) => this.message.error(error.message)
      );
    this.httpService
      .get(`/api/elections/${this.selectedElectionType.id}/vote/isAllowed`)
      .subscribe(
        (r) => this.canVote = r,
        (error) => this.message.error(error.message)
      );
  }

  loadCountries() {
    this.isLoading = true;
    this.httpService.get(`/api/countries`).subscribe((r) => {
      this.countries = r;
      var profile = JSON.parse(localStorage.getItem(`profile`));
      if (profile && profile.region && profile.region.country && this.countries.find(x => x.id === profile.region.country.id))
        this.countrySelected = this.countries.find(x => x.id === profile.region.country.id);
      else this.countrySelected = r[0];
      this.loadElectionData();
    });
  }

  changeCountry = (item) => {
    this.countrySelected = item;
    this.loadElectionData();
  }

  vote = (candidate) => {
    var partyId;
    if (this.selectedElectionType?.key === 'Congress') {
      if (!this.selectedParty) {
        this.message.error(this.translateService.instant('Politics.Elections.pleaseSelectParty'));
        return;
      }
      partyId = this.selectedParty.id;
    } else partyId = candidate.party.id;

    this.httpService.post(`/api/elections/votes`, { candidateId: candidate.candidate.id, partyId: partyId }).subscribe(
      (r) => {
        this.message.success(this.translateService.instant('Politics.Elections.youHaveVoted'));
        this.canVote = false;
        this.loadElectionData();
      },
      (error) => {
        this.message.error(error.message);
        this.isLoading = false;
      }
    );
  }

  parseGoals = (text) => {
    if (!text) return "";
    return text.replaceAll("\n", "<br/>")
  }
}
