import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { BattleArchiveListComponent } from './archive-list/archive-list.component';
import { BattleComponent } from './battle/battle.component';
import { BattleListComponent } from './list/list.component';
import { BattleStatsComponent } from './stats/stats.component';

export const routes: Routes = [
  {
    path: 'battle/:id',
    component: BattleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'battle/:id',
    component: BattleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'battle/:id/stats',
    component: BattleStatsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'battles/active',
    component: BattleListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'battles/archive',
    component: BattleArchiveListComponent,
    canActivate: [AuthGuard],
  },
];
