import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-battle-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
})
export class BattleStatsComponent implements OnInit {
  isLoading = false;
  battleId: string;

  chartData = {
    labels: [],
    datasets: [
      {
        type: 'line',
        label: this.translateService.instant('Military.Battles.Wall'),
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 2,
        fill: false,
        data: [],
      },
      {
        type: 'bar',
        label:  this.translateService.instant('Military.Battles.Attacker'),
        backgroundColor: 'rgb(255, 99, 132)',
        data: [],
        borderColor: 'white',
        borderWidth: 2,
      },
      {
        type: 'bar',
        label: this.translateService.instant('Military.Battles.Defender'),
        backgroundColor: 'rgb(54, 162, 235)',
        data: [],
      },
    ],
  };

  chartDataLastHour = {
    labels: [],
    datasets: [
      {
        type: 'line',
        label: this.translateService.instant('Military.Battles.Wall'),
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 2,
        fill: false,
        data: [],
      },
      {
        type: 'bar',
        label: this.translateService.instant('Military.Battles.Attacker'),
        backgroundColor: 'rgb(255, 99, 132)',
        data: [],
        borderColor: 'white',
        borderWidth: 2,
      },
      {
        type: 'bar',
        label: this.translateService.instant('Military.Battles.Defender'),
        backgroundColor: 'rgb(54, 162, 235)',
        data: [],
      },
    ],
  };

  fighters = {
    defenders: [],
    attackers: [],
  };

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private translateService: TranslateService,

  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.battleId = params['id'];
      this.loadBattleStats();
    });
  }

  loadBattleStats() {
    this.isLoading = true;

    this.httpService
      .get(`/api/military/battles/${this.battleId}/stats`)
      .subscribe((r) => {
        this.updateChartData(r['hourlyChartStats'], r['lastHourChartStats']);
        this.updateFightersData(r['fightersAttackers'], r['fightersDefenders']);
        this.isLoading = false;
      });
  }

  updateChartData(hourlyStats, lastHourStats) {
    this.chartData.labels = hourlyStats.map((x) => x.time);
    this.chartData.datasets[0].data = hourlyStats.map((x) => x.wallSize);
    this.chartData.datasets[1].data = hourlyStats.map((x) => x.attackerDamage);
    this.chartData.datasets[2].data = hourlyStats.map((x) => x.defenderDamage);

    this.chartDataLastHour.labels = lastHourStats.map((x) => x.time);
    this.chartDataLastHour.datasets[0].data = lastHourStats.map(
      (x) => x.wallSize
    );
    this.chartDataLastHour.datasets[1].data = lastHourStats.map(
      (x) => x.attackerDamage
    );
    this.chartDataLastHour.datasets[2].data = lastHourStats.map(
      (x) => x.defenderDamage
    );
  }

  updateFightersData(attackers, defenders) {
    this.fighters.attackers = attackers;
    this.fighters.defenders = defenders;
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
}