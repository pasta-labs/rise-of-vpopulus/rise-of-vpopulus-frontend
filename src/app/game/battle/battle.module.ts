import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BattleComponent } from './battle/battle.component';
import { BattleFightsComponent } from './battle/components/fights/fights.component';
import { BattleListComponent } from './list/list.component';
import { BattleStatsComponent } from './stats/stats.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BattleArchiveListComponent } from './archive-list/archive-list.component';
import { BattleTopbarComponent } from './battle/components/battle-topbar/battle-topbar.component';
import { BattleActionsComponent } from './battle/components/battle-actions/battle-actions.component';
import { NzFlexModule } from 'ng-zorro-antd/flex';

@NgModule({
  declarations: [
    BattleComponent, 
    BattleFightsComponent,
    BattleListComponent,
    BattleStatsComponent,
    BattleArchiveListComponent,
    BattleTopbarComponent, 
    BattleActionsComponent
  ],
  imports: [CommonModule, SharedModule, NzFlexModule],
  providers: [],
})
export class BattleModule {}
