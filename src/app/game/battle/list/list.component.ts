import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';
import { orderBy } from 'lodash';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-battle-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class BattleListComponent implements OnInit {
  isLoading = true;
  apiUrl = environment.apiUrl;
  list: any[] = [];

  constructor(private httpService: GenericHttpService,
    private graphQlService: GraphQlService,

  ) { }

  ngOnInit(): void {
    this.getlist();
  }

  getlist() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ battlesActive { id startingWallSize secondsLeft wallSize winnerId isResistance isCivilWar region { id name } attackerCountry { id name flagLocation(size: "xl") } defenderCountry { id name flagLocation(size: "xl") } } }`)
      .subscribe((r: any) => {
        this.list = r.data.battlesActive;
        this.list.forEach((item) => {
          item.endDateTime = new Date(Date.now())
          item.endDateTime.setSeconds(item.secondsLeft);
        });
        this.list = orderBy(
          this.list,
          (x) => x.secondsLeft
        );
        console.log('r', this.list)
        this.isLoading = false;
      })
  }
}
/* constructor(
  private httpService: GenericHttpService,
) { }

ngOnInit(): void {
  this.isLoading = true;
  this.httpService
    .get(`/api/military/battles/active?page=1&pageSize=100`)
    .subscribe((r) => {
      this.list = r;
      this.list.forEach((item) => {
        item.endDateTimeSeconds = item.endDateTimeSeconds * 1000;

      });
      this.isLoading = false;
    }); */