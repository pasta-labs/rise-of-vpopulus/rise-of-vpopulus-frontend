import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-battle-list-archive',
  templateUrl: './archive-list.component.html',
  styleUrls: ['./archive-list.component.scss']
})
export class BattleArchiveListComponent implements OnInit {
  isLoading = false;
  list = undefined;

  pageIndex = 1;
  pageSize = 15;
  total = 0;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.load();
  }

  load = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ battlesArchive(pageIndex: ${this.pageIndex}, pageSize: ${this.pageSize}){ id wallSize startingWallSize winnerId isResistance isCivilWar endGameDay
      attackerCountry { id name flagLocation }
      defenderCountry { id name flagLocation }
      region { id name }
      } }`).subscribe((r: any) => {
      this.total = r.data.extensions.total;
      console.log('list', r, this.total)
      this.list = r.data['battlesArchive'];
      this.list.forEach(b => {
        b.winnerCountry = b.winnerId === b.attackerCountry.id ? b.attackerCountry : b.defenderCountry
      })
      this.isLoading = false;
    })
  }

}
