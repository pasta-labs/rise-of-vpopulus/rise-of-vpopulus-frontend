import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { firstValueFrom } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-battle-topbar',
  templateUrl: './battle-topbar.component.html',
  styleUrl: './battle-topbar.component.scss'
})
export class BattleTopbarComponent {
  isLoading = false;

  @Input() battle;

  isCP = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService
  ) { }

  ngOnInit(): void {
    this.getIsCP();
  }

  getIsCP = async () => {
    const isAttackingCpResponse = await firstValueFrom(this.httpService.get(`/api/countries/${this.battle.attackerCountry.id}/members/government`));
    if (isAttackingCpResponse.find((position) => position.role === 'CountryPresident').citizen.id === localStorage.getItem('citizenId') !== null)
      this.isCP = true;

    if (!this.isCP) {
      const isDefendingCpResponse = await firstValueFrom(this.httpService.get(`/api/countries/${this.battle.defenderCountry.id}/members/government`));
      if (isDefendingCpResponse.find((position) => position.role === 'CountryPresident').citizen.id === localStorage.getItem('citizenId') !== null)
        this.isCP = true;
    }

  }

  retreat = () => {
    this.isLoading = true;
    this.httpService
      .post(`/api/military/battles/retreat`, { battleId: this.battle.id })
      .subscribe(
        (r) => {
          this.messageService.success(`Battle has been retreated!`)
          this.isLoading = false;
        },
        (error) => {
          this.messageService.error(
            error.error.Message
          );
          this.isLoading = false;
        }
      );
  }

}
