import { Component, Input } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';
import { TranslateService } from '@ngx-translate/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-battle-actions',
  templateUrl: './battle-actions.component.html',
  styleUrl: './battle-actions.component.scss'
})
export class BattleActionsComponent {
  isLoading = false;

  juices = [];
  juice = {
    quality: 0,
    amount: 0,
    max: 5,
  };

  weapons = [];
  weaponQuality = 0;

  hospitalQuality = 0;

  isCitizen = false;

  @Input() battle;

  @Input() hubConnection: HubConnection;
  @Input() jwtToken;

  infiniteFightTimeout = undefined;

  constructor(
    private httpService: GenericHttpService,
    private notificationService: NzNotificationService,
    private authService: AuthService,
    private translateService: TranslateService,

  ) { }

  ngOnInit() {
    if (+localStorage.getItem(`entityType`) === 1001) this.isCitizen = true;

    if (this.isCitizen === true) {
      this.loadInventory();
      this.loadHospital();
    }
  }

  ngOnDestroy() {
    this.shouldFightInfinite = false;
    clearTimeout(this.infiniteFightTimeout);
  }

  loadInventory() {
    this.httpService.get(`/api/inventory`).subscribe((r) => {
      this.juices = r.filter((x) => x.name === 'Item.Juice');
      if (this.juices.length > 0)
        this.juiceSelected(
          this.juices.sort((a, b) => {
            return b.quality - a.quality;
          })[0]
        );

      this.weapons = r.filter((x) => x.name === 'Item.Weapon');
      var cacheWeaponQuality = +localStorage.getItem(`weaponQualitySelected`);
      if (
        (cacheWeaponQuality &&
          this.weapons.filter((x) => x.quality === cacheWeaponQuality).length >
          0) ||
        cacheWeaponQuality === -1
      ) {
        this.weaponSelected({ quality: cacheWeaponQuality });
      } else {
        this.weaponSelected(
          (this.weaponQuality = this.weapons.sort((a, b) => {
            return b.quality - a.quality;
          })[0])
        );
      }
    });
  }

  juiceSelected(item) {
    this.juice.quality = item.quality;
    this.juice.max = item.amount;
  }

  useJuice() {
    this.isLoading = true;

    this.juice.amount = +this.juice.amount;
    this.httpService
      .post(`/api/entities/citizens/juices/use`, this.juice)
      .subscribe(
        (r) => {

          this.notificationService.success(
            this.translateService.instant('Military.Battles.actionCompleted'),
            this.translateService.instant('Military.Battles.youHaveDrankJuice', {
              quality: this.juice.quality,  
              wellness: this.juice.quality  
            })
          );
          
          this.juice.max -= this.juice.amount;
          this.juice.amount = 0;
          this.authService.updateTopBar.emit(`profile`);
          this.isLoading = false;
        },
        (error) => {
          this.notificationService.error(
            error.error.Message,
            `Something went wrong!`
          );
          this.isLoading = false;
        }
      );
  }

  weaponSelected(item) {
    localStorage.setItem(`weaponQualitySelected`, item.quality);
    this.weaponQuality = item.quality;
  }

  loadHospital() {
    this.httpService
      .get(`/api/constructions/hospitals/canUse`)
      .subscribe((quality) => {
        this.hospitalQuality = quality;
      });
  }

  useHospital() {
    this.isLoading = true;
    this.httpService.post(`/api/constructions/hospitals/use`, {}).subscribe(
      () => {
        this.notificationService.success(
          this.translateService.instant('Military.Battles.hospitalUsed'),
          this.translateService.instant('Military.Battles.hospitalUsedrestored', {
            wellness: 10 * this.hospitalQuality 
          })
        );
        this.hospitalQuality = 0;
        this.authService.updateTopBar.emit(`profile`);
        this.isLoading = false;
      },
      (error) => {
        this.notificationService.error(
          `Something went wrong!`,
          error.error.Message
        );
        this.isLoading = false;
      }
    );
  }

  useWellnessPack() {
    this.isLoading = true;

    this.httpService
      .post(`/api/entities/citizens/wellnesspack/use`, {})
      .subscribe(
        (r) => {
          this.notificationService.success(
            this.translateService.instant('Military.Battles.purchaseCompleted'),
            this.translateService.instant('Military.Battles.purchasedPackWellness')
          );
          this.authService.updateTopBar.emit();
          this.isLoading = false;
        },
        (error) => {
          this.notificationService.error(
            error.error.Message,
            `Something went wrong!`,
          );
          this.isLoading = false;
        }
      );
  }

  // = = = FIGHTING
  fight(sideId, isAttacker = true) {
    this.isLoading = true;
    var weaponQualitySelected = +localStorage.getItem(`weaponQualitySelected`);
    if (!weaponQualitySelected) weaponQualitySelected = 0;
    this.hubConnection
      .invoke(
        `Fight`,
        this.jwtToken,
        this.battle.id,
        sideId,
        isAttacker,
        weaponQualitySelected
      )
      .then(
        (r) => {
          this.isLoading = false;
          this.authService.updateTopBar.emit(`profile`);
          // this.missionsService.updateMissionsRequest.emit();
          console.log('ok?', r)
        },
        (error) => {
          this.shouldFightInfinite = false;
          this.isLoading = false;
        }
      )
  }

  // = = = = Infinite fight
  shouldFightInfinite = false;
  fightInfiniteClick(sideId, isAttacker = true) {
    this.shouldFightInfinite = true;
    this.fightInfinite(sideId, isAttacker);
  }

  fightInfinite(sideId, isAttacker = true) {
    if (this.shouldFightInfinite === true) {
      this.fight(sideId, isAttacker);
      this.infiniteFightTimeout = setTimeout(() => {
        this.fightInfinite(sideId, isAttacker);
      }, 1000);
    }
  }
}
