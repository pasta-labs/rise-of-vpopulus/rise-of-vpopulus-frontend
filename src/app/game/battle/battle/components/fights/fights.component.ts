import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-battle-fights',
  templateUrl: './fights.component.html',
  styleUrls: ['./fights.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BattleFightsComponent implements OnInit {
  @Input() battleHero;
  @Input() last5Fights = [];
  @Input() color = 'red';

  constructor() {}

  ngOnInit() {
    console.log(this.battleHero, this.last5Fights, this.color)
  }
}
