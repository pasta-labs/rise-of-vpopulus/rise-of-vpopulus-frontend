import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';
import * as signalR from '@aspnet/signalr';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-battle',
  templateUrl: './battle.component.html',
  styleUrls: ['./battle.component.scss'],
})
export class BattleComponent implements OnInit {
  isLoadingBattle = false;
  isLoadingActions = false;

  jwtToken;

  battleId;
  battle;

  watchers = [];
  secondsLeft = 100;
  timeLeft = '00:00:00';

  fightersData;

  deviceDisplayType = 'full';

  isCP = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.deviceDisplayType = localStorage.getItem(`deviceDisplayType`);

    this.jwtToken = localStorage.getItem('jwtToken');
    this.route.params.subscribe((params) => {
      this.battleId = params['id'];

      this.graphQlService.get(`game`, `{ battle(id: "${this.battleId}"){ createdBy { id name } id endDateTime startingWallSize wallSize winnerId isResistance isCivilWar secondsLeft
          attackerCountry { id name flagLocation(size: "xl") }
          defenderCountry { id name flagLocation(size: "xl") }
          region { id name }
        } }`)
        .subscribe((r: any) => {
          this.battle = r.data.battle;
          if (this.battle.winnerId)
            this.battle.winner = this.battle.attackerCountry.id === this.battle.winnerId ? this.battle.attackerCountry : this.battle.defenderCountry.id;

          console.log('battle', this.battle)

          setTimeout(() => {
            this.initSignalR();
          }, 300);
        })
    });
  }

  // = = = SignalR
  hubConnection: signalR.HubConnection;

  initSignalR() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/battleHub`)
      .build();

    this.hubConnection
      .start()
      .then(() => {
        this.hubConnection.invoke('ClientConnected', this.jwtToken, this.battleId);
        this.initializeListeners();
      })
      .catch((err) =>
        console.log(`Error while starting SignalR connection: ${err}`)
      );
  }

  private initializeListeners(): void {
    if (!this.battle.winnerId) {
      this.hubConnection.on('InitialSet', (endDateTimeSeconds) => {
        this.secondsLeft = endDateTimeSeconds * 1000; 
      });
    }

    this.hubConnection.on('UpdateWatchers', (list) => {
      this.watchers = JSON.parse(list);
    });

    this.hubConnection.on('UpdateFighters', (data) => {
      this.fightersData = data;
      console.log('fighters', this.fightersData)
    });

    this.hubConnection.on('FightError', (data) => {
      this.message.error(data)
      console.log('FightError', data)
    });
  }


}
