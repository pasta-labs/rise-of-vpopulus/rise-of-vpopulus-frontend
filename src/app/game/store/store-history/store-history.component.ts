import { Component } from '@angular/core';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-store-history',
  templateUrl: './store-history.component.html',
  styleUrl: './store-history.component.scss'
})
export class StoreHistoryComponent {
  isLoading = false;

  list = [];

  constructor(
    private graphQlService: GraphQlService,
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ storeTransactions { id service status description createdOn } }`).subscribe((r: any) => {
      this.list = r.data.storeTransactions;
      console.log('hisy', this.list)
      this.isLoading = false;
    })
  }

}
