import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';
// import { Pi } from '@pinetwork-js/sdk';
import { firstValueFrom } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StoreComponent implements OnInit {
  apiUrl = environment.apiUrl;
  isLoading = true;

  offers = [];

  walletId;

  selectedProvider;
  selectedOffer;
  acceptedTos = false;

  gameWalletId;

  deviceDisplayType = 'full';

  currentTab = undefined;

  gameVersion;

  constructor(
    private httpService: GenericHttpService, private graphQlService: GraphQlService,
    private messageService: NzMessageService
  ) { }

  ngOnInit(): void {
    this.deviceDisplayType = localStorage.getItem(`deviceDisplayType`);
    this.gameVersion = JSON.parse(localStorage.getItem(`gameConfig`)).type;

    this.isLoading = true;
    this.graphQlService.get(`game`, `{ storeOffers { id name type offers { id amount price priceCurrency } } }`).subscribe((r: any) => {
      this.offers = r.data.storeOffers;
      console.log('offers', this.offers)
      this.isLoading = false;

      setTimeout(() => {
        this.currentTab = 0;

      }, 200);

    })
  }

  onTabChanged = (i) => {
    console.log('tab change', i)
    this.selectedOffer = undefined;
    this.acceptedTos = false;
  }

  selectOffer(provider, id) {
    this.selectedProvider = this.offers.find((x) => x.id === provider);
    this.selectedOffer = this.selectedProvider.offers.find(x => x.id == id);
  }

  start = (provider) => {
    if(provider === "BinancePay") this.startBinance();
    else if(provider === "PiBrowser") this.initPiStore();
  }

  startBinance = () => {
    this.graphQlService.post(`game`, `mutation mutate($data: StoreBinanceCreateOrderCommand) { storeBinanceInit(data: $data) }`, {
      data: { offerId: this.selectedOffer.id }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else window.location.href = r.data['storeBinanceInit']
      this.isLoading = false;
    });
  }

  initPiStore = async () => {
    this.messageService.success(`Purchase initialized...`)
    const r2 = await window['Pi'].authenticate(['payments'], (payments) => { });

    this.isLoading = true;
    const payment = await window['Pi'].createPayment({
      amount: this.selectedOffer.price,
      memo: `Purchase of ${this.selectedOffer.amount} Gold`,
      metadata: { offerId: this.selectedOffer.id, citizenId: localStorage.getItem(`citizenId`) },
      uid: r2.user.uid
    }, {
      onReadyForServerApproval: async (paymentId) => {
        console.log('onReadyForServerApproval', paymentId);
        await firstValueFrom(this.graphQlService.get(`game`, `{ storePiNetworkApprove(paymentId: "${paymentId}", offerId: "${this.selectedOffer.id}") }`));
      },
      onReadyForServerCompletion: async (paymentId, txid) => {
        console.log('onReadyForServerCompletion', paymentId, txid);
        await firstValueFrom(this.graphQlService.get(`game`, `{ storePiNetworkComplete(paymentId: "${paymentId}", tx: "${txid}") }`));
      },
      onCancel: async (paymentId) => {
        console.log('ronCancel', paymentId);
        await firstValueFrom(this.graphQlService.get(`game`, `{ storePiNetworkCancel(paymentId: "${paymentId}") }`));
        this.isLoading = false;
      },
      onError: function (error, payment) {
        console.log('onError', error, payment);
        this.isLoading = false;
      }
    });
  }

  submit() {
    this.isLoading = true;
    this.httpService.post(`/api/store/start`, { providerId: this.selectedProvider.id, offerId: this.selectedOffer.id, walletId: this.walletId }).subscribe((r: any) => {
      this.gameWalletId = r.targetWalletId;
      this.isLoading = false;
    }, e => {
      console.log('e', e)
    })

  }

}
