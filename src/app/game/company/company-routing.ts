import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { CompanyComponent } from './company/company.component';
import { CompanyCreateComponent } from './create/create.component';
import { CompanyListComponent } from './list/list.component';

export const routes: Routes = [
  {
    path: 'companies',
    component: CompanyListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'companies/create',
    component: CompanyCreateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'company/:id',
    component: CompanyComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'company',
    component: CompanyComponent,
    canActivate: [AuthGuard],
  },
];
