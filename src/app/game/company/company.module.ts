import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyListComponent } from './list/list.component';
import { CompanyCreateComponent } from './create/create.component';
import { CompanyComponent } from './company/company.component';
import { CompanyEmployeesComponent } from './company/components/employees/employees.component';
import { CompanyManageComponent } from './company/components/manage/manage.component';
import { CompanyJobsOffersComponent } from './company/components/jobs-offers/jobs-offers.component';
import { CompanyItemsOffersComponent } from './company/components/items-offers/items-offers.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntityModule } from '../entity/entity.module';
import { CompanyExportLicensePurchaseComponent } from './company/components/export-license-purchase/export-license-purchase.component';

@NgModule({
  declarations: [
    CompanyListComponent,
    CompanyCreateComponent,
    CompanyComponent,
    CompanyEmployeesComponent,
    CompanyManageComponent,
    CompanyJobsOffersComponent,
    CompanyItemsOffersComponent,
    CompanyExportLicensePurchaseComponent,
  ],
  imports: [CommonModule, SharedModule, EntityModule],
})
export class CompanyModule {}
