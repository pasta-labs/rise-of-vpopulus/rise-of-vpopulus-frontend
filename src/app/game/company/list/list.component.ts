import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { CompanyCreateComponent } from '../create/create.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-company-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class CompanyListComponent implements OnInit {
  isLoading = false;

  list = [];

  constructor(private graplQlService: GraphQlService, 
    private modalService: NzModalService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.graplQlService.get(`game`, `{ companies { id name avatar quality industry { value symbol isRaw } region { id x y country { id name } } } }`).subscribe((r: any) => {
      this.list = r.data.companies;
      this.isLoading = false;
    })
  }

  create() {
    this.modalService.create({
      nzWidth: '400px',
      nzContent: CompanyCreateComponent,
      nzTitle: this.translateService.instant('Citizen.Company.createCompany'),
      nzFooter: null,
    });
  }
}
