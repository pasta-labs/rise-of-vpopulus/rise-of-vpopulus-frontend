import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { CompanyEmployeesComponent } from './components/employees/employees.component';
import { CompanyExportLicensePurchaseComponent } from './components/export-license-purchase/export-license-purchase.component';
import { CompanyManageComponent } from './components/manage/manage.component';
import { cloneDeep } from 'lodash-es';
import { MissionsService } from 'src/app/shared/components/layout/missions/missions.service';
import { firstValueFrom } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],
})
export class CompanyComponent implements OnInit {
  isLoading = [false, false];

  company;

  workResult = undefined;

  employees = [];
  employed;

  isEntity = false;

  isVisible = false;

  incomeExpansesChart;
  selectedStatsDateRange = 'daily';

  exportLicenses = undefined;

  tradeAgreements = [];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private authService: AuthService,
    private graphQlService: GraphQlService,
    private missionsService: MissionsService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.isLoading[0] = true;
    this.route.params.subscribe((params) => {
      this.company = { id: params['id'] };

      if (this.company.id == undefined) {
        this.httpService
          .get(
            `/api/companies/employees/${localStorage.getItem(
              'entityId'
            )}/current`
          )
          .subscribe(
            (r) => {
              if (r == null) this.router.navigateByUrl(`/market/jobs`);
              else this.router.navigateByUrl(`/company/${r.company.id}`);
            },
            (error) => {
              console.log('error', error);
              if (error.status === 404)
                this.router.navigateByUrl(`/market/jobs`);
              if (error.error.Message === 'NOT_EMPLOYED')
                this.router.navigateByUrl(`/market/jobs`);
            }
          );
      } else this.loadData();
    });
  }

  loadData() {
    this.isLoading[0] = true;
    this.graphQlService.get(`game`, `{ company(id: "${this.company.id}") { id name avatar quality stock rawStock queue owner { id name type avatar } industry { symbol value } industryRaw { symbol value } region { id name country { id name flagLocation currency { symbol value } } } } }`)
      .subscribe((r: any) => {
        if (r.data && r.data.company) {
          this.company = cloneDeep(r.data.company);
          // Additional checks for nested properties
          if (this.company.industry && this.company.industryRaw) {
            console.log('Industry and IndustryRaw data loaded correctly');
          } else {
            console.warn('Missing industry or industryRaw data');
          }

          if (localStorage.getItem('entityId') === this.company.owner.id.toString()) {
            this.isEntity = true;
          }
          this.isLoading[0] = false;
          this.loadEmployees();
          this.loadIncomeExpanseStats();
          this.loadExportLicenses();
        } else {
          console.error('Failed to load company data');
          this.isLoading[0] = false;
        }
      }, error => {
        console.error('Error loading company data:', error);
        this.isLoading[0] = false;
      });
  }


  loadEmployees() {
    this.isLoading[1] = false;
    this.httpService
      .get(`/api/companies/${this.company.id}/employees`)
      .subscribe((r) => {
        this.employees = r;

        this.employed = this.employees.filter(
          (x) => x.citizen.id == localStorage.getItem(`entityId`)
        )[0];
        if (this.employed) this.loadWorkStats();

        console.log('emp', this.employed)
        this.isLoading[1] = false;
      });
  }

  loadWorkStats() {
    this.workResult = undefined;
    this.httpService
      .get(
        `/api/companies/${this.company.id}/work/${localStorage.getItem(
          'entityId'
        )}/today`
      )
      .subscribe((r) => {
        this.workResult = r;
        console.log('work', this.workResult)
      });
  }

  work() {
    this.isLoading[1] = true;
    this.httpService.post(`/api/companies/work`, {}).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant('Citizen.Company.youHaveWorked'));
        this.loadData();
        this.authService.updateTopBar.emit();
        this.missionsService.updateDailyMissionsRequest.emit();
      },
      (error) => {
        if (error.error.Message)
          this.messageService.error(error.error.Message);
        this.isLoading[1] = false;
      }
    );
  }

  resign() {
    this.httpService
      .post(`/api/companies/employees/resign`, {})
      .subscribe(() => {
        this.loadData();
        this.messageService.success(this.translateService.instant('Citizen.Company.youResignedWorking'));
      });
  }

  showManageModal1(): void {
    var modal = this.modalService.create({
      nzContent: CompanyManageComponent,
      nzFooter: null,
      nzWidth: '70%',
      nzTitle: this.translateService.instant('Citizen.Company.manageCompany'),
      nzData: {
        sourceType: 'company',
        sourceManagedEntityId: this.company.id,
        sourceCompany: this.company,
      },
    });
    modal.afterClose.subscribe((r) => {
      if (r == 'OK') this.loadData();
    });
  }

  showEmployeesModal(): void {
    var modal = this.modalService.create({
      nzContent: CompanyEmployeesComponent,
      nzFooter: null,
      nzData: {
        sourceEmployees: this.employees,
        sourceCompanyId: this.company.id,
      },
      nzStyle: {
        width: '80%',
      },
    });
  }

  loadIncomeExpanseStats() {
    this.httpService
      .get(
        `/api/companies/${this.company.id}/stats/${this.selectedStatsDateRange}`
      )
      .subscribe((r) => {
        this.incomeExpansesChart = undefined;
        setTimeout(() => {
          this.incomeExpansesChart = {
            labels: r.map((x) => x.date),
            datasets: [
              {
                data: r.map((x) => x.expanse.raws + x.expanse.salaries),
                label: this.translateService.instant('Citizen.Company.Expanses'),
              },
              { data: r.map((x) => x.income.sales), label: this.translateService.instant('Citizen.Company.Income') },
            ],
          };
        }, 100);
      });
  }

  loadExportLicenses = () => {
    this.exportLicenses = undefined;
    this.graphQlService.get(`game`, `{ companyExportLicenses(companyId: "${this.company.id}") { id country { id name flagLocation currency { symbol value } } expireDate } }`).subscribe((r: any) => {
      this.exportLicenses = cloneDeep(r.data.companyExportLicenses);
      if (this.company.region.country && !this.exportLicenses.find(x => x.country.id === this.company.region.country.id))
        this.exportLicenses.push({ country: this.company.region.country, expireDate: "-" });
    });
  }

  buyExportLicenses = () => {
    var ref = this.modalService.create({
      nzContent: CompanyExportLicensePurchaseComponent,
      nzData: {
        company: this.company,
        licenses: this.exportLicenses
      },
      nzMaskClosable: false,
      nzMask: true,
      nzWidth: '70%',
      nzFooter: null,
      nzTitle: this.translateService.instant('Citizen.Company.purchaseExportLicense')
    });
    ref.afterClose.subscribe(r => r == "ok" ? this.loadExportLicenses() : "");
  }
}
