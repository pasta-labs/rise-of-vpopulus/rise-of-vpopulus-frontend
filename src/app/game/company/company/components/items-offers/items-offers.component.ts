import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { firstValueFrom } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-company-items-offers',
  templateUrl: './items-offers.component.html',
  styleUrls: ['./items-offers.component.scss'],
})
export class CompanyItemsOffersComponent implements OnInit {
  isLoading = false;

  @Input() company;
  @Input() canManage = false;

  @Input() countries = [];
  apiUrl = environment.apiUrl;
  tradeAgreements = [];
  selectedCountry;

  marketOffers;

  addIsVisible = false;

  @Output() refreshParent = new EventEmitter();

  form = new UntypedFormGroup({
    companyId: new UntypedFormControl(null, [Validators.required]),
    countryId: new UntypedFormControl(null, [Validators.required]),
    amount: new UntypedFormControl(1, [Validators.required, Validators.min(1)]),
    price: new UntypedFormControl(0.01, [Validators.required, Validators.min(0.01)]),
  });

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    this.form.get(`companyId`).setValue(this.company.id);
    this.loadMarektOffers();
    this.countries = this.countries.map(x => ({ ...x, type: 'el' }))

    this.form.get(`countryId`).valueChanges.subscribe(() => {
      this.selectedCountry = this.countries.find(x => x.country.id === this.form.get(`countryId`).value)?.country;
      if (!this.selectedCountry) this.selectedCountry = this.tradeAgreements.find(x => x.country.id === this.form.get(`countryId`).value)?.country;
    })

  }

  loadMarektOffers() {
    this.isLoading = true;
    this.httpService
      .get(`/api/economic/companies/${this.company.id}/items`)
      .subscribe((r) => {
        this.marketOffers = r;
        this.isLoading = false;
        console.log(this.marketOffers)
      });
  }

  showAddOfferModal = async () => {
    this.addIsVisible = true;
    this.isLoading = true;
    const tradeAgreements = await firstValueFrom(this.graphQlService.get(`game`, `{ countryTradeAgreements(countryId: "${this.company.region.country.id}"){ id name flagLocation(size: "s") currency { symbol value } } }`));
    this.tradeAgreements = tradeAgreements.data['countryTradeAgreements'].map(x => ({ country: x }));

    const countryLawsTradeEmbargo = await firstValueFrom(this.graphQlService.get(`game`, `{ countryLawsTradeEmbargo(countryId: "${this.company.region.country.id}"){ id } }`));
    countryLawsTradeEmbargo.data['countryLawsTradeEmbargo'].forEach(e => {
      let country = this.countries.find(x => x.country.id === e.id);
      if (country) country.disabled = true;
    })

    this.isLoading = false;
  }

  addMarketOffer() {
    this.isLoading = true;
    var formValues = this.form.getRawValue();
    this.httpService.post(`/api/economic/markets/items`, formValues).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant(`Economy.Market.Items.MarketOfferCreated`));
        this.addIsVisible = false;
        this.loadMarektOffers();
        this.refreshParent.emit("");
      },
      (error) => {
        if (error.error.code === "AMOUNT_TOO_LOW") this.messageService.error(this.translateService.instant('Economy.Items.amountLow'));
        if (error.error.code === "PRICE_TOO_LOW") this.messageService.error(this.translateService.instant('Economy.Items.priceLow'));
        if (error.error.code === "OFFER_EXISTS") this.messageService.error(this.translateService.instant('Economy.Items.offerExists'));
        if (error.error.code === "NOT_ENOUGH_STOCK") this.messageService.error(this.translateService.instant('Economy.Items.notStock'));
        if (error.error.code === "TRADE_EMBARGO") this.messageService.error(this.translateService.instant('Economy.Items.tradeEmbargo'));
        if (error.error.code === "COMPANY_NOT_EXISTS") this.messageService.error(this.translateService.instant('Economy.Items.companyNotExists'));
        if (error.error.code === "COUNTRY_NOT_FOUND") this.messageService.error(this.translateService.instant('Economy.Items.countryNotFound'));
        if (error.error.code === "CANNOT_SELL_ON_THIS_MARKET") this.messageService.error(this.translateService.instant('Economy.Items.cannotSellMarket'));
        this.isLoading = false;
      }
    );
  }

  deleteMarketOffer(id) {
    this.isLoading = true;
    this.httpService
      .delete(`/api/economic/companies/${this.company.id}/items/${id}`)
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant(`Economy.Market.Items.MarketOfferRemoved`));
          this.loadMarektOffers();
          this.refreshParent.emit("");
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
