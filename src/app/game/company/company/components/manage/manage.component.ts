import { Component, Input, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-company-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss'],
})
export class CompanyManageComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  upgradeCost = [20, 40, 60];
  downgradeCost = [10, 20, 30];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private modalRef: NzModalRef,
    private router: Router, 
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {  }

  downgrade() {
    this.isLoading = true;
    this.httpService
      .put(`/api/companies/downgrade`, {
        companyId: this.nzModalData.sourceManagedEntityId,
      })
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant(`Economy.Company.DowngradeSuccessful`));
          this.modalRef.close(`OK`);
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  dissolve() {
    this.isLoading = true;
    this.httpService
      .put(`/api/companies/downgrade`, {
        companyId: this.nzModalData.sourceManagedEntityId,
      })
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant(`Economy.Company.DissolveSuccesful`));
          this.modalRef.close(``);
          this.router.navigateByUrl(`/companies`);
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }

  upgrade() {
    this.isLoading = true;
    this.httpService
      .put(`/api/companies/upgrade`, { companyId: this.nzModalData.sourceManagedEntityId })
      .subscribe(
        () => {
          this.messageService.success(this.translateService.instant(`Economy.Company.UpgradeSuccessful`));
          this.modalRef.close(`OK`);
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
