import { Component, Input, OnInit, inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-company-export-license-purchase',
  templateUrl: './export-license-purchase.component.html',
  styleUrls: ['./export-license-purchase.component.scss']
})
export class CompanyExportLicensePurchaseComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  countries = [];

  selectedCountry;

  constructor(
    private graphQlService: GraphQlService,
    private messageService: NzMessageService,
    private modalRef: NzModalRef,
    private translateService: TranslateService,

  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData = () => {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ countries { id name flagLocation } }`).subscribe(async (r: any) => {
      let countries = r.data.countries.filter(x => x.id !== this.nzModalData.company.region.country.id);

      const licenses = await firstValueFrom(this.graphQlService.get(`game`, `{ companyExportLicenses(companyId: "${this.nzModalData.company.id}") { id country { id name flagLocation } } }`))
      this.countries = countries.filter(x => !licenses.data['companyExportLicenses'].map(l => l.country.id).includes(x.id));
      this.isLoading = false;
    });
  }

  purchase = () => {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: BuyExportLicenseCommand) { companyExportLicensePurchase(data: $data) }`, { data: { companyId: this.nzModalData.company.id, countryId: this.selectedCountry } }).subscribe((r: any) => {
      this.messageService.success(this.translateService.instant('Citizen.Company.exportLicenseAcquired'));
      this.modalRef.close(`ok`);
      this.isLoading = false;
    })
  }

}
