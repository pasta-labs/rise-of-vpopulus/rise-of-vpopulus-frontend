import { Component, Input, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-company-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
})
export class CompanyEmployeesComponent implements OnInit {
  sourceEmployees;
  @Input() sourceCompanyId;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  isLoading = false;

  expandSet = new Set<number>();

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    this.sourceEmployees = this.nzModalData.sourceEmployees;
    this.getStats();
  }

  reloadEmployees() {
    this.isLoading = true;
    this.httpService
      .get(`/api/companies/${this.sourceCompanyId}/employees`)
      .subscribe((r) => {
        this.sourceEmployees = r;
        this.isLoading = false;
      });
  }

  getStats() {
    for (let employee of this.sourceEmployees) {
      this.httpService
        .get(
          `/api/companies/${this.sourceCompanyId}/work/${employee.citizen.id}`
        )
        .subscribe((r) => {
          employee.stats = r;
        });
    }
  }

  onExpandChange(checked: boolean, employee): void {
    if (checked) {
      employee.datasets = [
        {
          data: employee.stats.map((day) => day.production),
          label: 'Production',
        },
        {
          data: employee.stats.map((day) => day.health),
          label: 'Health',
        },
      ];
      employee.labels = employee.stats.map((day) => day.dateTime.split('T')[0]);
      employee.options = {
        responsive: true,
        scales: {
          xAxes: [{}],
          yAxes: [
            {
              id: 'y-axis-0',
              position: 'left',
              ticks: {
                min: 0,
                max: 100,
              },
            },
            {
              id: 'y-axis-1',
              position: 'right',
              ticks: {
                fontColor: 'red',
                suggestedMin: 0,
              },
            },
          ],
        },
      };
      employee.viewChart = true;
      this.expandSet.add(employee.citizen.id);
    } else {
      employee.viewChart = false;
      this.expandSet.delete(employee.citizen.id);
    }
  }

  fireEmployee(id) {
    this.httpService
      .post(`/api/companies/employees/fire`, { employeeId: id })
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant(`Economy.Company.EmployeeFiredSuccess`));
          this.reloadEmployees();
        },
        (error) => {
          this.messageService.error(error.message);
        }
      );
  }

  changeSalary(employee) {
    const payload = {
      citizenId: employee.citizen.id,
      salary: employee.newSalary,
      companyId: this.sourceCompanyId,
    };
    this.httpService.put(`/api/companies/employees/salary`, payload).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant(`Economy.Company.SalaryChangedSuccess`));
        this.reloadEmployees();
      },
      (error) => {
        this.messageService.error(error.message);
      }
    );
    employee.isEdited = false;
    employee.newSalary = null;
  }

  cancelEditing(employee) {
    employee.isEdited = false;
    employee.newSalary = null;
  }
}
