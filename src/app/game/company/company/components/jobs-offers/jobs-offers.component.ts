import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-company-jobs-offers',
  templateUrl: './jobs-offers.component.html',
  styleUrls: ['./jobs-offers.component.scss'],
})
export class CompanyJobsOffersComponent implements OnInit {
  isLoading = false;

  @Input() companyId;
  @Input() canManage = false;

  jobOffers = [];
  addIsVisible = false;

  form2 = new UntypedFormGroup({
    companyId: new UntypedFormControl(),
    skill: new UntypedFormControl(0, Validators.required),
    salary: new UntypedFormControl(1, Validators.required),
    amount: new UntypedFormControl(1, Validators.required),
  });

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.loadJobs();
  }

  loadJobs() {
    this.isLoading = true;
    this.httpService
      .get(`/api/economic/companies/${this.companyId}/jobs`)
      .subscribe((r) => {
        this.jobOffers = r;
        this.isLoading = false;
      });
  }

  addJobOffer() {
    this.isLoading = true;
    var formValues = this.form2.getRawValue();
    formValues['companyId'] = this.companyId;
    this.httpService.post(`/api/economic/markets/jobs`, formValues).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant(`Economy.Market.JobOfferAddedSuccessfully`));
        this.addIsVisible = false;
        this.loadJobs();
      },
      (error) => {
        this.messageService.error(error.error.Message);
        this.isLoading = false;
      }
    );
  }

  deleteJobOffer(id) {
    this.httpService
      .delete(`/api/economic/companies/${this.companyId}/jobs/${id}`)
      .subscribe(
        (r) => {
          this.messageService.success(this.translateService.instant(`Economy.Market.JobOfferRemovedSuccessfully`));
          this.loadJobs();
        },
        (error) => {
          this.messageService.error(error.error.Message);
          this.isLoading = false;
        }
      );
  }
}
