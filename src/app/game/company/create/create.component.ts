import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-company-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CompanyCreateComponent implements OnInit {
  isLoading = false;

  form: UntypedFormGroup;

  selectedIndustry;
  industryTypes = [];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private modalRef: NzModalRef, 
    private translateService: TranslateService
  ) {
    this.form = new UntypedFormGroup({
      name: new UntypedFormControl('', Validators.required),
      industryType: new UntypedFormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.httpService.get(`/api/dictionaries/raws`).subscribe((r) => {
      this.industryTypes.push(...r);
    });
    this.httpService.get(`/api/dictionaries/items`).subscribe((r) => {
      this.industryTypes.push(...r);
    });
  }

  create() {
    this.form.get(`industryType`).setValue(this.selectedIndustry.id);

    this.isLoading = true;
    this.httpService.post(`/api/companies`, this.form.getRawValue()).subscribe(
      (r) => {
        this.messageService.success(this.translateService.instant('Citizen.Company.companyCreated'));
        this.isLoading = false;
        this.modalRef.close();
        this.router.navigateByUrl(`/company/${r}`);
      },
      (error) => {
        this.messageService.error(this.translateService.instant(`Common.Errors.` + error.error.code));
        this.isLoading = false;
      }
    );
  }
}
