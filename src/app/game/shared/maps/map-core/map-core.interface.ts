export interface MapConfig {
  angle: number;
  background: {
    color: string,
    hoverColor: string,
    selectedColor: string,
  };
  border: HexagonBorder;
  gradient: HexagonGradient;
  label: Partial<HexagonLabel>;
  numberOfHexagonSide: number;
  sideLength: number;
}

export interface HexagonBorder {
  color: string;
  width: number;
}

export interface HexagonPoint {
  x: number;
  y: number;
}

export interface HexagonPosition {
  hexX: number;
  hexY: number;
  screenX: number;
  screenY: number;
}

export interface HexagonLabel {
  color: string;
  fontFamily: string;
  fontSize: number;
  position: HexagonPoint;
  text: string;
}

export interface HexagonGradient {
  color: string;
  lineWidth: number;
}

export interface HexagonConfig extends HexagonPoint {
  background: string;
  borders: HexagonBorder[];
  gradient: HexagonGradient;
  label: HexagonLabel;
  points: HexagonPoint[];
}

