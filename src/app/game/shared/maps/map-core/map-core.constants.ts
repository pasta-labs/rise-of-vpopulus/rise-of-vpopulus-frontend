import { MapConfig } from './map-core.interface';

export const HEXAGON_CONFIG: MapConfig = {
  angle: 0.5,
  numberOfHexagonSide: 15,
  sideLength: 40,
  border: {
    color: 'grey',
    width: 2,
  },
  background: {
    color: 'rgba(0, 113, 19, 0.6)',
    hoverColor: 'rgba(157, 241, 0, 0.5)',
    selectedColor: 'rgba(241,217,0,0.5)',
  },
  gradient: null,
  label: {
    fontFamily: 'Arial',
    fontSize: 12,
    color: 'grey',
  },
};
