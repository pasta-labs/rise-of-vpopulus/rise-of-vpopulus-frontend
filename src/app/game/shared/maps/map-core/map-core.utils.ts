import {
  MapConfig,
  HexagonConfig,
  HexagonPoint,
  HexagonBorder,
  HexagonGradient,
} from './map-core.interface';
import { HEXAGON_CONFIG } from './map-core.constants';

export function getHexHeight(angle: number, sideLength: number): number {
  return Math.floor(Math.sin(angle) * sideLength);
}

export function getHexRadius(angle: number, sideLength: number): number {
  return Math.floor(Math.cos(angle) * sideLength);
}

export function getHexRectangleHeight(config: MapConfig): number {
  const {angle, sideLength} = config;
  return sideLength + 2 * getHexHeight(angle, sideLength);
}

export function hexagonsList(
  config: MapConfig = HEXAGON_CONFIG
): HexagonConfig[] {
  const hexList = [];
  const {
    angle,
    sideLength,
    border,
    numberOfHexagonSide,
    label,
    gradient,
    background,
  } = config;

  const hexHeight: number = getHexHeight(angle, sideLength);
  const hexRadius: number = getHexRadius(angle, sideLength);
  const hexRectangleHeight: number = getHexRectangleHeight(config);
  const hexRectangleWidth: number = 2 * hexRadius;
  for (let i = -4; i < numberOfHexagonSide - 4; ++i){
    for (let j = 0; j < numberOfHexagonSide; ++j){
      const x: number = i * hexRectangleWidth + (j % 2) * hexRadius;
      const y: number = j * (sideLength + hexHeight);

      const hexY: number = Math.floor(y / (hexHeight + sideLength));
      const hexX: number = Math.floor(
        (x - (hexY % 2) * hexRadius) / (2 * hexRadius)
      );
      const screenX: number = hexX * (2 * hexRadius) + (hexY % 2) * hexRadius;
      const screenY = hexY * (hexHeight + sideLength);
      const offset = border.width * 0.5;

      const hexagon: HexagonConfig = {
        x: hexX,
        y: hexY,
        points: [
          {
            x: x + hexRadius - offset,
            y: y + offset * 2,
          },
          {
            x: x + hexRectangleWidth - offset * 2,
            y: y + hexHeight,
          },
          {
            x: x + hexRectangleWidth - offset * 2,
            y: y + hexHeight + sideLength,
          },
          {
            x: x + hexRadius - offset,
            y: y + hexRectangleHeight - offset * 2,
          },
          {
            x,
            y: y + sideLength + hexHeight,
          },
          {
            x,
            y: y + hexHeight,
          },
        ],
        background: background.color,
        gradient,
        borders: new Array(6).fill({
          color: 'grey',
          width: border.width,
        }),
        label: {
          position: {
            x: screenX + (hexHeight * 2 - label.fontSize / 2),
            y: screenY + (hexHeight * 2 + label.fontSize / 2),
          },
          color: label.color,
          text: `${hexX}, ${hexY}`,
          fontFamily: label.fontFamily,
          fontSize: label.fontSize,
        },
      };

      hexList.push(hexagon);
    }
  }

  return hexList;
}

export function drawLine(
  canvas: HTMLCanvasElement,
  start: HexagonPoint,
  stop: HexagonPoint,
  line: HexagonBorder
): void {
  const context: CanvasRenderingContext2D = canvas.getContext('2d');
  context.lineWidth = line.width;
  context.strokeStyle = line.color;
  context.beginPath();
  context.moveTo(start.x, start.y);
  context.lineTo(stop.x, stop.y);
  context.closePath();
  context.stroke();
}

export function drawHexagonStroke(
  canvas: HTMLCanvasElement,
  hexagon: HexagonConfig
): void {
  for (let index = 0; index < 6; index++){
    const start: HexagonPoint = hexagon.points[index];
    const stop: HexagonPoint =
      index + 1 < hexagon.points.length
        ? hexagon.points[index + 1]
        : hexagon.points[0];

    drawLine(canvas, start, stop, hexagon.borders[index]);
  }
}

export function drawText(canvas: HTMLCanvasElement, hexagon: HexagonConfig): void {
  const context: CanvasRenderingContext2D = canvas.getContext('2d');
  {
    context.font = `${hexagon.label.fontSize}px ${hexagon.label.fontFamily}`;
    context.textAlign = 'center';
    context.fillStyle = hexagon.label.color;
    context.fillText(
      hexagon.label.text,
      hexagon.label.position.x,
      hexagon.label.position.y
    );
  }
}

export function drawHexagonFill(
  hexagon: HexagonConfig,
  fillColor: string | HTMLCanvasElement,
  context?: CanvasRenderingContext2D,
  canvas?: HTMLCanvasElement
): void {
  const ctx: CanvasRenderingContext2D = context || canvas.getContext('2d');

  if (!ctx) {
    return;
  }

  ctx.beginPath();
  ctx.lineWidth = hexagon.borders[0].width;
  ctx.fillStyle =
    typeof fillColor === 'string'
      ? fillColor
      : ctx.createPattern(fillColor, 'repeat');
  ctx.strokeStyle = 'transparent';
  for (let index = 0; index < hexagon.points.length; index++){
    const point: HexagonPoint = hexagon.points[index];

    if (index === 0) {
      ctx.moveTo(point.x, point.y);
    }
    ctx.lineTo(point.x, point.y);
  }
  ctx.closePath();
  ctx.fill();
}

export function getStripsPattern(
  gradientConfig: HexagonGradient
): HTMLCanvasElement {
  const x0 = 36;
  const x1 = -4;
  const y0 = -2;
  const y1 = 18;
  const offset = 32;

  const offCanvas = document.createElement('canvas');
  offCanvas.width = 32;
  offCanvas.height = 16;
  const context: any = offCanvas.getContext('2d');

  context.strokeStyle = gradientConfig.color;
  context.lineWidth = gradientConfig.lineWidth;
  context.beginPath();
  context.moveTo(x0, y0);
  context.lineTo(x1, y1);
  context.moveTo(x0 - offset, y0);
  context.lineTo(x1 - offset, y1);
  context.moveTo(x0 + offset, y0);
  context.lineTo(x1 + offset, y1);
  context.stroke();
  context.fillStyle = context.createPattern(offCanvas, 'repeat');

  return offCanvas;
}

export function getRandomColor(): string {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++){
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export function removeConnectedCountryBorders(regions): any {
  console.log('regions', regions)
  regions.forEach((region) => {
    const borderRegions = getRegionBorders(region.x, region.y);

    borderRegions.forEach((border) => {
      const borderRegion = getRegionByPosition(
        regions,
        border[0],
        border[1]
      );

      if (
        borderRegion &&
        borderRegion.countryColor &&
        region.countryColor &&
        borderRegion.countryColor === region.countryColor
      ) {
        region.borders[borderRegions.indexOf(border)] = {
          color: 'grey',
          width: 2,
        };
      }
    });
  });
  return regions;
}

export function getRegionByPosition(regions, x, y): any {
  return regions.find((region) => region.x === x && region.y === y);
}

export function getRegionBorders(x: number, y: number): [number, number][] {
  if (y === 0 || y % 2 === 0) {
    return [
      [x, y - 1],
      [x + 1, y],
      [x, y + 1],
      [x - 1, y + 1],
      [x - 1, y],
      [x - 1, y - 1],
    ];
  } else {
    return [
      [x + 1, y - 1],
      [x + 1, y],
      [x + 1, y + 1],
      [x, y + 1],
      [x - 1, y],
      [x, y - 1],
    ];
  }
}

export function getHexagonsRing(x: number, y: number, numberOfRings: number): [number, number][] {

  let listOfHexagons = [];
  x = x - numberOfRings;
  let n = 0;
  for (let i = 0; i <= numberOfRings; i++){

    n = (i % 2) ? n + 1 : n;

    for (let j = 0; j < ((numberOfRings * 2) + 1) - i; j++){

      if (i === 0) {
        listOfHexagons = [...listOfHexagons, [x + j, y]];
      } else if (i % 2 && !((y + i) % 2)) {
        listOfHexagons = [
          ...listOfHexagons,
          [x + j + n, y + i],
          [x + j + n, y - i],

        ];
      } else {
        listOfHexagons = [
          ...listOfHexagons,
          [x + j + i - n, y + i],
          [x + j + i - n, y - i],

        ];
      }

    }
  }
  return listOfHexagons;
}

export function getCleanContext(layer: HTMLCanvasElement): CanvasRenderingContext2D {
  const context: CanvasRenderingContext2D = layer?.getContext('2d');
  context?.clearRect(
    0,
    0,
    layer.width,
    layer.height
  );

  return context;
}

export function getHexagonsFromTo(startHex: HexagonPoint, stopHex: HexagonPoint): HexagonPoint[] {

  const hexList = [];

  for (let i = 0; i <= stopHex.x - startHex.x; i++){
    for (let j = 0; j <= stopHex.y - startHex.y; j++){
      hexList.push({x: startHex.x + i, y: startHex.y + j});
    }
  }
  return hexList;

}


