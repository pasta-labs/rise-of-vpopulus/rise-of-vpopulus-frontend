import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Debounce } from 'src/app/shared/decorators/debounce.decorator';
import { HEXAGON_CONFIG } from './map-core.constants';
import { HexagonConfig, HexagonPoint, HexagonPosition } from './map-core.interface';
import {
  getHexRadius,
  getHexRectangleHeight,
  getHexHeight,
  drawHexagonStroke,
  drawHexagonFill,
  getStripsPattern,
  drawText, getHexagonsRing, getCleanContext,
} from './map-core.utils';
import Hexes from '../../../../../assets/map.json';
import { cloneDeep } from 'lodash-es';
import { MapService } from '../map.service';

@Component({
  selector: 'app-map-core',
  templateUrl: './map-core.component.html',
  styleUrls: ['./map-core.component.scss'],
})
export class MapCoreComponent implements OnInit, AfterViewInit {
  private regionsData: any;
  @Input() midPoint = { x: 0, y: 0 };
  @Input() circleRadius = 2;
  @Input() customHexagons = undefined;
  @Input() updateRegionsEvent;
  @Input() mapHeight = '60vh';
  @Input() enabledRegions;

  @Output() selectedHexagon: EventEmitter<any> = new EventEmitter();
  @ViewChild('activeLayer', { static: true }) activeLayer: ElementRef;
  @ViewChild('grid', { static: true }) grid: ElementRef;
  @ViewChild('mapWrapper') mapWrapper: ElementRef;
  @ViewChild('selectLayer', { static: true }) selectLayer: ElementRef;

  canMove: boolean;
  hasMoved: boolean;
  canvasActiveLayer: HTMLCanvasElement;
  canvasGrid: HTMLCanvasElement;
  canvasSelectLayer: HTMLCanvasElement;
  hexagons: HexagonConfig[];
  hoveredRegionBase64: string;
  loading: any;
  positionLayer: { top: number; left: number } = { top: 0, left: 0 };
  selectedRegionBase64: string;
  readonly hexHeight: number = getHexHeight(HEXAGON_CONFIG.angle, HEXAGON_CONFIG.sideLength);
  readonly hexRadius: number = getHexRadius(HEXAGON_CONFIG.angle, HEXAGON_CONFIG.sideLength);
  readonly hexRectangleHeight: number = getHexRectangleHeight(HEXAGON_CONFIG);
  readonly hexagonConfig = HEXAGON_CONFIG;
  canvasHeight: number;
  canvasWidth: number;

  constructor(
    private mapService: MapService
  ) {
    this.canvasHeight = (HEXAGON_CONFIG.sideLength + this.hexHeight) * HEXAGON_CONFIG.numberOfHexagonSide
      + (HEXAGON_CONFIG.sideLength + this.hexHeight) / 2;
    this.canvasWidth = 2 * this.hexRadius * HEXAGON_CONFIG.numberOfHexagonSide + this.hexRadius + 10;
  }

  @Debounce(5)
  moveHex(event: MouseEvent): void {
    const { hexX, hexY, screenX, screenY } = this.getHexPosition(event);

    if (this.onGrid(hexX, hexY)) {
      this.redDraw(screenX, screenY, this.canvasActiveLayer, this.hoveredRegionBase64);
    }
  }

  selectRegion(event: any): void {
    const { hexX, hexY, screenX, screenY } = this.getHexPosition(event);

    if (this.onGrid(hexX, hexY)) {
      this.redDraw(screenX, screenY, this.canvasSelectLayer, this.selectedRegionBase64);
    }
  }

  ngOnInit(): void {
    if (!this.mapHeight) this.mapHeight = '60vh';

    this.hexagons = this.getCircleHexagonsList(this.midPoint, this.circleRadius);
    this.hoveredRegionBase64 = this.buildFilledHexagon(this.hexagonConfig.background.hoverColor);
    this.selectedRegionBase64 = this.buildFilledHexagon(this.hexagonConfig.background.selectedColor);
    this.positionLayer = {
      left: -this.hexagons[0].points[5].x,
      top: -this.hexagons[this.hexagons.length - (this.circleRadius * 2) - ((this.midPoint.y % 2) ? -1 : 1)].points[0].y
    };
    this.positionLayer.left += 50;
    this.positionLayer.top += 10;
  }

  ngAfterViewInit(): void {
    this.canvasGrid = this.grid.nativeElement;
    this.canvasActiveLayer = this.activeLayer.nativeElement;
    this.canvasSelectLayer = this.selectLayer.nativeElement;

    this.buildGrid();
    this.updateRegionsEvent.subscribe((r) => {

      this.regionsData = r;
      this.hexagons = this.regionsWithData(this.hexagons);

      this.clearContexts([this.canvasActiveLayer, this.canvasSelectLayer, this.canvasGrid]);
      this.buildGrid();
    });
    this.moveMapInit();
    this.mapService.resetResourcesFilters.subscribe(() => this.resetBackgrounds());
    this.mapService.resetCountries.subscribe(this.resetCountries);

    document.body.addEventListener("touchstart", e => {
      if (e.target['localName'] === "canvas") e.preventDefault();
    }, { passive: false });
  }

  private resetBackgrounds = () => {
    this.hexagons = this.hexagons.map((hex: HexagonConfig) => {
      if (hex.background !== "rgba(0, 113, 19, 0.6)") hex.background = "rgba(0, 113, 19, 0.6)";
      return hex;
    });
  }

  private resetCountries = () => {
    this.hexagons = this.hexagons.map((hex: HexagonConfig) => {
      if (hex.background !== "rgba(0, 113, 19, 0.6)") hex.background = "rgba(0, 113, 19, 0.6)";
      hex.borders = [
        { "color": "grey", "width": 2 },
        { "color": "grey", "width": 2 },
        { "color": "grey", "width": 2 },
        { "color": "grey", "width": 2 },
        { "color": "grey", "width": 2 },
        { "color": "grey", "width": 2 }
      ];
      return hex;
    });
  }

  private regionsWithData(hexagons): HexagonConfig[] {
    this.regionsData.forEach(region => {
      var hex = hexagons.find(_ => _.x === region.x && _.y === region.y);
      if (hex) hexagons[hexagons.indexOf(hex)] = { ...hex, ...region };
    });
    return hexagons;
  }

  private getCircleHexagonsList(midpoint: HexagonPoint, numberOfRings: number): HexagonConfig[] {
    const circleHexagonsList: HexagonConfig[] = [];
    const ring: [number, number][] = getHexagonsRing(midpoint.x, midpoint.y, numberOfRings);

    for (const cord of ring) {
      const hexs = cloneDeep(Hexes).find((hex) => {
        return cord[0] === hex.x && cord[1] === hex.y;
      });

      if (hexs) {
        circleHexagonsList.push(hexs);
      }
    }

    return circleHexagonsList;
  }

  private moveMapInit(): void {
    this.activeLayer.nativeElement.addEventListener('mousedown', (d) => this.preMoveMapStart(d));
    this.activeLayer.nativeElement.addEventListener('touchstart', (d) => this.preMoveMapStart(d.touches[0], true));

    window.addEventListener('mouseup', (e) => this.moveMapEnd(e));
    window.addEventListener('touchend', (e) => this.moveMapEnd(e.changedTouches[0], true));
  }

  moveMapTimeout;

  private preMoveMapStart = (d, isMobile = false) => {
    this.moveMapTimeout = setTimeout(() => this.moveMapStart(d, isMobile), 300);
  }

  private moveMapStart = (d, isMobile = false) => {
    console.log('move map start')
    clearTimeout(this.moveMapTimeout);
    this.moveMapTimeout = undefined;

    const boundingClientRect: ClientRect = this.activeLayer.nativeElement.getBoundingClientRect();
    const parentClientRect: ClientRect = this.mapWrapper.nativeElement.getBoundingClientRect();

    const left = d.clientX - boundingClientRect.left + parentClientRect.left;
    const top = d.clientY - boundingClientRect.top + parentClientRect.top;
    this.canMove = true;
    this.hasMoved = false;

    this.mapWrapper.nativeElement.addEventListener('mousemove', (e) => this.moveMapInProgress(e, left, top));
    this.mapWrapper.nativeElement.addEventListener('touchmove', (e) => this.moveMapInProgress(e.touches[0], left, top));
  }

  private moveMapInProgress = (e, left, top) => {
    if (!this.canMove) {
      return;
    }
    this.hasMoved = true;
    this.positionLayer = { left: e.clientX - left, top: e.clientY - top };
  }

  private moveMapEnd = (e, isMobile = false) => {
    console.log('move map end', this.moveMapTimeout)
    clearTimeout(this.moveMapTimeout);

    if (isMobile) {
      var rect = e.target.getBoundingClientRect();
      var x = e.pageX - rect.left;
      var y = e.pageY - rect.top;
      e = { offsetX: x, offsetY: y, clientX: e.clientX, clientY: e.clientY }
    }

    if ((this.hasMoved === false && this.canMove) || this.moveMapTimeout) {
      this.canMove = false;

      const { hexX, hexY } = this.getHexPosition(e);
      var hex: HexagonConfig = undefined;

      if (!this.onGrid(hexX, hexY)) return;
      hex = this.hexagons.find(hexagon => hexagon.x === hexX && hexagon.y === hexY);
      console.log('hex', hex)

      if (!hex) return;
      if (this.enabledRegions && this.enabledRegions.filter(region => region.x === hex.x && region.y === hex.y).length === 0) return;

      this.selectedHexagon.emit({ ...hex, clientX: e.clientX, clientY: e.clientY });
      this.selectRegion(e)
    } else
      this.canMove = false;
  }

  private onGrid(hexX: number, hexY: number): boolean {

    // check if is on the grid
    return this.hexagons.findIndex((hexagon: HexagonConfig) => {
      return hexagon.x === hexX && hexagon.y === hexY;
    }) > -1;

    // check if is on the layer
    // return (
    //   hexX >= 0 &&
    //   hexX < this.hexagonConfig.numberOfHexagonSide &&
    //   hexY >= 0 &&
    //   hexY < this.hexagonConfig.numberOfHexagonSide
    // );
  }

  private redDraw(screenX: number, screenY: number, layer: HTMLCanvasElement, src: string): void {
    if (!src || !layer) {
      return;
    }
    const img = new Image();
    img.src = src;

    getCleanContext(layer).drawImage(img, screenX, screenY);
  }

  private buildGrid(): void {
    if (!this.canvasGrid) {
      return;
    }

    this.hexagons.forEach((hexagon: HexagonConfig) => {
      if (hexagon.borders) {
        drawHexagonStroke(this.canvasGrid, hexagon);
      }

      if (hexagon.background) {
        drawHexagonFill(hexagon, hexagon.background, null, this.canvasGrid);
      }
      if (hexagon.gradient) {
        drawHexagonFill(
          hexagon,
          getStripsPattern(hexagon.gradient),
          null,
          this.canvasGrid
        );
      }
      if (hexagon.label) {
        drawText(this.canvasGrid, hexagon);
      }
    });
    this.loading = false;
  }

  private buildFilledHexagon(backgroundColor: string): string {
    const offCanvas = document.createElement('canvas');
    offCanvas.width = this.hexRectangleHeight;
    offCanvas.height = this.hexRectangleHeight;

    const context: CanvasRenderingContext2D = offCanvas.getContext('2d');
    drawHexagonFill(
      cloneDeep(Hexes[0]),
      backgroundColor,
      context
    );

    return offCanvas.toDataURL('image/png');
  }

  private getHexPosition(event: MouseEvent): HexagonPosition {
    const sideLength: number = this.hexagonConfig.sideLength;
    const x: number = event.offsetX;
    const y: number = event.offsetY;

    const hexY: number = Math.floor(y / (this.hexHeight + sideLength));
    const hexX: number = Math.floor(
      (x - (hexY % 2) * this.hexRadius) / (2 * this.hexRadius)
    );

    const screenX: number =
      hexX * (2 * this.hexRadius) + (hexY % 2) * this.hexRadius;
    const screenY = hexY * (this.hexHeight + sideLength);

    return { hexX, hexY, screenX, screenY };
  }

  increaseHexagons(): void {
    this.loading = false;
    this.circleRadius++;
    this.clearContexts([this.canvasActiveLayer, this.canvasSelectLayer, this.canvasGrid]);
    this.hexagons = this.regionsWithData(this.getCircleHexagonsList(this.midPoint, this.circleRadius));
    this.buildGrid();
  }

  decreaseHexagons(): void {
    this.circleRadius--;

    if (this.circleRadius < 0) {
      this.circleRadius = 0;
      return;
    }

    this.clearContexts([this.canvasActiveLayer, this.canvasSelectLayer, this.canvasGrid]);
    this.hexagons = this.regionsWithData(this.getCircleHexagonsList(this.midPoint, this.circleRadius));
    this.buildGrid();
  }

  globalHexagons(): void {
    this.loading = true;
    this.circleRadius = HEXAGON_CONFIG.numberOfHexagonSide + 10;
    this.clearContexts([this.canvasActiveLayer, this.canvasSelectLayer, this.canvasGrid]);
    this.hexagons = this.regionsWithData(this.getCircleHexagonsList(this.midPoint, this.circleRadius));
    this.buildGrid();
  }

  private clearContexts(arrayOfLayers: HTMLCanvasElement[]): void {
    arrayOfLayers.forEach((layer: HTMLCanvasElement) => {
      const context: CanvasRenderingContext2D = layer?.getContext('2d');
      context?.clearRect(
        0,
        0,
        layer.width,
        layer.height
      );
    });
  }
}
