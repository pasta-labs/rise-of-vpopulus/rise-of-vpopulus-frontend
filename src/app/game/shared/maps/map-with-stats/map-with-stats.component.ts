import { Component, EventEmitter, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { MapService } from '../map.service';

@Component({
  selector: 'app-map-with-stats',
  templateUrl: './map-with-stats.component.html',
  styleUrls: ['./map-with-stats.component.scss']
})
export class MapWithStatsComponent implements OnInit {

  currentRegion;

  filterType;

  resources = [];
  selectedResource;

  resourceQualityColors = [
    { quality: 1, color: 'rgba(240, 252, 3, 1)', count: 0 },
    { quality: 2, color: 'rgba(252, 144, 3, 1)', count: 0 },
    { quality: 3, color: 'rgba(252, 28, 3, 1)', count: 0 },
  ];
  countriesColors = [];

  regionsData = [];

  updateRegionsEvent = new EventEmitter();

  constructor(
    private graphQlService: GraphQlService,
    private httpService: GenericHttpService,
    private mapService: MapService
  ) { }

  ngOnInit(): void {
    this.graphQlService.get(`game`, `{ entity { region { x y }}}`).subscribe((r: any) => {
      this.currentRegion = r.data.entity.region;
      this.filterType = "politics";
      this.onFilterTypeChange();
    });
  }

  onFilterTypeChange = () => {
    this.mapService.resetResourcesFilters.emit();
    this.updateRegionsEvent.emit([]);
    this.regionsData = [];

    if (this.filterType === 'politics') this.loadCountries();
    else if (this.filterType === 'resources') this.loadResources();
  }

  loadResources = () => this.httpService.get(`/api/dictionaries/raws`).subscribe(r => this.resources = r);
  loadResourceData = () => {
    this.mapService.resetResourcesFilters.emit();

    this.httpService.get(`/api/maps/statistics/resources?resourceId=${this.selectedResource.id}`).subscribe(r => {
      this.regionsData = r;
      this.resourceQualityColors.forEach(q => q.count = this.regionsData.filter(x => x.quality === q.quality).length);

      let regionsChanges = [];
      this.regionsData.forEach((item) => {
        let region = regionsChanges.find((x) => x.regionId === item.regionId);
        if (!region) regionsChanges.push({ regionId: item.regionId, x: item.x, y: item.y, background: "" });
        region = regionsChanges.find((x) => x.regionId === item.regionId);
        region.background = this.resourceQualityColors.find(x => x.quality === item.quality).color;
      });

      this.updateRegionsEvent.emit(regionsChanges);
    });
  }

  loadCountries = () => this.httpService.get(`/api/maps/statistics/countries`).subscribe(r => this.countriesColors = r);


}
