import { Component } from '@angular/core';
import { MapModalService } from './map-modal.service';
import { isEqual } from 'lodash-es';
import { distinctUntilChanged } from 'rxjs/operators';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss']
})
export class MapModalComponent {
  region$: any = this.mapModalService.currentRegion.pipe(distinctUntilChanged(isEqual));

  isCp = false;

  constructor(
    private mapModalService: MapModalService, private httpService: GenericHttpService, private messageService: NzMessageService, private router: Router
  ) { }

  attack(region): void {
    this.httpService
      .post(`/api/military/battles`, { regionId: region.id })
      .subscribe(
        (r) => {
          this.messageService.success(
            `You have attacked region ${region.name}!`
          );
          this.router.navigateByUrl(`/battle/${r}`);
        },
        (err) => {
          if (err.error.Message === 'NO_DIRECT_BORDER') {
            this.messageService.error('No direct border detected!');
          } else if (err.error.Message === 'NO_WAR') {
            this.messageService.error('You need to declare war first!');
          } else if (err.error.Message === 'COUNTRY_PROTECTION_IS_ENABLED') {
            this.messageService.error('Country protection is enabled!');
          } else {
            this.messageService.error(err.error.Message);
          }
        });
  }

  startCountry = () => this.router.navigateByUrl(`/countries/create`);
  

}
