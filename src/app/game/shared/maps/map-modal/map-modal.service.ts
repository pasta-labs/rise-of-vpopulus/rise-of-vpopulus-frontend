import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapModalService {
  currentRegion: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() { }

  setCurrentRegion(region): void{
    console.log('selected region', region)
    this.currentRegion.next(region);
  }

}
