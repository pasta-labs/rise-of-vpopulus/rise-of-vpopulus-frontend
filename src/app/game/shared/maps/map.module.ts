import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map/map.component';
import { MapCoreComponent } from './map-core/map-core.component';
import { MapEditorComponent } from './map-editor/map-editor.component';
import { MapHistoryComponent } from './map-history/map-history.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapModalComponent } from './map-modal/map-modal.component';
import { MapModalService } from './map-modal/map-modal.service';
import { MapWithStatsComponent } from './map-with-stats/map-with-stats.component';
import { MapSelectorComponent } from '../map-selector/map-selector.component';
import { LegacyMapComponent } from '../legacy-map/legacy-map.component';
import { LegacyMapEditorComponent } from '../legacy-map-editor/legacy-map-editor.component';
import { LegacyMapHistoryComponent } from '../legacy-map-history/legacy-map-history.component';

@NgModule({
  declarations: [
    MapSelectorComponent,
    LegacyMapComponent,
    LegacyMapEditorComponent,
    LegacyMapHistoryComponent,
    MapComponent,
    MapCoreComponent,
    MapEditorComponent,
    MapHistoryComponent,
    MapModalComponent,
    MapWithStatsComponent
  ],
  exports: [MapCoreComponent, MapComponent, MapSelectorComponent],
  imports: [CommonModule, SharedModule],
  providers: [MapModalService]
})
export class MapModule { }
