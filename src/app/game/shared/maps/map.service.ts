import { EventEmitter, Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root',
})
export class MapService {
    resetResourcesFilters = new EventEmitter();
    resetCountries = new EventEmitter();

}