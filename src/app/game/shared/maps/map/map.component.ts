import {
  Component, ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { removeConnectedCountryBorders } from '../map-core/map-core.utils';
import { MapModalComponent } from '../map-modal/map-modal.component';
import { ModalService } from '../../../../shared/components/modal/modal.service';
import { MapModalService } from '../map-modal/map-modal.service';
import { HexagonPoint } from '../map-core/map-core.interface';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

import { cloneDeep } from 'lodash-es'

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {

  @ViewChild('mapComponentWrapper') mapComponentWrapper: ElementRef;
  region;

  mapData;

  @Input() showOptions = true;
  @Input() disableRegions;
  @Input() mapHeight;
  @Input() enabledRegions;

  @Output() regionSelected = new EventEmitter();

  updateRegionsEvent = new EventEmitter();
  @Input() incomingUpdateRegionsEvent = new EventEmitter();
  private isModalOpened: boolean;

  @Input() midPoint: HexagonPoint = { x: 0, y: 13 };
  @Input() circleRadius = 2;

  @Input() isViewOnly = false;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private router: Router,
    private modalService: ModalService,
    private mapModalService: MapModalService,
    private graphQlService: GraphQlService
  ) {
  }

  ngOnInit(): void {
    if (!this.midPoint) this.midPoint = { x: 0, y: 0 };
    this.loadData();
    this.incomingUpdateRegionsEvent.subscribe((r) => this.updateRegionsEvent.emit(r))
  }

  private handleModal(position = undefined): void {
    if (this.isModalOpened) {
      return;
    }
    this.isModalOpened = true;
    this.modalService.openModal(MapModalComponent, { position: position ? position : this.getModalPosition() }).subscribe(() => this.isModalOpened = false);
  }

  private getModalPosition(): { top: number, left: number } {
    const mapPosition = this.mapComponentWrapper.nativeElement.getBoundingClientRect();

    return { top: mapPosition.top + 20, left: mapPosition.left + 150 };
  }

  loadData(): void {
    this.httpService.get(`/api/maps`).subscribe((r) => {
      this.mapData = r;

      let regionsChanges = [];
      this.mapData.forEach((item) => {
        let region = regionsChanges.find((x) => x.regionId === item.regionId);
        if (!region) {
          regionsChanges.push({
            regionId: item.regionId,
            position: `${item.x}, ${item.y}`,
            x: +item.x,
            y: +item.y,
          });
        }
        region = regionsChanges.find((x) => x.regionId === item.regionId);
        if (item.color) {
          region[`borders`] = [
            ...new Array(6).fill({
              color: item.color,
              width: 3,
            }),
          ];
        }

        if (item.battle) {
          region.gradient = { color: `red`, lineWidth: 3 };
        }
      });
      regionsChanges = removeConnectedCountryBorders(regionsChanges);

      this.updateRegionsEvent.emit(regionsChanges);
    });
  }

  selectRegion(region): void {
    if (this.isViewOnly === true || !region) return;
    this.graphQlService.get(`game`, `{ region(x: ${region.x}, y: ${region.y}) { id name x y countryId country { id name } canAttack canSettle resources { symbol value quality } } }`).subscribe((r: any) => {
      this.region = cloneDeep(r.data.region);

      var mapData = this.mapData.find(x => x.regionId === this.region.id);
      if (mapData) {
        if (mapData.battle) this.region.battle = mapData.battle;
      }
      this.mapModalService.setCurrentRegion(this.region);
      this.handleModal({ top: region.clientY + 40, left: region.clientX - 150 });

      this.region.showOptions = this.showOptions;
      this.regionSelected.emit(this.region);
    })
  }
}
