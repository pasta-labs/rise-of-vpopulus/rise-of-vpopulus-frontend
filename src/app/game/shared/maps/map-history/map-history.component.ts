import { Component, EventEmitter, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { head, take } from 'lodash';
import { removeConnectedCountryBorders } from '../map-core/map-core.utils';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { MapService } from '../map.service';

@Component({
  selector: 'app-map-history',
  templateUrl: './map-history.component.html',
  styleUrls: ['./map-history.component.scss'],
})
export class MapHistoryComponent implements OnInit {
  isLoading = false;

  list = [];
  actions = [];

  currentDateStep = 0;

  autoPlayOn = false;

  updateRegionsEvent = new EventEmitter();

  constructor(private httpService: GenericHttpService, private mapService: MapService) { }

  ngOnInit(): void {
    this.isLoading = true;

    this.httpService.get(`/api/maps/history-timeline`).subscribe((r) => {
      this.list = r;
      console.log('list', this.list.map(x => ({ ...x, dateNew: Date.parse(x.date) })))
      this.selectDate(head(this.list).date);
      this.isLoading = false;
      this.autoPlay();
    });
  }

  selectDate(date, forceClear = false) {
    if (forceClear === true) {
      this.autoPlayOn = false;
      this.mapService.resetCountries.emit();
    }

    var item = this.list.find((x) => x.date === date);
    this.actions = item.actions;
    this.currentDateStep = this.list.indexOf(item);

    var regionsChanges = [];

    for (let i = 0; i <= this.currentDateStep; i++) {
      this.list[i].actions.forEach((action) => {
        var region = regionsChanges.find((x) => x.regionId === action.regionId);
        if (!region) {
          var position = action.regionName.split(',').map((x) => x.trim());
          regionsChanges.push({
            regionId: action.regionId,
            position: action.regionName,
            x: +position[0],
            y: +position[1],
          });
        }
        region = regionsChanges.find((x) => x.regionId === action.regionId);
        if (action.countryColor) region.countryColor = action.countryColor;

        if (action.action === 'CREATE_COUNTRY')
          region[`borders`] = [
            ...new Array(6).fill({
              color: action.countryColor,
              width: 3,
            }),
          ];

        if (action.action === 'BATTLE_STARTED') {
          region['gradient'] = { color: `red`, lineWidth: 3 };
        }

        if (action.action === 'BATTLE_ENDED') {
          region['gradient'] = null;
          region[`borders`] = [
            ...new Array(6).fill({
              color: action.countryColor,
              width: 3,
            }),
          ];
        }
      });
    }
    regionsChanges = removeConnectedCountryBorders(regionsChanges);

    this.updateRegionsEvent.emit(regionsChanges);
  }

  autoPlay() {
    setTimeout(() => {
      if (this.autoPlayOn === true) {
        var nextIndex = this.currentDateStep + 1;
        if (nextIndex >= this.list.length) { nextIndex = 0; this.mapService.resetCountries.emit(); }
        this.selectDate(this.list[nextIndex].date);
      }
      this.autoPlay();
    }, 2000);
  }

  startAutoPlay() {
    this.autoPlayOn = true;
  }

  pauseAutoPlay() {
    this.autoPlayOn = false;
  }

  getActionColor(type) {
    if (type === 'CREATE_COUNTRY') return 'blue';
    if (type === 'BATTLE_STARTED') return 'red';
    if (type === 'BATTLE_ENDED') return 'green';
  }
}
