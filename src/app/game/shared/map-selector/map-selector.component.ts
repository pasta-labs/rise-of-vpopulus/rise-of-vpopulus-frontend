import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-map-selector',
  templateUrl: './map-selector.component.html',
  styleUrls: ['./map-selector.component.scss']
})
export class MapSelectorComponent implements OnInit {

  @Input() mapHeight;
  @Input() circleRadius;
  @Input() currentRegionSelected;
  @Input() countryId;
  @Input() showRegionDetails;
  @Input() showFilters = false;

  @Output() regionSelected = new EventEmitter();

  gameVersion = undefined;

  constructor(
    private activateRouter: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activateRouter.url.subscribe(r => {
      console.log('url change?', r)
      if (r.length > 0 && r[0].path === "map") {
        this.showFilters = true;
        this.showRegionDetails = true;
      }
    })

    if (!this.mapHeight) this.mapHeight = '700px';

    this.gameVersion = JSON.parse(localStorage.getItem(`gameConfig`)).type;
  }

  onRegionSelected = (data) => this.regionSelected.emit(data);

}
