export function createStripedPattern(lineWidth, spacing, slope, color, backgroundColor) {
  const can = document.createElement('canvas');
  const len = Math.hypot(1, slope);

  const w = can.width = 1 / len + spacing + 0.5 | 0; // round to nearest pixel
  const h = can.height = slope / len + spacing * slope + 0.5 | 0;

  const ctx = can.getContext('2d');
  ctx.strokeStyle = color;
  ctx.lineWidth = lineWidth;
  ctx.beginPath();

  // Line through top left and bottom right corners
  ctx.moveTo(0, 0);
  ctx.lineTo(w, h);
  // Line through top right corner to add missing pixels
  ctx.moveTo(0, -h);
  ctx.lineTo(w * 2, h);
  // Line through bottom left corner to add missing pixels
  ctx.moveTo(-w, 0);
  ctx.lineTo(w, h * 2);

  ctx.fillStyle = backgroundColor;
  ctx.fillRect(0, 0, w, h)
  ctx.stroke();
  return ctx.createPattern(can, 'repeat');
};
