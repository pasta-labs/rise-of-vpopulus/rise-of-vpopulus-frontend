import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { environment } from 'src/environments/environment';
import Map from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import { OSM } from 'ol/source';
import View from 'ol/View';
import { toLonLat, transform } from 'ol/proj';
import { Vector } from 'ol/layer';
import { Vector as VSource } from 'ol/source';
import { Feature, MapBrowserEvent, Overlay } from 'ol';
import { Point, Polygon } from 'ol/geom';
import { Fill, Icon, Stroke, Style } from 'ol/style';

import { uniqueId, sum, max, divide, groupBy, keys } from 'lodash-es';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { createStripedPattern } from './legacy-map.utils';
import { toStringHDMS } from 'ol/coordinate';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-legacy-map',
  templateUrl: './legacy-map.component.html',
  styleUrls: ['./legacy-map.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LegacyMapComponent implements OnInit {
  hostUrl = environment.apiUrl;

  @Input() mapHeight = "700px";
  @Input() countryId = undefined;
  @Input() showTitle = true;
  @Input() showFilters = false;

  @Output() onRegionSelected = new EventEmitter();

  regions: any[] = [];
  isLoading = false;

  entityRegion = { capitalCoords: [0, 0], id: 0 };

  selectedRegion = undefined;

  @Output() mapLoaded = new EventEmitter();
  public map!: Map
  lastSelectedPathId = undefined;

  @Input() showRegionDetails = true;
  mapId;

  pattern = undefined;

  currentFilter = "default";
  filtersColors = ['#D1D1D1', '#D6EFD8', '#80AF81', '#508D4E'];
  filterResources = [];

  container;
  closer;
  overlay;
  showDetailsLoading = false;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private graphQlService: GraphQlService,
    private translateService: TranslateService,

  ) { }

  ngOnInit() {
    this.mapId = `map_${uniqueId()}`;
    this.loadRegions();
  }

  ngAfterViewInit() {
    this.container = document.getElementById('popup');
    this.closer = document.getElementById('popup-closer');

    this.closer.onclick = () => {
      this.overlay.setPosition(undefined);
      this.closer.blur();
      return false;
    };

    this.overlay = new Overlay({
      element: this.container,
      autoPan: {
        animation: {
          duration: 250,
        },
      },
    });

    this.map = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      overlays: [this.overlay],
      target: this.mapId,
      view: new View({
        center: [0, 0],
        zoom: 4,
        projection: "EPSG:900913"
      }),
    });

    this.map.on('click', e => this.mouseOver(e));
  }

  loadRegions() {
    this.isLoading = true;
    var query = `/api/maps/legacy`;
    if (this.countryId) query += `?countryId=${this.countryId}`;

    this.httpService.get(query).subscribe(r => {
      // = = = Add regions
      var vectorSource = new VSource();
      var regions = r.regions.filter(x => x.mapPath);
      regions.forEach(region => {
        var paths = [];
        region.mapPath.points.forEach(coords => {
          paths.push(transform([coords.lng, coords.lat], 'EPSG:4326', 'EPSG:3857'));
        });

        var fillColor = region.mapColor[0];
        if (region.battle) fillColor = createStripedPattern(4, 12, 3, "#FF0000", region.mapColor[0]);

        var feature = new Feature({
          geometry: new Polygon([paths]),
          originalColor: fillColor
        });

        feature.setStyle(new Style({
          stroke: new Stroke({
            color: "#d6a40f", width: 1
          }),
          fill: new Fill({
            color: fillColor
          })
        }))
        feature.setId(region.id);
        vectorSource.addFeature(feature);
      });
      var vectorLayer = new Vector({
        className: "map_region",
        source: vectorSource
      });
      this.map.addLayer(vectorLayer);
      this.regions = regions;

      // = = = If user is logged in and has its location
      if (r.entityRegion) {
        this.entityRegion = r.entityRegion;
        this.map.getView().setCenter(transform([this.entityRegion.capitalCoords[1], this.entityRegion.capitalCoords[0]], 'EPSG:4326', 'EPSG:3857'));

        const iconFeature = new Feature({
          geometry: new Point(transform([this.entityRegion.capitalCoords[1], this.entityRegion.capitalCoords[0]], 'EPSG:4326', 'EPSG:3857')),
          name: this.translateService.instant('Military.Army.currentLocation'),
        });

        var vectorLayer2 = new Vector({
          source: new VSource({
            features: [iconFeature]
          }),
          style: new Style({
            image: new Icon({
              anchorXUnits: 'pixels',
              anchorYUnits: 'pixels',
              src: './assets/img/panda-map.svg'
            })
          })
        })

        this.map.addLayer(vectorLayer2);

      }

      this.isLoading = false;
      this.mapLoaded.emit(this.map);
    }, e => {
      console.log('e', e);
    })
  }

  mouseOver = async (event: MapBrowserEvent<any>) => {
    const lastSelectedRegionId = this.lastSelectedPathId;
  
    this.resetSelection();
  
    var pixel = event.map.getEventPixel(event.originalEvent);
    if (event.map.hasFeatureAtPixel(pixel) === false) {
  
      this.selectedRegion = undefined;
      this.overlay.setPosition(undefined);
    } else {
      event.map.forEachFeatureAtPixel(pixel, async (feature: Feature) => {
        if (!feature.getId()) {
          this.overlay.setPosition(undefined);
          return;
        }
        if (lastSelectedRegionId == feature.getId()) {
          this.lastSelectedPathId = feature.getId();
          this.overlay.setPosition(event.coordinate);
          return;
        }
        this.resetSelection();
        this.overlay.setPosition(event.coordinate);
  
        this.lastSelectedPathId = feature.getId();
        var region = this.regions.find(x => x.id === feature.getId());
        this.selectedRegion = region;
        this.selectedRegion.Value = feature.get(`Value`);
  
        this.showDetailsLoading = true;
        document.getElementById('popup-title').innerHTML = `<a href="/region/${region.id}">${region.name}</a>`;
  
        const regionData: any = (await firstValueFrom(this.graphQlService.get(`game`, `{ region(id: "${region.id}") { country { id name flagLocation } coreCountry { id name flagLocation } isWasteland
          battle { id attackerCountry { id name flagLocation } isCivilWar isResistance }
          } }`)))
          .data['region'];
        console.log('region data fetched', regionData);
  
        // Tradução das mensagens
        const capitalName = this.translateService.instant('Common.Maps.capitalName', { capitalName: region.capitalName });
        const wastelandText = regionData.isWasteland ? `<h5>${this.translateService.instant('Common.Maps.wasteland')}</h5>` : '';
        const currentOwner = regionData.country ? `<div style="font-size: 13px;">
          ${this.translateService.instant('Common.Maps.currentOwner')}: <img src="${regionData.country.flagLocation}">
          <a href="/country/${regionData.country.id}">${regionData.country.name}</a>
        </div>` : '';
        const coreOwner = regionData.coreCountry ? `<div style="font-size: 13px;">
          ${this.translateService.instant('Common.Maps.coreOwner')}: <img src="${regionData.coreCountry.flagLocation}">
          <a href="/country/${regionData.coreCountry.id}">${regionData.coreCountry.name}</a>
        </div>` : '';
        const battleInfo = regionData.battle ? `<div style="font-size: 13px;">
          ${this.translateService.instant('Common.Maps.attackedBy')}: <img src="${regionData.battle.attackerCountry.flagLocation}">
          <a href="/country/${regionData.battle.attackerCountry.id}">${regionData.battle.attackerCountry.name}</a>
          ${regionData.battle.isCivilWar ? ` ${this.translateService.instant('Common.Maps.civilWar')}` : ''}
          ${regionData.battle.isResistance ? ` ${this.translateService.instant('Common.Maps.resistance')}` : ''}
  
          <a href="/battle/${regionData.battle.id}">
            <button nz-button nzDanger nzShape="round">${this.translateService.instant('Common.Maps.Battle')}</button>
          </a>
        </div>` : '';
  
        const filterContent = {
          'population': this.translateService.instant('Common.Maps.population', { value: feature.get('Value') }),
          'ds': this.translateService.instant('Common.Maps.defenseSystem', { value: feature.get('Value') }),
          'hospital': this.translateService.instant('Common.Maps.hospitalQ', { value: feature.get('Value') }),
          'company': this.translateService.instant('Common.Maps.companiesQ', { value: feature.get('Value') }),
          'raw': this.translateService.instant('Common.Maps.highestQuality', { value: feature.get('Value') })
        };
  
        const filterText = this.currentFilter && feature.get('Value') ? `<h4>${filterContent[this.currentFilter]}</h4>` : '';
  
        document.getElementById('popup-content').innerHTML = `<div>
          <h5>${capitalName}</h5>
          ${wastelandText}
          ${currentOwner}
          ${coreOwner}
          ${battleInfo}
          <hr />
          ${filterText}
        </div>`;
  
        this.showDetailsLoading = false;
  
        this.onRegionSelected.emit(region);
        var s = (feature.getStyle() as Style);
        s.setFill(new Fill({ color: 'grey' }));
        feature.setStyle(s);
      });
    }
  }
  

  resetSelection = () => {
    if (this.lastSelectedPathId) {
      var f = (this.map.getAllLayers()[1].getSource() as VSource).getFeatureById(this.lastSelectedPathId);
      if (f) {
        const colorToSet = this.currentFilter === "default" ? f.get('originalColor') : f.get('filterColor');
        (f.getStyle() as Style).setFill(new Fill({ color: colorToSet }))
      }
      this.lastSelectedPathId = undefined;
    }
  }

  changeFilter = async (e, val = undefined) => {
    this.overlay.setPosition(undefined);
    this.setAllRegionsToNoColor();
    this.currentFilter = e;

    this.isLoading = true;
    if (this.currentFilter === "default") {
      // = = = DEFAULT = = = //
      (this.map.getAllLayers()[1].getSource() as VSource).getFeatures().forEach(f => f.setStyle(new Style({
        stroke: new Stroke({ color: "#d6a40f", width: 1 }),
        fill: new Fill({ color: f.get('originalColor') })
      })))
    } else if (this.currentFilter === "population") {
      // = = = POP = = = //
      const response = await firstValueFrom(this.graphQlService.get(`game`, `{ mapLegacyStatsByPopulation }`));
      var list = JSON.parse(response.data['mapLegacyStatsByPopulation']);
      var total = sum(list.map(x => x.Count), x => x);
      list.forEach(i => i.Percetage = ((i.Count / total) * 100));

      var highestValue = max(list.map(x => x.Percetage));
      var point = divide(100, highestValue);

      list.forEach(i => {
        i.Percetage = i.Percetage * point;
        i.Color = this.lightenDarkenColor('#008000', 100 - i.Percetage);
        this.setRegionColor(i.Id, i.Color);
        this.setRegionValue(i.Id, `Value`, i.Count);
      });
    } else if (this.currentFilter === "hospital") {
      // = = = Hospitals = = = //
      const response = await firstValueFrom(this.graphQlService.get(`game`, `{ mapLegacyStatsByHospital }`));
      var list = JSON.parse(response.data['mapLegacyStatsByHospital']);
      list.forEach(i => {
        this.setRegionColor(i.Id, this.filtersColors[i.Quality])
        this.setRegionValue(i.Id, `Value`, i.Quality);
      });
    } else if (this.currentFilter === "ds") {
      // = = = DS = = = //
      const response = await firstValueFrom(this.graphQlService.get(`game`, `{ mapLegacyStatsByDefenseSystems }`));
      var list = JSON.parse(response.data['mapLegacyStatsByDefenseSystems']);
      list.forEach(i => {
        this.setRegionColor(i.Id, this.filtersColors[i.Quality]);
        this.setRegionValue(i.Id, `Value`, i.Quality);
      });
    } else if (this.currentFilter === "company") {
      // = = = COMPANIES = = = //
      const response = await firstValueFrom(this.graphQlService.get(`game`, `{ mapLegacyStatsByCompanies }`));
      var list = JSON.parse(response.data['mapLegacyStatsByCompanies']);
      var total = sum(list.map(x => x.Count), x => x);
      list.forEach(i => i.Percetage = ((i.Count / total) * 100));

      var highestValue = max(list.map(x => x.Percetage));
      var point = divide(100, highestValue);

      list.forEach(i => {
        i.Percetage = i.Percetage * point;
        i.Color = this.lightenDarkenColor('#008000', 100 - i.Percetage);
        this.setRegionColor(i.Id, i.Color);
        this.setRegionValue(i.Id, `Value`, i.Count);
      });
    } else if (this.currentFilter.includes('raw')) {
      // = = = RAWS = = = //
      const response = await firstValueFrom(this.graphQlService.get(`game`, `{ mapLegacyStatsByRaws${(val === "all" ? '' : `(rawId: "${val}")`)} }`));
      var list = JSON.parse(response.data['mapLegacyStatsByRaws']);
      list.forEach(i => {
        this.setRegionColor(i.RegionId, this.filtersColors[i.Quality]);
        this.setRegionValue(i.RegionId, `Value`, i.Quality);
      });

      if (val === "all")
        this.filterResources = keys(groupBy(list, x => x.ItemId)).map(key => ({ ...(list.find(x => x.ItemId === key)) }));
    }
    this.isLoading = false;
  }

  setAllRegionsToNoColor = () => {
    (this.map.getAllLayers()[1].getSource() as VSource).getFeatures().forEach(f => {
      f.setStyle(new Style({
        stroke: new Stroke({ color: "#d6a40f", width: 1 }),
        fill: new Fill({ color: '#D1D1D1' })
      }))
      f.set('filterColor', '#D1D1D1');
      f.set('Value', undefined);
    })
  }

  setRegionColor = (id, color) => {
    var item = (this.map.getAllLayers()[1].getSource() as VSource).getFeatureById(id);
    if (item) {
      item.setStyle(new Style({
        stroke: new Stroke({ color: "#d6a40f", width: 1 }),
        fill: new Fill({ color: color })
      }));
      item.set('filterColor', color);
    }
  }

  setRegionValue = (id, key, value) => {
    if (!id) return;
    var feature = (this.map.getAllLayers()[1].getSource() as VSource).getFeatureById(id);
    if (feature) feature.set(key, value);
  }

  lightenDarkenColor(col, amt) {
    var usePound = false;
    if (col[0] == "#") {
      col = col.slice(1);
      usePound = true;
    }

    var num = parseInt(col, 16);
    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;
    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;
    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + String("000000" + (g | (b << 8) | (r << 16)).toString(16)).slice(-6);
  }

}
