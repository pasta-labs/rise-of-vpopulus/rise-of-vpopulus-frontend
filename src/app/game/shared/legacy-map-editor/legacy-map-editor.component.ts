import { Component, ViewEncapsulation } from '@angular/core';
import Map from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import { ImageStatic, OSM } from 'ol/source';
import View from 'ol/View';
import { fromLonLat, Projection, transform } from 'ol/proj';
import { Vector } from 'ol/layer';
import { Vector as VSource } from 'ol/source';
import { Feature, MapBrowserEvent } from 'ol';
import {
  Draw,
  Modify,
  Select,
  Snap,
  defaults as defaultInteractions,
} from 'ol/interaction';
import { Geometry, Point, Polygon } from 'ol/geom';
import { Fill, Icon, Stroke, Style } from 'ol/style';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { ModifyEvent } from 'ol/interaction/Modify';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StyleFunction } from 'ol/style/Style';
import { groupBy, keys, cloneDeep, orderBy } from 'lodash-es';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-legacy-map-editor',
  templateUrl: './legacy-map-editor.component.html',
  styleUrl: './legacy-map-editor.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class LegacyMapEditorComponent {
  isLoading = false;

  mapHeight = "800px";

  public map!: Map;

  regions: any[] = [];

  countries = [];

  mode = 'view';

  createRegionForm: FormGroup;

  currentSelectedRegion = undefined;

  select: Select;
  modify: Modify;
  modifyData = undefined;
  deleteId = undefined;
  draw: Draw;
  snap: Snap;
  vectorSource: VSource;

  borders = undefined;
  addingNewBorder = false;

  sequentialColors = ['#6495ED', '#4682B4', '#5F9EA0', '#ADD8E6', '#1E90FF', '#00BFFF', '#4B0082', '#8A2BE2', '#4169E1', '#FFD700', '#7CFC00', '#228B22'];
  qualityColors = ['#D1D1D1', '#E5FFCC', '#80FF00', '#336600'];

  availableResources = [];
  resources = [];
  resourcesMapFilterValue = undefined;

  constructor(
    private httpService: GenericHttpService,
    private graphQlService: GraphQlService,
    private messageService: NzMessageService,
    private translateService: TranslateService,
  ) {
    this.createRegionForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      countryId: new FormControl(null, [Validators.required]),
      coreCountryId: new FormControl(null, [Validators.required]),
      capitalCoords: new FormControl(null, [Validators.required]),
      capitalName: new FormControl(null, [Validators.required]),
      isWasteland: new FormControl(true),
      paths: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit() { }

  ngAfterViewInit() {
    this.map = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      target: 'legacy-map-editor-id',
      view: new View({
        center: [0, 0],
        zoom: 4,
        projection: "EPSG:900913"
      }),
      // interactions: defaultInteractions().extend([select, modify]),
    });

    this.vectorSource = new VSource();

    this.loadRegions();
  }

  loadRegions() {
    const layers = this.map.getAllLayers();

    this.vectorSource = new VSource();

    if (layers.length > 1) this.map.removeLayer(layers[1]);
    this.isLoading = true;
    this.httpService.get(`/api/maps/legacy`).subscribe(r => {
      // = = = Add regions
      console.log('regions', r.regions)
      var regions = r.regions.filter(x => x.mapPath);
      regions.forEach(region => {
        var paths = [];

        region.mapPath.points.forEach(coords => {
          paths.push(transform([coords.lng, coords.lat], 'EPSG:4326', 'EPSG:3857'));
        });

        var fillColor = region.mapColor[0];

        var feature = new Feature({
          geometry: new Polygon([paths]),
          originalColor: fillColor,
          noColor: '#D1D1D1'
        });

        feature.setStyle(new Style({
          stroke: new Stroke({
            color: "#d6a40f", width: 1
          }),
          fill: new Fill({
            color: fillColor
          })
        }))

        feature.setId(region.id);
        this.vectorSource.addFeature(feature);
      });
      var vectorLayer = new Vector({
        className: "map_region",
        source: this.vectorSource
      });
      this.map.addLayer(vectorLayer);
      this.regions = regions;
      this.isLoading = false;
    }, e => {
      console.log('e', e);
    })
  }

  clearRegionCoords = () => {
    this.createRegionForm.get(`paths`).setValue([]);
    if (this.modifyData && this.modifyData.feature)
      this.vectorSource.removeFeature(this.modifyData.feature);

    this.modifyData = undefined;
    this.startDrawing();
  }

  startDrawing = () => {
    this.draw = new Draw({
      source: this.vectorSource,
      type: 'Polygon',
    });

    this.draw.on('drawend', e => {
      this.modifyData = {
        id: e.feature.getId(),
        feature: e.feature
      }

      this.createRegionForm.get(`paths`).setValue((e.feature.getGeometry() as Polygon).getCoordinates()[0].map(c => transform(c, 'EPSG:3857', 'EPSG:4326')));
      this.modify = new Modify({
        source: new VSource({
          features: [e.feature]
        })
      });
      this.modify.on(`modifyend`, e => {
        this.modifyData.feature = e.features.getArray()[0];
        this.createRegionForm.get(`paths`).setValue((e.features.getArray()[0].getGeometry() as Polygon).getCoordinates()[0].map(c => transform(c, 'EPSG:3857', 'EPSG:4326')))
      });
      this.map.addInteraction(this.modify);
      this.map.addInteraction(this.snap);
    })

    this.map.addInteraction(this.draw);
    this.map.addInteraction(this.snap);
  }

  onModeSwitch = (type) => {
    this.map.removeInteraction(this.select);
    this.map.removeInteraction(this.modify);
    this.map.removeInteraction(this.draw);
    this.map.removeInteraction(this.snap);

    this.select = new Select({ style: new Style({ fill: new Fill({ color: "rgba(255, 255, 255, 0.2)" }), stroke: new Stroke({ color: "rgba(0, 176, 0, 1)", width: 3 }) }) });
    this.snap = new Snap({ source: this.vectorSource });

    if (['create', 'modify', 'borders', 'resources'].includes(type))
      this.graphQlService.get(`modpanel`, `{ countriesAll { id name flagLocation } }`).subscribe((r) => this.countries = r.data['countriesAll']);

    if (['view', 'create', 'modify'].includes(type))
      this.setAllRegionsToOriginalColor();

    switch (type) {
      case "create":
        this.startDrawing();
        break;
      case "modify":
        this.modify = new Modify({ features: this.select.getFeatures() });
        this.modify.on(`modifyend`, e => {
          this.modifyData = {
            id: e.features.getArray()[0].getId(),
            feature: e.features.getArray()[0]
          };
          this.createRegionForm.get(`paths`).setValue((e.features.getArray()[0].getGeometry() as Polygon).getCoordinates()[0].map(c => transform(c, 'EPSG:3857', 'EPSG:4326')))
        });

        this.select.on(`select`, e => {
          if (e.selected.length > 0) {
            var regionSelected = this.regions.find(x => x.id === e.selected[0].getId());
            console.log('region selected', regionSelected)
            this.createRegionForm.patchValue(regionSelected);
            this.createRegionForm.get(`countryId`).setValue(regionSelected.countryId);
            this.createRegionForm.get(`capitalCoords`).setValue(regionSelected.capitalCoords.join(', '))
            this.createRegionForm.get(`coreCountryId`).setValue(regionSelected.coreCountryId);
            this.createRegionForm.get(`paths`).setValue((e.selected[0].getGeometry() as Polygon).getCoordinates()[0].map(c => transform(c, 'EPSG:3857', 'EPSG:4326')));
            this.modifyData = {
              id: e.selected[0].getId(),
              feature: e.selected[0]
            }
          } else {
            this.createRegionForm.reset();
            this.modifyData = undefined;
            this.loadRegions();
          }
        })

        this.map.addInteraction(this.select);
        this.map.addInteraction(this.modify);
        this.map.addInteraction(this.snap);
        break;
      case "delete":
        this.map.addInteraction(this.select);
        this.select.on(`select`, e => this.deleteId = e.selected[0].getId())
        break;
      case "borders":
        // = = = Reset all regions to no color
        this.setAllRegionsToNoColor();

        this.select.on(`select`, e => {
          if (e.selected.length > 0) {
            if (!this.addingNewBorder) this.getRegionBorders(e.selected[0].getId());
            else this.addNewBorder(e.selected[0].getId());
          } else
            this.getRegionBorders(undefined);

        });
        this.map.addInteraction(this.select);
        break;
      case "resources":
        // = = = Reset all regions to no color
        this.setAllRegionsToNoColor();
        this.availableResources = [];

        this.graphQlService.get(`game`, `{ regionsResources { symbol value quality regionId itemId } }`).subscribe((r) => {
          this.resources = r.data['regionsResources'];
          keys(groupBy(this.resources, x => x.itemId))
            .forEach(k => {
              var i = this.resources.find(r => r.itemId === k);
              this.availableResources.push({ symbol: i.symbol, value: i.value, itemId: i.itemId, quality: 0 });
            });
          this.resourcesMapFilterValueChange(undefined);
        })

        this.select.on(`select`, e => {
          if (e.selected.length > 0) {
            this.currentSelectedRegion = this.regions.find(x => x.id === e.selected[0].getId());
            this.currentSelectedRegion.country = this.countries.find(x => x.id === this.currentSelectedRegion.countryId);
            this.currentSelectedRegion.resources = cloneDeep(this.availableResources);

            this.currentSelectedRegion.resources.forEach(r => {
              r.quality = this.resources.find(x => x.regionId === this.currentSelectedRegion.id && x.itemId === r.itemId)?.quality ?? 0;
            })
          }
        })
        this.map.addInteraction(this.select);
        break;
    }
  }

  getCurrentRegionResourceQuality = (itemId) => {
    var item = this.currentSelectedRegion.resources.find(x => x.itemId === itemId);
    if (item) return item.quality;
    return 0;
  }

  getRegionBorders = (id) => {
    // = = Clear previous list
    if (this.borders)
      this.borders.filter(x => x.id !== id).forEach(b => this.setRegionColor(b.id, '#D1D1D1'));
    if (this.currentSelectedRegion)
      this.setRegionColor(this.currentSelectedRegion.id, '#D1D1D1');
    this.borders = [];

    if (!id) return;

    this.isLoading = true;

    this.currentSelectedRegion = this.regions.find(x => x.id === id);
    this.currentSelectedRegion.country = this.countries.find(x => x.id === this.currentSelectedRegion.countryId);

    this.graphQlService.get(`modpanel`, `{ regionBorders(regionId: "${this.currentSelectedRegion.id}") { id name countryId } }`)
      .subscribe((r) => {
        if (r.errors) {
          this.graphQlService.displayErrors(r.errors);
          this.isLoading = false;
          return;
        }
        this.borders = r.data['regionBorders'];

        // = = = Set border region color
        this.borders.forEach((b, i) => {
          b.country = this.countries.find(x => x.id === b.countryId);
          b.color = this.sequentialColors[i];
          this.setRegionColor(b.id, b.color);
          this.setRegionStyle(this.currentSelectedRegion.id, this.select.getStyle())

          this.isLoading = false;
        });
        console.log('borders', this.borders);
      });
  }

  resourcesMapFilterValueChange = (e) => {
    this.resourcesMapFilterValue = e;
    this.setAllRegionsToNoColor();

    var features: Array<Feature<Geometry>> = (this.map.getAllLayers()[1].getSource() as VSource).getFeatures();

    var resources = this.resources;
    if (e) resources = resources.filter(x => x.itemId === e);
    resources = orderBy(resources, x => x.quality, 'asc');

    resources.forEach(r => {
      var region = features.find(x => x.getId() === r.regionId)
      if (region)
        region.setStyle(new Style({
          stroke: new Stroke({ color: "#d6a40f", width: 1 }),
          fill: new Fill({ color: this.qualityColors[r.quality] })
        }));
    })
  }

  addNewBorder = (id) => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($region1Id: Guid, $region2Id: Guid) { regionBorderCreate(region1Id: $region1Id, region2Id: $region2Id) }`,
      { region1Id: id, region2Id: this.currentSelectedRegion.id })
      .subscribe((r: any) => {
        if (r.errors) this.graphQlService.displayErrors(r.errors);
        else {
          this.messageService.success(this.translateService.instant('Common.Maps.regionLinkAdded'));
          this.getRegionBorders(this.currentSelectedRegion.id);
        }
        this.isLoading = false;
      });
  }

  removeBorderSubmit = (id) => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($region1Id: Guid, $region2Id: Guid) { regionBorderDelete(region1Id: $region1Id, region2Id: $region2Id) }`,
      { region1Id: id, region2Id: this.currentSelectedRegion.id })
      .subscribe((r: any) => {
        if (r.errors) this.graphQlService.displayErrors(r.errors);
        else {
          this.messageService.success(this.translateService.instant('Common.Maps.regionLinkRemoved'));
          this.getRegionBorders(this.currentSelectedRegion.id);
        }
        this.isLoading = false;
      });
  }

  setRegionColor = (id, color) => {
    (this.map.getAllLayers()[1].getSource() as VSource).getFeatureById(id).setStyle(new Style({
      stroke: new Stroke({ color: "#d6a40f", width: 1 }),
      fill: new Fill({ color: color })
    }))
  }

  setRegionStyle = (id, style) => {
    (this.map.getAllLayers()[1].getSource() as VSource).getFeatureById(id).setStyle(style)
  }

  setAllRegionsToNoColor = () => {
    (this.map.getAllLayers()[1].getSource() as VSource).getFeatures().forEach(f => f.setStyle(new Style({
      stroke: new Stroke({ color: "#d6a40f", width: 1 }),
      fill: new Fill({ color: '#D1D1D1' })
    })))
  }

  setAllRegionsToOriginalColor = () => {
    (this.map.getAllLayers()[1].getSource() as VSource).getFeatures().forEach(f => f.setStyle(new Style({
      stroke: new Stroke({ color: "#d6a40f", width: 1 }),
      fill: new Fill({ color: f.get(`originalColor`) })
    })))
  }

  createPathSubmit = () => {
    if (this.createRegionForm.status == "INVALID")
      return;

    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: MapLegacyRegionCreateCommand) { mapEditorPathCreate(data: $data) }`,
      {
        data: {
          ...this.createRegionForm.getRawValue(),
          paths: JSON.stringify(this.createRegionForm.get(`paths`).value)
        }
      })
      .subscribe((r: any) => {
        if (r.errors) this.graphQlService.displayErrors(r.errors);
        else {
          this.messageService.success(this.translateService.instant('Common.Maps.regionAdded'));
          this.createRegionForm.reset();
          this.onModeSwitch('view');
          this.loadRegions();
        }
        this.isLoading = false;
      });
  }

  updatePathSubmit = () => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: MapLegacyRegionUpdateCommand) { mapEditorPathUpdate(data: $data) }`,
      {
        data: {
          ...this.createRegionForm.getRawValue(),
          paths: JSON.stringify(this.createRegionForm.get(`paths`).value),
          id: this.modifyData.id,
        }
      })
      .subscribe((r: any) => {
        if (r.errors) this.graphQlService.displayErrors(r.errors);
        else {
          this.messageService.success(this.translateService.instant('Common.Maps.coordinatesUpdated'));
          this.modifyData = undefined;
        }
        this.isLoading = false;
      });
  }

  deletePathSubmit = () => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($regionId: Guid) { mapEditorPathDelete(regionId: $regionId) }`, { regionId: this.deleteId }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.success(this.translateService.instant('Common.Maps.regionDeleted'));
        this.deleteId = undefined;
        this.loadRegions();
      }
      this.isLoading = false;
    });
  }

  updateResourcesSubmit = () => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: RegionResourcesUpdateCommand) { regionResourcesUpdate(data: $data) }`,
      {
        data: {
          regionId: this.currentSelectedRegion.id,
          resources: JSON.stringify(this.currentSelectedRegion.resources.map(x => ({ ItemId: x.itemId, Quality: x.quality })))
        }
      })
      .subscribe((r: any) => {
        if (r.errors) this.graphQlService.displayErrors(r.errors);
        else {
          this.messageService.success(this.translateService.instant('Common.Maps.resourcesUpdated'));
          this.currentSelectedRegion.resources.forEach(r => {
            var item = this.resources.find(f => f.regionId === this.currentSelectedRegion.id && f.itemId === r.itemId);
            if (item) item.quality = r.quality;
          })
          this.resourcesMapFilterValueChange(this.resourcesMapFilterValue);
        }
        this.isLoading = false;
      });
  }

}
