import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { groupBy, last, keys } from 'lodash-es';
import { Fill } from 'ol/style';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { Vector as VSource } from 'ol/source';
import { createStripedPattern } from '../legacy-map/legacy-map.utils';

@Component({
  selector: 'app-legacy-map-history',
  templateUrl: './legacy-map-history.component.html',
  styleUrl: './legacy-map-history.component.scss',
  encapsulation: ViewEncapsulation.None
})
export class LegacyMapHistoryComponent implements OnInit {
  isLoading = false;

  history = [];
  defaultRegions = [];

  daysList = [];
  currentDay = 0;

  map;

  isAutoPlayOn = false;
  autoPlayInterval = undefined;

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `
      { mapLegacyWorldHistory {
        gameDay
        changes { newColor type
          region { id name }
          battle { id
            attackerCountry { id name flagLocation }
            defenderCountry { id name flagLocation }
            winnerId
      } } } }`).subscribe((r: any) => {
      var history = [];
      r.data.mapLegacyWorldHistory.forEach(h => {
        if (h.gameDay === -1) {
          this.defaultRegions = h.changes;
        } else if (h.gameDay >= 0) {
          history.push({
            position: 'right',
            day: h.gameDay,
            color: 'grey'
          })
          history.push({
            color: 'grey',
            position: 'left',
            day: h.gameDay,
            data: h.changes.map(c => ({
              ...c, winner: c.battle.attackerCountry.id === c.battle.winnerId ? c.battle.attackerCountry : c.battle.defenderCountry
            }))
          })
        }
      })
      this.daysList = r.data.mapLegacyWorldHistory.filter(x => x.gameDay >= 0).map(x => x.gameDay);
      this.history = history;
      console.log('history', this.history, this.daysList, this.map, this.defaultRegions);
      if (this.map) this.resetMap();
      this.isLoading = false;
    });
  }

  nextDay = () => {
    var nextDay = 0;
    var index = this.daysList.indexOf(this.currentDay);
    if (index == -1) nextDay = this.daysList[0];
    else nextDay = this.daysList[index + 1];

    console.log('next day', nextDay)
    if (nextDay) {
      this.currentDay = nextDay;
      this.selectDay();
      return true;
    } else
      return false;
  }

  selectDay = () => {
    console.log('show day X', this.currentDay);
    this.history.filter(x => x.day === this.currentDay).forEach(d => d.color = "blue");
    this.history.filter(x => x.day < this.currentDay).forEach(d => d.color = "green");
    this.history.filter(x => x.day > this.currentDay).forEach(d => d.color = "grey");

    this.resetMap();
    var listOfChanges = [];
    this.history.filter(x => x.day <= this.currentDay && x.data).forEach(d => {
      d.data.forEach(c => {
        listOfChanges.push({ regionId: c.region.id, newColor: c.newColor, newBattle: c.type === "BattleStart" })
      })
    })
    var listOfChangesGrouped = groupBy(listOfChanges, x => x.regionId);

    keys(listOfChangesGrouped).forEach(key => {
      var lastItem = last(listOfChangesGrouped[key]);
      if (lastItem.newBattle === true) this.setRegionColor(lastItem.regionId, createStripedPattern(4, 12, 3, "#FF0000", lastItem.newColor.split(':')[0]));
      else {
        if (lastItem.newColor) this.setRegionColor(lastItem.regionId, lastItem.newColor.split(':')[0]);
      }
    })
  }

  previousDay = () => {
    var nextDay = 0;
    var index = this.daysList.indexOf(this.currentDay);
    if (index == -1) nextDay = this.daysList[0];
    else nextDay = this.daysList[index - 1];

    if (nextDay) {
      this.currentDay = nextDay;
      this.selectDay();
    }
  }

  resetMap = () => {
    this.defaultRegions.forEach(r => this.setRegionColor(r.region.id, r.newColor.split(':')[0]))
  }

  setRegionColor = (id, color) => {
    var item = (this.map.getAllLayers()[1].getSource() as VSource).getFeatureById(id);
    if (item) {
      item.setStyle(new Style({
        stroke: new Stroke({ color: "#d6a40f", width: 1 }),
        fill: new Fill({ color: color })
      }));
      item.set('filterColor', color);
    }
  }


  startAutoPlay = () => {
    this.isAutoPlayOn = true;
    clearTimeout(this.autoPlayInterval);
    this.autoPlayInterval = setTimeout(() => {
      var r = this.nextDay();
      if (r) this.startAutoPlay();
      else this.pauseAutoPlay();
    }, 1000);
  }

  pauseAutoPlay = () => {
    clearTimeout(this.autoPlayInterval);
    this.isAutoPlayOn = false;
  }

}
