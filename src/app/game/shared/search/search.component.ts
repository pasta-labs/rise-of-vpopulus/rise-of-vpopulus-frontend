import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  isLoading = false;

  searchValue = '';

  list = [];
  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.searchValue = params['q'];

      this.loadData();
    });
  }

  loadData() {
    this.isLoading = true;
    this.list = [];
    forkJoin(
      this.httpService.get(`/api/entities/search?name=${this.searchValue}`),
      this.httpService.get(
        `/api/entities/citizens/search?name=${this.searchValue}`
      )
    ).subscribe(([entities, citizens]) => {
      this.list.push(...citizens);
      this.list.push(...entities);
      this.isLoading = false;
    });
  }
}
