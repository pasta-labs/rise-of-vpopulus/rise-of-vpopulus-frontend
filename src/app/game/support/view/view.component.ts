import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-support-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
})
export class SupportViewComponent implements OnInit {
  isLoading = false;

  ticket;

  constructor(
    private graphQlService: GraphQlService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.loadData(params['id']);
    });
  }

  loadData(id) {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ supportTicket(id: "${id}"){ id title description type { symbol } status { symbol } } }`)
      .subscribe((r) => {
        if (r.errors) {
          if (r.errors[0].extensions.data['CODE'] === "ISSUE_NOT_FOUND")
            this.router.navigateByUrl(`/support`);
          else {
            this.graphQlService.displayErrors(r.errors);
            return;
          }
        }

        this.ticket = r.data['supportTicket'];
        this.isLoading = false;
      });
  }

  loadComments() {
    // this.isLoadingComments = true;
    // this.httpService
    //   .getSimplan(
    //     `${environment.simplanUrl}/api/integrations/support/tickets/${this.ticketId}/comments`
    //   )
    //   .subscribe((r) => {
    //     this.comments = r;
    //     this.comments.forEach((comment) => {
    //       comment.author = comment.createdBy == null ? 'You' : 'Support team';
    //     });
    //     this.isLoadingComments = false;
    //     console.log(this.comments);
    //   });
  }

  postComment() {
    // this.isLoadingComments = true;
    // this.httpService
    //   .postSimplan(
    //     `${environment.simplanUrl}/api/integrations/support/tickets/comments`,
    //     { taskId: this.ticketId, description: this.commentValue }
    //   )
    //   .subscribe((r) => {
    //     this.commentValue = '';
    //     this.loadComments();
    //   });
  }
}
