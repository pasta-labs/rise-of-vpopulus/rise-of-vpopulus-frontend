import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/auth/auth-guard';
import { SupportCreateComponent } from './create/create.component';
import { SupportListComponent } from './list/list.component';
import { SupportViewComponent } from './view/view.component';

export const routes: Routes = [
  {
    path: 'support/create',
    component: SupportCreateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'support',
    component: SupportListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'support/:id',
    component: SupportViewComponent,
    canActivate: [AuthGuard],
  },
];
