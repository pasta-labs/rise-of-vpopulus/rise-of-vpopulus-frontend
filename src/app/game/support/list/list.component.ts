import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-support-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class SupportListComponent implements OnInit {
  isLoading = true;

  list = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ supportTickets { id title type { symbol } status { symbol } } }`)
      .subscribe((r) => {
        this.list = r.data['supportTickets'];
        this.isLoading = false;
      });
  }
}
