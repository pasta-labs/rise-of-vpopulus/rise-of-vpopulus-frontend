import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-view-comments',
  templateUrl: './view-comments.component.html',
  styleUrl: './view-comments.component.scss'
})
export class ViewCommentsComponent {
  isLoading = false;

  @Input() issueId;
  list = [];

  createForm: FormGroup;

  constructor(
    private graphQlService: GraphQlService,
    private messageService: NzMessageService,
    private router: Router
  ) {
    this.createForm = new FormGroup({
      issueId: new FormControl(null, [Validators.required]),
      content: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit() {
    this.createForm.get(`issueId`).setValue(this.issueId);
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.graphQlService.get(`game`, `{ supportTicketComments(issueId: "${this.issueId}"){ content createdOn createdBy } }`)
      .subscribe((r) => {
        if (r.errors) {
          if (r.errors[0].extensions.data['CODE'] === "ISSUE_NOT_FOUND")
            this.router.navigateByUrl(`/support`);
          else {
            this.graphQlService.displayErrors(r.errors);
            return;
          }
        }

        this.list = r.data['supportTicketComments'];
        this.isLoading = false;
      });
  }

  create = () => {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: SupportTicketCommentCreateCommand) { supportTicketCommentCreate(data: $data) }`, {
      data: this.createForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) {
        this.graphQlService.displayErrors(r.errors);
        this.isLoading = false;
      }
      else {
        this.messageService.success(`Comment created!`);
        this.createForm.get(`content`).setValue(undefined);
        this.loadData();
      }
    });
  }

}
