import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { SupportCreateComponent } from './create/create.component';
import { SupportListComponent } from './list/list.component';
import { SupportViewComponent } from './view/view.component';
import { ViewCommentsComponent } from './view-comments/view-comments.component';

@NgModule({
  declarations: [
    SupportCreateComponent,
    SupportListComponent,
    SupportViewComponent,
    ViewCommentsComponent
  ],
  imports: [CommonModule, SharedModule],
})
export class SupportModule {}
