import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-support-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class SupportCreateComponent implements OnInit {
  isLoading = false;

  types = undefined;

  createForm: FormGroup;

  constructor(
    private graphQlService: GraphQlService,
    private router: Router,
    private messageService: NzMessageService,
    private translateService: TranslateService,

  ) {
    this.createForm = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
      typeId: new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {
    this.graphQlService.get(`game`, `{ supportTypes { id symbol } }`)
      .subscribe(r => this.types = r.data['supportTypes']);
  }

  submit() {
    this.isLoading = true;
    this.graphQlService.post(`game`, `mutation mutate($data: SupportTicketCreateCommand) { supportTicketCreate(data: $data) }`, {
      data: this.createForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) {
        this.graphQlService.displayErrors(r.errors);
        this.isLoading = false;
      }
      else {
        this.messageService.success(this.translateService.instant('Common.Extra.Support.supportTicketCreated'));
        this.router.navigateByUrl(`/support/${r.data.supportTicketCreate}`);
      }
    });
  }
}
