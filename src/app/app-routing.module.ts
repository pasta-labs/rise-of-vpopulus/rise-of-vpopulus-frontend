import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/auth/auth-guard';

import { HomeComponent } from './game/home/home.component';
import { StoreComponent } from './game/store/store.component';
import { MapComponent } from './game/shared/maps/map/map.component';
import { SearchComponent } from './game/shared/search/search.component';

import { marketsRoutes } from './game/markets/markets-routing';
import { routes as companyRoutes } from './game/company/company-routing';
import { routes as countryRoutes } from './game/countries/country-routing';
import { routes as regionRoutes } from './game/region/region-routing';
import { routes as rankingRoutes } from './game/ranking/ranking-routing';
import { routes as warRoutes } from './game/war/war-routing';
import { routes as battleRoutes } from './game/battle/battle-routing';
import { routes as tradeRoutes } from './game/trade/trade-routing';
import { routes as supportRoutes } from './game/support/support-routing';
import { routes as articleRoutes } from './game/article/article-routing';
import { routes as newspaperRoutes } from './game/newspaper/newspaper-routing';
import { routes as electionsRoutes } from './game/elections/elections-routing';
import { routes as citizenRoutes } from './game/citizen/citizen-routing';
import { routes as entityRoutes } from './game/entity/entity-routing';
import { routes as organizationRoutes } from './game/organization/organization-routing';
import { routes as armyRoutes } from './game/army/army-routing';
import { routes as partyRoutes } from './game/party/party-routing';
import { routes as modPanelRoutes } from './mod-panel/mod-panel-routing';
import { routes as miniGamesRoutes } from './game/mini-games/mini-games-routing';
import { MapHistoryComponent } from './game/shared/maps/map-history/map-history.component';
import { CitizenReferralsComponent } from './game/citizen/referrals/referrals.component';
import { PpComponent } from './legal/pp/pp.component';
import { TosComponent } from './legal/tos/tos.component';
import { MapWithStatsComponent } from './game/shared/maps/map-with-stats/map-with-stats.component';
import { LegacyMapComponent } from './game/shared/legacy-map/legacy-map.component';
import { MapSelectorComponent } from './game/shared/map-selector/map-selector.component';

import { ChatAppComponent } from 'projects/chat/src/app/app.component';
import { routes as ChatAppRoutes } from 'projects/chat/src/app/app.routes';
import { AuthAppComponent } from 'projects/auth/src/app/app.component';
import { routes as LandingRoutes } from 'projects/auth/src/app/app.routes';
import { LegacyMapEditorComponent } from './game/shared/legacy-map-editor/legacy-map-editor.component';
import { LegacyMapHistoryComponent } from './game/shared/legacy-map-history/legacy-map-history.component';
import { AuthService } from './shared/services/auth.service';
import { OauthOutsideComponent } from './auth/oauth-outside/oauth-outside.component';



const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },


  ...entityRoutes,
  ...citizenRoutes,
  ...organizationRoutes,
  ...armyRoutes,
  ...partyRoutes,

  ...companyRoutes,
  ...marketsRoutes,
  ...tradeRoutes,

  ...countryRoutes,
  ...rankingRoutes,
  ...electionsRoutes,

  ...warRoutes,
  ...battleRoutes,

  ...regionRoutes,

  ...supportRoutes,

  ...articleRoutes,
  ...newspaperRoutes,

  ...modPanelRoutes,
  ...miniGamesRoutes,
  {
    path: 'chats',
    component: ChatAppComponent,
    children: ChatAppRoutes
  },
  {
    path: 'landing',
    component: AuthAppComponent,
    canActivate: [AuthService],
    children: LandingRoutes
  },
  {
    path: 'search/:q',
    component: SearchComponent,
    canActivate: [AuthGuard],
  },

  { path: 'map', component: MapSelectorComponent },
  { path: 'map/editor', component: LegacyMapEditorComponent },
  { path: 'map/history', component: LegacyMapHistoryComponent },
  { path: 'store', component: StoreComponent, canActivate: [AuthGuard] },
  { path: 'referral/:token', component: CitizenReferralsComponent },
  { path: 'oauth-outside', component: OauthOutsideComponent },

  { path: 'privacy-policy', component: PpComponent },
  { path: 'terms-of-service', component: TosComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
