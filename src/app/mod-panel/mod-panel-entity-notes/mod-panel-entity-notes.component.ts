import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-mod-panel-entity-notes',
  standalone: true,
  imports: [
    CommonModule, SharedModule
  ],
  templateUrl: './mod-panel-entity-notes.component.html',
  styleUrl: './mod-panel-entity-notes.component.scss'
})
export class ModPanelEntityNotesComponent {
  isLoading = false;

  @Input() entityId;
  @Input() entityType;

  list = [];

  createNewForm = undefined;

  constructor(
    private graphQlService: GraphQlService,
    private modalService: NzModalService,
    private messageService: NzMessageService
  ) { }

  ngOnInit() {
    this.load();
  }

  load = async () => {
    this.isLoading = true;
    const r = await firstValueFrom(this.graphQlService.get(`modpanel`, `{ modPanelEntityNotes(entityId: "${this.entityId}") { id content createdOn moderator { id name } } }`));
    this.list = r.data['modPanelEntityNotes'];
    console.log('list', this.list)
    this.isLoading = false;
  }

  createInit = () => {
    this.createNewForm = new FormGroup({
      entityId: new FormControl(this.entityId, [Validators.required]),
      entityType: new FormControl(this.entityType, [Validators.required]),
      content: new FormControl(null, [Validators.required]),
    })
  }

  create = () => {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: EntityNoteCreateCommand) { modPanelEntityNoteCreate(data: $data) }`, {
      data: this.createNewForm.getRawValue()
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.warning(`Note added!`);
        this.createNewForm = undefined;
        this.load();
      }
      this.isLoading = false;
    });
  }



}
