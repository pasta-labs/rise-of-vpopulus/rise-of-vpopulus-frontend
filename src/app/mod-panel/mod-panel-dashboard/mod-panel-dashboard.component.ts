import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-mod-panel-dashboard',
  templateUrl: './mod-panel-dashboard.component.html',
  styleUrl: './mod-panel-dashboard.component.scss'
})
export class ModPanelDashboardComponent implements OnInit {
  isLoading = false;

  currentIndex = 0;

  list = [];

  shareIpDetails = undefined;
  sharePasswordDetails = undefined;

  qrCode = undefined;

  verifyCode = undefined;

  isMod = false;

  constructor(
    private graphQlService: GraphQlService,
    private messageService: NzMessageService
  ) { }

  ngOnInit() {
    this.isMod = localStorage.getItem(`modPermissions`) !== null;
    this.onTabChange({ index: 0 })
  }

  verify = async () => {
    const r = await firstValueFrom(this.graphQlService.get(`modpanel`, `{ moderatorAuthenticatorVerify(code: "${this.verifyCode}") }`));
    if (r.errors) this.graphQlService.displayErrors(r.errors);
    else {
      localStorage.setItem(`modPermissions`, r.data['moderatorAuthenticatorVerify'] || "{}");
      this.isMod = true;
    }
  }

  reset2fa = async () => {
    const r = await firstValueFrom(this.graphQlService.post(`modpanel`, `mutation { moderatorRest2fa }`, {}));
    if (r.errors) this.graphQlService.displayErrors(r.errors);
    else {
      this.messageService.success(`New 2FA code has been sent!`);
    }
  }

  onTabChange = (v) => {
    this.list = [];

    this.currentIndex = v.index;
    if (this.currentIndex === 0) this.loadReports();
    else if (this.currentIndex === 1) this.loadSharedIp();
    else if (this.currentIndex === 2) this.loadSharedPasswords();
  }

  loadReports = async () => {
    this.isLoading = true;
    const r = await firstValueFrom(this.graphQlService.get(`modpanel`, `{ reportsEntities(status: "Pending") { id entity { id name type } reason status createdOn createdBy { id name } } }`));
    this.list = r.data['reportsEntities'];
    this.isLoading = false;
  }

  loadSharedIp = async () => {
    this.isLoading = true;
    const r = await firstValueFrom(this.graphQlService.get(`modpanel`, `{ moderatorCitizenIpSharing { ipAddress citizens { id name count lastAccessed } } }`));
    this.list = r.data['moderatorCitizenIpSharing'];
    this.isLoading = false;
  }

  loadSharedPasswords = async () => {
    this.isLoading = true;
    const r = await firstValueFrom(this.graphQlService.get(`modpanel`, `{  moderatorCitizenPasswordSharing { password citizens { id name lastAccessed } } }`));
    this.list = r.data['moderatorCitizenPasswordSharing'];
    this.isLoading = false;
  }

}
