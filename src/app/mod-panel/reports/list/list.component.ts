import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { head } from 'lodash';

@Component({
  selector: 'app-mp-reports-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class MpReportsListComponent implements OnInit {
  isLoading = false;

  tabs = [];

  constructor(private httpService: GenericHttpService) {}

  ngOnInit(): void {
    this.loadTabs();
  }

  loadTabs() {
    this.isLoading = true;
    this.httpService.get(`/api/mp/reports`).subscribe((r) => {
      this.tabs = r;
      this.isLoading = false;
    });
  }
}
