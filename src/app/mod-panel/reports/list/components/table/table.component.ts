import { Component, Input, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';

@Component({
  selector: 'app-mp-reports-list-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class MpReportsListTableComponent implements OnInit {
  isLoading = false;

  list = [];
  totalCount = 0;

  @Input() reportType;
  @Input() type;

  constructor(private httpService: GenericHttpService) {}

  ngOnInit(): void {
    if (this.reportType === 'pending') this.loadPending();
    else if (this.reportType === 'history') this.loadHistory();
  }

  loadPending() {
    this.isLoading = false;
    this.httpService
      .get(`/api/mp/reports/pending/${this.type.item1}`)
      .subscribe((r) => {
        this.list = r.list;
        this.totalCount = r.totalCount;
        this.isLoading = false;
      });
  }

  loadHistory() {
    this.isLoading = false;
    this.httpService
      .get(`/api/mp/reports/history/${this.type.item1}`)
      .subscribe((r) => {
        this.list = r.list;
        this.totalCount = r.totalCount;
        this.isLoading = false;
      });
  }

  resolve() {}
}
