import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { EntitySessionActionsComponent } from '../entity-session-actions/entity-session-actions.component';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-modpanel-entity-sessions',
  standalone: true,
  imports: [
    CommonModule, SharedModule, EntitySessionActionsComponent
  ],
  templateUrl: './entity-sessions.component.html',
  styleUrl: './entity-sessions.component.scss'
})
export class ModPanelEntitySessionsComponent implements OnInit {
  isLoading = false;

  @Input() citizenId;

  list = [];

  constructor(
    private graphQlService: GraphQlService,
    private modalService: NzModalService
  ) { }

  ngOnInit() {
    this.load();
  }

  load = async () => {
    this.isLoading = true;
    const r = await firstValueFrom(this.graphQlService.get(`modpanel`, `{ modPanelCitizenSessions(citizenId: "${this.citizenId}") { id ip userAgent actionsCount createdOn } }`));
    this.list = r.data['modPanelCitizenSessions'];
    console.log('list', this.list)
    this.isLoading = false;
  }

  showActions = (sessionId) => {
    this.modalService.create({
      nzContent: EntitySessionActionsComponent,
      nzTitle: `Mod access`,
      nzFooter: null,
      nzWidth: '90%',
      nzMaskClosable: false,
      nzData: {
        entityId: this.citizenId,
        sessionId: sessionId
      }
    });
  }
}
