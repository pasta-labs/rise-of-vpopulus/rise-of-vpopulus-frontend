import { CommonModule } from '@angular/common';
import { Component, inject, Input } from '@angular/core';
import { NZ_MODAL_DATA } from 'ng-zorro-antd/modal';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-entity-session-actions',
  standalone: true,
  imports: [
    CommonModule, SharedModule
  ],
  templateUrl: './entity-session-actions.component.html',
  styleUrl: './entity-session-actions.component.scss'
})
export class EntitySessionActionsComponent {
  isLoading = false;

  list = [];

  @Input() entityId;
  @Input() sessionId = null;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit(): void {
    if(this.nzModalData) {
      if(this.nzModalData.entity) {
        this.entityId = this.nzModalData.entity.id;
      } else {
        this.entityId = this.nzModalData.entityId;
        this.sessionId = this.nzModalData.sessionId;
      }
    }

    this.isLoading = true;
    this.graphQlService.get(`modpanel`, `{ entitySessionActions(entityId: "${this.entityId}", sessionId: ${this.sessionId ? `"${this.sessionId}"` : null}) { id entity { id name type } citizenId agent ipAddress action payload createdOn } }`)
      .subscribe((r) => {
        this.list = r.data['entitySessionActions'];
        this.isLoading = false;
      });
  }
}
