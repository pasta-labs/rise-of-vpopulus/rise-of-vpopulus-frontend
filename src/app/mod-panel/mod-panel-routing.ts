import { Routes } from '@angular/router';
import { AuthGuard } from '../shared/auth/auth-guard';
import { ModeratorListComponent } from './moderators/list/list.component';
import { MpReportsListComponent } from './reports/list/list.component';
import { ModPanelDashboardComponent } from './mod-panel-dashboard/mod-panel-dashboard.component';
import { AuthGuardMod } from '../shared/auth/auth-guard-mod';

export const routes: Routes = [
  {
    path: 'mp/moderators',
    component: ModeratorListComponent,
    canActivate: [AuthGuard, AuthGuardMod],
  },
  {
    path: 'mp',
    component: ModPanelDashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mp/reports',
    component: MpReportsListComponent,
    canActivate: [AuthGuard, AuthGuardMod],
  },
];
