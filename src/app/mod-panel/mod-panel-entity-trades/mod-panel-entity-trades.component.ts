import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-mod-panel-entity-trades',
  standalone: true,
  imports: [
    CommonModule, SharedModule
  ],
  templateUrl: './mod-panel-entity-trades.component.html',
  styleUrl: './mod-panel-entity-trades.component.scss'
})
export class ModPanelEntityTradesComponent {
  isLoading = false;

  @Input() entityId;

  list = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.load();
  }

  load = async () => {
    this.isLoading = true;
    const r = await firstValueFrom(this.graphQlService.get(`game`, `{ trades(entityId: "${this.entityId}") { id createdOn toAccepted fromAccepted isInstant from { id name type } to { id name type } } }`));
    this.list = r.data['trades'];
    console.log('list', this.list)
    this.isLoading = false;
  }
}
