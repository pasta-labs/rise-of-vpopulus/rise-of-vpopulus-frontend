import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModeratorCreateComponent } from './moderators/create/create.component';
import { ModeratorListComponent } from './moderators/list/list.component';
import { ModeratorLogsComponent } from './moderators/logs/logs.component';
import { ModeratorPermissionsComponent } from './moderators/permissions/permissions.component';
import { SharedModule } from '../shared/shared.module';
import { MpReportsListComponent } from './reports/list/list.component';
import { MpReportsListTableComponent } from './reports/list/components/table/table.component';
import { ModPanelDashboardComponent } from './mod-panel-dashboard/mod-panel-dashboard.component';

@NgModule({
  declarations: [
    ModeratorListComponent,
    ModeratorLogsComponent,
    ModeratorPermissionsComponent,
    ModeratorCreateComponent,
    MpReportsListComponent,
    MpReportsListTableComponent,
    ModPanelDashboardComponent,
  ],
  imports: [CommonModule, SharedModule],
  exports: []
})
export class ModPanelModule {}
