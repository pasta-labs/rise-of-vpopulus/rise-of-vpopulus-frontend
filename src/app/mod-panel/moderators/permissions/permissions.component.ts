import { Component, Input, OnInit, inject } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-mp-moderators-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss'],
})
export class ModeratorPermissionsComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  @Input() moderator;

  constructor(
    private messageService: NzMessageService,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.moderator = this.nzModalData.moderator;

    console.log('mod', this.moderator)

    if (!this.moderator.permissions) this.moderator.permissions = {};
    if (!this.moderator.permissions.Citizens)
      this.moderator.permissions.Citizens = {};
    if (!this.moderator.permissions.Currencies)
      this.moderator.permissions.Currencies = {};
    if (!this.moderator.permissions.Articles)
      this.moderator.permissions.Articles = {};
    if (!this.moderator.permissions.Entities)
      this.moderator.permissions.Entities = {};
    if (!this.moderator.permissions.Politics)
      this.moderator.permissions.Politics = {};
    if (!this.moderator.permissions.Inventory)
      this.moderator.permissions.Inventory = {};
    if (!this.moderator.permissions.Store)
      this.moderator.permissions.Store = {};
  }

  save() {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: ModeratorUpdatePermissionsCommand) { moderatorUpdatePermissions(data: $data) }`, {
      data: {
        moderatorId: this.moderator.id,
        permissions: JSON.stringify(this.moderator.permissions),
      }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else this.messageService.success(`Moderator updated!`);
      this.isLoading = false;
    });
  }
}
