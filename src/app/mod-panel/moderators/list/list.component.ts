import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { ModeratorCreateComponent } from '../create/create.component';
import { ModeratorLogsComponent } from '../logs/logs.component';
import { ModeratorPermissionsComponent } from '../permissions/permissions.component';
import { orderBy } from 'lodash';
import { GraphQlService } from 'src/app/shared/services/graphql.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-mp-moderators-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ModeratorListComponent implements OnInit {
  isLoading = true;

  list = [];

  constructor(
    private modalService: NzModalService,
    private graphQlService: GraphQlService,
    private messageService: NzMessageService
  ) { }

  ngOnInit() {
    this.loadList();
  }

  loadList() {
    this.isLoading = true;
    this.graphQlService.get(`modpanel`, `{ moderators { id name avatar permissions } }`)
      .subscribe((r) => {
        this.list = r.data['moderators']
          .map(x => ({ ...x, permissions: JSON.parse(x.permissions || "{}") }))
          .map((x) => {
            return {
              ...x,
              isActive:
                x.permissions.IsAdmin === true ||
                x.permissions.IsModerator === true,
            };
          });
        this.list = orderBy(
          this.list,
          ['isActive', 'name'],
          ['desc', 'asc']
        );
        this.isLoading = false;
      });
  }

  remove(id) {
    this.isLoading = true;
    this.graphQlService.post(`modpanel`, `mutation mutate($data: ModeratorRemoveCommand) { moderatorDelete(data: $data) }`, {
      data: { moderatorId: id }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.success(`Moderator has been disabled!`);
        this.loadList();
      }
      this.isLoading = false;
    });
  }

  add() {
    var modal = this.modalService.create({
      nzContent: ModeratorCreateComponent,
      nzTitle: 'Add new moderator',
      nzFooter: null,
      nzWidth: '60%',
    });
    modal.afterClose.subscribe(() => this.loadList());
  }

  permissions(moderator) {
    var modal = this.modalService.create({
      nzContent: ModeratorPermissionsComponent,
      nzTitle: `Moderator ${moderator.name} - Permissions`,
      nzFooter: null,
      nzWidth: '80%',
      nzData: {
        moderator: moderator,
      },
    });
    modal.afterClose.subscribe(() => this.loadList());
  }

  logs(moderator) {
    this.modalService.create({
      nzContent: ModeratorLogsComponent,
      nzTitle: `Moderator ${moderator.name} - Logs`,
      nzFooter: null,
      nzWidth: '90%',
      nzData: {
        moderator: moderator
      }
    });
  }
}
