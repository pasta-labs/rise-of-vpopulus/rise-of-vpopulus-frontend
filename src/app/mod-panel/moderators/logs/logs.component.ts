import { Component, Input, OnInit, inject } from '@angular/core';
import { NZ_MODAL_DATA } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-mp-moderators-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
})
export class ModeratorLogsComponent implements OnInit {
  isLoading = false;

  readonly nzModalData = inject(NZ_MODAL_DATA, { optional: true });

  @Input() moderator: any;

  page = 1;

  totalCount = 0;
  list = [];

  constructor(
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.moderator = this.nzModalData.moderator;

    this.getLogs();
  }

  getLogs() {
    this.isLoading = true;
    this.graphQlService.get(`modpanel`, `{ moderatorLogs(moderatorId: "${this.moderator.id}"){ entity { id type name } action payload agent ipAddress createdOn } }`)
      .subscribe((logs) => {
        this.list = logs.data['moderatorLogs'];
        this.totalCount = this.list.length;
        this.isLoading = false;
      });
  }
}
