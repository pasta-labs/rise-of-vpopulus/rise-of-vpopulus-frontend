import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { GenericHttpService } from 'src/app/shared/services/generic-http-service';
import { GraphQlService } from 'src/app/shared/services/graphql.service';

@Component({
  selector: 'app-mp-moderator-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class ModeratorCreateComponent implements OnInit {
  isLoading = true;

  searchNameValue = '';
  list = [];

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
    private modalRef: NzModalRef,
    private graphQlService: GraphQlService
  ) { }

  ngOnInit() {
    this.isLoading = false;
  }

  searchCitizens(name) {
    if (name.length < 3) return;
    this.isLoading = true;
    this.httpService
      .get(`/api/entities/citizens/search?name=${name}`)
      .subscribe((r) => {
        this.list = r;
        this.isLoading = false;
      });
  }

  add() {
    this.graphQlService.post(`modpanel`, `mutation mutate($data: ModeratorCreateCommand) { moderatorCreate(data: $data) }`, {
      data: { newModeratorId: this.searchNameValue }
    }).subscribe((r: any) => {
      if (r.errors) this.graphQlService.displayErrors(r.errors);
      else {
        this.messageService.success(`Moderator added!`);
        this.modalRef.close();
      }
      this.isLoading = false;
    });
  }
}
