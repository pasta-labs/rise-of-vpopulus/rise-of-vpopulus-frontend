import { NgModule } from '@angular/core';
import { APOLLO_NAMED_OPTIONS } from 'apollo-angular';
import {
  ApolloClientOptions,
  ApolloLink,
  InMemoryCache,
  Observable,
} from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { environment } from 'src/environments/environment';
import { WebSocketLink } from '@apollo/client/link/ws';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { createClient } from 'graphql-ws';

export function createNamedApollo(
  httpLink: HttpLink
): Record<string, ApolloClientOptions<any>> {
  return {
    chat: {
      name: 'chat',
      link: ApolloLinkCreator(httpLink, `chat`),
      cache: new InMemoryCache(),
    },
    common: {
      name: 'common',
      link: ApolloLinkCreator(httpLink, `common`),
      cache: new InMemoryCache(),
    },
    game: {
      name: 'game',
      link: ApolloLinkCreator(httpLink, `game`),
      cache: new InMemoryCache(),
    },
    modpanel: {
      name: 'modpanel',
      link: ApolloLinkCreator(httpLink, `modpanel`),
      cache: new InMemoryCache(),
    },
    minigames: {
      name: 'minigames',
      link: ApolloLinkCreator(httpLink, `minigames`),
      cache: new InMemoryCache(),
    },
  };
}

export function ApolloLinkCreator(httpLink, area, addWs = false) {
  const auth = setContext((operation, context) => {
    const token = localStorage.getItem('token');
    if (token === null) {
      return {};
    } else {
      return {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
    }
  });

  const error = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      console.log(`[other error]: ${graphQLErrors}`);
      if (
        (graphQLErrors[0].extensions &&
          graphQLErrors[0].extensions.code === 'authorization') ||
        graphQLErrors[0].message.includes(
          `You are not authorized to run this query`
        )
      ) {
        localStorage.clear();
        window.location.assign(`/landing`);
        setTimeout(() => {
          window.location.reload();
        }, 100);
      }
    }
    if (networkError) {
      console.log(`[Network error]: `, networkError);
      if (networkError['error'] && networkError['error'].errors[0].message.includes(`You are not authorized to run this query`)) {
        localStorage.clear();
        window.location.assign(`/landing`);
        setTimeout(() => {
          window.location.reload();
        }, 100);
      }
    }
  });

  const ForwardExtensionsLink = new ApolloLink((operation, forward) => {
    return new Observable(observer => {
      const sub = forward(operation).subscribe({
        next: result => {
          if (result.data)
            result.data.extensions = result.extensions
          observer.next(result)
        },
        complete: observer.complete.bind(observer),
      })
      return () => {
        if (sub) sub.unsubscribe()
      }
    })
  })

  var links = [ForwardExtensionsLink, auth, error,
    httpLink.create({
      uri: `${environment.apiUrl}/graphql/${area}`,
    })];

  if (addWs === true) {
    const wsUrl = `${environment.apiUrl}/graphql/${area}`.replace(
      'https://',
      'wss://'
    );
    const ws = new GraphQLWsLink(
      createClient({
        url: wsUrl,
        connectionParams: () => {
          return {
            Authorization: `Bearer ${localStorage.getItem(`jwtToken`)}`
          };
        },
      }),
    );
    console.log('new ws', ws)
    links.push(ws);
  }
  return ApolloLink.from(links);
}

@NgModule({
  providers: [
    {
      provide: APOLLO_NAMED_OPTIONS,
      deps: [HttpLink],
      useFactory: createNamedApollo,
    },
  ],
})
export class GraphQLModule { }
