import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {
  HttpClient,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { AuthGuard } from './shared/auth/auth-guard';
import { JwtInterceptor } from './shared/auth/jwt-interceptor';


import { AppComponent } from './app.component';
import { HomeComponent } from './game/home/home.component';

import { TopbarComponent } from './shared/components/layout/topbar/topbar.component';
import { NavbarComponent } from './shared/components/layout/navbar/navbar.component';
import { SharedModule } from './shared/shared.module';
import { CustomI18NLoader } from './shared/others/custom-i18n-loader';
import { MarketsModule } from './game/markets/markets.module';
import { CompanyModule } from './game/company/company.module';
import { CountryModule } from './game/countries/country.module';
import { RegionModule } from './game/region/region.module';
import { RankingModule } from './game/ranking/ranking.module';
import { WarModule } from './game/war/war.module';
import { BattleModule } from './game/battle/battle.module';
import { TradeModule } from './game/trade/trade.module';
import { SupportModule } from './game/support/support.module';
import { ArticleModule } from './game/article/article.module';
import { NewspaperModule } from './game/newspaper/newspaper.module';
import { StoreComponent } from './game/store/store.component';
import { CitizenModule } from './game/citizen/citizen.module';
import { SearchComponent } from './game/shared/search/search.component';
import { OrganizationModule } from './game/organization/organization.module';
import { ArmyModule } from './game/army/army.module';
import { PartyModule } from './game/party/party.module';
import { ModPanelModule } from './mod-panel/mod-panel.module';
import { EntityModule } from './game/entity/entity.module';
import { MapModule } from './game/shared/maps/map.module';
import { ElectionsModule } from './game/elections/elections.module';
import { MiniGamesModule } from './game/mini-games/mini-games.module';
import { PpComponent } from './legal/pp/pp.component';
import { TosComponent } from './legal/tos/tos.component';
import { RulesComponent } from './legal/rules/rules.component';
import { ContentComponent } from './shared/components/layout/content/content.component';
import { GraphQLModule } from './graphql.module';
import { ChatModule as NewChatModule } from 'projects/chat/src/app/chat.module';
import { DashboardEventsComponent } from './game/home/dashboard-events/dashboard-events.component';
import { LandingModule } from 'projects/auth/src/app/landing.module';
import { StoreHistoryComponent } from './game/store/store-history/store-history.component';
import { AuthGuardMod } from './shared/auth/auth-guard-mod';
import { OauthOutsideComponent } from './auth/oauth-outside/oauth-outside.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    HomeComponent,
    DashboardEventsComponent,
    // = = = = Shared
    TopbarComponent,
    NavbarComponent,

    StoreComponent,
    StoreHistoryComponent,

    SearchComponent,

    PpComponent,

    TosComponent,

    RulesComponent,

    OauthOutsideComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    EntityModule,
    GraphQLModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomI18NLoader,
        deps: [HttpClient],
      },
    }),

    /*  AuthModule, */
    LandingModule,
    SharedModule,
    MarketsModule,
    CompanyModule,
    CountryModule,
    RegionModule,
    RankingModule,
    WarModule,
    BattleModule,
    TradeModule,
    SupportModule,
    ArticleModule,
    NewspaperModule,
    CitizenModule,
    OrganizationModule,
    ArmyModule,
    ElectionsModule,
    PartyModule,
    ModPanelModule,
    MapModule,
    MiniGamesModule,
    NewChatModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AuthGuard, AuthGuardMod
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
