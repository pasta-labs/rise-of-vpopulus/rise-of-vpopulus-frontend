export const environment = {
  production: true,
  // apiUrl: 'https://api-legacy.vpopulus.net',
  apiUrl: 'https://localhost:5301',
  simplanUrl: 'https://simplan.io',
  platform: "web",
  // platform: "mobile"
};
