export const environment = {
  production: true,
  apiUrl: 'https://api-riseof.vpopulus.net',
  simplanUrl: 'https://simplan.io',
  platform: "mobile"
};
