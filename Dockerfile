# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:18.13.0 as build-stage

WORKDIR /app

# Copy package.json and yarn.lock first to leverage Docker cache
COPY package.json yarn.lock ./

# Install dependencies using Yarn
RUN yarn install --frozen-lockfile

# Copy the rest of the application files
COPY ./ /app/

# Build the Angular app
RUN yarn build --configuration production

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.27.4

# Copy the compiled Angular app from the build stage
COPY --from=build-stage /app/dist/Frontend/ /usr/share/nginx/html

# Copy custom Nginx configuration
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf